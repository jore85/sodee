# The parsed info-files, which come out of the SODEEcore are formatted as follows:

# Started 2017-03-17T20:40:38.358Z
# Article retrieved       505
# Matched Articles        79
# Nounphrases extracted   1053
# Nounphrases (final)     316
# End     2017-03-17T21:57:59.092Z

#today="2017-02-21"
today=$(date +%Y'-'%m'-'%d)
articles_retrieved=$(awk -F '\t' 'FNR == 2 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')
articles_processed=$(awk -F '\t' 'FNR == 3 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')
np_extracted=$(awk -F '\t' 'FNR == 4 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')
np_final=$(awk -F '\t' 'FNR == 5 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')

printf "${today}\t${articles_retrieved}\t${articles_processed}\t${np_extracted}\t${np_final}\n"
