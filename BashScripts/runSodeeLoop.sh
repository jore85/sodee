# This script runs SODEE forever
# Johannes Reiss in May 2017
#
# TO RUN THE SCRIPT PROPERLY INSERT THE NEWS FEED CREDENTIALS FIRST!

while :
do
run_start=$(date +%Y-%m-%d'T'%H':'%M':'%S'Z') 
echo "Start Run:  ${run_start}"

# Check if a lock file exists (avoid to start a second run)
if [ -e "$(find ${HOME} -name sodeeRuns_*.lock)" ] 
	then 
		echo "There is already a SODEE Instance running or an error occurs in execution. Wait until it is finished."
		run_end=$(date +%Y-%m-%d'T'%H':'%M':'%S'Z') 
		echo "End: ${run_end}"
		exit 3
fi

# Directory in which the news article files would be saved
news_directory="/home/johannes/jre/newsLive/"
#echo $news_directory

# Directory in which the news article files would be moved
# after processing
news_archive="/home/johannes/jre/processedNews/"
#echo $news_archive

# SODEE Timemarker file
time_marker="/home/johannes/jre/output/timemarker.tm"
last_sodee_run="/home/johannes/jre/output/last_sodee_run.tm"
#echo $time_marker
#timezone=$(date +%z)

# Get date & time of the latest news article file
# News data will be downloaded from that time on
if [ -e "$time_marker" ] 
	then 
		#echo "${time_marker} exists. Last run: " 
		getTime_unixtime=$(stat -c %y ${time_marker})
		#getTime_unixtime="1970-01-01 ${getTime_unixtime} sec"
		echo "Timemarker at the beginning (Set in last run to begin from time): ${getTime_unixtime}"
		start_time=$(date -d "${getTime_unixtime}" +%Y-%m-%d'T'%H':'%M':'%S'Z') # Start from last run
		echo "Time, which will set to the news retrieval script: ${start_time}"
	else 
		start_time=$(date +%Y-%m-%d'T'%H':'%M':'%S'Z') # Start now
fi 
echo "Data is retrieved from set time: ${start_time}"

# Insert and replace §§credentials§§ with the correct credentials for the news stream
python /home/johannes/jre/http2fs_singleDir.py §§credentials§§ -o "${news_directory}" -a "${start_time}" >> /home/johannes/jre/output/logs/newsFetcher.log 2>> /home/johannes/jre/output/logs/newsFetcher.log &
newsGetter=$!
echo "PID http2fs.py: ${newsGetter}"

# Download some news data for a timespan and then the processing with SODEE will start

#retrieval_duration=4 # Duration for the news retrieval in minutes
sleep 1m
kill -15 $newsGetter
# Set the start time for the next run to the last downloaded news file date & time
latest_news_file_day=$(ls -r1 "${news_directory}" |  head -n 1 | grep -Eo '[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}')
latest_news_file_time=$(ls -r1 "${news_directory}" |  head -n 1 | grep -Eo 'T[[:digit:]]{2}-[[:digit:]]{2}-[[:digit:]]{2}' | tr - :)
latest_news_file="${latest_news_file_day}${latest_news_file_time}"
echo "Time of the last news file retrieved: ${latest_news_file}"

# in case of no news files were retrieved
if [ -z "${latest_news_file}" ]
        then 
                start_time=$(date -d "${start_time%?} MEZ" +%Y-%m-%d' '%H':'%M':'%S)
		echo "Start time converted: ${start_time}"
		time_next_run=$(date -d "${start_time} MEZ +2 min" +%Y%m%d%H%M'.'%S)
                echo "The next run will start with: ${time_next_run}, because no news files were retrieved, 2 mins had been added."
		sleep 2m # avoid that the system will  retrieve news from the future
 	else 
		start_time_copy=${start_time}
		time_next_run=$(date -d "${latest_news_file}" +%Y%m%d%H%M'.'%S)
		echo "The next run will start with: ${time_next_run}"
fi
touch -t $time_next_run $time_marker
start_time_copy=${start_time}

#time_next_run=$(date -d "${latest_news_file}" +%Y%m%d%H%M'.'%S)
#echo "The next run will start with: ${time_next_run}"
#touch -t $time_next_run $time_marker

#start_time=${start_time_copy}
#echo $start_time
# Start SODEE
java -jar -XX:+UseG1GC -Xmx24G /home/johannes/jre/dist/SODEE.jar "1" "${news_directory}" "${news_archive}" > /home/johannes/jre/output/logs/output_SODEE_"${start_time}".log 2> /home/johannes/jre/output/logs/errors_SODEE_"${start_time}".log &
sodeePID=$!
echo "PID SODEE: ${sodeePID}"
wait ${sodeePID}
sodee_exit_status=$?
echo "SODEE exit status: ${sodee_exit_status}"
touch $last_sodee_run
#if [ "${sodee_exit_status}" -eq 254 ]
#	then 
#		time_next_run=$(date -d "${start_time} + 10 min" +%Y%m%d%H%M'.'%S)
#		echo "The next run will start with: ${time_next_run}, because no new files were found, 10min had been added."
#		touch -t $time_next_run $time_marker
# 
#fi
run_end=$(date +%Y-%m-%d'T'%H':'%M':'%S'Z') 

echo "End Run: ${run_end}" 
done
