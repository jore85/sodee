# SODEE: A System For Online Detection Of Emerging Entities #

**Author: Johannes Reiß**

### Content of this repository ###

1. A JAVA program, representing the *SODEEcore*. [Source code](./SODEECore/JAVA_SRC/SODEE/) and an [example properties-file](./SODEECore/JAVA_SRC/SODEE/SODEEConfig.properties).
2. A simple [python script](./SODEECore/http2fs_singleDir.py) to obtain the news articles.
3. Diverse Bash-Scripts for the operation of the system and some other tasks. [Bash-Scripts](./BashScripts/).
4. Sources for the website (Back/Front-End). [Back-End](./Web/Back-End/) and [Front-End](./Web/Front-End/).
5. [List-files](./toplists/), which are used to filter the news input.
6. A brief [manual](./SODEE_Manual.md), which gives support to install the system on a machine.
