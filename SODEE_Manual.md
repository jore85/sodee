# SODEE Manual

Author: Johannes Reiß

Status: 04/26/2017

### Preamble

All Java programs require Java Version: 7 (1.7)
Note, that the current StanfordNLP models are by now compiled with Java 8 and are not supported yet.
Older versions < 3.5.0 ([e.g., see here for the NER](https://nlp.stanford.edu/software/CRF-NER.shtml#History)) must be used.

The system used a MySQL Database, so it requires access to a MySQL Database server.

## SODEE core

### 1. Run SODEE

Once SODEE was built, the single component could be started as follows:

    java -jar -XX:+UseG1GC -Xmx24G /home/johannes/jre/dist/SODEE.jar "1" "${news_directory}" "${news_archive}" > /home/johannes/jre/output/logs/output_SODEE_"${start_time}".log 2> /home/johannes/jre/output/logs/errors_SODEE_"${start_time}".log &

#### Start Parameters

* *-XX:+UseG1GC* : Special garbage collection of the JVM.
* *-Xmx24G* : max. Heap-Size, value is dependent of the data size, which should processed (i.e. 24G was sufficient in most cases).
* *"1"* : Indicates, that the program is in Live-Mode.
* *${news_directory}* : The directory, which contains the downloaded news files in .xml.gz-Format.
* *${news_archive}* : The directory, where the processed files should be archived.

The programm should be started in background on the machine and disowned from the started shell. So, add
a *&* in the end of the command. And if SODEE was started from shell, execute to avoid terminating the program after closing the shell:

    disown -h {process id}

#### Protocol files

The protocols are written via STDOUT to file:

*  \> /home/johannes/jre/output/logs/output_SODEE_"${start_time}".log

The path of the **normal** protocol file.

* 2> /home/johannes/jre/output/logs/errors_SODEE_"${start_time}".log

The path of the **error** protocol file.

In each run an additionally **info-file** is written directly to a determined directory.
These files are named as: *info_{Timestamp}.txt*

{Timestamp} is the timestamp of the current date & time and formatted
as: *YYYY-MM-DDTHH:mm:ss.SSSZ*

Example: *info_2017-03-01T10:15:16.154Z.txt*

The info-files could used for statistical analysis. From the script: *dailyDataStats.sh*:

    #today="2017-02-21"
    today=$(date +%Y'-'%m'-'%d)
    articles_retrieved=$(awk -F '\t' 'FNR == 2 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')
    articles_processed=$(awk -F '\t' 'FNR == 3 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')
    np_extracted=$(awk -F '\t' 'FNR == 4 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')
    np_final=$(awk -F '\t' 'FNR == 5 {print $2}' /home/johannes/jre/output/info_${today}*.txt | awk '{sum+=$1} END {print sum}')

    printf "${today}\t${articles_retrieved}\t${articles_processed}\t${np_extracted}\t${np_final}\n"

This script has to execute in the info-files directory to work properly.
The paths have to be adjusted to the paths currently used.

#### Lock files

In an old version, there were lock-files generated to make SODEE run only once. These files are
saved to the *Home Directory* of the user and could recognised by the pattern: *sodeeRuns-{Number}.lock*.

The existence of such a file is **not checked** in the SODEE core program.
The lock file is only relevant in the context of automatic starting by a script, crown-job etc..

### 2. Code adjustments

#### SODEE Configuration with properties-File

The latest version of the SODEEcore comes with an file-based configuration by
properties-file, which will read on start of the SODEEcore jar.

The file *SODEEConfig.properties* must be contained in the same directory as
the *SODEE.jar*.

The following listing shows an example properties-File. Each parameter is described
below.

    # Configuration File for SODEE
    # This file replaces the old java config class

    startDate=2015-04-07
    endDate=2015-04-21
    newsPath=/home/johannes/jre/jsi-small/
    outputPath=/home/johannes/jre/output/
    modelPathPOSTagger=/home/johannes/jre/models/stanfordNLP/english-left3words-distsim.tagger
    rejectionRulesFile=/home/johannes/jre/models/RBNPE/rejectionRules.txt
    positiveRulesFile=/home/johannes/jre/models/RBNPE/positiveRules.txt
    annotationPath=/home/johannes/jre/wpm-copy/
    modelPathNER=/home/johannes/jre/models/stanfordNLP/english.conll.4class.distsim.crf.ser.gz
    redLinksFile=/home/johannes/jre/pagelinks/redlinks-enwiki-20150403.gz
    googleNGramServiceInput=/home/johannes/jre/NgramService/input/
    googleNGramServiceOutput=/home/johannes/jre/NgramService/output/
    allSurfaceForms=/home/johannes/jre/wikipedia-entities/sfforms_2017_02_en.txt  
    savedWekaModels=/home/johannes/jre/models/weka/
    savedWekaModel=/home/johannes/jre/models/weka/SVM_06.model
    arffOutput=/home/johannes/jre/output/arff/
    sqlite_DB=/home/johannes/jre/output/sqlite/
    numberOfThreads=10

    # Filter lists
    path_topList_feedTitle=/home/johannes/jre/toplists/top10-feedtitle-mfa.txt
    path_topList_feedURI=/home/johannes/jre/toplists/top10-feeduri-mfa.txt
    path_topList_HostName=/home/johannes/jre/toplists/top10-hostname-mfa.txt
    path_topList_SourceName=/home/johannes/jre/toplists/top10-sourcenames-mfa.txt
    maxArticleLength=4000
    language=eng
    minNPLength=4
    maxNPLength=50

    vm1_ip=172.21.40.201
    vm1_host=scc-vm-191.scc.kit.edu
    SQLITE_DB_FILENAME=sodee.db
    userAgent=SODEE/2.0 (ugcrf@student.kit.edu)
    annotationsWebServiceIP=http://scc-vm-151.scc.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score/
    pageViewAPI_BASE=http://wikimedia.org/api/rest_v1/metrics/pageviews/

    # DB Config and Credentials
    DB_NAME=
    DB_SERVER=
    DB_PORT=
    ARTICEL_NP_DB_USER=
    ARTICEL_NP_DB_PWD=
    PAGEVIEW_DB_USER=
    PAGEVIEW_DB_PWD=    
    RESULTS_DB_USER=
    RESULTS_DB_PWD=

#### Description of the configuration parameters

* *outputPath* : all outputs of the program are made to this directory.
* *modelPathPOSTagger* : the Stanford PoS-Tagger model is load from there.
* *rejectionRulesFile* : path to the rejection rules of the noun phrases extraction.
* *positiveRulesFile* : path to the positive rules of the noun phrases extraction.
* *modelPathNER* : the Stanford NER model is load from there. (The system only support the 4-Class versions.)
* *googleNGramServiceInput* : the input directory for the external ngram service. (SODEE writes its file for the retrieval into this directory, ngram service watches this directory)
* *googleNGramServiceOutput* : the output directory of the external ngram service. (SODEE watches for results, ngram services writes its results there)
* *allSurfaceForms* : the path to the list of all surface forms, which is used for the surface form linking.
* *savedWekaModels* : the path to the directory where the WEKA models are saved. The models are imported from there.
* *arffOutput* : every run produces several WEKA *arff-files*, which could use to check the processed data. Those files are saved in that directory.
* *sqlite_DB* : the path to a fallback SQlite 3 DB.
* *path_topList_feedTitle* : the path to the **Feed Title** filter list.
* *path_topList_feedURI* : the path to the **Feed URI** filter list.
* *path_topList_HostName* : the path to the **Host Name** filter list.
* *path_topList_SourceName*: the path to the **Source Name** filter list.
* *vm1_ip* : the IP-Address of the machine, if localhost would not be used.
* *vm1_host* : the host address of the machine, if localhost would not be used.
* *SQLITE_DB_FILENAME* : the file name of the fallback SQlite 3 DB.
* *userAgent* : the user agent for the Wikipedia API client. See the details at [Wikimedia API Docs](https://www.mediawiki.org/wiki/API:Main_page#Identifying_your_client).
* *annotationsWebServiceIP* : the ip-address of the annotation webservice.
* *pageViewAPI_BASE* : the basis part of the URL of the pageview API request.
* *savedWekaModel* : the complete path to the WEKA model file.
* *DB_NAME* : the name of the database.
* *DB_SERVER* : the hostname/server-adress of the database-server, mostly "localhost".
* *DB_PORT* : the port of the database-server.
* *ARTICEL_NP_DB_USER* : the database-user.
* *ARTICEL_NP_DB_PWD* : the password of the database user.
* *PAGEVIEW_DB_USER* : the user of the database for the pageviews.
* *PAGEVIEW_DB_PWD* : the password of the pageview-db-user.    
* *RESULTS_DB_USER* : the user of the result-db (not used yet, set to the same as Articel_NP_DB_USER).
* *RESULTS_DB_PWD* : the password of the result-db-user (not used yet, set to the same as Articel_NP_DB_USER).
* *numberOfThreads* : the number threads used in multithreading methods.
* *maxArticleLength* : process only Articles shorter than maxArticleLength
* *language* : process onyl articles with the set language
* *minNPLength* : process only NPs longer than minNPLength
* *maxNPLength* : process only NPs shorter than maxNPLength

*Note: If something went wrong or errors occur, check the config-file first. Sometimes,
there would be unwanted whitespaces or other artefacts/typos in it.

#### Change hardcoded options

Some options are set and hardcoded in the execution part of the program (main class).
So, it could found here: *\src\SODEEexec.java*

To find the variables the simple search function of any used editor could be used.

* *String infoFile* : the file name of the info-file.
* *lockFilePath* : the path and the file name of the lock-file.

      Boolean labelingOn = true; // true -> the retrieved data is labelled, for live run that option must be "false"
      boolean isLiveRunning = false; // true -> the run is a live run, retrieval uses API-Methods and no labelling
      boolean onlyRetrieving = true; // true -> no classifier will be used
      boolean isClassifyOnly = false; // true -> the retrieval part is skipped, data will be load directly from db into the classifier
      int liveMode = Integer.parseInt(args[0]); // 0 = no live mode, 1 = live mode
      int maxArticleLength = 4000; //process only Articles shorter than maxArticleLength
      String language = "eng"; // process onyl articles with the set language
      int minNPLength = 4; //process only NPs longer than minNPLength
      int maxNPLength = 50; //process only NPs shorter than maxNPLength
      boolean neFilterOn = true; //a NP is only in further processing, if it's a recognized Named Entity
      boolean isTopListFilterOn = true; //filter with matching list entries, e.g. top 10 hostnames etc.
      boolean hasMySQL = true;

#### Structure of SODEE main class

The main class is structured according to the steps in the pipeline.
At 9. the processing of the input data starts.

Navigation to the sections could be done with the search function of any editor.

1. //CREATE A LOCK FILE (AVOID STARTING A SECOND SODEE RUN)
2. //FLUSH THE NGRAM-SERVICE DIRECTORIES
3. //RUNNING OPTIONS
4. //SETUP THE DIRECTORIES
5. //SETUP THE FILTER LISTS FOR THE ARTICLE FILTER
6. //OPTIONS FOR THE NP FILTER
7. //CLASSIFIER OPTIONS
8. //LIVE SYSTEM OPTIONS
9. //START PROCESSING PERIODICAL DATA --> here the program logic starts
  * // ARTICLE PARSING
  * // MOVE THE PROCESSED NEWS ARTICLE FILES INTO A ARCHIVE
  * //ANNOTATOR | //ANNOTATION PARSING
  * //SET FILTERS
  * // WAIT FOR THE SURFACE FORMS
  * //START PROCESSING
  * //CALCULATE THE TIMESPAN FOR PERIODICAL FEATURES
  * //STARTING THE NGRAM CALCULATION ON AN EXTERN SERVICE
  * //LINK ANNOTATION WITH NOUNPHRASE + FEATURE CALCULATION OF ANNOTATION FEATURES
  * //WAITING FOR FINISHING THE NGRAM-RETRIEVAL
  * //SET THE DATA FOR THE DB-OUTPUT
  * //START INSERTING THE DATA INTO DB
  * //WAITING FINISHING THE DB WRITE OUT
  * //GET NPs OF THE DAY BEFORE TO CALCULATE THE PERIODICAL FEATURES
  * //CALCULATING THE PERIODICAL FEATURES

10. //BEGIN CLASSIFIER SECTION

## SODEE Run script

The complete retrieval system is started with an BASH script: *runSodeeLoop.sh*

### 1. Start System

It is executed with the following command:

    /home/johannes/jre/runSodeeLoop.sh >> /home/johannes/jre/output/logs/sodeeRuns.log 2>> /home/johannes/jre/output/logs/sodeeRuns_errors.log &

The programm should be started in background on the machine and disowned from the started shell. So, add
a *&* in the end of the command. And if SODEE was started from shell, execute to avoid terminating the program after closing the shell:

    disown -h {process id}

Once the script was started, it runs forever.

##### Some notes on the operation

Since the *runSodeeLoop.sh* only executes the components and control the date & time
parameters of the runs, it does not support proper terminating operations.

> Tip: Once the script is started, note the process id of the BASH script process. With the pid, the
main process could identified easyly.

When the script was started, one must terminate the current running component manually.
Two possible options exists to stop the system:

1. Use htop to find the processes and terminate them.
2. Use a dedicated user account on the machine, which runs only SODEE.

**Option 1**

1. Start **htop**
2. Change the view to the tree-view with **F5**
3. Look for the corresponding process of the BASH script
4. Kill this process
5. Look for the process of the component (either the IJS-Newsfeed Crawler or the SODEE core)
6. Kill the component's process

To avoid, that the script process keep running when component was stopped,
the script process must stopped at first.

**Option 2**

1. Use *killall*

#### Protocolling
The protocols are written via STDOUT to file:

*  \>> /home/johannes/jre/output/logs/sodeeRuns.log

The path of the **normal** protocol file.

* 2>> /home/johannes/jre/output/logs/sodeeRuns_errors.log

The path of the **error** protocol file.

### 2. Code adjustments

#### Change directories, file-paths etc.

 * *time_marker* : The timemarker file, which holds the start timestamp for the news retrieval.
 * *last_sodee_run* : The timemarker file, which holds the timestamp of the last run.
 * *time_next_run* : The timemarker file, which holds the date & time for the next run.
 (Date & Time of the next news retrieval.)
 * *news_directory* : The directory, where the IJS-Newsfeed crawler saves the retrieved news files.
 * *news_archive* : The directory, where the processed news are archived.

#### Change the inital start time

To set a initial start date & time, use the *touch* command.

    touch -t "date & time" "time_marker"

* *date & time* : The initial start date & time in format *YYYYMMDDHHmm.ss*
* *time_marker* : The timemarker file

Example for 02/10/2015 00:11

    touch -t 201502100011.00 test

<div class="page-break" />

## IJS-Newsfeed Crawler

This python script downloads the news files as .xml.gz-files into a selected directory.

Original file: *http2fs.py*. To download the files into a single directory, use the changed script: *http2fs_singleDir.py*.

    python /home/johannes/jre/http2fs_singleDir.py "access-credentials" -o "${news_directory}" -a "${start_time}" >> /home/johannes/jre/output/logs/newsFetcher.log 2>> /home/johannes/jre/output/logs/newsFetcher.log &

#### Start Parameters

 * *access-credentials* : The access credentials of the IJS Newsfeed.
 * *${news_directory}* : The directory the files are downloaded into.
 * *${start_time}* : The start date & time the files are downloaded from.

#### Protocolling
 The protocols are written via STDOUT to file:

 * \>> /home/johannes/jre/output/logs/newsFetcher.log

The path of the **normal** protocol file.

 * 2>> /home/johannes/jre/output/logs/newsFetcher.log

The path of the **error** protocol file.

<div class="page-break" />

## SODEE Ngram Service

Source could obtained from git repo: https://bitbucket.org/jore85/ngramservice/src

SODEE core uses a ngram service to acccess and retrieve the Google ngram index.
The built Ngram Service is started by the following command:

    java -jar -XX:+UseG1GC /home/johannes/jre/NgramService/src/NgramService.jar "Input Directory" "Output Directory" "Ngram Index Directory" > /home/johannes/jre/NgramService/log/log.txt 2> /home/johannes/jre/NgramService/log/err.txt &

The programm should be started in background on the machine and disowned from the started shell. So, add
a *&* in the end of the command. And if SODEE was started from shell, execute to avoid terminating the program after closing the shell:

    disown -h {process id}

Once the program was started, it runs forever.

#### Start Parameters

  * *Input Directory* : The path of the input directory, which is watched by the service.
  * *Output Directory* : The path of the output directory, which is used to write the result file into.
  * *Ngram Index Directory* : The path of the ngram index directory, which contains the ngram index.

#### Protocolling
The protocols are written via STDOUT to file:

 * \> /home/johannes/jre/NgramService/log/log.txt

The path of the **normal** protocol file.

 * 2> /home/johannes/jre/NgramService/log/err.txt

The path of the **error** protocol file.

<div class="page-break" />

## Databases

The system uses a MySQL database containing two tables.

#### articleNPFeatures

Database connector class: *\src\DatabasesNetwork\ArticleNPDatabaseConnector.java*

This table holds all extracted noun phrases and their corresponding article meta data.
The table could be created by the following statement:

    CREATE TABLE `articleNPFeatures` (
    `np_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `np` varchar(255) CHARACTER SET utf8 NOT NULL,
    `articleID` int(11) NOT NULL,
    `retrievedDate` datetime DEFAULT NULL,
    `publishedDate` datetime DEFAULT NULL,
    `title` longblob,
    `articleURI` longblob,
    `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `feedTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `feedURI` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `hostName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `sourceName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `dayOfWeek` int(11) DEFAULT NULL,
    `lastFullHour` int(11) DEFAULT NULL,
    `sourceTags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `npPositionInArticle` double DEFAULT NULL,
    `articleCoverage` double DEFAULT NULL,
    `titleContainsNP` tinyint(1) DEFAULT NULL,
    `npOccurenceInArticle` int(11) DEFAULT NULL,
    `neClass` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`np_id`),
    UNIQUE KEY `np_id_UNIQUE` (`np_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=813291 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#### allNPFeatures

Database connector class: *\src\DatabasesNetwork\AllNPFeaturesDBConnector.java*, *\src\Classifier\DBDataImporteur.java*

This table holds all finally processed noun phrases with their label and features.
The table could be created by the following statement:

    CREATE TABLE `allNPFeatures` (
      `rowID` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `NP` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `articleID` int(11) NOT NULL,
      `NPTimestamp` timestamp NULL DEFAULT NULL,
      `Named_Entity_Class` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
      `PoS` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
      `isAllCaps` tinyint(1) DEFAULT NULL,
      `containsDigitAndAlpha` tinyint(1) DEFAULT NULL,
      `containsNonAlpha` tinyint(1) DEFAULT NULL,
      `endsWithPeriod` tinyint(1) DEFAULT NULL,
      `firstCapital` tinyint(1) DEFAULT NULL,
      `firstLetterCapitalized` tinyint(1) DEFAULT NULL,
      `containsRealLetter` tinyint(1) DEFAULT NULL,
      `hasInternalApostrophe` tinyint(1) DEFAULT NULL,
      `hasInternalPeriod` tinyint(1) DEFAULT NULL,
      `internalCaps` tinyint(1) DEFAULT NULL,
      `isHyphenated` tinyint(1) DEFAULT NULL,
      `npPositionInArticle` double DEFAULT NULL,
      `articleCoverage` double DEFAULT NULL,
      `titleContainsNP` tinyint(1) DEFAULT NULL,
      `npOccurenceInArticle` int(11) DEFAULT NULL,
      `retrievedDate` datetime DEFAULT NULL,
      `publishedDate` datetime DEFAULT NULL,
      `title` longblob,
      `articleURI` longblob,
      `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `feedTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `dayOfWeek` int(11) DEFAULT NULL,
      `lastFullHour` int(11) DEFAULT NULL,
      `wikiEntity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `hasRedLink` tinyint(1) DEFAULT NULL,
      `pageView24hExist` tinyint(1) DEFAULT NULL,
      `pageView24hSum` int(11) DEFAULT NULL,
      `pageViewDays7d` bigint(20) DEFAULT NULL,
      `pageViewSum7d` bigint(20) DEFAULT NULL,
      `pageViewDays14d` bigint(20) DEFAULT NULL,
      `pageViewDays7dMin100` bigint(20) DEFAULT NULL,
      `pageViewDays14dMin100` bigint(20) DEFAULT NULL,
      `pageViewSlope24hSlope` double DEFAULT NULL,
      `pageViewSlope24hIntercept` double DEFAULT NULL,
      `pageViewSlope24hRSquare` double DEFAULT NULL,
      `pageViewSlope24hSlopeStdErr` double DEFAULT NULL,
      `pageViewSlope24hInterceptStdErr` double DEFAULT NULL,
      `pageViewSlope14dSlope` double DEFAULT NULL,
      `pageViewSlope14dIntercept` double DEFAULT NULL,
      `pageViewSlope14dRSquare` double DEFAULT NULL,
      `pageViewSlope14dSlopeStdErr` double DEFAULT NULL,
      `pageViewSlope14dInterceptStdErr` double DEFAULT NULL,
      `googleNgramFrequency` bigint(20) DEFAULT NULL,
      `googleNgramSlope` double DEFAULT NULL,
      `googleNgramR2` double DEFAULT NULL,
      `usageSinceYear` int(11) DEFAULT NULL,
      `matchCount1899` bigint(20) DEFAULT NULL,
      `percentProperCaps` double DEFAULT NULL,
      `npOccurrenceNo1h` int(11) DEFAULT NULL,
      `npOccurrenceNo24h` int(11) DEFAULT NULL,
      `npOccurrenceSlope24hSlope` double DEFAULT NULL,
      `npOccurrenceSlope24hIntercept` double DEFAULT NULL,
      `npOccurrenceSlope24hRSquare` double DEFAULT NULL,
      `npOccurrenceSlope24hSlopeStdErr` double DEFAULT NULL,
      `npOccurrenceSlope24hInterceptStdErr` double DEFAULT NULL,
      `mention` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `wikiTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `wikiURI` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `annotationWeight` double DEFAULT NULL,
      `noveltyClass` int(11) DEFAULT NULL,
      `timeToInsertion` bigint(20) DEFAULT NULL,
      `class` tinyint(1) DEFAULT NULL,
      `probabilityTRUE` double DEFAULT NULL,
      `probabilityFALSE` double DEFAULT NULL,
      `processedTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`rowID`)
    ) ENGINE=InnoDB AUTO_INCREMENT=307381 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


<div class="page-break" />

## Website

### 1. Back-End

The Back-End consists of a PHP script: *sodee-np-data.php* and is interpreted by a web server.

Tasks of *sodee-np-data.php* are:
* Query the SODEE database
* Make queried data ready for delivering it via JSON

#### Changeable variables

The following variables have to be adjusted:

* *$lastrun_timestamp* : The path to the timemarker file, which holds the timestamp of the last run
* *$db_host*, *$db_user*, *$db_pwd*, *$db_name* : The credentials for the database connection

#### Queries / DB Access

Two possible database queries could be made.

1. Default (when web server recieve a *HTTP-GET*) : Query data from the last 24h after the last run
2. User (when web server recieve a *HTTP-POST with valid date*) : Query data from the given date

#### JSON Format
Once the script was interpreted and all queried data had been processed, the data is converted
as JSON, according the following format (first item) and example (second item):

    {
      "draw": 0,
      "data": [
        [
          <Count>,
          <rowID>,
          <NP>,
          <NPTimestamp>,
          <Named_Entity_Class>,
          <DuckduckGo Search String>,
          <News Sources as Hostname>,
          <predicted class>,
          <probability true>
        ],
        [
          2,
          "305762",
          "Alan Baldwin",
          "2017-02-28 14:09:45",
          "PERSON",
          "<a href='https://duckduckgo.com/?q=\"Alan+Baldwin\"&t=h_&ia=web' target=_blank><b>DuckduckGo</b></a>",
          "<a href='http://www.dailymail.co.uk/wires/reuters/article-4267722/Motor-racing-Hamilton-goes-faster-second-test-day.html?ITO=1490&ns_mchannel=rss&ns_campaign=1490' target=_blank><b>dailymail.co.uk</b></a></br><a href='http://www.reuters.com/article/us-motor-f1-testing-idUSKBN1671HB?feedType=RSS&feedName=sportsNews&utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%253A%2Breuters%252FsportsNews%2B%2528Reuters%2BSports%2BNews%2529' target=_blank><b>reuters.com</b></a></br>",
          "<span class='label success'>true</span>",
          0.9942
        ]
      ]
    }

> TODO: No HTML in the raw JSON. Set the values and make the conversion in the Front-End with JS.

### 2. Front-End

The Front-End consists of a PHP-script *sodeemysql_dt.php*, which is interpreted by the
web server when a user looked up the corresponding website by a web browser.

It is based on the framework [Foundation 5](http://foundation.zurb.com/sites/docs/v/5.5.3/), [jquery](https://jquery.com/) with [Datatables plug-in](https://datatables.net/). The external
scripts and CSS are loaded via a CDN and so, no further files are required.

#### Changeable variables

The following variables have to be adjusted:

* *$lastrun_timestamp* : The path to the timemarker file, which holds the timestamp of the last run

#### How is the data load into the Front-End?

After the website was requested by the user's web browser and the HTML document was build, the datatable made a request on the Back-End and process the result. The following code shows the function to build the datatable. (Details on the syntax and options could be found in the [Docs](https://datatables.net/reference/index))

The following Javascript snippets show the function, which builds the table from the obtained data.

The *ajax* option defines, that AJAX is used to obtain the data from the given *url* with given request method (*type*).
*date_selected* holds the date, if a date is selected by the datepicker on the website.

    var npTable = $('#np-grid').DataTable( {				
        "ajax":{
          url: "sodee-np-data.php",
          type: "POST",
          //serverSide: true,
          data: date_selected
        },			
        "order": [[4, 'desc'], [3, 'desc']], //sorting initial per NE-Class & Timestamp
        "columnDefs": [
          { "visible": false, "targets": [ 0 ]},
          { "type": "moment", "targets": 3},
          {"orderable": false, "targets": "_all"},

The *render* option holds the code, which should execute during the table is built. Here, it executes the function for the *bar rating*.

          {"render":	function ( data, type, row ) {
            return '<span data-tooltip aria-haspopup="true" class="has-tip tip-right" title="' + row[8] + '">'
            +'<span class="br-wrapper br-theme-bars-1to10">'+buildRating(scaleProbability(row[8]))+'</span></span>';						
          },
            "targets": 8 },
        ],
        "language": {
          "search": "Search in table:"
        },
        "displayLength": 10,
        "stripeClasses": [ 'odd-row', 'even-row' ],

All code contained in *drawCallback* is executed after the table was drawn sucessfully.

        "drawCallback": function (settings) {
          var api = this.api();
        var rows = api.rows( {page:'current'} ).nodes();
        var last = null;
        },

All code contained in *initComplete* is executed when the basic frame of the table was built. Here, it is the implementation of the selection filter for the Named Entities.

        // NE-Class filter
        "initComplete": function () {
          this.api().columns([4]).every( function () {
          var column = this;
          var select = $('<select class="in_table_select"><option value=""></option></select>')
            .appendTo( $(column.header()) )
            .on( 'change', function () {
              var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );

              column
                .search( val ? '^'+val+'$' : '', true, false )
                .draw();
            } );

            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
          } );
        },

      } );

The following code snippets show the functions for the datepicker,
which enables the date selection function on the website.

Definition of the datepicker. In the document the HTML element is identified by *npDatepick*.

    var today = new Date();
		$(function(){
			$('#npDatepick').fdatepicker({
				//initialDate: today,
				format: 'yyyy-mm-dd',
				disableDblClickSelection: false,
				leftArrow:'<<',
				rightArrow:'>>',
				closeIcon:'X',
				closeButton: true
			});
		});			

If a date was selected, the user have to click on the "Go" button and then the following function is executed. The function makes a *HTTP-POST* request to the given script/url of the Back-End and inserts the results into the
existing table.

		$("#goDate").click(function(){
			date_selected = $("#npDatepick").val();
			$.post(
				"sodee-np-data.php",
				{"date" : date_selected },
				function(res){
					date_selected = $("#npDatepick").val();
					var newData = $.parseJSON(res);
					//console.log(newData.data);
					var npTable = $('#np-grid').DataTable();
					npTable.clear().rows.add(newData.data).draw();
				});
			});


<div class="page-break" />

## Install the system

The following overview show a tree-based view of the directories required for the system based on a demo install.
The view contains the directories and necessary files

### SODEE core, ngram service, IJS-Newsfeed crawler

    /home/johannes/
    ├── (sodeeRuns_4.lock) --> Lock file, which indicates, that SODEE core currently runs
    │
    └── jre
        │
        ├── runSodeeLoop.sh --> BASH script to start & control the complete System
        ├── http2fs_singleDir.py --> the IJS-Newsfeed crawler python script
        │
        ├── dist    --> SODEE core Java
        │   └── lib --> required libs for the SODEE core
        │
        ├── models  
        │   ├── RBNPE       --> positiveRules and rejectionRules files
        │   ├── stanfordNLP --> the Stanford NLP model files (only NER)
        │   └── weka        --> Classifier model
        │
        ├── newsLive --> The obtained news files are saved here & SODEE core reads from here
        │
        ├── NgramService
        │   ├── input   --> Ngram Service listen this directory (SODEE core must write here)
        │   ├── log     --> Log files of the ngram service
        │   ├── output  --> Ngram Service writes here (SODEE core must listen here)
        │   └── src     --> the executable is saved here
        │       └── lib --> the required libs for the ngram service
        │
        ├── output    --> the output directory, all outputs are saved here (and its subdirectories)
        │   ├── arff  --> SODEE core saves the processed dataset before classification in two *.arff files
        │   ├── logs  --> each run writes a error and normal log file in this directory
        │   │   
        │   ├── info_YYYY-MM-DDTHH:mm:ss.SSSZ.txt --> The info files, which are used for general stats
        │   ├── dailyDataStats.sh --> the script to create general stats
        │   ├── sodee_stats.txt --> contains the stats for each day as tab-separated values
        │   ├── sodeeRuns.log --> the log for the BASH script
        │   ├── last_sodee_run.tm --> Timemarker indicating the last run of SODEE
        │   └── timemarker.tm --> Timemarker indicates the start timestamp for the next run
        │
        ├── processedNews  --> the news archive for the processed news files
        │
        └── toplists  --> the filter lists are saved here
            ├── top10-feedtitle-mfa.txt
            ├── top10-feeduri-mfa.txt
            ├── top10-hostname-mfa.txt
            └── top10-sourcenames-mfa.txt

### Website

*/var/www/html* is the directory, which is accessed by the web server.

    /var/www/html
    ├── error_protocol.php --> displays the last error protocol of the SODEE core
    ├── sodeemysql_dt.php  --> Front-End website
    ├── sodee-np-data.php  --> Back-End script with DB queries and processing
    └── sodee_protocol.php --> displays the last normal protocol of the SODEE core

### Statistics

To create statistics, besides database-based statistics, every day a script interprets the info-files and add the
values to a statistic file. For the example structure above, the following *cronjob* was created and runs every day at 23:55

    # m h  dom mon dow   command
    55 23 * * * /home/johannes/jre/output/dailyDataStats.sh >> /home/johannes/jre/output/sodee_stats.txt &
