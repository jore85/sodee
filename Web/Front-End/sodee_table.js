// Datatable Docu: https://datatables.net/reference/index

var date_selected = new Date();

$(document).ready(function() {
  $.fn.dataTable.moment = function ( format, locale ) {
    var types = $.fn.dataTable.ext.type;
    // Add type detection
    types.detect.unshift( function ( d ) {
      return moment( d, format, locale, true ).isValid() ?
      'moment-'+format :
      null;
    } );
    // Add sorting method - use an integer for the sorting
    types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
      return moment( d, format, locale, true ).unix();
    } ;
  } ;

  $.fn.dataTable.moment( "YYYY-MM-DD HH:mm:ss" );

  var npTable = $('#np-grid').DataTable( {
    "ajax":{
      url: "sodee-np-data.php",
      type: "POST",
      //serverSide: true,
      data: date_selected
    },
    //"processing" : true,
    "order": [[4, 'desc'], [3, 'desc']], //sorting initial per NE-Class & Timestamp
    "columnDefs": [
      { "visible": false, "targets": [ 0 ]},
      { "type": "moment", "targets": 3},
      {"orderable": false, "targets": "_all"},
      {"render":	function ( data, type, row ) {
        return '<span data-tooltip aria-haspopup="true" class="has-tip tip-right" title="' + row[8] + '">'
        +'<span class="br-wrapper br-theme-bars-1to10">'+buildRating(scaleProbability(row[8]))+'</span></span>';
        //return '<span style="cursor:pointer;" title="' + row[8] + '">'
        //+'<span class="br-wrapper br-theme-bars-1to10">'+buildRating(scaleProbability(row[8]))+'</span></span>';
        //return '<span data-tooltip aria-haspopup="true" class="has-tip tip-right" title="' + row[8] + '">'+
        //'<select id="probability"><option value="'+scaleProbability(row[8])+'">'
        //+scaleProbability(row[8])+'</option></select></span>';
      },
      "targets": 8 },
    ],
    //"bSort": false,
    "language": {
      "search": "Search in table:"
    },
    "displayLength": 10,
    "stripeClasses": [ 'odd-row', 'even-row' ],
    "drawCallback": function (settings) {
      var api = this.api();
      var rows = api.rows( {page:'current'} ).nodes();
      var last = null;

      // Grouping and ordering per NE-Class
      /*
      api.column(4, {page:'current'} ).data().each( function (group, i) {
      if(last !== group ) {
      $(rows).eq( i ).before(
      '<tr class="group"><td colspan="8"><b>'+group+'</b></td></tr>'
    );

    last = group;
  }
} );
*/
},

// NE-Class filter
"initComplete": function () {
  this.api().columns([4]).every( function () {
    var column = this;
    var select = $('<select class="in_table_select"><option value=""></option></select>')
    .appendTo( $(column.header()) )
    .on( 'change', function () {
      var val = $.fn.dataTable.util.escapeRegex(
        $(this).val()
      );

      column
      .search( val ? '^'+val+'$' : '', true, false )
      .draw();
    } );

    column.data().unique().sort().each( function ( d, j ) {
      select.append( '<option value="'+d+'">'+d+'</option>' )
    } );
  } );
},

} );

//build the confidence rating for the result table
function buildRating( rate ){
  var displayRating = '<span class="br-widget">';
  for( i = 0; i < 10; i++){
    if( i <= rate ){
      displayRating = displayRating + '<a class="br-selected"></a>';
    } else {
      displayRating = displayRating + '<a ></a>';
    }
  }
  displayRating = displayRating + '</span>';
  return displayRating;
};

function scaleProbability( prob ){
  var rate;
  if( prob >= 0.5 && prob < 0.6){
    return 1;
  } else if( prob >= 0.5 && prob < 0.55 ) {
    return 2;
  } else if( prob >= 0.55 && prob < 0.65) {
    return 3;
  } else if( prob >= 0.65 && prob < 0.7) {
    return 4;
  } else if( prob >= 0.7 && prob < 0.75) {
    return 5;
  } else if( prob >= 0.75 && prob < 0.8) {
    return 6;
  } else if( prob >= 0.8 && prob < 0.85) {
    return 7;
  } else if( prob >= 0.85 && prob < 0.9) {
    return 8;
  } else if(prob >= 0.9 && prob < 0.95) {
    return 9;
  } else if( prob >= 0.95 && prob < 0.975){
    return 10;
  } else if( prob >= 0.975 && prob < 0.980) {
    return 11;
  } else if (prob >= 0.980 && prob < 0.985) {
    return 12;
  } else if (prob >= 0.985 && prob < 0.990) {
    return 13;
  } else if (prob >= 0.990 && prob < 0.995) {
    return 14;
  } else if (prob >= 0.995 && prob <= 1) {
    return 15;
  }
};
} );

// DATEPICKER and POST-CALL for the costumized selection of results
var today = new Date();
$(function(){
  $('#npDatepick').fdatepicker({
    //initialDate: today,
    format: 'yyyy-mm-dd',
    disableDblClickSelection: false,
    leftArrow:'<<',
    rightArrow:'>>',
    closeIcon:'X',
    closeButton: true
  });
});

$("#goDate").click(function(){
  date_selected = $("#npDatepick").val();
  $.post(
    "sodee-np-data.php",
    {"date" : date_selected },
    function(res){
      date_selected = $("#npDatepick").val();
      var newData = $.parseJSON(res);
      //console.log(newData.data);
      var npTable = $('#np-grid').DataTable();
      npTable.clear().rows.add(newData.data).draw();
    });
  });
