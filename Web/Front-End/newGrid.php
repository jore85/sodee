<?php
$lastrun_timestamp = filemtime("/home/johannes/jre/output/last_sodee_run.tm");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--<meta http-equiv="refresh" content="30">-->
  <title>SODEE Live</title>
</head>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.min.css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/foundation-datepicker/1.5.5/css/foundation-datepicker.css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<link href="https://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/bars-horizontal.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/bars-1to10.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.foundation.min.css"/>

<style>
select.in_table_select {
  height: inherit;
}

.br-theme-bars-1to10 .br-widget a {
  display: block;
  width: 2px;
  padding: 5px 0;
  height: 5px;
  float: left;
  background-color: rgba(208, 0, 0, 0.4);
  margin: 1px;
  text-align: center;
  cursor: pointer;
  <!--pointer-events: none;-->
}

.br-theme-bars-1to10 .br-widget a.br-active, .br-theme-bars-1to10 .br-widget a.br-selected {
  background-color: #43AC6A;
  cursor: pointer;
  <!--pointer-events: none;-->
}

</style>

<body>
  <div class="row">
    <div class="large-12 columns">
      <h2>SODEE: A System For Online Detection Of Emerging Entities</h2>
    </div>
    <hr>
  </div>

  <div class="row">
    <div class="alert-box info">
      <h6>Last run was on: <?php echo date("m/d/Y H:i:s", $lastrun_timestamp);?></h6>
    </div>
    <div class="large-12 columns">
      <canvas id="sodee_stats" class="large-9-columns"></canvas>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <ul class="tabs" data-tab>
        <li class="tab-title"><a href="#protocol_panel">SODEE Protocol</a></li>
        <li class="tab-title"><a href="#error_protocol_panel">Error Protocol</a></li>
        <li class="tab-title"><a href="#filterlist">Filter Lists</a></li>
      </ul>
      <div class="tabs-content">
        <div class="content" id="protocol_panel">
          <div class="large-12 columns">
            <div class="row">
              <h6><a href="sodee_protocol.php" target="_blank">open in new window</a></h6>
            </div>
            <div class="row">
              <p id="protocol" class="panel callout" style="overflow-y: scroll; height: 15em"></p>
            </div>
          </div>
        </div>
        <div class="content" id="error_protocol_panel">
          <div class="large-12 columns">
            <div class="row">
              <h6><a href="error_protocol.php" target="_blank">open in new window</a></h6>
            </div>
            <div class="row">
              <p id="error_protocol" class="panel callout" style="overflow-y: scroll; height: 15em"></p>
            </div>
          </div>
        </div>
        <div class="content" id="filterlist">
          <div class="large-12 columns">
            <ul class="tabs vertical" data-tab>
              <li class="tab-title"><a href="#feedtitle_panel">Feed Titles</a></li>
              <li class="tab-title"><a href="#feeduri_panel">Feed URIs</a></li>
              <li class="tab-title"><a href="#hostname_panel">Hostnames</a></li>
              <li class="tab-title"><a href="#sourcename_panel">Sourcenames</a></li>
            </ul>

            <div class="tabs-content">
              <div class="content" id="feedtitle_panel" >
                <div class="large-8 columns">
                  <ul class="pricing-table" id="feedtitle"></ul>
                </div>
              </div>
              <div class="content" id="feeduri_panel">
                <div class="large-8 columns">
                  <ul class="pricing-table" id="feeduri"></ul>
                </div>
              </div>
              <div class="content" id="hostname_panel">
                <div class="large-8 columns">
                  <ul class="pricing-table" id="hostname" ></ul>
                </div>
              </div>
              <div class="content" id="sourcename_panel">
                <div class="large-8 columns">
                  <ul class="pricing-table" id="sourcename"></ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr>
    </div>
  </div>

  <div class="row">
    <div class="large-12 columns">
      <div class="large-4 columns">
        <form name="date_selection" data-abide>
          <div class="row collapse">
            <label>Select a day:</label>
            <div class="small-10 columns">
              <input type="text" id="npDatepick" name="date" pattern="date">
            </div>
            <div class="small-2 columns">
              <button type="button" class="button postfix" id="goDate">Go</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="large-12 columns">
      <table class="display" id="np-grid" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>ID</th>
            <th width="300">NP</th>
            <th width="150">Date &amp; Time</th>
            <th width="250">Named Entity Class</th>
            <th width="150">Search NP</th>
            <th width="200">Article Link &amp; Host</th>
            <th>Class</th>
            <th width="40">Quality</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.13/sorting/datetime-moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation-datepicker/1.5.5/js/foundation-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation-datepicker/1.5.5/js/locales/foundation-datepicker.en-GB.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
crossorigin="anonymous"></script>
<script type="text/javascript" src="sodee_table.js"></script>

<script type="text/javascript" language="javascript">

$(document).foundation();
$( "tabs" ).click(function() {
 $( this ).toggleClass( "active" );
});

// GET & DRAW THE STATISTICS OF SODEE SYSTEM
var labels = '';
var article_all = '';
var article_processed = '';
var np_extracted = '';
var np_final = '';

var statsData = $.ajax({
  type: 'GET',
  url: 'sodee-stats.php',
  dataType: "json",
  async: true,
  success: function(data){
    labels = data.labels;
    article_all = data.datasets[0];
    article_processed = data.datasets[1];
    np_extracted = data.datasets[2];
    np_final = data.datasets[3];
    drawStatChart(labels, article_all, article_processed, np_extracted, np_final);
  }
});

var drawStatChart = function(labels, article_all, article_processed, np_extracted, np_final){
  //console.log(article_all);
  var sysStatCanvas = document.getElementById('sodee_stats').getContext('2d');
  var sysStatChart = new Chart(sysStatCanvas, {
    type: 'line',
    data: {
      labels: labels,
      datasets: [{
        // All articles
        fillColor: "rgba(0, 112, 149, 1)",
        backgroundColor: "rgba(0, 112, 149, 0.25)",
        pointBackgroundColor: "rgba(0, 112, 149, 1)",
        borderColor: "rgba(0, 112, 149, 1)",
        fill: true,
        label: 'All articles',
        data: article_all.data
      }, {
        fillColor: "rgba(0, 68, 90, 1)",
        backgroundColor: "rgba(0, 68, 90, 0.25)",
        pointBackgroundColor: "rgba(0, 68, 90, 1)",
        borderColor: "rgba(0, 68, 90, 1)",
        fill: true,
        label: 'Finally processed Articles',
        data: article_processed.data
      }, {
        fillColor: "rgba(0, 175, 59, 1)",
        backgroundColor: "rgba(0, 175, 59, 0.25)",
        pointBackgroundColor: "rgba(0, 175, 59, 1)",
        borderColor: "rgba(0, 175, 59, 1)",
        fill: true,
        label: 'NPs extracted',
        data: np_extracted.data
      }, {
        fillColor: "rgba(0, 106, 36, 1)",
        backgroundColor: "rgba(0, 106, 36, 0.25)",
        pointBackgroundColor: "rgba(0, 106, 36, 1)",
        borderColor: "rgba(0, 106, 36, 1)",
        fill: true,
        label: 'Finally inserted NPs',
        data: np_final.data
      }
    ]
  }
});
};

// AJAX CALL PROTOCOL DATA
var protocol_text = 'no protocol data';

function getProtocolData(type){
  var source = 'sodee_protocol.php';
  var panelId = 'protocol';
  if(type == 'errors'){
    source = 'error_protocol.php';
    panelId = 'error_protocol';
  }
  $.ajax({
    type: 'GET',
    dataType: "html",
    url: source,
    success: function(data){
      protocol_text = data;
      drawProtocol(protocol_text, panelId);
      setTimeout(function(){getProtocolData(type);}, 30000); // reload the protocol data every 30sec
    }
  });
}

function drawProtocol(protocol_text, panelId){
  document.getElementById(panelId).innerHTML = protocol_text;
}

getProtocolData('normal');
getProtocolData('errors');

// AJAX CALL FILTERLIST DATA
var feedtitle_data = 'no data';
var feeduri_data = 'no data';
var hostname_data = 'no data';
var sourcename_data = 'no data';

function getFilterlistData(){
  $.ajax({
    type: 'GET',
    dataType: 'json',
    url: 'sodee-news-src.php',
    success: function(data){
      feedtitle_data = data.feedtitle;
      createFilterlistDisplay(feedtitle_data, 'feedtitle', 'Feed Titles');
      feeduri_data = data.feeduri;
      createFilterlistDisplay(feeduri_data, 'feeduri', 'Feed URIs');
      hostname_data = data.hostname;
      createFilterlistDisplay(hostname_data, 'hostname', 'Hostnames');
      sourcename_data = data.sourcename;
      createFilterlistDisplay(sourcename_data, 'sourcename', 'Sourcenames');
    }
  });
}

function createFilterlistDisplay(data, listId, title){
    var list_panel = document.getElementById(listId);
    var list_items = '<li class="title">'+ title + '</li>';
    $(data).each(function(index, item){
      list_items = list_items + '<li class="bullet-item">' + item + '</li>';
    });
    list_panel.innerHTML = list_items;
}

getFilterlistData();

</script>

</html>
