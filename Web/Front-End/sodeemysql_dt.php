<?php
$lastrun_timestamp = filemtime("/home/johannes/jre/output/last_sodee_run.tm");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--<meta http-equiv="refresh" content="30">-->
    <title>SODEE Live</title>
    </head>
  
  <body>
    <div class="row">
      <div class="large-12 columns">
        <h1>Novel and Emerging Entities</h1>
      </div>
    </div>
	<div class="row">
		<div class="large-9 columns">
		Last run was on: <?php echo date("d.m.Y H:i:s", $lastrun_timestamp);?>
		</div>
		<form name="date_selection">
			<div class="row collapse">
			<div class="large-2 columns">
				<label>Select a day:</label>
			</div>
			<div class="large-2 columns">				
				<input type="text" id="npDatepick" name="date">	
			</div>
			<div class="large-1 columns">
				<button type="button" class="button postfix" id="goDate">Go</button>		
			</div>
			</div>			
		</form>
	</div>
	<div class="row">
	<div class="large-12 columns">
	<table class="display" id="np-grid" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>#</th>
			<th>ID</th>
			<th width="300">NP</th>			
			<th width="150">Date &amp; Time</th>
			<th width="250">Named Entity Class</th>
			<th width="150">Search NP</th>
			<th width="150">Article Link &amp; Host</th>
			<th>Class</th>
			<th width="40">Quality</th>
		</tr>
	</thead>
	<!--<tfoot>
		<tr>
			<th>#</th>
			<th>ID</th>
			<th width="300">NP</th>			
			<th width="150">Date &amp; Time</th>
			<th width="150">Named Entity Class</th>
			<th width="150">Search NP</th>
			<th width="150">Article Link</th>
			<th>Class</th>
			<th width="40">Probability for class</th>
		</tr>
	</tfoot>-->
	</table>
	</div>
	</div>
	<div class="row">
		<div class="large-16-columns">
			<!--<canvas id="np_occurrence" class="large-16-columns"></canvas>-->
		</div>
	</div>
</body>
<!-- jQuery 1.12.4 
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
  -->
<!-- Compressed CSS 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.0/css/foundation.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.0/js/foundation.min.js"></script>
-->
<!-- Datatable 
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<!-- include all Scripts/CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.foundation.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.foundation.min.css"/>
 
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.13/sorting/datetime-moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation-datepicker/1.5.5/js/foundation-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation-datepicker/1.5.5/js/locales/foundation-datepicker.en-GB.js"></script>

<!-- jQuery UI 1.12 -->
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
  
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/foundation-datepicker/1.5.5/css/foundation-datepicker.css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/bars-horizontal.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/bars-1to10.min.css" />
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />-->
	<style>
		select.in_table_select {
			height: inherit;
		}
		
		.br-theme-bars-1to10 .br-widget a {
			display: block;
			width: 2px;
			padding: 5px 0;
			height: 5px;
			float: left;
			background-color: rgba(208, 0, 0, 0.4);
			margin: 1px;
			text-align: center;
			cursor: pointer;
			<!--pointer-events: none;-->
		}
		
		.br-theme-bars-1to10 .br-widget a.br-active, .br-theme-bars-1to10 .br-widget a.br-selected {
			background-color: #43AC6A;
			cursor: pointer;
			<!--pointer-events: none;-->
		}
		
	</style>


<script type="text/javascript" language="javascript">
// Datatable Docu: https://datatables.net/reference/index		

		var date_selected = new Date();
		
		$(document).ready(function() {	
			$.fn.dataTable.moment = function ( format, locale ) {
				var types = $.fn.dataTable.ext.type; 
				// Add type detection
				types.detect.unshift( function ( d ) {
					return moment( d, format, locale, true ).isValid() ?
					'moment-'+format :
					null;
				} ); 
				// Add sorting method - use an integer for the sorting
				types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
					return moment( d, format, locale, true ).unix();
				} ;
			} ;
			
			$.fn.dataTable.moment( "YYYY-MM-DD HH:mm:ss" );
			
			var npTable = $('#np-grid').DataTable( {				
				"ajax":{
					url: "sodee-np-data.php",
					type: "POST",
					//serverSide: true,
					data: date_selected
				},
				//"processing" : true,				
				"order": [[4, 'desc'], [3, 'desc']], //sorting initial per NE-Class & Timestamp
				"columnDefs": [
					{ "visible": false, "targets": [ 0 ]},
					{ "type": "moment", "targets": 3},
					{"orderable": false, "targets": "_all"},
					{"render":	function ( data, type, row ) {
						return '<span data-tooltip aria-haspopup="true" class="has-tip tip-right" title="' + row[8] + '">'
						+'<span class="br-wrapper br-theme-bars-1to10">'+buildRating(scaleProbability(row[8]))+'</span></span>';
						//return '<span style="cursor:pointer;" title="' + row[8] + '">'
						//+'<span class="br-wrapper br-theme-bars-1to10">'+buildRating(scaleProbability(row[8]))+'</span></span>';
						//return '<span data-tooltip aria-haspopup="true" class="has-tip tip-right" title="' + row[8] + '">'+
						//'<select id="probability"><option value="'+scaleProbability(row[8])+'">'
						//+scaleProbability(row[8])+'</option></select></span>';							
					},
						"targets": 8 }, 
				],
				//"bSort": false,
				"language": {
					"search": "Search in table:"
				},
				"displayLength": 10,
				"stripeClasses": [ 'odd-row', 'even-row' ],
				"drawCallback": function (settings) {
					var api = this.api();
				var rows = api.rows( {page:'current'} ).nodes();
				var last = null;
				
				// Grouping and ordering per NE-Class
				/*
				api.column(4, {page:'current'} ).data().each( function (group, i) {
					if(last !== group ) {
						$(rows).eq( i ).before(
						'<tr class="group"><td colspan="8"><b>'+group+'</b></td></tr>'
						);
						
						last = group;
					}
				} );
				*/
				},
				
				// NE-Class filter
				"initComplete": function () {
					this.api().columns([4]).every( function () {
					var column = this;
					var select = $('<select class="in_table_select"><option value=""></option></select>')
						.appendTo( $(column.header()) )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
 
							column
								.search( val ? '^'+val+'$' : '', true, false )
								.draw();
						} );
 
						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				},
				
			} );		

			function buildRating( rate ){
				var displayRating = '<span class="br-widget">';
				for( i = 0; i < 10; i++){
					if( i <= rate ){
						displayRating = displayRating + '<a class="br-selected"></a>';						
					} else {
						displayRating = displayRating + '<a ></a>';
					}
				}
				displayRating = displayRating + '</span>';
				return displayRating;
			};
			
			function scaleProbability( prob ){
				var rate;
				if( prob >= 0.5 && prob < 0.6){
					return 1;
				} else if( prob >= 0.5 && prob < 0.55 ) {
					return 2;
				} else if( prob >= 0.55 && prob < 0.65) {
					return 3;
				} else if( prob >= 0.65 && prob < 0.7) {
					return 4;
				} else if( prob >= 0.7 && prob < 0.75) {
					return 5;
				} else if( prob >= 0.75 && prob < 0.8) {
					return 6;
				} else if( prob >= 0.8 && prob < 0.85) {
					return 7;					
				} else if( prob >= 0.85 && prob < 0.9) {
					return 8;
				} else if(prob >= 0.9 && prob < 0.95) {
					return 9;
				} else if( prob >= 0.95 && prob < 0.975){
					return 10;
				} else if( prob >= 0.975 && prob < 0.980) {
					return 11;
				} else if (prob >= 0.980 && prob < 0.985) {
					return 12;
				} else if (prob >= 0.985 && prob < 0.990) {
					return 13;
				} else if (prob >= 0.990 && prob < 0.995) {
					return 14;
				} else if (prob >= 0.995 && prob <= 1) {
					return 15;
				}
			};
			
			/*
			//didn't work correctly
			$('#np-grid tbody').on( 'click', 'tr.group', function () {
				var currentOrder = table.order()[0];
				if( currentOrder[0] === 4 && currentOrder[1] === 'desc' ){
					table.order( [4, 'desc'], [3, 'desc'] ).draw();
				} else {
					table.order( [4, 'desc'], [3, 'desc'] ).draw();
				}
			} );
			*/
			
		} ); //end document
		
		var today = new Date();
		$(function(){
			$('#npDatepick').fdatepicker({
				//initialDate: today,
				format: 'yyyy-mm-dd',
				disableDblClickSelection: false,
				leftArrow:'<<',
				rightArrow:'>>',
				closeIcon:'X',
				closeButton: true
			});
		});			
		
		$("#goDate").click(function(){
			date_selected = $("#npDatepick").val();
			$.post(
				"sodee-np-data.php", 
				{"date" : date_selected }, 
				function(res){
					date_selected = $("#npDatepick").val();
					var newData = $.parseJSON(res);
					//console.log(newData.data);
					var npTable = $('#np-grid').DataTable();
					npTable.clear().rows.add(newData.data).draw();
				});
			});

		
		// create a chartjs chart for the np occurrence
		/*
		var ctx = document.getElementById("np_occurrence");
		var myChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: ["Red"],
				datasets: [{
				label: '# of Votes',
				data: [12, 19, 3, 5, 2, 3],
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)'
				],
				borderColor: [
					'rgba(255,99,132,1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
						}
					}]
				}
			}
		*/
		
		// Source: http://microbuilder.io/blog/2016/01/10/plotting-json-data-with-chart-js.html
		/*
		function drawLineChart() {
		
		Date.prototype.formatMMDDYYYY = function() {
			return (this.getMonth() + 1) +
				"/" +  this.getDate() +
				"/" +  this.getFullYear();
		}
		
		var jsonData = $.ajax({
			url: 'sodee-timeline-data.php',
			dataType: 'json',
			}).done(function (results) {

			// Split timestamp and data into separate arrays
			var labels = [], data=[];
			var nps = results;
			for (var i = 0; i < nps.length; i++) {
				labels.push(new Date(nps[1]).formatMMDDYYYY());
				data.push(nps[2]);
			//results[0].forEach(function(np) {
			//	labels.push(new Date(np[1]).formatMMDDYYYY());
			//	data.push(np[2]);
			};

		// Create the chart.js data structure using 'labels' and 'data'
		var tempData = {
			labels : labels,
			datasets : [{
			fillColor             : "rgba(151,187,205,0.2)",
			strokeColor           : "rgba(151,187,205,1)",
			pointColor            : "rgba(151,187,205,1)",
			pointStrokeColor      : "#fff",
			pointHighlightFill    : "#fff",
			pointHighlightStroke  : "rgba(151,187,205,1)",
			data                  : data
			}]
		};
		*/

		// Get the context of the canvas element we want to select
		//var ctx = document.getElementById("np_occurrence").getContext("2d");

		// Instantiate a new chart
		//var myLineChart = new Chart.Line(ctx, tempData);
		//});
		//}
				
		//drawLineChart();

		
		// get occurrence data from db by ajax
		// http://microbuilder.io/blog/2016/01/10/plotting-json-data-with-chart-js.html
		// http://www.chartjs.org/docs/#line-chart-example-usage
		/*
		var GetOccurrenceData = function() {
			$ajax({
				
				method: 'GET',
				dataType: 'json',
				success: function (d) {
					chartData = {
						labels: 
					}
				}
			}
				
		}; 
		*/
	</script>

</html>
