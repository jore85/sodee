<?php
$lastrun_timestamp = filemtime("/home/johannes/jre/output/last_sodee_run.tm");
$sodee_stats_file = "/home/johannes/jre/output/sodee_stats.txt";
$delimiter = "\t";

/* The stats file has the following format:

DATE    ARTICLE_ALL     ARTICLE_PROCESSED       NP_EXTRACTED    NP_FINAL
2017-02-25      28550   4278    63738   24439

*/

$stats_tsv = file_get_contents($sodee_stats_file);
$stats_lines = explode("\n", $stats_tsv);
$head = str_getcsv(array_shift($stats_lines), "\t");

// build the array from the tab-separated file of the SODEE system stats
$stats_array = array();
$days = array();
$article_all = array();
$article_processed = array();
$np_extracted = array();
$np_final = array();
foreach($stats_lines as $line) {
	$line_splitted = str_getcsv($line, "\t");
	$days[] = array_shift($line_splitted);
	$article_all[] = array_shift($line_splitted);
	$article_processed[] = array_shift($line_splitted);
	$np_extracted[] = array_shift($line_splitted);
	$np_final[] = array_shift($line_splitted);
}

// build the formatted array to encode into json
$chart_values = array(
	"labels" => $days,
	"datasets" => array(
		array("label" => $head[1], "data" => $article_all),
		array("label" => $head[2], "data" => $article_processed),
		array("label" => $head[3], "data" => $np_extracted),
		array("label" => $head[4], "data" => $np_final))
);

$json = json_encode($chart_values);
echo($json);
?>
