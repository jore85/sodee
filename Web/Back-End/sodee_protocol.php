<?php
//http://stackoverflow.com/a/1491076
$path = "/home/johannes/jre/output/logs";

$latest_mtime = 0;
$latest_filename = '';

$d = dir($path);
while (false !== ($entry = $d->read())) {
  $filepath = "{$path}/{$entry}";
  // could do also other checks than just checking whether the entry is a file
  if (is_file($filepath) && fnmatch("output_*.log",$entry) && filemtime($filepath) > $latest_mtime) {
    $latest_mtime = filemtime($filepath);
    $latest_filename = $entry;
    $latest_filepath = $filepath;
  }
}
//echo $latest_filename;
//echo $latest_filepath;
?>

<?php
echo "<pre>" . file_get_contents($latest_filepath) . "</pre>";
?>

