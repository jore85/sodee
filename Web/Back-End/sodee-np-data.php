<?php

//date_default_timezone_set("Europe/London");
// get the time of the last run (simple check the log-file change datetime
$lastrun_timestamp = filemtime("/home/johannes/jre/output/last_sodee_run.tm");
$time_minus24h = $lastrun_timestamp - (24 * 60 * 60);

//DB Connection
$db_host = "localhost";
$db_user = "johannes";
$db_pwd = "DtG3cvfEoJ7B";
$db_name = "johannes";
$db_connect = mysqli_connect($db_host, $db_user, $db_pwd, $db_name) 
or die("Connection failed: " . mysqli_error());

$requestData= $_POST["date"];
//print_r($_POST);

$cols = array(
	0 => 'rowID', 
	1 => 'NP', 
	2 => 'timestamps', 
	3 => 'Named_Entity_Class', 
	4 => 'title',
	5 => 'article_list', 
	6 => 'articleID', 
	7 => 'class',
	8 => 'probabilityTRUE'
);

$sql_request = "SELECT rowID";
$sql_request.=" FROM allNPFeatures";
$sql_request.=" WHERE class = 1";
//echo $sql_request."\n";
//$sql_query=mysqli_query($db_connect, $sql_request) or die("sodee-np-data.php: get all NPs");
//$totalData=mysqli_num_rows($sql_query);
//$totalFiltered = $totalData;

$sql_request = "SELECT rowID, NP, NPTimestamp, Named_Entity_Class, title, group_concat(articleURI) AS article_list, articleID, class, probabilityTRUE, probabilityFALSE"; 
$sql_request.=" FROM allNPFeatures";
$sql_request.=" WHERE class = 1";

//echo $sql_request."\n";
// database selects for table view (sorting, date-pick)
if( isset($requestData) && !empty($requestData) ){
	$sql_request.=" AND  DATE(NPTimestamp) = '".$requestData."'";
} else {
	// if no date is selected the normal view is shown
	$sql_request.=" AND processedTime >= from_unixtime(" . $time_minus24h . ")";
}
$sql_request.="GROUP BY NP";

//echo $sql_request."\n";
//$sql_query=mysqli_query($db_connect, $sql_request) or die("sodee-np-data.php: get all NPs");
//$totalFiltered = mysqli_num_rows($sql_query); 
//echo $totalFiltered."\n";
//$sql_request.=" ORDER BY NPTimestamp DESC";
//" ORDER BY ". $cols[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
//echo $sql_request."\n";
$sql_query=mysqli_query($db_connect, $sql_request) or die("sodee-np-data.php: get all NPs");
//echo mysqli_num_rows($sql_query)."\n"; 

$data = array();

//convert a comma-sep list of links into a real list
function convertArticleList($listToConvert) {
	$return_list = "";
	$link_elements = array_unique( explode (",", $listToConvert) );
	foreach($link_elements as $link_element) {
		$hostURL = get_domain($link_element);
		$return_list.="<a href='".$link_element."' target=_blank><b>".$hostURL."</b></a></br>";
	}
	return $return_list;
}

// http://stackoverflow.com/a/15498686
function get_domain($url)
{
      $urlobj=parse_url($url);
      $domain=$urlobj['host'];
      if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
      }
      return false;
}


while( $row=mysqli_fetch_array($sql_query) ) {
	if( $row["class"] == 1) { $class="<span class='label success'>true</span>";} else {$class="<span class='label success'>false</span>";};
	$nestedData=array();
	$nestedData[] = $count = $count+ 1;
	$nestedData[] = $row["rowID"];
	$nestedData[] = $row["NP"];
	$nestedData[] = $row["NPTimestamp"];
	$nestedData[] = $row["Named_Entity_Class"];
	//$nestedData[] = $row["title"];
	//$nestedData[] = $row["articleID"];
	$nestedData[] = "<a href='https://duckduckgo.com/?q=\"".urlencode($row["NP"])."\"&t=h_&ia=web' target=_blank><b>DuckduckGo</b></a>";
	//$nestedData[] = "<a class='button small' href='".$row["articleURI"]."' target=_blank><b>Go to Article</b></a>";
	$nestedData[] = convertArticleList($row["article_list"]);
	$nestedData[] = $class;
	$nestedData[] = round($row["probabilityTRUE"], 4);
	//$nestedData[] = "<div class='progress large-10 success rounded'><span class='meter' style='width: "
	//.(100-((100-(round($row["probabilityTRUE"], 4)*100)) / 5))."%'>".round($row["probabilityTRUE"], 4)."</span></div>";
	
	$data[] = $nestedData;
}

$json_data = array(
	"draw" => intval( $requestData['draw'] ),
	"data" => $data
	);

echo json_encode($json_data);

?>
