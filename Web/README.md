# Notes #

The website resources are devided in [Back-End](./Back-End/) and [Front-End](./Front-End/).

Two different Front-Ends/Website versions exist:

### 1. A very basic one

![SODEE_basic.JPG](https://bitbucket.org/repo/ypp9d7p/images/1297454044-SODEE_basic.JPG "Screenshot of the simple website")

[sodeemysql_dt.php](./Front-End/sodeemysql_dt.php)

That one requires only the [sodee-np-data.php](./Back-End/sodee-np-data.php) in the Back-End.

### 2. An enhanced and more informative version

![SODEE_enhanced_small.jpg](https://bitbucket.org/repo/ypp9d7p/images/4103028046-SODEE_enhanced_small.jpg "Screenshot of the enhanced website")

[newGrid.php](./Front-End/newGrid.php)

That one requires all files in the [Back-End](./Back-End/)-folder. To work properly, there must be also the *stats-file*, which is a tab-seperated file, containing daily stats of the system.

To obtain the daily stats, the script "[dailyDataStats.sh](/BashScripts/dailyDataStats.sh)" must be executed on a daily basis (e.g., plan a cron-job every day at 23:55).
