# Notes #

Some local dependencies, which are not avaiable over central reposorities or causes issues, must be manually set.
The pom is ready, alone the artefacts must be copied into a folder *"libs"* under the project folder.
The manually added *"libs"*-folder must contain the following jars:

* **stanford-postagger-3.4.1.jar**, obtain the Stanford PoS-Tagger (v 3.4.1) [here](https://nlp.stanford.edu/software/tagger.shtml#History)
* **stanford-ner.jar**, obtain the Stanford Named Entity Recognizer [here](https://nlp.stanford.edu/software/CRF-NER.shtml#History)
* **JSI-Reader-for-NoveltyDetection-20160519.jar**, private
* **GoogleNgramUtilityM2016.jar**, private

With installed Maven execute the following command in the project directory:

    mvn clean package

**If the build was successful**, a directory *"target"* could be found in the project directory, which contains several directories and two jar-files. One of these jars' names contains
something like *"with-dependencies"*, that jar could used to run the SODEEcore.

**For the use with the Bash-Script the name must adjusted either in the script or the jar must renamed to "SODEE.jar"!**
