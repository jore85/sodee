package Utility;

import RBBNPE.ArchivedNP;
import RBBNPE.EnhancedNP;
import edu.stanford.nlp.util.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * This class contains diverse Helper Methods.
 *
 * @author Johannes Reiss
 */
public class Helper {

    /*
     Source: http://stackoverflow.com/questions/2689379/how-to-get-a-list-of-dates-between-two-dates-in-java/23932946#23932946
     */
    public static String[] allDates(String startDate, String endDate) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);
        List<String> totalDates = new ArrayList<>();
        while (!start.isAfter(end)) {
            String d = dtf.print(start);
            System.out.println(d);
            totalDates.add(d);
            start = start.plusDays(1);
        }
        return totalDates.toArray(new String[totalDates.size()]);
    }

    public static ArrayList<String> readFileLines(String pathToFile) {
        ArrayList<String> output = new ArrayList<>();
        String line;
        InputStream fileInput = null;
        int count = 0;
        try {
            fileInput = new FileInputStream(new File(pathToFile));
        } catch (FileNotFoundException ex) {
            System.err.println("Something went wrong while reading file: "
                    + pathToFile);
        }
        InputStreamReader isr = new InputStreamReader(fileInput, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(isr);
        try {
            while ((line = br.readLine()) != null) {
                output.add(line.trim());
                //System.out.println("Read line: " + count++);
            }
        } catch (IOException ex) {
            System.err.println("Something went wrong on reading a line of file.");
        }
        try {
            br.close();
        } catch (IOException ex) {
            System.err.println("Something went wrong on closing the buff.reader");
        }
        return output;
    }

    public static ArrayList<String> readFileLinesAllLowerCase(String pathToFile) {
        ArrayList<String> output = new ArrayList<>();
        String line;
        InputStream fileInput = null;
        int count = 0;
        try {
            fileInput = new FileInputStream(new File(pathToFile));
        } catch (FileNotFoundException ex) {
            System.err.println("Something went wrong while reading file: "
                    + pathToFile);
        }
        InputStreamReader isr = new InputStreamReader(fileInput, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(isr);
        try {
            while ((line = br.readLine()) != null) {
                output.add(line.trim().toLowerCase());
                //System.out.println("Read line: " + count++);
            }
        } catch (IOException ex) {
            System.err.println("Something went wrong on reading a line of file.");
        }
        try {
            br.close();
        } catch (IOException ex) {
            System.err.println("Something went wrong on closing the buff.reader");
        }
        return output;
    }

    public static HashSet<String> readGZFilePerLines(String pathToGZFile) {
        HashSet<String> output = new HashSet<>();
        GZFileReader fr = new GZFileReader(pathToGZFile);
        Thread t = new Thread(fr);
        t.start();

        try {
            t.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (t.getState() == Thread.State.TERMINATED) {
            output = fr.getFileContent();
            System.out.println("Reading " + pathToGZFile + "is finished");
        }
        return output;
    }

    // http://stackoverflow.com/a/4050276
    public static String getLastElementOfURI(String uri) {
        return uri.replaceFirst(".*/([^/?]+).*", "$1");
    }

    public static String hashSetToCSString(HashSet<String> s) {
        String result = "";
        if (s != null) {
            Iterator<String> iterator = s.iterator();
            while (iterator.hasNext()) {
                result = result + "," + iterator.next();
            }
        }
        return result;
    }

    public static String followCVSformat(String value) {

        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    /**
     * Helper Method to write a file of noun phrases for the ngram File. After
     * completation of the writing process a file *.finished will be created to
     * signal the other program the completation.
     *
     * @param filePath The path write the file to.
     * @param nps A set of the enhanced noun phrases which will be saved into
     * the file.
     */
    public static boolean writeNPFile(String filePath, HashSet<EnhancedNP> nps) {
        boolean result = false;
        try {
            FileWriter fw = new FileWriter(filePath);
            Iterator<EnhancedNP> iterator = nps.iterator();
            while (iterator.hasNext()) {
                EnhancedNP np = iterator.next();
                int articleID = np.getArticleID();
                String dayDate = np.getDayDateTime();
                String phraseString = np.getNp().getPhraseString();
                String w = +articleID + "\t" + dayDate + "\t" + phraseString + "\n";
                fw.write(w);
            }
            fw.close();
            //Create a empty marker file to signal the other programm that writing the file is finished
            File finishFile = new File(filePath + ".finished");
            finishFile.createNewFile();
            result = true;
        } catch (IOException ex) {
            System.err.println("Something went wrong with the output file for NP-File");
        }
        return result;
    }

    public static String[] combineValueArrays(List<String[]> arrays) {
        String[] result = new String[0];
        Iterator<String[]> iterator = arrays.iterator();
        int resultLength = result.length;
        while (iterator.hasNext()) {
            result = ArrayUtils.addAll(result, iterator.next());
        }
        return result;
    }

    public static boolean cleanDirectory(String pathToDir) {
        boolean isClean = false;
        File dir = new File(pathToDir);
        try {
            FileUtils.cleanDirectory(dir);
            isClean = true;
        } catch (IOException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isClean;
    }

    //SELECT NP, NpTimestamp, npOccurenceInArticle
    public static List<ArchivedNP> convertResultSetToNPList(ResultSet rs) throws SQLException {
        List<ArchivedNP> npLs = new ArrayList<>();
        while (rs.next()) {
            String npString = rs.getString(1);
            long npTimestamp = rs.getLong(2);
            int npOccurrenceInArticle = rs.getInt(3);
            ArchivedNP np = new ArchivedNP(npString, npTimestamp, npOccurrenceInArticle);
            npLs.add(np);
        }
        //Collections.sort(npLs, new TimestampComparator());
        return npLs;
    }

    public static boolean moveFilesToAnotherDir(String pathMoveFrom, String pathMoveTo) {
        boolean moved = false;
        File originDir = new File(pathMoveFrom); // caution: this should be a directory
        File destinationDir = new File(pathMoveTo);
        Path destinationPath = destinationDir.toPath();
        if (originDir.isDirectory()) {
            File[] content = originDir.listFiles();
            for (int i = 0; i < content.length; i++) {
                Path originFile = content[i].toPath();
                Path dest = new File(destinationDir, content[i].getName()).toPath();
                try {
                    Files.move(originFile, dest, REPLACE_EXISTING);
                    moved = true;
                } catch (IOException ex) {
                    moved = false;
                    System.err.println("Could not move the file " + originFile + " to " + dest);
                }
            }
        }
        return moved;
    }

    public static void writeTXTFile(String data, String path) {
        try {
            FileWriter fw = new FileWriter(new File(path));
            fw.write(data);
            fw.flush();
            fw.close();
        } catch (IOException ex) {
            System.out.println("No file was written. Some error occured.");
        }
    }

    /**
     * Sorts a given set of noun phrases and returns the first and last timestamp of it.
     * 
     * Array: [0] = first occurred NP; [1] = last occurred NP
     * 
     * @param setOfNPs A set of noun phrases.
     * @return A array with both timestamp and the aboved coding.
     */
    public static DateTime[] getFirstAndLastNPTimestamp(Set<EnhancedNP> setOfNPs) {
        DateTime[] result = new DateTime[2];
        LinkedList<EnhancedNP> arrayList = new LinkedList<>(setOfNPs);
        Collections.sort(arrayList, new EnhancedNPTimeComperator());
        result[0] = arrayList.getFirst().getTimestamp(); // first occurred NP in list
        result[1] = arrayList.getLast().getTimestamp(); // last occurred NP in list
        return result;
    }

    public static String simpleStopwordRemoval(HashSet<String> stopWords, String s) {
        String[] splitted = s.split(" ");
        for (int i = 0; i < splitted.length; i++) {
            String s_i = splitted[i];
            if (stopWords.contains(s_i.toLowerCase())) { // case insensitive matching
                s = s.replace(s_i, "");
                s = s.trim();
            }
        }
        s.replace("  ", " "); // replace two whitespaces with one whitespace
        return s.trim();
    }

    public static String toFirstCapitalize(String s) {
        s = s.toLowerCase();
        return StringUtils.capitalize(s);
    }

}
