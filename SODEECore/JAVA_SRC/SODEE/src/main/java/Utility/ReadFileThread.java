
package Utility;

import java.util.ArrayList;

/**
 *
 * @author Johannes Reiss
 */
public class ReadFileThread extends Thread{
    
    private String filePath;
    private ArrayList<String> fileContent = new ArrayList<>();

    public ReadFileThread(String filePath) {
        this.filePath = filePath;
    }

    public ArrayList<String> getFileContent() {
        return fileContent;
    }
    

    @Override
    public void run() {
        this.fileContent.addAll(Helper.readFileLinesAllLowerCase(filePath));
        System.out.println("Read lines of " + filePath + " : " + this.fileContent.size());
    }
    
}
