/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package Utility;

import RBBNPE.ArchivedNP;
import java.util.Comparator;

/**
 *
 * @author johan
 */
public class TimestampComparator implements Comparator<ArchivedNP>{

    @Override
    public int compare(ArchivedNP t, ArchivedNP t1) {
        return Long.compare(t.getUnixTime(), t1.getUnixTime());
    }

       
    
}
