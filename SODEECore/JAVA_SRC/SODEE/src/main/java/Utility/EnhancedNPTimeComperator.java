/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package Utility;

import RBBNPE.EnhancedNP;
import java.util.Comparator;

/**
 * This class implements a comparator for the timestamps of the noun phrases,
 * which is used to sort the collections containing nps.
 * 
 * @author Johannes Reiss
 */
public class EnhancedNPTimeComperator implements Comparator<EnhancedNP> {

    @Override
    public int compare(EnhancedNP t, EnhancedNP t1) {
        return t.getTimestamp().compareTo(t1.getTimestamp());
    }

}
