package Utility;



import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class DateUtility {
	public synchronized static Date floorToDay(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	public synchronized static Date addDays(Date d, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DAY_OF_MONTH, days);
		return c.getTime();
	}

	public synchronized static int diffDays(Date d1, Date d2) {
		return (int) ((d1.getTime() - d2.getTime()) / (24 * 3600 * 1000));
	}
	
	public synchronized static Date floorToHour(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return new Timestamp(c.getTime().getTime());
	}
}
