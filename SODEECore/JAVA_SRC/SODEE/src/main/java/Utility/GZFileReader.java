/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package Utility;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;

/**
 * This class contains methods to read gz-files from a given path concurrently.
 * 
 * @author Johannes Reiss
 */
public class GZFileReader implements Runnable {
    
    private String gzFilePath;
    private HashSet<String> fileContent;

    public GZFileReader(String gzFilePath) {
        this.gzFilePath = gzFilePath;
    }

    public void setGzFilePath(String gzFilePath) {
        this.gzFilePath = gzFilePath;
    }

    public HashSet<String> getFileContent() {
        return fileContent;
    }

    @Override
    public void run() {
        System.out.println("Start reading " + this.gzFilePath + "...");
        HashSet<String> output = new HashSet<>();
        String line;
        GZIPInputStream fileInput = null;
        int count = 0;
        try {
            fileInput = new GZIPInputStream(new FileInputStream(this.gzFilePath));
        } catch (FileNotFoundException ex) {
            System.err.println("Something went wrong while reading file: "
                    + this.gzFilePath);
        } catch (IOException ex) {
            System.err.println("Something went wrong while reading file: "
                    + this.gzFilePath);
        }
        InputStreamReader isr = new InputStreamReader(fileInput, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(isr);
        try {
            while ((line = br.readLine()) != null) {
                output.add(line.trim());
            }
        } catch (IOException ex) {
            System.err.println("Something went wrong on reading a line of file.");
        }
        try {
            br.close();
        } catch (IOException ex) {
            System.err.println("Something went wrong on closing the buff.reader");
        }
        this.fileContent = output;
        System.out.println("End reading " + this.gzFilePath);
    }
    
}
