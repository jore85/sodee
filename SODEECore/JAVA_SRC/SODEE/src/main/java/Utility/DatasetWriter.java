package Utility;

import Config.Constants;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 *
 * @author jore
 */
public class DatasetWriter extends Thread {
    
    private final String outputFolder;
    private final Character seperator;
    private CSVWriter csvW;
    
    private String fileName;
    
    //private String[] head = {"NP","articleID","Timestamp","Named Entity Class","PoS-Tag","isAllCaps","containsDigitAndAlpha","containsNonAlpha","endsWithPeriod","firstCapital","firstLetterCapitalized","containsRealLetter","hasInternalApostrophe","hasInternalPeriod","internalCaps","isHyphenated","npPositionInArticle","articleCoverage","titleContainsNP","npOccurenceInArticle","retrievedDate","publishedDate","title","articleURI","country","feedTitle","dayOfWeek","lastFullHour","wikiEntity","hasRedLink","pageView24hExist","pageView24hSum","pageViewDays7d","pageViewSum7d","pageViewDays14d","pageViewDays7dMin100","pageViewDays14dMin100","pageViewSlope24hSlope","pageViewSlope24hIntercept","pageViewSlope24hRSquare","pageViewSlope24hSlopeStdErr","pageViewSlope24hInterceptStdErr","pageViewSlope14dSlope","pageViewSlope14dIntercept","pageViewSlope14dRSquare","pageViewSlope14dSlopeStdErr","pageViewSlope14dInterceptStdErr","googleNgramFrequency","googleNgramSlope","googleNgramR2","usageSinceYear","matchCount1899","percentProperCaps","mention","wikiTitle","wikiURI","annotationWeight","noveltyClass","timeToInsertion", "TargetLabel"};
    private List<String[]> values; //List: Lines; Array: Values of a line
    
    public DatasetWriter(String outputFolder) {
        this.outputFolder = outputFolder;
        this.seperator = '\t';        
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public void setValues(List<String[]> values) {
        this.values = values;
    }
    
    @Override
    public void run() {
        String filePath = "";
        if (this.fileName != null) {
            filePath = this.outputFolder + this.fileName;
        }
        try {
            this.csvW = new CSVWriter(new FileWriter(filePath), this.seperator);
            System.out.println("Dataset will be writen to file " + filePath + "\t" + DateTime.now(DateTimeZone.UTC));
            this.csvW.writeNext(Constants.HEAD);
            this.csvW.writeAll(values);
            this.csvW.close();
            System.out.println("Dataset was writen to file " + filePath + "\t" + DateTime.now(DateTimeZone.UTC));
        } catch (IOException ex) {
            System.err.println("Something went wring on writing a csv-File of Data."
                    + "The file was not found: " + filePath);
        }
        
    }
    
     
    
}
