/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabasesNetwork;

import Config.Credentials;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jore
 */
public class PageViewDatabaseConnector {
    
    private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final String DB_NAME;
    private final String DB_SERVER;
    private final String DB_URL;
    private final String DB_USER;
    private final String DB_PWD;

    private Connection conn;

    public PageViewDatabaseConnector() {
        this.DB_NAME = "NEDRONAA4S";
        this.DB_SERVER = "aifb-ls3-maia.aifb.kit.edu";
        this.DB_URL = "jdbc:mysql://" + DB_SERVER + ":3306/" + DB_NAME + "?rewriteBatchedStatements=true";
        this.DB_USER = Credentials.PAGEVIEW_DB_USER;
        this.DB_PWD = Credentials.PAGEVIEW_DB_PWD;
        
        try {
            //System.err.println("Pageview-DB: " + this.DB_URL);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            try {
                this.conn = DriverManager.getConnection(this.DB_URL, this.DB_USER, 
                        this.DB_PWD);
            } catch (SQLException ex) {
                Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConn() {
        return conn;
    }
    
}
