/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package DatabasesNetwork;

import Classifier.NPLabeler;
import Config.Credentials;
import Config.SODEEConfig;
import FeatureExtraction.AnnotationFeatures;
import FeatureExtraction.ArticleFeatures;
import FeatureExtraction.CombinedArticleNPFeatures;
import FeatureExtraction.NgramFeatures;
import FeatureExtraction.PeriodicalFeatures.PeriodicalFeatures;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.EnhancedNP;
import Utility.Helper;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * This class manages all requests on the database.
 *
 * @author Johannes Reiss
 */
public class AllNPFeaturesDBConnector implements Runnable {

    private final String JDBC_DRIVER;
    private final String DB_NAME;
    private final String DB_SERVER;
    private final String DB_URL;
    private final String DB_USER;
    private final String DB_PWD;
    private final String SQLITE_PATH;
    private String SQLITE_DB_URL;
    private String SQLITE_DB_FILENAME;
    private Connection conn;
    private PreparedStatement statementsAllNPsDB = null;

    NPLabeler labeler;

    private HashSet<EnhancedNP> nps;
    private HashMap<EnhancedNP, AnnotationFeatures> annotationFeaturesMap;
    private HashMap<EnhancedNP, WikipediaFeatures> wikiFeaturesMap;
    private HashMap<Integer, ArticleFeatures> articlesFeaturesMap;
    private HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeaturesMap;
    private HashMap<EnhancedNP, NgramFeatures> ngramFeaturesMap;

    private HashSet<EnhancedNP> npsNew = new HashSet<>();
    private final String DB_PORT;

    /**
     * Constructs a database connector with the credentials.
     * 
     * @param hasMySQL Must be "true" if a mysql db should be used by the system.
     */     
    public AllNPFeaturesDBConnector(boolean hasMySQL) {
        this.JDBC_DRIVER = "com.mysql.jdbc.Driver";
        this.DB_NAME = SODEEConfig.DB_NAME;
        this.DB_SERVER = SODEEConfig.DB_SERVER;
        this.DB_PORT = SODEEConfig.DB_PORT;
        this.DB_URL = "jdbc:mysql://" + DB_SERVER + ":" + DB_PORT + "/" + DB_NAME + "?rewriteBatchedStatements=true";
        this.DB_USER = Credentials.ARTICEL_NP_DB_USER;
        this.DB_PWD = Credentials.ARTICEL_NP_DB_PWD;
        this.SQLITE_PATH = SODEEConfig.sqlite_DB;
        this.SQLITE_DB_FILENAME = SODEEConfig.SQLITE_DB_FILENAME;

        if (hasMySQL) {
            System.err.println("All NP Features DB: " + this.DB_URL);
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                try {
                    this.conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PWD);
                    if (this.conn.isValid(0)) {
                        System.out.println("The Database Connection for all NPs Features is valid"
                                + "\t" + DateTime.now(DateTimeZone.UTC));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                this.SQLITE_DB_URL = "jdbc:sqlite:" + this.SQLITE_PATH + this.SQLITE_DB_FILENAME;
                System.out.println("SQLite Path: " + this.SQLITE_DB_URL);
                Class.forName("org.sqlite.JDBC").newInstance();
                try {
                    this.conn = DriverManager.getConnection(SQLITE_DB_URL);
                    if (this.conn.isValid(0)) {
                        System.out.println("The SQLITE Database Connection for all NPs Features is valid"
                                + "\t" + DateTime.now(DateTimeZone.UTC));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public HashSet<EnhancedNP> getNpsNew() {
        return npsNew;
    }

    public void setNps(HashSet<EnhancedNP> nps) {
        this.nps = nps;
    }

    public void setAnnotationFeaturesMap(HashMap<EnhancedNP, AnnotationFeatures> annotationFeaturesMap) {
        this.annotationFeaturesMap = annotationFeaturesMap;
    }

    public void setWikiFeaturesMap(HashMap<EnhancedNP, WikipediaFeatures> wikiFeaturesMap) {
        this.wikiFeaturesMap = wikiFeaturesMap;
    }

    public void setArticlesFeaturesMap(HashMap<Integer, ArticleFeatures> articlesFeaturesMap) {
        this.articlesFeaturesMap = articlesFeaturesMap;
    }

    public void setCombinedFeaturesMap(HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeaturesMap) {
        this.combinedFeaturesMap = combinedFeaturesMap;
    }

    public void setNgramFeaturesMap(HashMap<EnhancedNP, NgramFeatures> ngramFeaturesMap) {
        this.ngramFeaturesMap = ngramFeaturesMap;
    }

    public void setLabeler(NPLabeler labeler) {
        this.labeler = labeler;
    }

    /**
     * This method runs the np insertion. First the completeness of the retrieved
     * features is checked and after that, the nps are inserted into the db.
     * 
     */
    @Override
    public void run() {

        System.out.println("Start inserting the " + nps.size() + " nps into a database: " + this.DB_NAME
                + "\t" + DateTime.now(DateTimeZone.UTC));

        String stmnt = "INSERT INTO allNPFeatures (NP, articleID, NPTimestamp, Named_Entity_Class, PoS, isAllCaps, containsDigitAndAlpha, containsNonAlpha, endsWithPeriod, firstCapital, firstLetterCapitalized, containsRealLetter, hasInternalApostrophe, hasInternalPeriod, internalCaps, isHyphenated, npPositionInArticle, articleCoverage, titleContainsNP, npOccurenceInArticle, retrievedDate, publishedDate, title, articleURI, country, feedTitle, dayOfWeek, lastFullHour, wikiEntity, hasRedLink, pageView24hExist, pageView24hSum, pageViewDays7d, pageViewSum7d, pageViewDays14d, pageViewDays7dMin100, pageViewDays14dMin100, pageViewSlope24hSlope, pageViewSlope24hIntercept,  pageViewSlope24hRSquare, pageViewSlope24hSlopeStdErr, pageViewSlope24hInterceptStdErr, pageViewSlope14dSlope, pageViewSlope14dIntercept, pageViewSlope14dRSquare, pageViewSlope14dSlopeStdErr, pageViewSlope14dInterceptStdErr, googleNgramFrequency, googleNgramSlope, googleNgramR2, usageSinceYear, matchCount1899, percentProperCaps, npOccurrenceNo1h, npOccurrenceNo24h, npOccurrenceSlope24hSlope, npOccurrenceSlope24hIntercept, npOccurrenceSlope24hRSquare, npOccurrenceSlope24hSlopeStdErr, npOccurrenceSlope24hInterceptStdErr, mention, wikiTitle, wikiURI, annotationWeight, noveltyClass, timeToInsertion, class) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

        try {
            this.statementsAllNPsDB = conn.prepareStatement(stmnt);
            Iterator<EnhancedNP> iterator = nps.iterator();
            int i = 0;
            int j = 0;
            int npsSize = nps.size();
            while (iterator.hasNext()) {
                EnhancedNP np = iterator.next();
                int id = np.getArticleID();
                int completeChecker = 0;

                //System.out.println("NP: " + np.getNp());
                HashMap<EnhancedNP, CombinedArticleNPFeatures> combined = this.combinedFeaturesMap.get(id);
                //System.out.println("Combined Features Size: " + combined.size());
                //Check wether a np has a complete feature set (= the java objects for that np are not NULL)
                if (combined.containsKey(np)) {
                    completeChecker++;
                }

                if (this.articlesFeaturesMap.containsKey(id)) {
                    completeChecker++;
                }

                if (this.wikiFeaturesMap.containsKey(np)) {
                    completeChecker++;
                }

                if (this.ngramFeaturesMap.containsKey(np)) {
                    completeChecker++;
                }

                if (this.annotationFeaturesMap.containsKey(np)) {
                    completeChecker++;
                }

                //System.out.println("Complete Checker: " + completeChecker);
                if (completeChecker == 5) { // == 5 for a complete dataset, else the csv is not correct formatted                    

                    //System.out.println("is in Combined Features Map");
                    CombinedArticleNPFeatures combinedFeature = combined.get(np);
                    //System.out.println(combinedFeature.toString());

                    //System.out.println("is in Article Features Map");
                    ArticleFeatures articleFeat = this.articlesFeaturesMap.get(id);
                    //System.out.println(articleFeat.toString());

                    //System.out.println("is in Wiki Features Map");
                    WikipediaFeatures wikiFeat = this.wikiFeaturesMap.get(np);
                    //System.out.println(wikiFeat.toString());

                    //System.out.println("is in Ngram Features Map");
                    NgramFeatures ngramFeat = this.ngramFeaturesMap.get(np);
                    //System.out.println(ngramFeat.toString());
                    //System.out.println("is in Annotation Features Map");
                    AnnotationFeatures annotFeat = this.annotationFeaturesMap.get(np);
                    //System.out.println(annotFeat.toString());

                    //Labeling (if enabled)
                    np = labeler.addLabel(np, annotFeat);

                    npsNew.add(np);

                    j++;
                    try {
                        statementsAllNPsDB = insertAllNPFeaturesIntoDB(np, combinedFeature, articleFeat, wikiFeat, ngramFeat, annotFeat, statementsAllNPsDB);
                        statementsAllNPsDB.addBatch();
                    } catch (SQLException ex) {
                        Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
                        //System.err.println("Something went wrong on write the data row " + i + " into the database. There will be some data lost.");
                    }
                }
                i++;
                if (((i % 10000) == 0) || i == nps.size()) { // insert into database every 1000 nps
                    int[] executeBatch = statementsAllNPsDB.executeBatch();
                    System.out.println(j + " rows were added out of " + nps.size() + " existing nps." + "\t" + DateTime.now(DateTimeZone.UTC)); // \nReturns: " + executeBatch.length);                    
                }
            }
            System.out.println("End inserting the " + j + " nps into a database: " + this.conn.getMetaData().getURL()
                    + "\t" + DateTime.now(DateTimeZone.UTC));
            //System.out.println("Create a index on the DB." + "\t" + DateTime.now(DateTimeZone.UTC));
            //this.statementsAllNPsDB = conn.prepareStatement("CREATE INDEX `dateIdx` ON `allNPFeatures` (`NP` ,`NPTimestamp` )");
            //this.statementsAllNPsDB.execute();
        } catch (SQLException ex) {
            //System.err.println("Something went wrong on access the database: " +  this.conn.getMetaData().getURL() + "\n");
            //ex.printStackTrace();
            Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Because the db driver could not deal with "NULL" objects it will checked for existence and if necessary a 0.0 is inserted
    public static PreparedStatement insertAllNPFeaturesIntoDB(EnhancedNP np, CombinedArticleNPFeatures combinedFeature,
            ArticleFeatures articleFeat, WikipediaFeatures wikiFeat, NgramFeatures ngramFeat,
            AnnotationFeatures annotFeat, PreparedStatement stmnt) throws SQLException {
        stmnt.setString(1, np.getNp().getPhraseString()); //NP VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
        stmnt.setInt(2, np.getArticleID()); //articleID INT(11) NOT NULL,
        stmnt.setTimestamp(3, new Timestamp(np.getTimestamp().getMillis())); //Timestamp DATETIME, //for sqlite millis / 1000
        stmnt.setString(4, np.getNeClass()); //Named_Entity_Class VARCHAR(13),
        stmnt.setString(5, np.getPosTag()); //PoS VARCHAR(13),
        stmnt.setBoolean(6, np.isAllCaps()); //isAllCaps BOOLEAN,
        stmnt.setBoolean(7, np.isContainsDigitAndAlpha()); //containsDigitAndAlpha BOOLEAN,
        stmnt.setBoolean(8, np.isContainsNonAlpha()); //containsNonAlpha BOOLEAN,
        stmnt.setBoolean(9, np.isEndsWithPeriod()); //endsWithPeriod BOOLEAN,
        stmnt.setBoolean(10, np.isFirstCapital()); //firstCapital BOOLEAN,
        stmnt.setBoolean(11, np.isFirstLetterCapitalized()); //firstLetterCapitalized BOOLEAN,
        stmnt.setBoolean(12, np.isContainsRealLetter()); //containsRealLetter BOOLEAN,
        stmnt.setBoolean(13, np.isHasInternalApostrophe()); //hasInternalApostrophe BOOLEAN,
        stmnt.setBoolean(14, np.isHasInternalPeriod()); //hasInternalPeriod BOOLEAN,
        stmnt.setBoolean(15, np.isInternalCaps());//internalCaps BOOLEAN,
        stmnt.setBoolean(16, np.isIsHyphenated());//isHyphenated BOOLEAN,
        stmnt.setDouble(17, combinedFeature.getNpPositionInArticle());//npPositionInArticle DOUBLE,
        stmnt.setDouble(18, combinedFeature.getArticleCoverage()); //articleCoverage DOUBLE,
        stmnt.setBoolean(19, combinedFeature.isTitleContainsNP()); //titleContainsNP BOOLEAN,
        stmnt.setInt(20, combinedFeature.getNpOccurenceInArticle()); //npOccurenceInArticle INTEGER,
        stmnt.setTimestamp(21, new Timestamp(articleFeat.getRetrievedDate().getMillis()));//retrievedDate DATETIME,
        stmnt.setTimestamp(22, new Timestamp(articleFeat.getPublishedDate().getMillis()));//publishedDate DATETIME,
        stmnt.setString(23, articleFeat.getTitle()); //title VARCHAR(255),
        stmnt.setString(24, articleFeat.getArticleURI()); //articleURI VARCHAR(255),
        stmnt.setString(25, articleFeat.getCountry()); //country VARCHAR(255),
        stmnt.setString(26, articleFeat.getFeedTitle()); //feedTitle VARCHAR(255),
        stmnt.setInt(27, articleFeat.getDayOfWeek()); //dayOfWeek INTEGER,
        stmnt.setInt(28, articleFeat.getLastFullHour());//lastFullHour INTEGER,
        stmnt.setString(29, wikiFeat.getWikiEntity()); //wikiEntity VARCHAR(255),
        stmnt.setBoolean(30, wikiFeat.isHasRedLink()); //hasRedLink BOOLEAN,
        stmnt.setBoolean(31, wikiFeat.isPageView24hExist()); //pageView24hExist BOOLEAN,
        stmnt.setInt(32, wikiFeat.getPageView24hSum());//pageView24hSum INTEGER,
        stmnt.setLong(33, wikiFeat.getPageViewDays7d()); //pageViewDays7d BIGINT,
        stmnt.setLong(34, wikiFeat.getPageViewSum7d()); //pageViewSum7d BIGINT,
        stmnt.setLong(35, wikiFeat.getPageViewDays14d()); //pageViewDays14d BIGINT,
        stmnt.setLong(36, wikiFeat.getPageViewDays7dMin100()); //pageViewDays7dMin100 BIGINT,
        stmnt.setLong(37, wikiFeat.getPageViewDays14dMin100());//pageViewDays14dMin100 BIGINT,
        if (!Double.isInfinite(wikiFeat.getPageViewSlope24hSlope()) && !Double.isNaN(wikiFeat.getPageViewSlope24hSlope())) {
            stmnt.setDouble(38, wikiFeat.getPageViewSlope24hSlope()); //pageViewSlope24hSlope DOUBLE,
        } else {
            stmnt.setDouble(38, 0.0); //pageViewSlope24hSlope DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope24hIntercept()) && !Double.isNaN(wikiFeat.getPageViewSlope24hIntercept())) {
            stmnt.setDouble(39, wikiFeat.getPageViewSlope24hIntercept());//pageViewSlope24hIntercept DOUBLE,
        } else {
            stmnt.setDouble(39, 0.0);//pageViewSlope24hIntercept DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope24hRSquare()) && !Double.isNaN(wikiFeat.getPageViewSlope24hRSquare())) {
            stmnt.setDouble(40, wikiFeat.getPageViewSlope24hRSquare()); //pageViewSlope24hRSquare DOUBLE,
        } else {
            stmnt.setDouble(40, 0.0); //pageViewSlope24hRSquare DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope24hSlopeStdErr()) && !Double.isNaN(wikiFeat.getPageViewSlope24hSlopeStdErr())) {
            stmnt.setDouble(41, wikiFeat.getPageViewSlope24hSlopeStdErr()); //pageViewSlope24hSlopeStdErr DOUBLE,
        } else {
            stmnt.setDouble(41, 0.0); //pageViewSlope24hSlopeStdErr DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope24hInterceptStdErr()) && !Double.isNaN(wikiFeat.getPageViewSlope24hInterceptStdErr())) {
            stmnt.setDouble(42, wikiFeat.getPageViewSlope24hInterceptStdErr()); //pageViewSlope24hInterceptStdErr DOUBLE,
        } else {
            stmnt.setDouble(42, 0.0); //pageViewSlope24hInterceptStdErr DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope14dSlope()) && !Double.isNaN(wikiFeat.getPageViewSlope14dSlope())) {
            stmnt.setDouble(43, wikiFeat.getPageViewSlope14dSlope()); //pageViewSlope14dSlope DOUBLE,
        } else {
            stmnt.setDouble(43, 0.0); //pageViewSlope24hInterceptStdErr DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope14dIntercept()) && !Double.isNaN(wikiFeat.getPageViewSlope14dIntercept())) {
            stmnt.setDouble(44, wikiFeat.getPageViewSlope14dIntercept()); //pageViewSlope14dIntercept DOUBLE,
        } else {
            stmnt.setDouble(44, 0.0); //pageViewSlope24hInterceptStdErr DOUBLE,
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope14dRSquare()) && !Double.isNaN(wikiFeat.getPageViewSlope14dRSquare())) {
            stmnt.setDouble(45, wikiFeat.getPageViewSlope14dRSquare()); //pageViewSlope14dRSquare DOUBLE,
        } else {
            stmnt.setDouble(45, 0.0);
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope14dRSquare()) && !Double.isNaN(wikiFeat.getPageViewSlope14dRSquare())) {
            stmnt.setDouble(46, wikiFeat.getPageViewSlope14dRSquare()); //pageViewSlope14dSlopeStdErr DOUBLE,
        } else {
            stmnt.setDouble(46, 0.0);
        }
        if (!Double.isInfinite(wikiFeat.getPageViewSlope24hInterceptStdErr()) && !Double.isNaN(wikiFeat.getPageViewSlope24hInterceptStdErr())) {
            stmnt.setDouble(47, wikiFeat.getPageViewSlope24hInterceptStdErr());//pageViewSlope14dInterceptStdErr DOUBLE,
        } else {
            stmnt.setDouble(47, 0.0);
        }
        /*
         stmnt.setNull(48, java.sql.Types.BIGINT);
         stmnt.setNull(49, java.sql.Types.DOUBLE);
         stmnt.setNull(50, java.sql.Types.DOUBLE);
         stmnt.setNull(51, java.sql.Types.INTEGER);
         stmnt.setNull(52, java.sql.Types.BIGINT);
         stmnt.setNull(53, java.sql.Types.DOUBLE);
         */

        stmnt.setLong(48, ngramFeat.getGoogleNgramFrequency()); //googleNgramFrequency BIGINT,
        if (!Double.isInfinite(ngramFeat.getGoogleNgramSlope()) && !Double.isNaN(ngramFeat.getGoogleNgramSlope())) {
            stmnt.setDouble(49, ngramFeat.getGoogleNgramSlope()); //googleNgramSlope DOUBLE,
        } else {
            stmnt.setDouble(49, 0.0);
        }
        if (!Double.isInfinite(ngramFeat.getGoogleNgramR2()) && !Double.isNaN(ngramFeat.getGoogleNgramR2())) {
            stmnt.setDouble(50, ngramFeat.getGoogleNgramR2()); //googleNgramR2 DOUBLE,
        } else {
            stmnt.setDouble(50, 0.0);
        }
        stmnt.setInt(51, ngramFeat.getUsageSinceYear()); //usageSinceYear INTEGER,
        stmnt.setLong(52, ngramFeat.getMatchCount1899());//matchCount1899 BIGINT,
        if (!Double.isInfinite(ngramFeat.getPercentProperCaps()) && !Double.isNaN(ngramFeat.getPercentProperCaps())) {
            stmnt.setDouble(53, ngramFeat.getPercentProperCaps());//percentProperCaps DOUBLE,
        } else {
            stmnt.setDouble(53, 0.0);
        }
        stmnt.setNull(54, java.sql.Types.INTEGER);//npOccurrenceNo1h INTEGER,
        stmnt.setNull(55, java.sql.Types.INTEGER);//npOccurrenceNo24h INTEGER,
        stmnt.setNull(56, java.sql.Types.DOUBLE);//npOccurrenceSlope24hSlope DOUBLE,
        stmnt.setNull(57, java.sql.Types.DOUBLE); //npOccurrenceSlope24hIntercept DOUBLE,
        stmnt.setNull(58, java.sql.Types.DOUBLE);//npOccurrenceSlope24hRSquare DOUBLE,
        stmnt.setNull(59, java.sql.Types.DOUBLE);//npOccurrenceSlope24hSlopeStdErr DOUBLE,
        stmnt.setNull(60, java.sql.Types.DOUBLE); //npOccurrenceSlope24hInterceptStdErr DOUBLE,
        stmnt.setString(61, annotFeat.getMention()); //mention VARCHAR(255),
        stmnt.setString(62, annotFeat.getWikiTitle()); //wikiTitle VARCHAR(255),
        stmnt.setString(63, annotFeat.getWikiURI()); //wikiURI VARCHAR(255),
        if (!Double.isInfinite(annotFeat.getAnnotationWeight()) && !Double.isNaN(annotFeat.getAnnotationWeight())) {
            stmnt.setDouble(64, annotFeat.getAnnotationWeight()); //annotationWeight DOUBLE,
        } else {
            stmnt.setDouble(64, 0.0);
        }
        stmnt.setInt(65, annotFeat.getNoveltyClass()); //noveltyClass INTEGER,
        stmnt.setLong(66, annotFeat.getTimeToInsertion()); //timeToInsertion BIGINT,
        if (np.getTargetLabel() != null) {
            stmnt.setBoolean(67, np.getTargetLabel()); //targetLabel BOOLEAN);   
        } else {
            stmnt.setNull(67, java.sql.Types.BOOLEAN);
        }

        return stmnt;
    }

    //For periodical features (unused)
    public Map<EnhancedNP, List<String[]>> queryNPsWithTime(Set<EnhancedNP> nps) { // t_24h = t1 - 24h
        Map<EnhancedNP, List<String[]>> result = new HashMap<>();
        String stmntString = "SELECT rowID, NP, NPTimestamp, npOccurenceInArticle FROM allNPFeatures WHERE (NP = ?)  AND (NPTimestamp BETWEEN ? AND ?)";
        Iterator<EnhancedNP> iterator = nps.iterator();
        while (iterator.hasNext()) {
            try {
                List<String[]> npResults = new ArrayList<>(nps.size());
                EnhancedNP next = iterator.next();
                long t_1 = next.getTimestamp().getMillis();
                long t_24h = t_1 - (24 * 60 * 60 * 1000);
                PreparedStatement stmnt = this.conn.prepareStatement(stmntString);
                stmnt.setString(1, next.getNp().getPhraseString());
                stmnt.setLong(2, t_24h);
                stmnt.setLong(3, t_1);
                ResultSet executeQuery = stmnt.executeQuery();
                while (executeQuery.next()) {
                    String[] row = new String[4];
                    row[0] = String.valueOf(executeQuery.getInt("rowID"));
                    row[1] = executeQuery.getString("NP");
                    row[2] = String.valueOf(executeQuery.getTimestamp("NPTimestamp").getTime());
                    row[3] = String.valueOf(executeQuery.getInt("npOccurenceInArticle"));
                    npResults.add(row);
                }
                result.put(next, npResults);
            } catch (SQLException ex) {
                Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    /**
     * Get the noun phrases of the given timespan.
     * @param currentDay The UNIX-Timestamp of the current day at 00h:00m
     * @param dayBefore The UNIX-Timestamp of the day before at 00h:00m
     * @return A database result set with the result of the query
     */
    public ResultSet getNPsOfDay(long currentDay, long dayBefore) {
        ResultSet result = null;
        System.out.println("Current DayTime: " + currentDay);
        System.out.println("Day Before DayTime (lower bound): " + dayBefore);
        String stmntString = "SELECT NP, NPTimestamp, npOccurenceInArticle "
                + "FROM allNPFeatures "
                + "WHERE (NPTimestamp >= from_unixtime(?) AND NPTimestamp <= from_unixtime(?) ) "
                + "ORDER BY NPTimestamp";
        System.out.println(stmntString);
        try {
            PreparedStatement stmnt = this.conn.prepareStatement(stmntString);
            stmnt.setLong(1, dayBefore);
            stmnt.setLong(2, currentDay);
            result = stmnt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public List<String[]> queryNPWithTime(String npString, long t_1) throws SQLException {
        List<String[]> result = new ArrayList<>();
        String stmntString = "SELECT rowID, NP, NPTimestamp, npOccurenceInArticle "
                + "FROM allNPFeatures WHERE (NP = ?)  AND (NPTimestamp >= from_unixtime(?) AND NPTimestamp <= from_unixtime(?))";
        PreparedStatement stmnt = this.conn.prepareStatement(stmntString);
        long t_24h = t_1 - (24 * 60 * 60 * 1000);
        stmnt.setString(1, npString);
        stmnt.setLong(2, t_24h);
        stmnt.setLong(3, t_1);
        ResultSet executeQuery = stmnt.executeQuery();
        while (executeQuery.next()) {
            String[] row = new String[4];
            row[0] = String.valueOf(executeQuery.getInt("rowID"));
            row[1] = executeQuery.getString("NP");
            row[2] = String.valueOf(executeQuery.getTimestamp("NPTimestamp").getTime());
            row[3] = String.valueOf(executeQuery.getInt("npOccurenceInArticle"));
            result.add(row);
        }
        return result;
    }

    /**
     * Method to update the database respectively the corresponding noun phrases
     * with the retrieved periodical features.
     * 
     * @param rowID The row ID of the noun phrase.
     * @param periodicalFeats The retrieved periodical features.
     * @return True if the update was sucessful.
     * @throws SQLException If there went something wrong in the SQL UPDATE an
     * exception is thrown.
     */
    public boolean updateNPPeriodicalFeatures(int rowID, PeriodicalFeatures periodicalFeats) throws SQLException {
        boolean updated = false;
        String stmntString = "UPDATE allNPFeatures SET npOccurrenceNo1h = ?, npOccurrenceNo24h = ?, npOccurrenceSlope24hSlope = ?, "
                + "npOccurrenceSlope24hIntercept = ?, npOccurrenceSlope24hRSquare = ?, "
                + "npOccurrenceSlope24hSlopeStdErr = ?, npOccurrenceSlope24hInterceptStdErr = ? "
                + "WHERE rowID = ? ";
        System.out.println(periodicalFeats.toString());
        PreparedStatement stmnt = conn.prepareStatement(stmntString);
        stmnt.setInt(1, periodicalFeats.getNpOccurrenceNo1h());
        stmnt.setInt(2, periodicalFeats.getNpOccurrenceNo24h());
        stmnt.setDouble(3, periodicalFeats.getNpOccurrenceSlope24hSlope());
        stmnt.setDouble(4, periodicalFeats.getNpOccurrenceSlope24hIntercept());
        stmnt.setDouble(5, periodicalFeats.getNpOccurrenceSlope24hRSquare());
        stmnt.setDouble(6, periodicalFeats.getNpOccurrenceSlope24hSlopeStdErr());
        stmnt.setDouble(7, periodicalFeats.getNpOccurrenceSlope24hInterceptStdErr());
        stmnt.setInt(8, rowID);
        //stmnt.setTimestamp(9, new Timestamp(np.getTimestamp().getMillis()));
        stmnt.execute();
        return updated;
    }

    /**
     * 
     * @param periodicalFeatMap
     * @return 
     */
    public boolean updateNPsPeriodicalFeatures(Map<EnhancedNP, PeriodicalFeatures> periodicalFeatMap) {
        boolean updated = false;
        System.out.println("Start updating " + periodicalFeatMap.size() + " nps into a database: " + this.DB_NAME
                + "\t" + DateTime.now(DateTimeZone.UTC));

        String stmntString = "UPDATE allNPFeatures SET npOccurrenceNo1h = ?, npOccurrenceNo24h = ?, npOccurrenceSlope24hSlope = ?, "
                + "npOccurrenceSlope24hIntercept = ?, npOccurrenceSlope24hRSquare = ?, "
                + "npOccurrenceSlope24hSlopeStdErr = ?, npOccurrenceSlope24hInterceptStdErr = ? "
                + "WHERE NP = ? AND NPTimestamp = from_unixtime(?) ";

        try {
            PreparedStatement stmnt = conn.prepareStatement(stmntString);
            Iterator<Map.Entry<EnhancedNP, PeriodicalFeatures>> iterator = periodicalFeatMap.entrySet().iterator();
            int i = 0;
            while (iterator.hasNext()) {
                Map.Entry<EnhancedNP, PeriodicalFeatures> next = iterator.next();
                EnhancedNP np = next.getKey();
                //Timestamp timestmp = new Timestamp(np.getTimestamp().getMillis() / 1000);
                //System.out.println(timestmp + " || " + np.getTimestamp() + " || " + np.getTimestamp().getMillis() / 1000 );
                PeriodicalFeatures periodicalFeat = next.getValue();
                stmnt.setInt(1, periodicalFeat.getNpOccurrenceNo1h());
                stmnt.setInt(2, periodicalFeat.getNpOccurrenceNo24h());
                stmnt.setDouble(3, periodicalFeat.getNpOccurrenceSlope24hSlope());
                stmnt.setDouble(4, periodicalFeat.getNpOccurrenceSlope24hIntercept());
                stmnt.setDouble(5, periodicalFeat.getNpOccurrenceSlope24hRSquare());
                stmnt.setDouble(6, periodicalFeat.getNpOccurrenceSlope24hSlopeStdErr());
                stmnt.setDouble(7, periodicalFeat.getNpOccurrenceSlope24hInterceptStdErr());
                stmnt.setString(8, np.getNp().getPhraseString());
                stmnt.setLong(9, np.getTimestamp().getMillis() / 1000);
                stmnt.addBatch();
                i++;
                updated = false;
                if (((i % 10000) == 0) || i == periodicalFeatMap.size()) { // insert into database every 10000 nps
                    int[] executeBatch = stmnt.executeBatch();
                    System.out.println(i + " rows were updated out of " + periodicalFeatMap.size()
                            + " existing nps." + "\t" + DateTime.now(DateTimeZone.UTC)); // \nReturns: " + executeBatch.length); 
                    updated = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(AllNPFeaturesDBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updated;
    }

}
