/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package DatabasesNetwork;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author johan
 */
public class CreateNewSQlite {

    /**
     * Create a new SQlite3 DB for SODEE.
     *
     * @param fileName The file name of the database.
     */
    public static void createNewDatabase(String fileName) {

        String url = "jdbc:sqlite:H:/" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void createALLNPFeaturesTable(String fileName) {
        String url = "jdbc:sqlite:H:/" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                Statement stmnt = conn.createStatement();
                stmnt.execute("DROP TABLE allNPFeatures");                
                String createString = "CREATE TABLE allNPFeatures (\n"
                        + "NP VARCHAR(255) NOT NULL,\n"
                        + "articleID INT(11) NOT NULL,\n"
                        + "NPTimestamp DATETIME,\n"
                        + "Named_Entity_Class VARCHAR(13),\n"
                        + "PoS VARCHAR(13),\n"
                        + "isAllCaps BOOLEAN,\n"
                        + "containsDigitAndAlpha BOOLEAN,\n"
                        + "containsNonAlpha BOOLEAN,\n"
                        + "endsWithPeriod BOOLEAN,\n"
                        + "firstCapital BOOLEAN,\n"
                        + "firstLetterCapitalized BOOLEAN,\n"
                        + "containsRealLetter BOOLEAN,\n"
                        + "hasInternalApostrophe BOOLEAN,\n"
                        + "hasInternalPeriod BOOLEAN,\n"
                        + "internalCaps BOOLEAN,\n"
                        + "isHyphenated BOOLEAN,\n"
                        + "npPositionInArticle DOUBLE,\n"
                        + "articleCoverage DOUBLE,\n"
                        + "titleContainsNP BOOLEAN,\n"
                        + "npOccurenceInArticle INTEGER,\n"
                        + "retrievedDate DATETIME,\n"
                        + "publishedDate DATETIME,\n"
                        + "title VARCHAR(255),\n"
                        + "articleURI VARCHAR(255),\n"
                        + "country VARCHAR(255),\n"
                        + "feedTitle VARCHAR(255),\n"
                        + "dayOfWeek INTEGER,\n"
                        + "lastFullHour INTEGER,\n"
                        + "wikiEntity VARCHAR(255),\n"
                        + "hasRedLink BOOLEAN,\n"
                        + "pageView24hExist BOOLEAN,\n"
                        + "pageView24hSum INTEGER,\n"
                        + "pageViewDays7d BIGINT,\n"
                        + "pageViewSum7d BIGINT,\n"
                        + "pageViewDays14d BIGINT,\n"
                        + "pageViewDays7dMin100 BIGINT,\n"
                        + "pageViewDays14dMin100 BIGINT,\n"
                        + "pageViewSlope24hSlope DOUBLE,\n"
                        + "pageViewSlope24hIntercept DOUBLE,\n"
                        + "pageViewSlope24hRSquare DOUBLE,\n"
                        + "pageViewSlope24hSlopeStdErr DOUBLE,\n"
                        + "pageViewSlope24hInterceptStdErr DOUBLE,\n"
                        + "pageViewSlope14dSlope DOUBLE,\n"
                        + "pageViewSlope14dIntercept DOUBLE,\n"
                        + "pageViewSlope14dRSquare DOUBLE,\n"
                        + "pageViewSlope14dSlopeStdErr DOUBLE,\n"
                        + "pageViewSlope14dInterceptStdErr DOUBLE,\n"
                        + "googleNgramFrequency BIGINT,\n"
                        + "googleNgramSlope DOUBLE,\n"
                        + "googleNgramR2 DOUBLE,\n"
                        + "usageSinceYear INTEGER,\n"
                        + "matchCount1899 BIGINT,\n"
                        + "percentProperCaps DOUBLE,\n"
                        + "npOccurrenceNo1h INTEGER,\n"
                        + "npOccurrenceNo24h INTEGER,\n"
                        + "npOccurrenceSlope24hSlope DOUBLE,\n"
                        + "npOccurrenceSlope24hIntercept DOUBLE,\n"
                        + "npOccurrenceSlope24hRSquare DOUBLE,\n"
                        + "npOccurrenceSlope24hSlopeStdErr DOUBLE,\n"
                        + "npOccurrenceSlope24hInterceptStdErr DOUBLE,\n"
                        + "mention VARCHAR(255),\n"
                        + "wikiTitle VARCHAR(255),\n"
                        + "wikiURI VARCHAR(255),\n"
                        + "annotationWeight DOUBLE,\n"
                        + "noveltyClass INTEGER,\n"
                        + "timeToInsertion BIGINT,\n"
                        + "class BOOLEAN)";
                stmnt.executeUpdate(createString);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateNewSQlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //createNewDatabase("sodee.db");
        createALLNPFeaturesTable("sodee.db");

    }

}
