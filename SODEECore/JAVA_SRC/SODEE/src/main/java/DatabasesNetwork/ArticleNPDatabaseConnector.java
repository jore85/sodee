package DatabasesNetwork;

import Config.Constants;
import Config.Credentials;
import Config.SODEEConfig;
import RBBNPE.EnhancedNP;
import FeatureExtraction.CombinedArticleNPFeatures;
import FeatureExtraction.ArticleFeatures;
import Utility.Helper;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author johan
 */
public class ArticleNPDatabaseConnector implements Runnable {

    private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final String DB_NAME;
    private final String DB_SERVER;
    private final String DB_URL;
    private final String DB_USER;
    private final String DB_PWD;
    private final String DB_PORT;

    private Connection conn;

    private PreparedStatement statementArticleNPFeatures = null;

    private HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeatures;
    private HashMap<Integer, ArticleFeatures> articleFeatures;
    private HashSet<EnhancedNP> nps;

    public void setCombinedFeatures(HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeatures) {
        this.combinedFeatures = combinedFeatures;
    }

    public void setArticleFeatures(HashMap<Integer, ArticleFeatures> articleFeatures) {
        this.articleFeatures = articleFeatures;
    }

    public void setNps(HashSet<EnhancedNP> nps) {
        this.nps = nps;
    }

    public ArticleNPDatabaseConnector() {
        this.DB_NAME = SODEEConfig.DB_NAME;
        this.DB_SERVER = SODEEConfig.DB_SERVER;
        this.DB_PORT = SODEEConfig.DB_PORT;
        this.DB_URL = "jdbc:mysql://" + DB_SERVER + ":" + DB_PORT + "/" + DB_NAME + "?rewriteBatchedStatements=true";
        this.DB_USER = Credentials.ARTICEL_NP_DB_USER;
        this.DB_PWD = Credentials.ARTICEL_NP_DB_PWD;
        try {
            System.err.println("ArticleNP-DB: " + this.DB_URL);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            try {
                this.conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PWD);
            } catch (SQLException ex) {
                Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PreparedStatement insertArticleFeaturesIntoDB(EnhancedNP np, ArticleFeatures artclFeatures,
            CombinedArticleNPFeatures combinedFeatures, PreparedStatement stmnt) throws SQLException {
        stmnt.setString(1, np.getNp().getPhraseString());
        stmnt.setString(20, np.getNeClass());
        if (artclFeatures != null) {
            stmnt.setInt(2, artclFeatures.getArticleID());
            stmnt.setTimestamp(3, new Timestamp(artclFeatures.getRetrievedDate().getMillis()));
            stmnt.setTimestamp(4, new Timestamp(artclFeatures.getPublishedDate().getMillis()));
            stmnt.setString(5, artclFeatures.getTitle());
            stmnt.setString(6, artclFeatures.getArticleURI());
            stmnt.setString(7, artclFeatures.getCountry());
            stmnt.setString(8, artclFeatures.getFeedTitle());
            stmnt.setString(9, artclFeatures.getFeedURI());
            stmnt.setString(10, artclFeatures.getHostName());
            stmnt.setString(11, artclFeatures.getSourceName());
            stmnt.setInt(12, artclFeatures.getDayOfWeek());
            stmnt.setInt(13, artclFeatures.getLastFullHour());
            stmnt.setString(14, Helper.hashSetToCSString(artclFeatures.getSourceTags()));
            stmnt.setString(15, Helper.hashSetToCSString(artclFeatures.getTags()));
        }

        stmnt.setDouble(16, combinedFeatures.getNpPositionInArticle());
        stmnt.setDouble(17, combinedFeatures.getArticleCoverage());
        stmnt.setBoolean(18, combinedFeatures.isTitleContainsNP());
        stmnt.setInt(19, combinedFeatures.getNpOccurenceInArticle());

        return stmnt;
    }

    /*
     public void insertIntoDB(HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeatures,
     HashMap<Integer, ArticleFeatures> articleFeatures, HashSet<EnhancedNP> nps) {
    
     String stmnt = "INSERT articleNPFeatures (np, articleID, retrievedDate, publishedDate, "
     + "title, articleURI, country, feedTitle, feedURI, hostName, sourceName, "
     + "dayOfWeek, lastFullHour, sourceTags, tags, npPositionInArticle, articleCoverage, "
     + "titleContainsNP, npOccurenceInArticle, neClass) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
     try {
     this.statementArticleNPFeatures = conn.prepareStatement(stmnt);
     Iterator<EnhancedNP> iterator = nps.iterator();
     int i = 0;
     while (iterator.hasNext()) {
     EnhancedNP currentNP = iterator.next();
     int id = currentNP.getArticleID();
     HashMap<EnhancedNP, CombinedArticleNPFeatures> getCombiFeatures = combinedFeatures.get(id);
     CombinedArticleNPFeatures combiFeatures = getCombiFeatures.get(currentNP);
     ArticleFeatures artclFeatures = articleFeatures.get(id);
     statementArticleNPFeatures = insertArticleFeaturesIntoDB(currentNP, artclFeatures,
     combiFeatures, statementArticleNPFeatures);
     statementArticleNPFeatures.addBatch();
     if (i % 1000 == 0 || i == nps.size()) { // insert into database every 1000 nps
     statementArticleNPFeatures.executeBatch();
     }
     }
     } catch (SQLException ex) {
     Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
     }
    
     }
     */
    @Override
    public void run() {

        String stmnt = "INSERT articleNPFeatures (np, articleID, retrievedDate, publishedDate, "
                + "title, articleURI, country, feedTitle, feedURI, hostName, sourceName, "
                + "dayOfWeek, lastFullHour, sourceTags, tags, npPositionInArticle, articleCoverage, "
                + "titleContainsNP, npOccurenceInArticle, neClass) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            this.statementArticleNPFeatures = conn.prepareStatement(stmnt);
            Iterator<EnhancedNP> iterator = nps.iterator();
            int i = 0;
            while (iterator.hasNext()) {
                EnhancedNP currentNP = iterator.next();
                int id = currentNP.getArticleID();
                HashMap<EnhancedNP, CombinedArticleNPFeatures> getCombiFeatures = combinedFeatures.get(id);
                CombinedArticleNPFeatures combiFeatures = getCombiFeatures.get(currentNP);
                if (combiFeatures != null) {
                    ArticleFeatures artclFeatures = articleFeatures.get(id);
                    statementArticleNPFeatures = insertArticleFeaturesIntoDB(currentNP, artclFeatures,
                            combiFeatures, statementArticleNPFeatures);
                    statementArticleNPFeatures.addBatch();
                }

                i++;
                if (i % 1000 == 0 || i == nps.size()) { // insert into database every 1000 nps
                    statementArticleNPFeatures.executeBatch();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArticleNPDatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
