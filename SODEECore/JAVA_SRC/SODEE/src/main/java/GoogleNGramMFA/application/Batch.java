package GoogleNGramMFA.application;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import datastructure.CountContainer;
import datastructure.Pair;
import utilities.NGramUtility;

public class Batch extends Thread {

    private NGramUtility ngu;

    public static void main(String[] args) throws IOException {
        String pathToIndexTopDir = args[0];
        String pathToNPFile = args[1];
        Batch b = new Batch(pathToIndexTopDir);

        BufferedReader br = new BufferedReader(new FileReader(pathToNPFile));
        String sCurrentLine;
        StringBuffer sb = new StringBuffer("");
        int i = 0;
        while ((sCurrentLine = br.readLine()) != null) {
            i++;
            int length = sCurrentLine.split(" ").length;
            try {
                // 1st file
//			System.out.println(sCurrentLine + "\t" + b.getFrequency(sCurrentLine, length)
//					+ "\t" + b.getSlope(sCurrentLine, length, true)
//					+ "\t" + b.getR2(sCurrentLine, length, true)
//					+ "\t" + b.getUsageSinceYear(sCurrentLine, length, true)
//					+ "\t" + b.getPercentProperCaps(sCurrentLine, length));
                sb.append(sCurrentLine + "\t"
                        + b.getFrequency(sCurrentLine, length) + "\t"
                        + b.getSlope(sCurrentLine, length, true) + "\t"
                        + b.getR2(sCurrentLine, length, true) + "\t"
                        + b.getUsageSinceYear(sCurrentLine, length, true) + "\t"
                        + b.getPercentProperCaps(sCurrentLine, length) + "\n");
                // 2nd file
//				int sum = 0;
//				for(Integer i : b.getMatchCountWithinYearRange(sCurrentLine, length, 1, 1899, 0, true)) {
//					sum += i;
//				}
//				System.out.println(sCurrentLine + "\t" + sum);
            } catch (Exception e) {
                System.err.println(e.getLocalizedMessage());
                // 1st file
//				System.out.println(sCurrentLine + "\tNONE\tNONE\tNONE\tNONE\tNONE");
                sb.append(sCurrentLine + "\tNONE\tNONE\tNONE\tNONE\tNONE\n");
                // 2nd file
//				System.out.println(sCurrentLine + "\tNONE");
                continue;
            }
            if (i % 500 == 0) {
                System.out.println(sb.toString());
                sb = new StringBuffer("");
            }
        }
        System.out.println(sb.toString());
        br.close();
        b.ngu.closeDatabase();
    }
    private final String pathToIndexTopDir;
    
    @Override
    public void run(){
        System.out.println("INIT DATABASE");
        ngu = new NGramUtility(pathToIndexTopDir, "I");
    }

    public Batch(String pathToIndexTopDir) {
        this.pathToIndexTopDir = pathToIndexTopDir;
        //System.out.println("INIT DATABASE");
//		ngu = new NGramUtility("/media/google-ngrams-bwfilestorage/", "I");
        //ngu = new NGramUtility(pathToIndexTopDir, "I"); // "/media/michael/5TB/datasets/google-ngrams/"

//		//shotdown hook for closing the database properly
//		Runtime.getRuntime().addShutdownHook(new Thread(){
//			@Override
//			public void run() {
//				System.out.println("CLOSE DATABASE TEST");
//				ngu.closeDatabase();
//			}});
    }

    // new: with end year
    public List<Integer> getMatchCountWithinYearRange(
            String phrase,
            int n,
            int yearBegin,
            int yearEnd,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (yearBegin == 0)) {
            return ngu.getAllMatchCount(phrase, n, caseSensitive);
        } else if (yearBegin == 0) {
            return ngu.getAllMatchCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAllMatchCountWithinYearRange(phrase, n, yearBegin, yearEnd, caseSensitive);
        }
        return null;
    }

    public List<Integer> getMatchCount(
            String phrase,
            int n,
            int year,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (year == 0)) {
            return ngu.getAllMatchCount(phrase, n, caseSensitive);
        } else if (year == 0) {
            return ngu.getAllMatchCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAllMatchCountSinceYear(phrase, n, year, caseSensitive);
        }
        return null;
    }

    public Float getAvgMathcount(
            String phrase,
            int n,
            int year,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (year == 0)) {
            return ngu.getAvgMatchCount(phrase, n, caseSensitive);
        } else if (year == 0) {
            return ngu.getAvgMatchCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAvgMatchCountSinceYear(phrase, n, year, caseSensitive);
        }
        return null;
    }

    public List<Integer> getVolumeCount(
            String phrase,
            int n,
            int year,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (year == 0)) {
            return ngu.getAllVolumeCount(phrase, n, caseSensitive);
        } else if (year == 0) {
            return ngu.getAllVolumeCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAllVolumeCountSinceYear(phrase, n, year, caseSensitive);
        }
        return null;
    }

    public Float getAvgVolcount(
            String phrase,
            int n,
            int year,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (year == 0)) {
            return ngu.getAvgVolumeCount(phrase, n, caseSensitive);
        } else if (year == 0) {
            return ngu.getAvgVolumeCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAvgVolumeCountSinceYear(phrase, n, year, caseSensitive);
        }
        return null;
    }

    public CountContainer getBothCount(
            String phrase,
            int n,
            int year,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (year == 0)) {
            return ngu.getAllMatchAndVolumeCount(phrase, n, caseSensitive);
        } else if (year == 0) {
            return ngu.getAllMatchAndVolumeCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAllMatchAndVolumeCountSinceYear(phrase, n, year, caseSensitive);
        }
        return null;
    }

    public Pair<Float, Float> getBothAvgCount(
            String phrase,
            int n,
            int year,
            int count,
            boolean caseSensitive) {
        if ((count == 0) && (year == 0)) {
            return ngu.getAvgMatchAndVolumeCount(phrase, n, caseSensitive);
        } else if (year == 0) {
            return ngu.getAvgMatchAndVolumeCount(phrase, n, count, caseSensitive);
        } else if (count == 0) {
            return ngu.getAvgMatchAndVolumeCountSinceYear(phrase, n, year, caseSensitive);
        }
        return null;
    }

    public float getSlope(
            String phrase,
            int n,
            boolean caseSensitive) {
        return ngu.getSlope(phrase, n, caseSensitive);
    }

    public float getR2(
            String phrase,
            int n,
            boolean caseSensitive) {
        return ngu.getR2(phrase, n, caseSensitive);
    }

    public int getUsageSinceYear(
            String phrase,
            int n,
            boolean caseSensitive) {
        return ngu.getUsageSinceYear(phrase, n, caseSensitive);
    }

    public float getPercentProperCaps(
            String phrase,
            int n) {
        return ngu.getPercentProperCaps(phrase, n);
    }

    public int getFrequency(
            String phrase,
            int n) {
        return ngu.getFrequency(phrase, n);
    }
}
