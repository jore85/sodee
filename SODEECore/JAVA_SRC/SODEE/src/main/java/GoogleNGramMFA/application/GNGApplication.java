package GoogleNGramMFA.application;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("")
public class GNGApplication extends Application {

    private Set<Object> singletons = new HashSet<Object>();
    private Set<Class<?>> empty = new HashSet<Class<?>>();

    public GNGApplication() {
        System.out.println("testapp started");
        //this.singletons.add(new SingletonTest(17));
//		this.singletons.add(new GoogleNGramsAPI());
    }

    public Set<Class<?>> getClasses() {
        return empty;
    }

    public Set<Object> getSingletons() {
        return singletons;
    }

}
