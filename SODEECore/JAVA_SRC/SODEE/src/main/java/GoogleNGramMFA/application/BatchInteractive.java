package GoogleNGramMFA.application;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import datastructure.CountContainer;
import datastructure.Pair;
import utilities.NGramUtility;

public class BatchInteractive {
	private NGramUtility ngu;
	
	public static void main(String[] args) throws IOException {
		String pathToIndexTopDir = args[0];
		BatchInteractive b = new BatchInteractive(pathToIndexTopDir);
//		b.getFrequency("Michael", 1);
		Scanner input = new Scanner(System.in);
		String inputString;
	    while (true) {
	      System.out.println("Input an ngram or file path:");
	      inputString = input.nextLine();
	 
	      if (inputString.equals("quit") || inputString.equals("exit")) {
	        break;
	      }
	      if(inputString.startsWith("file://")) {
	    	  // read input from file
	    	  String pathToFile = inputString.substring("file://".length());
	    	  BufferedReader br = new BufferedReader(new FileReader(pathToFile));
	    	  String sCurrentLine;
	    	  while ((sCurrentLine = br.readLine()) != null) {
	    		  int length = sCurrentLine.split(" ").length;
	    		  System.out.println(sCurrentLine + "\t" + b.getFrequency(sCurrentLine, length));
	    	  }
	    	  br.close();
	      }
	      else {
	    	  int length = inputString.split(" ").length;
	    	  System.out.println(inputString + "\t" + b.getFrequency(inputString, length));
	      }
	    }
	    b.ngu.closeDatabase();
	}

	public BatchInteractive(String pathToIndexTopDir) {
		System.out.println("INIT DATABASE");
//		ngu = new NGramUtility("/media/google-ngrams-bwfilestorage/", "I");
		ngu = new NGramUtility(pathToIndexTopDir, "I"); // "/media/michael/5TB/datasets/google-ngrams/"
		
//		//shotdown hook for closing the database properly
//		Runtime.getRuntime().addShutdownHook(new Thread(){
//			@Override
//			public void run() {
//				System.out.println("CLOSE DATABASE TEST");
//				ngu.closeDatabase();
//			}});
	}
	
	public List<Integer> getMatchCount(		
			String phrase,
			int n, 
			int year,
			int count,
			boolean caseSensitive) {
		if ((count == 0) && (year == 0)) {
			return ngu.getAllMatchCount(phrase, n, caseSensitive);
		} else if (year == 0) {
			return ngu.getAllMatchCount(phrase, n, count, caseSensitive);
		} else if (count == 0) {
			return ngu.getAllMatchCountSinceYear(phrase, n, year, caseSensitive);
		}
		return null;
	}
	
	public Float getAvgMathcount(		
			String phrase,
			int n, 
			int year,
			int count,
			boolean caseSensitive) {
		if ((count == 0) && (year == 0)) {
			return ngu.getAvgMatchCount(phrase, n, caseSensitive);
		} else if (year == 0) {
			return ngu.getAvgMatchCount(phrase, n, count, caseSensitive);
		} else if (count == 0) {
			return ngu.getAvgMatchCountSinceYear(phrase, n, year, caseSensitive);
		}
		return null;
	}
	
			
	public List<Integer> getVolumeCount(
			String phrase,
			int n, 
			int year,
			int count,
			boolean caseSensitive) {
		if ((count == 0) && (year == 0)) {
			return ngu.getAllVolumeCount(phrase, n, caseSensitive);
		} else if (year == 0) {
			return ngu.getAllVolumeCount(phrase, n, count, caseSensitive);
		} else if (count == 0) {
			return ngu.getAllVolumeCountSinceYear(phrase, n, year, caseSensitive);
		}
		return null;
	}
	
	public Float getAvgVolcount(		
			String phrase,
			int n, 
			int year,
			int count,
			boolean caseSensitive) {
		if ((count == 0) && (year == 0)) {
			return ngu.getAvgVolumeCount(phrase, n, caseSensitive);
		} else if (year == 0) {
			return ngu.getAvgVolumeCount(phrase, n, count, caseSensitive);
		} else if (count == 0) {
			return ngu.getAvgVolumeCountSinceYear(phrase, n, year, caseSensitive);
		}
		return null;
	}
	
	public CountContainer getBothCount(
			String phrase,
			int n, 
			int year,		
			int count,
			boolean caseSensitive) {
		if ((count == 0) && (year == 0)) {
			return ngu.getAllMatchAndVolumeCount(phrase, n, caseSensitive);
		} else if (year == 0) {
			return ngu.getAllMatchAndVolumeCount(phrase, n, count, caseSensitive);
		} else if (count == 0) {
			return ngu.getAllMatchAndVolumeCountSinceYear(phrase, n, year, caseSensitive);
		}
		return null;
	}
	
	
	public Pair<Float, Float> getBothAvgCount(
			String phrase,
			int n, 
			int year,		
			int count,
			boolean caseSensitive) {
		if ((count == 0) && (year == 0)) {
			return ngu.getAvgMatchAndVolumeCount(phrase, n, caseSensitive);
		} else if (year == 0) {
			return ngu.getAvgMatchAndVolumeCount(phrase, n, count, caseSensitive);
		} else if (count == 0) {
			return ngu.getAvgMatchAndVolumeCountSinceYear(phrase, n, year, caseSensitive);
		}
		return null;
	}
	
	public float getSlope(
			String phrase,
			int n, 
			boolean caseSensitive) {
		return ngu.getSlope(phrase, n, caseSensitive);
	}
	

	public float getR2(
			String phrase,
			int n, 
			boolean caseSensitive) {
		return ngu.getR2(phrase, n, caseSensitive);
	}
	
	public int getUsageSinceYear(
			String phrase,
			int n, 
			boolean caseSensitive) {
		return ngu.getUsageSinceYear(phrase, n, caseSensitive);
	}
	
	public float getPercentProperCaps(
			String phrase,
			int n) {
		return ngu.getPercentProperCaps(phrase, n);
	}
	
	public int getFrequency(
			String phrase,
			int n) {
		return ngu.getFrequency(phrase, n);
	}
}
