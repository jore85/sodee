/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author johan
 */
public class AnnotationWebServiceResult {

    private final int articleID;
    private final List<Annotation> annotations;

    public AnnotationWebServiceResult(int articleID, List<Annotation> annotations) {
        this.articleID = articleID;        
        this.annotations = annotations;        
    }

    public int getArticleID() {
        return articleID;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public static List<Annotation> convertTextAnnotationToAnnotationList(TextAnnotation annotations, int articleID) {
        List<Annotation> result = new ArrayList<>();
        List<MentionInAnnotation> listOfMentions = annotations.getListOfMentions();
        String displayName = annotations.getDisplayName();
        String url_en = annotations.getURL_EN();
        float weight = annotations.getWeight();
        Iterator<MentionInAnnotation> iterator = listOfMentions.iterator();
        while (iterator.hasNext()) {
            MentionInAnnotation next = iterator.next();
            int start = next.getStart();
            int end = next.getEnd();
            String surfaceform = next.getSurfaceform();
            if(!surfaceform.isEmpty() && !displayName.isEmpty() && !url_en.isEmpty()){
                result.add(new Annotation(surfaceform, displayName, start, end, url_en, weight, articleID));
            }
            
        }
        return result;
    }
}
