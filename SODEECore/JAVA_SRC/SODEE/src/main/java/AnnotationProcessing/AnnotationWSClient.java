package AnnotationProcessing;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;

/**
 *
 * @author Johannes Reiss
 */
public class AnnotationWSClient {

    private DefaultHttpClient client;
    private String annotWSURL;

    public AnnotationWSClient(String annotWSURL) {
        this.annotWSURL = annotWSURL;
        this.client = new DefaultHttpClient();
        this.client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);
    }

    public String getAnnotWSURL() {
        return annotWSURL;
    }
    

    public String makeRequestToAnnotWS(String postText) {
        long startMillis = DateTime.now().getMillis();
        long endMillis = 0;
        long duration = 0;
        System.out.println("Request to the annotation web service. "
                + "Number characters of request text: " + postText.length());
        postText = checkInputText(postText);        
        String resultXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<item>\n"
                + "<text>\r\n</text>\n<WikifiedText></WikifiedText>\n"
                + "<annotations>\n</annotations>\n</item>";
        CloseableHttpResponse postResponse = null;
        HttpPost postRequest = new HttpPost(this.annotWSURL);
        int byteLength = 0;
        try {
            byte[] bytesToPost = postText.getBytes("UTF-8");
            ByteArrayInputStream postInputStrm = new ByteArrayInputStream(bytesToPost);
            postRequest.setHeader("Content-Type", "application/xml");
            postRequest.setEntity(new InputStreamEntity(postInputStrm));
            //System.out.println(postRequest.toString());
            postResponse = this.client.execute(postRequest);
        } catch (UnsupportedEncodingException ex) {
            System.err.println("The annotation webservice request could not "
                    + "be executed, because an encoding error.");
        } catch (IOException ex) {
            System.err.println("The annotation webservice request could not "
                    + "be executed. No response was received.");
        }
        if (postResponse != null) {
            HttpEntity responseEntity = postResponse.getEntity();
            if (postResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                try {
                    resultXML = EntityUtils.toString(responseEntity);
                    EntityUtils.consumeQuietly(responseEntity);
                } catch (IOException | ParseException ex) {
                    System.err.println("The annotation webservice was requested, "
                            + "a response was received and \"OK\", "
                            + "but it could not processed properly.");
                }
            }
            EntityUtils.consumeQuietly(responseEntity);
        }
        client.close();
        resultXML = resultXML.replaceAll("\\n", ""); // remove line breaks;
        endMillis = DateTime.now().getMillis();
        duration = endMillis - startMillis;
        System.out.println("Request to the annotation web service. "
                + "Number characters of received xml: " + resultXML.length() 
                + "\n" + "Response time (ms): " + duration);
        if(resultXML.isEmpty()){
            resultXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<item>\n"
                + "<text>\r\n</text>\n<WikifiedText></WikifiedText>\n"
                + "<annotations>\n</annotations>\n</item>";
        }
        return resultXML;
    }

    private String checkInputText(String input) {
        input = StringEscapeUtils.escapeXml10(input);
        if (input.startsWith("<item><text>") && input.endsWith("</text></item>")) {
            return input;
        } else {
            return "<item><text>" + input + "</text></item>";
        }
    }

    public static void main(String[] args) {
        AnnotationWSClient annotationWSClient
                = new AnnotationWSClient("http://scc-vm-151.scc.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score/");
        String text = "<item><text>British anti-EU campaigner Nigel Farage posted "
                + "a picture of him having \"dinner with The Donald\" on Twitter, "
                + "the latest meeting between U.S. President Donald Trump and "
                + "the critic of Prime Minister Theresa May. && </text></item>";
        String responseXML = annotationWSClient.makeRequestToAnnotWS(text);
        System.out.println(responseXML);
    }

}
