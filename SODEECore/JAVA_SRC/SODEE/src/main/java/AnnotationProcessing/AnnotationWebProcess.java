/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *
 * @author johan
 */
public class AnnotationWebProcess implements Callable<AnnotationWebServiceResult> {

    private final FullTextAnnotationClientXML annotParser;
    private TextAnnotationWSClientv2 annotRESTClient;
    private AnnotationWSClient annotWSClient;

    private final String bodyText;
    private final int articleID;

    public AnnotationWebProcess(FullTextAnnotationClientXML annotParser,
            TextAnnotationWSClientv2 annotRESTClient, String bodyText, int articleID) {
        this.annotParser = annotParser;
        this.annotRESTClient = annotRESTClient;
        this.bodyText = bodyText;
        this.articleID = articleID;
    }

    AnnotationWebProcess(FullTextAnnotationClientXML annotParser, 
            AnnotationWSClient annotRESTClient, String bodyText, int articleID) {
        this.annotParser = annotParser;
        this.annotWSClient = annotRESTClient;
        this.bodyText = bodyText;
        this.articleID = articleID;
    }

    @Override
    public AnnotationWebServiceResult call() throws Exception {
        AnnotationWebServiceResult result = null;
        List<Annotation> allAnnotationsOfDoc = new ArrayList<>();        
        String annotDocXML
                //= this.annotRESTClient.retrieveXMLFromServiceGivenWholeDoc(bodyText);
                = this.annotWSClient.makeRequestToAnnotWS(bodyText);
        //System.out.println(annotDocXML);
        List<TextAnnotation> annotations
                //= this.annotParser.getAnnotationsByWholeDocAnnotation(annotDocXML);
                = FullTextAnnotationClientXML.getAllAnnotations(annotDocXML);
        if (annotations != null) {
            Iterator<TextAnnotation> iterator = annotations.iterator();
            while (iterator.hasNext()) {
                TextAnnotation annotation = iterator.next();
                allAnnotationsOfDoc.addAll(AnnotationWebServiceResult.
                        convertTextAnnotationToAnnotationList(annotation, articleID));
            }
            //System.out.println("Annotations of Article: " + articleID + ": " + allAnnotationsOfDoc.size());
            result = new AnnotationWebServiceResult(articleID, allAnnotationsOfDoc);
        }
        return result;
    }

}
