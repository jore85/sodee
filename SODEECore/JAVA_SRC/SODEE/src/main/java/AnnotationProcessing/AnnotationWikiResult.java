/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import FeatureExtraction.AnnotationFeatures;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.EnhancedNP;
import java.util.HashMap;

/**
 *
 * @author johan
 */
public class AnnotationWikiResult {

    HashMap<EnhancedNP, AnnotationFeatures> annotationFeats;
    HashMap<EnhancedNP, WikipediaFeatures> wikiFeats;

    public AnnotationWikiResult(HashMap<EnhancedNP, AnnotationFeatures> annotationFeats, HashMap<EnhancedNP, WikipediaFeatures> wikiFeats) {
        this.annotationFeats = annotationFeats;
        this.wikiFeats = wikiFeats;
    }

    public HashMap<EnhancedNP, AnnotationFeatures> getAnnotationFeats() {
        return annotationFeats;
    }

    public HashMap<EnhancedNP, WikipediaFeatures> getWikiFeats() {
        return wikiFeats;
    }
    

}
