package AnnotationProcessing;

import Utility.Helper;


/**
 *
 * @author jore
 */
public class Annotation {
    
    private String mention;
    private String kb_entity;
    private int startOffSet;
    private int endOffSet;
    private String kb_url;
    private float weight;
    private int docID;
    private int noveltyClass;
    private String unknownField;

    public Annotation(String mention, int startOffSet, int endOffSet, 
            String kb_url, float weight, int docID, int noveltyClass, String unknownField) {
        this.mention = mention;
        this.kb_entity = Helper.getLastElementOfURI(kb_url);
        this.startOffSet = startOffSet;
        this.endOffSet = endOffSet;
        this.kb_url = kb_url;
        this.weight = weight;
        this.docID = docID;
        this.unknownField = unknownField;
        this.noveltyClass = noveltyClass;
    }
    
    public Annotation(String mention, String kb_entity, int startOffset, 
            int endOffset, String kb_url, float weight, int docID){
        this.mention = mention;
        this.kb_entity = kb_entity;
        this.startOffSet = startOffset;
        this.endOffSet = endOffset;
        this.kb_url = kb_url;
        this.weight = weight;
        this.docID = docID;
        this.noveltyClass = -1;
    }
    
    /**
     * Creates a NIL Annotation with 0 values and example url.
     */
    public Annotation(){
        this.mention = "NIL";
        this.kb_entity = "NIL";
        this.startOffSet = 0;
        this.endOffSet = 0;
        this.kb_url = "http://example.org";
        this.weight = 0;
        this.docID = 0;
        this.unknownField = "NIL";
        this.noveltyClass = 0;
    }

    public String getKb_entity() {
        return kb_entity;
    }

    public void setKb_entity(String kb_entity) {
        this.kb_entity = kb_entity;
    }

    public int getStartOffSet() {
        return startOffSet;
    }

    public void setStartOffSet(int startOffSet) {
        this.startOffSet = startOffSet;
    }

    public int getEndOffSet() {
        return endOffSet;
    }

    public void setEndOffSet(int endOffSet) {
        this.endOffSet = endOffSet;
    }

    public String getKb_url() {
        return kb_url;
    }

    public void setKb_url(String kb_url) {
        this.kb_url = kb_url;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getDocID() {
        return docID;
    }

    public void setDocID(int docID) {
        this.docID = docID;
    }

    public String getMention() {
        return mention;
    }

    public void setMention(String mention) {
        this.mention = mention;
    }

    public String getUnknownField() {
        return unknownField;
    }

    public void setUnknownField(String unknownField) {
        this.unknownField = unknownField;
    }

    public int getNoveltyClass() {
        return noveltyClass;
    }

    public void setNoveltyClass(int noveltyClass) {
        this.noveltyClass = noveltyClass;
    }

    @Override
    public String toString() {
        return "Annotation{" + "mention=" + mention + ", kb_entity=" + kb_entity + ", kb_url=" + kb_url + '}';
    }
    
}
