package AnnotationProcessing;

import RBBNPE.EnhancedNP;
import FeatureExtraction.AnnotationFeatures;
import FeatureExtraction.WikipediaFeatures;
import Utility.Helper;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.Callable;

/**
 * This class provides the linking methods to link annotation and np.
 * @author Johannes Reiss
 */
public class AnnotationNPProcessor {

    private Annotation ann;
    private EnhancedNP np;
    
    private HashSet<String> stopWords;

    private AnnotationFeatures annotationFeat;
    private HashSet<String> surfaceFormsSet;

    /**
     * Creates an annotation np processor and set the stop words list.
     * 
     * list of stopwords: http://alvinalexander.com/java/jwarehouse/lucene/src/java/org/apache/lucene/analysis/StopAnalyzer.java.shtml
     * some extensions were made:
     * "some", "many", "new", "our", "me", "p.m.", "a.m.", "'s"
     * 
     */
    public AnnotationNPProcessor() {        
        this.stopWords = new HashSet<>(Arrays.asList("a", "an", "and", "are", "as", 
                "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", 
                "no", "not", "of", "on", "or", "such", "that", "the", "their", 
                "then", "there", "these", "they", "this", "to", "was", "will", "with", "'s",
                "some", "many", "new", "our", "me", "p.m.", "a.m."));
    }

    public void setAnn(Annotation ann) {
        this.ann = ann;
    }

    public void setNp(EnhancedNP np) {
        this.np = np;
    }

    public AnnotationFeatures getAnnotationFeat() {
        return annotationFeat;
    }
    
    void setSurfaceForms(HashSet<String> surfaceFormsSet) {
        this.surfaceFormsSet = surfaceFormsSet;
    }

    /**
     * This method tries to link a given annotation with an given np.
     * 
     * @param ann A annotation.
     * @param np A noun phrase.
     * @return The linked noun phrase with the matched annotation. If a np could not be
     * linked, a "NIL" Annotation is created and returned.
     */
    public AnnotationFeatures process(Annotation ann, EnhancedNP np) {
        boolean result = false;
        int annotationStart = ann.getStartOffSet();
        int annotationEnd = ann.getEndOffSet();
        int npStart = np.getNp().getStartOffset();
        int npEnd = np.getNp().getEndOffset();
        String mention = ann.getMention();
        String npString = np.getNp().getPhraseString();
        Annotation nilAnn = new Annotation();
        //System.out.println("Annotation: " + ann.getMention() + " NP " + np.getNp().getPhraseString());
        //Try linking by offsets first
        if (npStart == annotationStart) {
            if (npEnd == annotationEnd) {
                return this.annotationFeat = new AnnotationFeatures(np.getArticleID(), np.getNp().getPhraseString(),
                        ann.getMention(), ann.getKb_entity(), ann.getKb_url(),
                        ann.getWeight(), ann.getNoveltyClass());
                //System.out.println("Annotation is linked per matching offset.");
                //result true;
            }            
        }
        //Else try linking by case-insensitive string match of mention an np
        if (npString.equalsIgnoreCase(mention)) {
            return this.annotationFeat = new AnnotationFeatures(np.getArticleID(), np.getNp().getPhraseString(),
                    ann.getMention(), ann.getKb_entity(), ann.getKb_url(),
                    ann.getWeight(), ann.getNoveltyClass());
            //System.out.println("Annotation " + mention + " and NP " + npString + " is linked per matching string.");
            //result = true;
        // otherwise link to an NIL-Annotation, with np as mention    
        } else {
            // Check if the np matches any surface form
            String npLowerCase = npString.toLowerCase();
            //System.out.println("Convert " + npString + " to lower case: " + npLowerCase);
            if(this.surfaceFormsSet.contains(npLowerCase)){
                System.out.println(npString + " matched surface form.");
                return this.annotationFeat = new AnnotationFeatures(np.getArticleID(), np.getNp().getPhraseString(),
                    np.getNp().getPhraseString(), ann.getKb_entity(), ann.getKb_url(),
                    ann.getWeight(), ann.getNoveltyClass());
            }            
            //do some advanced matching approach, e.g. stopword removal (many of the np contain words as "the, this, his, her")
            // remove stopwords
            String npString_noStopwords = Helper.simpleStopwordRemoval(stopWords, npString);
            if(npString_noStopwords.equalsIgnoreCase(mention)){ // check again if the mention of annotation matches the np
                return this.annotationFeat = new AnnotationFeatures(np.getArticleID(), np.getNp().getPhraseString(),
                    ann.getMention(), ann.getKb_entity(), ann.getKb_url(),
                    ann.getWeight(), ann.getNoveltyClass());
            }
        }
        // if no match, then return the "NIL" annotation
        //System.out.println("NP: " + npString + " has nil annotation: " + nilAnn.getMention() + " (in AnnotationNPProcessor)");
        return this.annotationFeat = new AnnotationFeatures(np.getArticleID(), np.getNp().getPhraseString(),
                nilAnn.getMention(),
                nilAnn.getKb_entity(), nilAnn.getKb_url(),
                nilAnn.getWeight(), nilAnn.getNoveltyClass());
    }
}
