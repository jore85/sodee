/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import Classifier.NPLabeler;
import DatabasesNetwork.PageViewDatabaseConnector;
import FeatureExtraction.AnnotationFeatures;
import FeatureExtraction.WikipediaFeat.PageViewAPIClientNew.WMPageViewAPIClientNew;
import FeatureExtraction.WikipediaFeat.RedLinkChecker;
import FeatureExtraction.WikipediaFeat.WikipediaNPProcessor;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.EnhancedNP;
import Utility.Helper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.Callable;

/**
 * This class provides the linking logic for annotation and noun phrase linking.
 *
 * @author Johannes Reiss
 */
public class AnnotationNPLinker implements Callable<AnnotationWikiResult> {

    private AnnotationNPProcessor annotProcessor;
    private WikipediaNPProcessor wikiProcessor;

    private PageViewDatabaseConnector pvDBConnection;
    private RedLinkChecker rlChecker;
    private WMPageViewAPIClientNew pvAPIClient;

    private boolean isLiveRunning;

    private HashMap<Integer, ArrayList<Annotation>> annotations;
    private HashSet<EnhancedNP> nps;
    private HashSet<String> surfaceFormsSet;

    HashMap<EnhancedNP, AnnotationFeatures> annotationFeats;
    HashMap<EnhancedNP, WikipediaFeatures> wikiFeats;

    /**
     * Create a new annotation np linker.
     */
    public AnnotationNPLinker() {
        this.annotProcessor = new AnnotationNPProcessor();
        this.wikiProcessor = new WikipediaNPProcessor();
        this.pvDBConnection = new PageViewDatabaseConnector();
    }

    /**
     * Create a new annotation np linker with given components.
     *
     * @param wikiProcessor A Wikipedia processor to retrieve the Wikipedia
     * features.
     * @param pvDBConnection A db connection for the pageview data (only used if
     * not live).
     * @param rlChecker Checker for the red link feature.
     * @param pvAPIClient A client making the pageview api request and response
     * handling.
     * @param isLiveRunning Indicates if the system is in live mode.
     */
    public AnnotationNPLinker(WikipediaNPProcessor wikiProcessor,
            PageViewDatabaseConnector pvDBConnection, RedLinkChecker rlChecker,
            WMPageViewAPIClientNew pvAPIClient, boolean isLiveRunning) {
        //this.processedAnnotations = 0;
        this.annotProcessor = new AnnotationNPProcessor();
        this.wikiProcessor = wikiProcessor;
        this.pvDBConnection = pvDBConnection;
        this.rlChecker = rlChecker;
        this.isLiveRunning = isLiveRunning;
        this.pvAPIClient = pvAPIClient;
    }

    public void setIsLiveRunning(boolean isLiveRunning) {
        this.isLiveRunning = isLiveRunning;
    }

    public void setPvAPIClient(WMPageViewAPIClientNew pvAPIClient) {
        this.pvAPIClient = pvAPIClient;
    }

    public void setRlChecker(RedLinkChecker rlChecker) {
        this.rlChecker = rlChecker;
    }

    public void setAnnotations(HashMap<Integer, ArrayList<Annotation>> annotations) {
        this.annotations = annotations;
    }

    public void setSurfaceFormsSet(HashSet<String> surfaceFormsSet) {
        this.surfaceFormsSet = surfaceFormsSet;
    }

    public void setNps(HashSet<EnhancedNP> nps) {
        this.nps = nps;
    }

    public HashMap<Integer, ArrayList<Annotation>> getAnnotations() {
        return annotations;
    }

    public HashMap<EnhancedNP, AnnotationFeatures> getAnnotationFeats() {
        return annotationFeats;
    }

    public HashMap<EnhancedNP, WikipediaFeatures> getWikiFeats() {
        return wikiFeats;
    }

    /**
     * This method links the annotation and nps. And initiates the retrieval of
     * the Wikipedia features.
     *
     * The linking logic is mostly done by the AnnotationNPProcessor.
     *
     * @param annotations A map of annotations grouped by their article id.
     * @param nps A set of noun phrases.
     */
    public void processAnnotations(HashMap<Integer, ArrayList<Annotation>> annotations,
            HashSet<EnhancedNP> nps) {
        this.annotProcessor.setSurfaceForms(this.surfaceFormsSet);
        this.annotationFeats = new HashMap<>();
        this.wikiFeats = new HashMap<>();
        boolean validPageViewDB = false;
        try {
            if (pvDBConnection.getConn().isValid(0)) {
                validPageViewDB = true;
                System.out.println("The Database Connection for the Wikipedia PageView Features was established and is valid.");
                this.wikiProcessor.setPvDBConnection(pvDBConnection);
            }
        } catch (SQLException ex) {
            System.err.println("There went something wrong in the Database Connection for the Wikipedia PageView Features.");
        }
        this.wikiProcessor.setRlChecker(rlChecker);
        this.wikiProcessor.setPvAPIClient(pvAPIClient);
        Iterator<EnhancedNP> npIterator = nps.iterator();
        System.out.println("Number of nps in annotations processor: " + nps.size());
        //System.out.println("Size of SFs in annotations processor: " + surfaceFormsSet.size());
        while (npIterator.hasNext()) {
            EnhancedNP np = npIterator.next();
            //System.out.println("\nNP: " + np.getNp().getPhraseString());
            int id = np.getArticleID();
            ArrayList<Annotation> annotationsOfArticle = annotations.get(id);
            //System.out.print("Number of annotations in article " + id);
            if (annotationsOfArticle != null) {
                //System.out.print(": " + annotationsOfArticle.size() + "\n");
                //Iterator<Annotation> annotIterator = annotationsOfArticle.iterator();
                //annotIterator = annotationsOfArticle.iterator();
                Annotation annotation = null;
                AnnotationFeatures processedAnnotationNP = null;
                //System.out.println("No. annotations: " + annotationsOfArticle.size());
                annotationLoop:
                for (int i = 0; i < annotationsOfArticle.size(); i++) {
                    annotation = annotationsOfArticle.get(i);
                    if (annotation != null) {
                        //System.out.println(i + " Annotation: " + annotation.getMention() + " KB Entity: " + annotation.getKb_entity());
                        //LINKING STEPS 
                        processedAnnotationNP = annotProcessor.process(annotation, np);
                        if (processedAnnotationNP != null) {
                            if (!processedAnnotationNP.getMention().equals("NIL")) {
                                // the np is linked with an annotation -> not a possible candidate
                                System.out.println("NP (linked, no candidate): " + np.getNp().getPhraseString() + " " + processedAnnotationNP.getMention() + " " + processedAnnotationNP.getWikiTitle());
                                break annotationLoop;
                            } else {
                                // the np has no annotation -> possible candidate for a true novel entity
                                //if (!surfaceFormsSet.contains(np.getNp().getPhraseString())) {
                                if (i + 1 == annotationsOfArticle.size()) { // if the whole list of surface forms retrieved and no link is found -> np is a save candidate 
                                    System.out.println("NP (candidate): " + np.getNp().getPhraseString() + " " + processedAnnotationNP.getMention() + " " + processedAnnotationNP.getWikiTitle());
                                    this.annotationFeats.put(np, processedAnnotationNP);
                                    if (validPageViewDB || this.isLiveRunning == true) {
                                        WikipediaFeatures wikiFeature = wikiProcessor.process(new Annotation(), np, this.isLiveRunning);
                                        //System.out.println(np.getNp().getPhraseString() + " in AnnotNPPeriodHandler || " + wikiProcessor.getWikiFeatures().toString());
                                        //WikipediaFeatures wikiFeature = wikiProcessor.getWikiFeatures();
                                        if (wikiFeature != null) {
                                            //System.out.println("NP " + np.getNp().getPhraseString() + " Wikipedia " + wikiFeature.getWikiEntity());
                                            this.wikiFeats.put(np, wikiFeature);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public AnnotationWikiResult call() throws Exception {
        processAnnotations(this.annotations, this.nps);
        return new AnnotationWikiResult(this.annotationFeats, this.wikiFeats);
    }

}
