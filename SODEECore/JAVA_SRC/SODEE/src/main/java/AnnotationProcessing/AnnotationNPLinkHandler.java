/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import DatabasesNetwork.PageViewDatabaseConnector;
import FeatureExtraction.AnnotationFeatures;
import FeatureExtraction.WikipediaFeat.PageViewAPIClientNew.WMPageViewAPIClientNew;
import FeatureExtraction.WikipediaFeat.RedLinkChecker;
import FeatureExtraction.WikipediaFeat.WikipediaNPProcessor;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.EnhancedNP;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * This class handles the linking process of annotations and noun phrases.
 * Additionally the wikipedia and annotatin features are extracted.
 *
 * @author Johannes Reiss
 */
public class AnnotationNPLinkHandler {

    private WikipediaNPProcessor wikiProcessor;
    private PageViewDatabaseConnector pvDBConnection;
    private RedLinkChecker rlChecker;
    private final boolean isLiveRunning;
    private int numberOfThreads;

    private HashMap<EnhancedNP, AnnotationFeatures> annotationFeats;
    private HashMap<EnhancedNP, WikipediaFeatures> wikiFeats;
    private final String userAgent;
    private final String pageViewAPIBase;
    HashSet<String> surfaceFormsSet;

    /**
     * Creates a annotation and np link handler.
     *
     * @param userAgent The user agent for the Wikipedia API requests.
     * @param pageViewAPIBase The basis URI of the Wikipedia PageViewAPI.
     * @param isLiveRunning Indicator if the system runs in the live mode (and
     * therefore the APIs are used)
     */
    public AnnotationNPLinkHandler(String userAgent, String pageViewAPIBase, boolean isLiveRunning) {
        this.userAgent = userAgent;
        this.pageViewAPIBase = pageViewAPIBase;
        this.isLiveRunning = isLiveRunning;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public void setSurfaceFormsSet(HashSet<String> surfaceFormsSet) {
        this.surfaceFormsSet = surfaceFormsSet;
    }

    public HashMap<EnhancedNP, AnnotationFeatures> getAnnotationFeats() {
        return annotationFeats;
    }

    public HashMap<EnhancedNP, WikipediaFeatures> getWikiFeats() {
        return wikiFeats;
    }

    /**
     * Process the given annotations and nps with the help of an executor
     * service.
     *
     * @param annotations A map of annotations grouped by their article id.
     * @param nps A set of extracted noun phrases.
     */
    public void processParallel(HashMap<Integer, ArrayList<Annotation>> annotations,
            HashSet<EnhancedNP> nps) {
        this.annotationFeats = new HashMap<>();
        this.wikiFeats = new HashMap<>();
        List<HashSet<EnhancedNP>> splits = splitSet(nps, nps.size() / this.numberOfThreads);
        ExecutorService es = Executors.newFixedThreadPool(this.numberOfThreads);
        List<Future<AnnotationWikiResult>> results = new ArrayList<>();
        Iterator<HashSet<EnhancedNP>> iterator = splits.iterator();
        ArrayList<AnnotationNPLinker> processes = new ArrayList<>(splits.size());
        while (iterator.hasNext()) {
            HashSet<EnhancedNP> next = iterator.next();
            wikiProcessor = new WikipediaNPProcessor();
            pvDBConnection = new PageViewDatabaseConnector();
            rlChecker = new RedLinkChecker(this.userAgent);
            //wikiProcessor.setPvAPIClient(new WMPageViewAPIClientNew(pageViewAPIBase, userAgent));
            //wikiProcessor.setPvDBConnection(pvDBConnection);
            //wikiProcessor.setRlChecker(rlChecker);
            // INIT PROCESSING THE ANNOTATIONS AND THE NOUN PHRASES
            AnnotationNPLinker annotNPPeriodHandler
                    = new AnnotationNPLinker(wikiProcessor, pvDBConnection,
                            rlChecker, new WMPageViewAPIClientNew(pageViewAPIBase, userAgent),
                            isLiveRunning);
            System.out.println("Length NP: " + next.size());
            annotNPPeriodHandler.setSurfaceFormsSet(surfaceFormsSet);
            annotNPPeriodHandler.setNps(new HashSet<>(next));
            annotNPPeriodHandler.setAnnotations(new HashMap<>(annotations));
            System.out.println("Length annotations: " + annotNPPeriodHandler.getAnnotations().size());
            processes.add(annotNPPeriodHandler);
        }
        try {
            results = es.invokeAll(processes); // processing of the linking steps
        } catch (InterruptedException ex) {
            Logger.getLogger(AnnotationNPLinkHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("No. of parts parallel annotation + np processing: " + processes.size()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        Iterator<Future<AnnotationWikiResult>> futureIterator = results.iterator();
        while (futureIterator.hasNext()) {
            Future<AnnotationWikiResult> next = futureIterator.next(); // get results of the processing
            try {
                AnnotationWikiResult get = next.get();
                System.out.println("No. of annotation features: " + get.annotationFeats.size());
                System.out.println("No. of wikipedia features: " + get.wikiFeats.size());
                this.annotationFeats.putAll(get.annotationFeats);
                this.wikiFeats.putAll(get.wikiFeats);
            } catch (InterruptedException ex) {
                Logger.getLogger(AnnotationNPLinkHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(AnnotationNPLinkHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("No. results of parallel annotation + np processing: " + results.size()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        es.shutdown();
    }

    /**
     * Split the list of nps into smaller chunks of nps and process them
     * concurrently.
     *
     * Source:
     * http://www.bmchild.com/2014/01/split-java-list-into-list-of-sublists.html
     *
     * @param nps A set of nps.
     * @param length The number of chunks.
     * @return A list with the chunks.
     */
    public static List<HashSet<EnhancedNP>> splitSet(Set<EnhancedNP> nps, final int length) {
        List<EnhancedNP> list = new ArrayList<>(nps);
        List<HashSet<EnhancedNP>> parts = new ArrayList<>();
        final int size = list.size();
        for (int i = 0; i < size; i += length) {
            HashSet<EnhancedNP> arrayList = new HashSet<>(list.subList(i, Math.min(size, i + length)));
            //System.out.println("length: " + arrayList.size());
            parts.add(arrayList);
        }
        return parts;
    }

}
