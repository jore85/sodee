/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import FeatureExtraction.CombinedArticleNPFeatures;
import RBBNPE.BaseNounPhrase;
import RBBNPE.EnhancedNP;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The Named Entity and noun phrase linker provides functionaltiy to link
 * recognised named entities to noun phrases of the same text.
 *
 * @author Johannes Reiss
 */
public class NPNELinker {

    /**
     * Links the given named entity to a given noun phrase.
     *
     * @param ne The representation of the named entity.
     * @param np The noun phrase link to.
     * @return An new noun phrase object with the corresponding NE-Class if
     * exists else "NONE" is the NE-Class of the returned np object.
     */
    public static EnhancedNP linkNEtoNP(String[] ne, EnhancedNP np) {
        int originalStartOffset = np.getNp().getStartOffset();
        int originalEndOffset = np.getNp().getEndOffset();
        if (ne.length == 4) { //[0]: entity string; [1] NE-Class; [2] originalStartOffset; [3] originalEndOffset
            String namedEntity = ne[0];
            String neClass = ne[1];
            if (neClass != null) {
                //    neClass = "NONE";
                //}
                int startOffset = Integer.parseInt(ne[2]);
                int endOffset = Integer.parseInt(ne[3]);
                //Try link by offset
                if (startOffset == originalStartOffset) {
                    if (endOffset == originalEndOffset) {
                        np.setNeClass(neClass);
                    }
                } else if (np.getNp().getPhraseString().contains(namedEntity)) {
                    np.setNeClass(neClass);
                }
            } else {
                np.setNeClass("NONE");
            }
        }
        return np;
    }

    // not used
    //only return the NE-Tag
    private static String linkNEString(String[] ne, EnhancedNP np) {
        String result = "NoNE";
        int originalStartOffset = np.getNp().getStartOffset();
        int originalEndOffset = np.getNp().getEndOffset();
        if (ne.length == 4) { //[0]: entity string; [1] NE-Class; [2] originalStartOffset; [3] originalEndOffset
            String namedEntity = ne[0];
            String neClass = ne[1];
            if (neClass != null) {
                int startOffset = Integer.parseInt(ne[2]);
                int endOffset = Integer.parseInt(ne[3]);
                System.out.println("NP: " + np.getNp().getPhraseString() + " NE: " + ne[0]
                        + " start(NE): " + startOffset + " start(orig): " + originalStartOffset
                        + " end(NE): " + endOffset + " end(orig): " + originalEndOffset);
                //Try link by offset
                if (startOffset == originalStartOffset) {
                    if (endOffset == originalEndOffset) {
                        result = neClass;
                        System.out.println("Link NE per offset: " + neClass);
                    }
                } else if (np.getNp().getPhraseString().contains(namedEntity)) {
                    result = neClass;
                    System.out.println("Link NE per regex match: " + neClass);
                }
            }
        }
        return result;
    }

    // not used
    public static EnhancedNP linkNEtoNP(ArrayList<String[]> namedEntities, EnhancedNP np) {
        Iterator<String[]> iterator = namedEntities.iterator();
        while (iterator.hasNext()) {
            String[] next = iterator.next();
            EnhancedNP enp = NPNELinker.linkNEtoNP(next, np);
            if (enp.getNeClass() != null) {
                //System.out.println(enp);
                np.setNeClass(enp.getNeClass());
                return np;
            }
        }
        return np;
    }

    // not used
    private static String extractNEString(List<String[]> namedEntities, EnhancedNP np) {
        String neString = "NoNE";
        Iterator<String[]> iterator = namedEntities.iterator();
        while (iterator.hasNext()) {
            String[] next = iterator.next();
            neString = NPNELinker.linkNEString(next, np);
            if (!neString.equals("NoNe")) {
                return neString;
            }
        }
        return neString;
    }

    /**
     * Go through all extracted named entities and find the corresponding np.
     * 
     * Use this method to get the corresponding named entity class as string.
     *
     * @param namedEntities A list with all named entities of an article.
     * @param np A noun phrase extracted from the article.
     * @return The corresponding NE-Class if there is one, else "NoNE".
     */
    public static String getNEString(List<String[]> namedEntities, BaseNounPhrase np) {
        int originalStartOffset = np.getStartOffset();
        int originalEndOffset = np.getEndOffset();
        for (int i = 0; i < namedEntities.size(); i++) {
            String[] ne = namedEntities.get(i);
            if (ne.length == 4) { //[0]: entity string; [1] NE-Class; [2] originalStartOffset; [3] originalEndOffset
                String namedEntity = ne[0];
                if (ne[1] != null) {
                    int startOffset = Integer.parseInt(ne[2]);
                    int endOffset = Integer.parseInt(ne[3]);
                    //System.out.println("NP: " + np.getPhraseString() + " NE: " + ne[0]
                    //        + " start(NE): " + startOffset + " start(orig): " + originalStartOffset
                    //        + " end(NE): " + endOffset + " end(orig): " + originalEndOffset);
                    //Try link by offset and length
                    if (startOffset == originalStartOffset) {
                        if (ne[0].length() == np.getPhraseString().length()) {
                            //System.out.println("Link NE per offset: " + ne[1]);
                            return ne[1];
                        }
                        // Link with CONTAINS is unstable, but sometimes the offsets are not identically, 
                        // because the np extraction and the ne recogniction extract different parts of a real np
                    } else if (np.getPhraseString().contains(namedEntity)) {
                        //System.out.println("Link NE per regex match: " + ne[1]);
                        return ne[1];
                    }
                }
            }
        }
        return "NoNE";
    }


    /*
     public static CombinedArticleNPFeatures setNEClassAsFeature(EnhancedNP np, CombinedArticleNPFeatures combined) {
     //System.out.println("NP get NE Class: " + np.getNEClass());
     combined.setNeClass(np.getNeClass());
     return combined;
     }
     */
}
