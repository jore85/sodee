/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import core.NFArticle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 *
 * @author johan
 */
public class AnnotationWebServiceHandler implements Runnable {

    private final FullTextAnnotationClientXML annotParser;
    private TextAnnotationWSClientv2 annotRESTClient;
    private AnnotationWSClient annotWSClient;
    private String annotWSClientURL;

    private Set<NFArticle> articles;
    private Map<Integer, ArrayList<Annotation>> annotationsOfArticles;

    private int numberOfThreads;
    private int maxArticleSize;
    private HashSet<String> topList_feedTitle;
    private HashSet<String> topList_feedURI;
    private HashSet<String> topList_HostName;
    private HashSet<String> topList_SourceName;
    private boolean isTopListFilterOn;
 

    public AnnotationWebServiceHandler(FullTextAnnotationClientXML annotParser,
            TextAnnotationWSClientv2 annotRESTClient) {
        this.annotParser = annotParser;
        this.annotRESTClient = annotRESTClient;
    }

    public AnnotationWebServiceHandler(FullTextAnnotationClientXML annotParser,
            AnnotationWSClient annotWSClient) {
        this.annotParser = annotParser;
        this.annotWSClient = annotWSClient;
    }
    
    public AnnotationWebServiceHandler(FullTextAnnotationClientXML annotParser, String annotWSClientURL){
        this.annotParser = annotParser;
        this.annotWSClientURL = annotWSClientURL;
    }

    public void setArticlesOfDay(Set<NFArticle> articles) {
        this.articles = articles;
    }

    public Map<Integer, ArrayList<Annotation>> getAnnotationsOfArticles() {
        return annotationsOfArticles;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public void setMaxArticleSize(int maxArticleSize) {
        this.maxArticleSize = maxArticleSize;
    }

    public void setTopList_feedTitle(HashSet<String> topList_feedTitle) {
        this.topList_feedTitle = topList_feedTitle;
    }

    public void setTopList_feedURI(HashSet<String> topList_feedURI) {
        this.topList_feedURI = topList_feedURI;
    }

    public void setTopList_HostName(HashSet<String> topList_HostName) {
        this.topList_HostName = topList_HostName;
    }

    public void setTopList_SourceName(HashSet<String> topList_SourceName) {
        this.topList_SourceName = topList_SourceName;
    }

    public void setIsTopListFilterOn(boolean topListFilterOn) {
        this.isTopListFilterOn = topListFilterOn;
    }

    public void process() {
        this.annotationsOfArticles = new HashMap<>();
        Iterator<NFArticle> articleIterator = this.articles.iterator();
        ExecutorService es = Executors.newFixedThreadPool(this.numberOfThreads);
        List<Future<AnnotationWebServiceResult>> results = new ArrayList<>(this.articles.size());
        List<AnnotationWebProcess> articlesToProcess = new ArrayList<>(this.articles.size());
        while (articleIterator.hasNext()) {
            NFArticle article = articleIterator.next();
            if (article.get_bodyClearText().length() <= this.maxArticleSize) {
                boolean topListPositive = false;
                if (isTopListFilterOn) {
                    topListPositive = topList_feedTitle.contains(article.get_feedTitle())
                            || topList_feedURI.contains(article.get_feedUri())
                            || topList_HostName.contains(article.get_Hostname())
                            || topList_SourceName.contains(article.get_srcName());
                } else {
                    topListPositive = true;
                }
                if (topListPositive) {
                    FullTextAnnotationClientXML annotParserCopy = this.annotParser;
                    TextAnnotationWSClientv2 annotRESTClientCopy = this.annotRESTClient;
                    //AnnotationWSClient annotWSClientCopy = this.annotWSClient;
                    AnnotationWSClient annotWSClientCopy = new AnnotationWSClient(this.annotWSClientURL);
                    //System.err.println(annotWSClientCopy.getAnnotWSURL());
                    //articlesToProcess.add(new AnnotationWebProcess(annotParserCopy,
                    //        annotRESTClientCopy, article.get_bodyClearText(),
                    //        article.get_articleID()));
                    articlesToProcess.add(new AnnotationWebProcess(annotParserCopy,
                            annotWSClientCopy , article.get_bodyClearText(),
                            article.get_articleID()));
                }
            }
        }
        try {
            results = es.invokeAll(articlesToProcess);
        } catch (InterruptedException ex) {
            Logger.getLogger(AnnotationWebServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("No. requests for the annotations service: " + articlesToProcess.size()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        Iterator<Future<AnnotationWebServiceResult>> futureIterator = results.iterator();
        while (futureIterator.hasNext()) {
            Future<AnnotationWebServiceResult> next = futureIterator.next();
            try {
                AnnotationWebServiceResult get = next.get();
                this.annotationsOfArticles.put(get.getArticleID(), new ArrayList<>(get.getAnnotations()));
            } catch (InterruptedException ex) {
                Logger.getLogger(AnnotationWebServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(AnnotationWebServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("No. request results of the annotations service: " + results.size()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        es.shutdown();
    }

    @Override
    public void run() {
        process();
    }

    public static void main(String[] args) {
        FullTextAnnotationClientXML annotParser = new FullTextAnnotationClientXML();
        //TextAnnotationWSClientv2 annotRESTClient = new TextAnnotationWSClientv2("http://scc-vm-151.scc.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score", false);
        AnnotationWSClient annotRESTClient = new AnnotationWSClient("http://scc-vm-151.scc.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score");
        String bodyText = "The Senate on Thursday confirmed the two former Republican presidential & candidates to serve in President Trump’s Cabinet after a long delay. ";
        AnnotationWebProcess toProcessing = new AnnotationWebProcess(annotParser, annotRESTClient, bodyText, 0);
        List<Annotation> annots = new ArrayList<>();
        try {
            AnnotationWebServiceResult result = toProcessing.call();
            annots = result.getAnnotations();
        } catch (Exception ex) {
            Logger.getLogger(AnnotationWebServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        Iterator<Annotation> iterator = annots.iterator();
        while (iterator.hasNext()) {
            Annotation next = iterator.next();
            System.out.println(next.getMention() + " " + next.getKb_entity()
                    + " start: " + next.getStartOffSet()
                    + " end: " + next.getEndOffSet());
        }
    }

}
