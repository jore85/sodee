/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package AnnotationProcessing;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HttpURLConnectionFactory;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author johan
 */
public class TextAnnotationWSClientv2 {

    private final WebResource webResource;
    private Client client;
    // so far monolingual
    private String TEXT_ANNOT_BASE_URI; //"http://141.52.223.38:8080/text-annotation-with-offset-Nov14";

    public TextAnnotationWSClientv2(String TEXT_ANNOT_BASE_URI, boolean withProxy) {
        ClientConfig config = new DefaultClientConfig();

        if (withProxy == true) {
            client = createClientWithProxyConnection(config);
        } else {
            client = Client.create(config);
        }
        webResource = client.resource(TEXT_ANNOT_BASE_URI);
    }

    private Client createClientWithProxyConnection(ClientConfig config) {
        // with proxy
        return client = new Client(new URLConnectionClientHandler(
                new HttpURLConnectionFactory() {
                    Proxy p = null;

                    @Override
                    public HttpURLConnection getHttpURLConnection(URL url)
                    throws IOException {
                        if (p == null) {
                            if (System.getProperties().containsKey("http.proxyHost")) {
                                p = new Proxy(Proxy.Type.HTTP,
                                        new InetSocketAddress(
                                                System.getProperty("http.proxyHost"),
                                                Integer.getInteger("http.proxyPort", 80)));
                            } else {
                                p = Proxy.NO_PROXY;
                            }
                        }
                        return (HttpURLConnection) url.openConnection(p);
                    }
                }), config);
    }

    /**
     * input is one string, should be the whole input text document which is
     * annotated (2nd annotation version) TODO: replace with better xml
     * processing. not only get string back.
     *
     * @param docAsString
     * @return
     * @throws UniformInterfaceException
     */
    public String retrieveXMLFromServiceGivenWholeDoc(String docAsString)
            throws UniformInterfaceException {
//			// first escape '&', otherwise SAX exception in XML parsing
//			// TODO: in result set, transform back to & ?!?
//			docAsString = docAsString.replaceAll( "(&(?!amp;))", "&amp;" );
        // replaced by docAsString = StringEscapeUtils.escapeXml(docAsString); already in previous class (search for it), probably XlikeExtract_Pred...(so that also SRL input has same string and so that offsets are the same!)
//			System.out.println("After escaping:");
//			System.out.println(docAsString);
        // try several times, if we get an exception...
        for (int retries = 0;; retries++) { // http://stackoverflow.com/questions/13239972/how-do-you-implement-a-re-try-catch
            try {

                // POST method
                // DON'T FORGE .path("/"), otherwise empty result!
                // TODO: better XML transformation!
                String strToTransfer = "<item><text>" + docAsString + "</text></item>";
                ClientResponse response = webResource.path("/").accept("text/xml").type("application/xml").post(ClientResponse.class, strToTransfer);
		        // accept("application/xml").type("application/xml")

                // check response status code
                if (response.getStatus() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + response.getStatus());
                }
                if (response.getLength() == 0) {
                    System.err.print("Return Length: " + response.getLength());
                    System.err.print(" Input text maybe too long or special chars, so no end tags.\n");
//		        	System.out.print("Text to be annotated is/was:" + docAsString);
                    //System.out.println();
                    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<item>\n<text>\r\n</text>\n<WikifiedText></WikifiedText>\n<annotations>\n</annotations>\n</item>";
                    // try again!
//		        	throw new RuntimeException("Failed. Too short response. "
//		                    + response.getStatus());
                }

                // display response
                String outputXml = response.getEntity(String.class);
                //System.err.println("Annotation-XML: " + outputXml);
                return outputXml;

            } catch (Exception e) {
                e.printStackTrace();
                if (retries < 5) { // try up to 10 times
                    try {
                        Thread.sleep(2000);         // wait
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    continue;
                } else {
                    // Result was too bad, so return just dummy annotation (no annotation at all):
//			            throw e;
                    System.out.println("not annotated.");
                    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<item>\n<text>\r\n</text>\n<WikifiedText></WikifiedText>\n<annotations>\n</annotations>\n</item>";
                }
            }
        }
    }

    public void close() {
        client.destroy();
    }
}
