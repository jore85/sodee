package AnnotationProcessing;

import Utility.Helper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 *
 * @author jore
 */
public class AnnotationParser extends Thread {

    String annotationsDirectory;
    long parsedAnnotations;
    String annotFilesDir;

    Map<Integer, ArrayList<Annotation>> parsedAnnotationsMap;

    public AnnotationParser(String annotationsDirectory) {
        this.annotationsDirectory = annotationsDirectory;
    }

    public long getParsedAnnotations() {
        return parsedAnnotations;
    }

    public void setAnnotFilesDir(String annotFilesDir) {
        this.annotFilesDir = annotFilesDir;
    }

    public Map<Integer, ArrayList<Annotation>> getParsedAnnotationsMap() {
        return parsedAnnotationsMap;
    }

    //old version, could not used for thread
    public HashMap<Integer, ArrayList<Annotation>> getAnnotationsOfDay(String dayDate) {
        String annotationFilePath = annotationsDirectory + dayDate + ".txt";
        System.out.println("Parse annotations from file: " + annotationFilePath);
        HashMap<Integer, ArrayList<Annotation>> annotationMap = new HashMap<>();
        ArrayList<String> lines = Helper.readFileLines(annotationFilePath);

        Iterator<String> lineIterator = lines.iterator();
        while (lineIterator.hasNext()) {
            String line = lineIterator.next();
            Annotation ann = convertStringIntoAnnotation(line);
            if (annotationMap.containsKey(ann.getDocID())) {
                annotationMap.get(ann.getDocID()).add(ann);
            } else {
                ArrayList<Annotation> annotations = new ArrayList<>();
                annotations.add(ann);
                this.parsedAnnotations = parsedAnnotations + annotations.size();
                annotationMap.put(ann.getDocID(), annotations);
            }
        }
        return annotationMap;
    }

    public Map<Integer, ArrayList<Annotation>> getAnnotationsOfDay() {
        String annotationFilePath = annotationsDirectory + annotFilesDir + ".txt";
        System.out.println("Parse annotations from file: " + annotationFilePath);
        Map<Integer, ArrayList<Annotation>> annotationMap = new HashMap<>();
        ArrayList<String> lines = Helper.readFileLines(annotationFilePath);

        Iterator<String> lineIterator = lines.iterator();
        while (lineIterator.hasNext()) {
            String line = lineIterator.next();
            Annotation ann = convertStringIntoAnnotation(line);
            if (annotationMap.containsKey(ann.getDocID())) {
                annotationMap.get(ann.getDocID()).add(ann);
            } else {
                ArrayList<Annotation> annotations = new ArrayList<>();
                annotations.add(ann);
                this.parsedAnnotations = parsedAnnotations + annotations.size();
                annotationMap.put(ann.getDocID(), annotations);
            }
        }
        System.out.println("End parsing annotations. Annotations parsed: " + this.parsedAnnotations
                + "\t" + DateTime.now(DateTimeZone.UTC));
        return annotationMap;
    }

    //File scheme:
    //ArticleID	mention Start End WikiURL Weight Class
    private Annotation convertStringIntoAnnotation(String line) {
        //System.out.println("Annotation: " + line);
        String[] annElements = line.split("\u0009");
        Annotation ann = null;
        if (annElements.length == 8) {
            for (int i = 0; i < annElements.length; i++) {
                ann = new Annotation(annElements[1], Integer.parseInt(annElements[2]),
                        Integer.parseInt(annElements[3]), annElements[4], Float.parseFloat(annElements[5]),
                        Integer.parseInt(annElements[0]), Integer.parseInt(annElements[6]), annElements[7]);
            }
        } else {
            //ann = new Annotation();
            System.out.println("Line format of the recent line in the annotation file is incorrect.");
        }
        return ann;
    }

    @Override
    public void run() {
        this.parsedAnnotationsMap = getAnnotationsOfDay();
    }

}
