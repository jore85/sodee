package Config;

import java.text.SimpleDateFormat;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
/**
 *
 * @author johan
 */
public class Constants {

    public final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");
    public final static DateTimeFormatter DTF_YYYY_MM_DD = DateTimeFormat.forPattern("yyyy-MM-dd");
    public final static DateTimeFormatter DTF_yyyy_MM_dd_T_HH_mm_ss_Z = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH-mm-ss");
    public final static DateTimeFormatter DTF_YYYYMMDD00 = DateTimeFormat.forPattern("yyyyMMdd" + "00");

    public final static String[] HEAD = {"NP", "articleID", "Timestamp", "Named Entity Class", "PoS-Tag", "isAllCaps", "containsDigitAndAlpha", "containsNonAlpha", "endsWithPeriod", "firstCapital", "firstLetterCapitalized", "containsRealLetter", "hasInternalApostrophe", "hasInternalPeriod", "internalCaps", "isHyphenated", "npPositionInArticle", "articleCoverage", "titleContainsNP", "npOccurenceInArticle", "retrievedDate", "publishedDate", "title", "articleURI", "country", "feedTitle", "dayOfWeek", "lastFullHour", "wikiEntity", "hasRedLink", "pageView24hExist", "pageView24hSum", "pageViewDays7d", "pageViewSum7d", "pageViewDays14d", "pageViewDays7dMin100", "pageViewDays14dMin100", "pageViewSlope24hSlope", "pageViewSlope24hIntercept", "pageViewSlope24hRSquare", "pageViewSlope24hSlopeStdErr", "pageViewSlope24hInterceptStdErr", "pageViewSlope14dSlope", "pageViewSlope14dIntercept", "pageViewSlope14dRSquare", "pageViewSlope14dSlopeStdErr", "pageViewSlope14dInterceptStdErr", "googleNgramFrequency", "googleNgramSlope", "googleNgramR2", "usageSinceYear", "matchCount1899", "percentProperCaps", "npOccurrenceNo1h", "npOccurrenceNo24h", "npOccurrenceSlope24hSlope", "npOccurrenceSlope24hIntercept", "npOccurrenceSlope24hRSquare", "npOccurrenceSlope24hSlopeStdErr", "npOccurrenceSlope24hInterceptStdErr", "mention", "wikiTitle", "wikiURI", "annotationWeight", "noveltyClass", "timeToInsertion", "TargetLabel"};

    public final static String localHostIP = "127.0.0.1";
    public final static String localHost = "localhost";
}
