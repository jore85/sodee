package Config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class provides the paths to all external resources and the DB credentials.
 *
 * @author Johannes Reiss
 */
public final class SODEEConfig {

    private final Properties props;
    private final String propsFilePath;

    public SODEEConfig(String propFilePath) {
        super();
        this.props = new Properties();
        this.propsFilePath = propFilePath;
        boolean propertiesSuccess = getProperties();
        if (propertiesSuccess) {
            setPropsValuesToVariables();
        }
        System.err.println("The following parameters are loaded:\n"
                + toString() + "\n ---");
    }

    public static String startDate = "2015-04-07";
    public static String endDate = "2015-04-21";
    public static String newsPath = "/home/johannes/jre/jsi-small/";
    //public static  String newsPath = "/home/johannes/jre/jsi_newsfeed_2015_copy/";
    public static String outputPath = "/home/johannes/jre/output/";
    //public static  String csvOutput = "/home/johannes/jre/output/csv/";
    public static String modelPathPOSTagger = "/home/johannes/jre/models/stanfordNLP/english-left3words-distsim.tagger";
    public static String rejectionRulesFile = "/home/johannes/jre/models/RBNPE/rejectionRules.txt";
    public static String positiveRulesFile = "/home/johannes/jre/models/RBNPE/positiveRules.txt";
    public static String annotationPath = "/home/johannes/jre/wpm-copy/";
    public static String modelPathNER = "/home/johannes/jre/models/stanfordNLP/english.conll.4class.distsim.crf.ser.gz";
    public static String redLinksFile = "/home/johannes/jre/pagelinks/redlinks-enwiki-20150403.gz";
    //public static  String wikipediaMinerConfigFilePath = "/home/johannes/jre/wikipedia-template-en.xml";
    //public static  String userAgent = "SODEE/0.1 (ugcrf@student.kit.edu)";
    //public static  String ouputLog = "/home/johannes/jre/output/outputSystem.log";
    //public static  String googleNGramIndex = "/media/google-ngrams-bwfilestorage/";
    public static String googleNGramServiceInput = "/home/johannes/jre/NgramService/input/";
    public static String googleNGramServiceOutput = "/home/johannes/jre/NgramService/output/";
    public static String allSurfaceForms = "/home/johannes/jre/wikipedia-entities/sfforms_2017_02_en.txt";
    public static String savedWekaModels = "/home/johannes/jre/models/weka/";
    public static String arffOutput = "/home/johannes/jre/output/arff/";
    public static String sqlite_DB = "/home/johannes/jre/output/sqlite/";
    
    public static String path_topList_feedTitle = "/home/johannes/jre/toplists/top10-feedtitle-mfa.txt";
    public static String path_topList_feedURI = "/home/johannes/jre/toplists/top10-feeduri-mfa.txt";
    public static String path_topList_HostName = "/home/johannes/jre/toplists/top10-hostname-mfa.txt";
    public static String path_topList_SourceName = "/home/johannes/jre/toplists/top10-sourcenames-mfa.txt";
    public static String numberOfThreads = "10";
    public static String maxArticleLength = "4000";
    public static String language = "eng";
    public static String minNPLength = "4";
    public static String maxNPLength = "50";

    public static String vm1_ip = "172.21.40.201";
    public static String vm1_host = "scc-vm-191.scc.kit.edu";
    public static String SQLITE_DB_FILENAME = "sodee.db";
    //public static  String SQLITE_DB_FILENAME_LIVE = "sodee_live.db";
    public static String userAgent = "SODEE/1.2 (ugcrf@student.kit.edu)";
    public static String annotationsWebServiceAddress = "http://scc-vm-151.scc.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score/"; // http://aifb-ls3-vm1.aifb.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score/ http://141.52.223.39:8080/text-annotation-with-offset-Nov14-low-conf-score/
    public static String pageViewAPI_BASE = "http://wikimedia.org/api/rest_v1/metrics/pageviews/";
    public static String savedWekaModel = savedWekaModels + "SVM_06.model";

    public static String DB_NAME = "sodee";
    public static String DB_SERVER = "localhost";
    public static String DB_PORT = "3306";
    public static String ARTICEL_NP_DB_USER = "sodee";
    public static String ARTICEL_NP_DB_PWD = "sodee";
    public static String PAGEVIEW_DB_USER = "NEDRONAA4S";
    public static String PAGEVIEW_DB_PWD = "fU0rM1f85341";
    public static String RESULTS_DB_USER = "sodee";
    public static String RESULTS_DB_PWD = "sodee";

    /**
     * Get/Read the properties from the given properties-file.
     */
    private boolean getProperties() {
        InputStream inStr = null;
        try {
            inStr = new FileInputStream(propsFilePath);
        } catch (FileNotFoundException ex) {
            System.err.println("Something went wrong with loading the properties-file from: "
                    + propsFilePath);
        }
        if (inStr != null) {
            try {
                this.props.load(inStr);
                return true;
            } catch (IOException ex) {
                System.err.println("Something went wrong with loading the properties-file.");
                return false;
            }
        } else {
            return false;
        }

    }

    private void setPropsValuesToVariables() {
        SODEEConfig.startDate = this.props.getProperty("startDate", "2015-04-07");
        SODEEConfig.endDate = this.props.getProperty("endDate", "2015-04-21");
        SODEEConfig.newsPath = this.props.getProperty("newsPath", "/home/johannes/jre/jsi-small/");
        // SODEEConfig.newsPath = this.props.getProperty("newsPath", "/home/johannes/jre/jsi_newsfeed_2015_copy/");
        SODEEConfig.outputPath = this.props.getProperty("outputPath", "/home/johannes/jre/output/");
        // SODEEConfig.csvOutput = this.props.getProperty("csvOutput", "/home/johannes/jre/output/csv/");
        SODEEConfig.modelPathPOSTagger = this.props.getProperty("modelPathPOSTagger", "/home/johannes/jre/models/stanfordNLP/english-left3words-distsim.tagger");
        SODEEConfig.rejectionRulesFile = this.props.getProperty("rejectionRulesFile", "/home/johannes/jre/models/RBNPE/rejectionRules.txt");
        SODEEConfig.positiveRulesFile = this.props.getProperty("positiveRulesFile", "/home/johannes/jre/models/RBNPE/positiveRules.txt");
        SODEEConfig.annotationPath = this.props.getProperty("annotationPath", "/home/johannes/jre/wpm-copy/");
        SODEEConfig.modelPathNER = this.props.getProperty("modelPathNER", "/home/johannes/jre/models/stanfordNLP/english.conll.4class.distsim.crf.ser.gz");
        SODEEConfig.redLinksFile = this.props.getProperty("redLinksFile", "/home/johannes/jre/pagelinks/redlinks-enwiki-20150403.gz");
        // SODEEConfig.wikipediaMinerConfigFilePath = this.props.getProperty("wikipediaMinerConfigFilePath", "/home/johannes/jre/wikipedia-template-en.xml");
        // SODEEConfig.ouputLog = this.props.getProperty("ouputLog", "/home/johannes/jre/output/outputSystem.log");
        // SODEEConfig.googleNGramIndex = this.props.getProperty("googleNGramIndex", "/media/google-ngrams-bwfilestorage/");
        SODEEConfig.googleNGramServiceInput = this.props.getProperty("googleNGramServiceInput", "/home/johannes/jre/NgramService/input/");
        SODEEConfig.googleNGramServiceOutput = this.props.getProperty("googleNGramServiceOutput", "/home/johannes/jre/NgramService/output/");
        SODEEConfig.allSurfaceForms = this.props.getProperty("allSurfaceForms", "/home/johannes/jre/wikipedia-entities/sfforms_2017_02_en.txt");
        SODEEConfig.savedWekaModels = this.props.getProperty("savedWekaModels", "/home/johannes/jre/models/weka/");
        SODEEConfig.arffOutput = this.props.getProperty("arffOutput", "/home/johannes/jre/output/arff/");
        SODEEConfig.sqlite_DB = this.props.getProperty("sqlite_DB", "/home/johannes/jre/output/sqlite/");
        SODEEConfig.path_topList_feedTitle = this.props.getProperty("path_topList_feedTitle", "/home/johannes/jre/toplists/top10-feedtitle-mfa.txt");
        SODEEConfig.path_topList_feedURI = this.props.getProperty("path_topList_feedURI", "/home/johannes/jre/toplists/top10-feeduri-mfa.txt");
        SODEEConfig.path_topList_HostName = this.props.getProperty("path_topList_HostName", "/home/johannes/jre/toplists/top10-hostname-mfa.txt");
        SODEEConfig.path_topList_SourceName = this.props.getProperty("path_topList_SourceName", "/home/johannes/jre/toplists/top10-sourcenames-mfa.txt");
        SODEEConfig.vm1_ip = this.props.getProperty("vm1_ip", "172.21.40.201");
        SODEEConfig.vm1_host = this.props.getProperty("vm1_host", "scc-vm-191.scc.kit.edu");
        SODEEConfig.SQLITE_DB_FILENAME = this.props.getProperty("SQLITE_DB_FILENAME", "sodee.db");
        //SODEEConfig.SQLITE_DB_FILENAME_LIVE = this.props.getProperty("SQLITE_DB_FILENAME_LIVE", "sodee_live.db");
        SODEEConfig.userAgent = this.props.getProperty("userAgent", "SODEE/2.0 (ugcrf@student.kit.edu)");
        SODEEConfig.annotationsWebServiceAddress = this.props.getProperty("annotationsWebServiceIP", "http://scc-vm-151.scc.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score/"); // http://aifb-ls3-vm1.aifb.kit.edu:8080/text-annotation-with-offset-Nov14-low-conf-score/ http://141.52.223.39:8080/text-annotation-with-offset-Nov14-low-conf-score/
        SODEEConfig.pageViewAPI_BASE = this.props.getProperty("pageViewAPI_BASE", "http://wikimedia.org/api/rest_v1/metrics/pageviews/");
        SODEEConfig.savedWekaModel = this.props.getProperty("savedWekaModel", savedWekaModels + "SVM_06.model");
        SODEEConfig.DB_NAME = this.props.getProperty("DB_NAME", "sodee");
        SODEEConfig.DB_SERVER = this.props.getProperty("DB_SERVER", "localhost");
        SODEEConfig.DB_PORT = this.props.getProperty("DB_PORT", "3306");
        SODEEConfig.ARTICEL_NP_DB_USER = this.props.getProperty("ARTICEL_NP_DB_USER", "sodee");
        SODEEConfig.ARTICEL_NP_DB_PWD = this.props.getProperty("ARTICEL_NP_DB_PWD", "sodee");
        SODEEConfig.PAGEVIEW_DB_USER = this.props.getProperty("PAGEVIEW_DB_USER", "NEDRONAA4S");
        SODEEConfig.PAGEVIEW_DB_PWD = this.props.getProperty("PAGEVIEW_DB_PWD", "fU0rM1f85341");
        SODEEConfig.RESULTS_DB_USER = this.props.getProperty("RESULTS_DB_USER", "sodee");
        SODEEConfig.RESULTS_DB_PWD = this.props.getProperty("RESULTS_DB_PWD", "sodee");
        SODEEConfig.numberOfThreads = this.props.getProperty("numberOfThreads", "10");
        SODEEConfig.maxArticleLength = this.props.getProperty("maxArticleLength", "4000");
        SODEEConfig.language = this.props.getProperty("language", "eng");
        SODEEConfig.minNPLength = this.props.getProperty("minNPLength", "4");
        SODEEConfig.maxNPLength = this.props.getProperty("maxNPLength", "50");
    }

    @Override
    public String toString() {
        return SODEEConfig.startDate
                + "\n" + SODEEConfig.endDate
                + "\n" + SODEEConfig.newsPath
                + "\n" + SODEEConfig.outputPath
                + "\n" + SODEEConfig.modelPathPOSTagger
                + "\n" + SODEEConfig.rejectionRulesFile
                + "\n" + SODEEConfig.positiveRulesFile
                + "\n" + SODEEConfig.annotationPath
                + "\n" + SODEEConfig.modelPathNER
                + "\n" + SODEEConfig.redLinksFile
                + "\n" + SODEEConfig.googleNGramServiceInput
                + "\n" + SODEEConfig.googleNGramServiceOutput
                + "\n" + SODEEConfig.allSurfaceForms
                + "\n" + SODEEConfig.savedWekaModels
                + "\n" + SODEEConfig.arffOutput
                + "\n" + SODEEConfig.sqlite_DB
                + "\n" + SODEEConfig.path_topList_feedTitle
                + "\n" + SODEEConfig.path_topList_feedURI
                + "\n" + SODEEConfig.path_topList_HostName
                + "\n" + SODEEConfig.path_topList_SourceName
                + "\n" + SODEEConfig.vm1_ip
                + "\n" + SODEEConfig.vm1_host
                + "\n" + SODEEConfig.SQLITE_DB_FILENAME
                + "\n" + SODEEConfig.userAgent
                + "\n" + SODEEConfig.annotationsWebServiceAddress
                + "\n" + SODEEConfig.pageViewAPI_BASE
                + "\n" + SODEEConfig.savedWekaModel
                + "\n" + SODEEConfig.DB_NAME
                + "\n" + SODEEConfig.DB_SERVER
                + "\n" + SODEEConfig.DB_PORT
                + "\n" + SODEEConfig.ARTICEL_NP_DB_USER
                + "\n" + SODEEConfig.ARTICEL_NP_DB_PWD
                + "\n" + SODEEConfig.PAGEVIEW_DB_USER
                + "\n" + SODEEConfig.PAGEVIEW_DB_PWD
                + "\n" + SODEEConfig.RESULTS_DB_USER
                + "\n" + SODEEConfig.RESULTS_DB_PWD
                + "\n" + SODEEConfig.numberOfThreads
                + "\n" + SODEEConfig.maxArticleLength
                + "\n" + SODEEConfig.language
                + "\n" + SODEEConfig.minNPLength
                + "\n" + SODEEConfig.maxNPLength;
    }

    public static void main(String[] args) {
        SODEEConfig sodeeConf = new SODEEConfig("SODEEConfig.properties");
    }

}
