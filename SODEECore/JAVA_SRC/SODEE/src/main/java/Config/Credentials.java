/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package Config;

/**
 *
 * @author johan
 */
public class Credentials {
   
    public static final String ARTICEL_NP_DB_USER = SODEEConfig.ARTICEL_NP_DB_USER;
    public static final String ARTICEL_NP_DB_PWD = SODEEConfig.ARTICEL_NP_DB_PWD;
    
    public static final String PAGEVIEW_DB_USER = SODEEConfig.PAGEVIEW_DB_USER;
    public static final String PAGEVIEW_DB_PWD = SODEEConfig.PAGEVIEW_DB_PWD;
    
    public static final String RESULTS_DB_USER = SODEEConfig.RESULTS_DB_USER;
    public static final String RESULTS_DB_PWD = SODEEConfig.RESULTS_DB_PWD;
    
}
