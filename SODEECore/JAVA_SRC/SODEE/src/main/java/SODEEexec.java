
import AnnotationProcessing.*;
import Classifier.*;
import Config.*;
import DatabasesNetwork.*;
import FeatureExtraction.*;
import FeatureExtraction.GoogleNGramms.NGramNPProcessor;
import FeatureExtraction.GoogleNGramms.NgramRetriever;
import FeatureExtraction.PeriodicalFeatures.PeriodicalFeatureProcesser;
import FeatureExtraction.WikipediaFeat.PageViewAPIClientNew.WMPageViewAPIClientNew;
import FeatureExtraction.WikipediaFeat.RedLinkChecker;
import Utility.Helper;
import RBBNPE.EnhancedNP;
import NewsProcessing.*;
import RBBNPE.ArchivedNP;
import Utility.ReadFileThread;
import core.NFArticle;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.joda.time.*;

/**
 * The main method for the SODEE core. It represents the full pipeline of
 * retrieval, feature extraction, noun phrase linking and classifying.
 *
 * @author Johannes Reiss
 */
public class SODEEexec {

    public static void main(String[] args) {
        long startMilliSec = DateTime.now().getMillis();
        long endMilliSec = 0;
        long durationRun = 0;
        double durationRunMinutes = 0.0;
        SODEEConfig sodeeConf = new SODEEConfig("./SODEEConfig.properties");
        DateTimeZone.setDefault(DateTimeZone.UTC);
        StringBuilder infoSB = new StringBuilder();
        String infoFile = SODEEConfig.outputPath + "info";
        infoSB.append("Started\t").append(DateTime.now(DateTimeZone.UTC)).append("\n");
        System.out.println("Started: " + DateTime.now(DateTimeZone.UTC));
        Logger.getRootLogger().setLevel(Level.ERROR);

        //System.out.println(DateTime.now(DateTimeZone.forID("Europe/Berlin")));
        final DateTime beginDT = DateTime.now(DateTimeZone.forID("Europe/Berlin"));

        //CREATE A LOCK FILE (AVOID STARTING A SECOND SODEE RUN)
        String lockFilePath = System.getProperty("user.home") + "/sodeeRuns_" + new Random().nextInt(12) + ".lock";
        File lockFile = new File(lockFilePath);
        try {
            lockFile.createNewFile();
            lockFile.deleteOnExit();
            System.out.println("A lock file was created: " + lockFilePath);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(SODEEexec.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        //String pathToGoogleNgramIndex = SODEEConfig.googleNGramIndex;
        //FLUSH THE NGRAM-SERVICE DIRECTORIES
        String ngramServiceInput = SODEEConfig.googleNGramServiceInput;
        String ngramServiceOutput = SODEEConfig.googleNGramServiceOutput;
        if (Helper.cleanDirectory(ngramServiceInput)) {
            System.out.println("Ngram Service input folder is clean.");
        }
        if (Helper.cleanDirectory(ngramServiceOutput)) {
            System.out.println("Ngram Service output (result) folder is clean.");
        }

        //RUNNING OPTIONS
        boolean labelingOn = true; // true -> the retrieved data is labelled, for live run that option must be "false"
        boolean isLiveRunning = false; // true -> the run is a live run, retrieval uses API-Methods and no labelling
        boolean onlyRetrieving = true; // true -> no classifier will be used
        boolean isClassifyOnly = false; // true -> the retrieval part is skipped, data will be load directly from db into the classifier

        int liveMode = Integer.parseInt(args[0]); // 0 = no live mode, 1 = live mode
        String[] periode = null;
        if (liveMode == 0) {
            String startDate = args[1];
            String endDate = args[2];
            periode = Helper.allDates(startDate, endDate);
        }
        int numberOfThreads = Integer.parseInt(SODEEConfig.numberOfThreads);

        //SETUP THE DIRECTORIES
        String newsDirectory = SODEEConfig.newsPath;
        String posTaggerModel = SODEEConfig.modelPathPOSTagger;
        String nerModel = SODEEConfig.modelPathNER;
        String rejectionRulesFile = SODEEConfig.rejectionRulesFile;
        String positiveRulesFile = SODEEConfig.positiveRulesFile;
        String annotationDirectory = SODEEConfig.annotationPath;
        String classifierFile = SODEEConfig.savedWekaModel;

        String annotationsWebServiceIP = SODEEConfig.annotationsWebServiceAddress;

        //SETUP THE FILTER LISTS FOR THE ARTICLE FILTER
        String path_topList_feedTitle = SODEEConfig.path_topList_feedTitle;
        String path_topList_feedURI = SODEEConfig.path_topList_feedURI;
        String path_topList_HostName = SODEEConfig.path_topList_HostName;
        String path_topList_SourceName = SODEEConfig.path_topList_SourceName;
        HashSet<String> topList_feedTitle = new HashSet(Helper.readFileLines(path_topList_feedTitle));
        HashSet<String> topList_feedURI = new HashSet(Helper.readFileLines(path_topList_feedURI));
        HashSet<String> topList_HostName = new HashSet(Helper.readFileLines(path_topList_HostName));
        HashSet<String> topList_SourceName = new HashSet(Helper.readFileLines(path_topList_SourceName));
        int maxArticleLength = Integer.parseInt(SODEEConfig.maxArticleLength); //4000; //process only Articles shorter than maxArticleLength
        String language = SODEEConfig.language; //"eng"; // process onyl articles with the set language

        HashSet<String> surfaceFormsSet = new HashSet<>();

        //OPTIONS FOR THE NP FILTER
        int minNPLength = Integer.parseInt(SODEEConfig.minNPLength); //4; //process only NPs longer than minNPLength
        int maxNPLength = Integer.parseInt(SODEEConfig.maxNPLength); //50; //process only NPs shorter than maxNPLength        
        boolean neFilterOn = true; //a NP is only in further processing, if it's a recognized Named Entity
        boolean isTopListFilterOn = true; //filter with matching list entries, e.g. top 10 hostnames etc.

        boolean hasMySQL = true;
        String userAgent = SODEEConfig.userAgent;
        String pageViewAPI_BASE = SODEEConfig.pageViewAPI_BASE;

        //CLASSIFIER OPTIONS
        HashSet<String> redLinkList_EN = Helper.readGZFilePerLines(SODEEConfig.redLinksFile);
        RedLinkChecker rlChecker = new RedLinkChecker(userAgent);
        rlChecker.setRedLinkList_EN(redLinkList_EN);

        WMPageViewAPIClientNew pvAPIClient = new WMPageViewAPIClientNew(pageViewAPI_BASE, userAgent);
        /*
         ArticleNPPeriodHandler periodHandler
         = new ArticleNPPeriodHandler(posTaggerModel,
         rejectionRulesFile, positiveRulesFile, artclNP_DBConn, nerModel, minNPLength, maxNPLength, maxArticleLength, neFilterOn);
         */
        AnnotationParser annotParser = new AnnotationParser(annotationDirectory);

        //Thread dsWriterThread = new Thread(dsWriter);
        //dsWriterThread.setName("Dataset Writer");
        //ClassificationHandler clHandler = new ClassificationHandler();
        //HashMap<String, Instances> classifiedInstances = new HashMap<>();
        NgramRetriever ngramRetrvr = new NgramRetriever();
        /*
         if (googleNgramBatchThread.getState() == Thread.State.TERMINATED) {
         ngramRetrvr.setGoogleNGramBatchProcesser(googleNgramBatch);
         System.out.println("Google Ngrams index is built. SODEE can start.");
         }
         */
        String newsArchive = "";

        //LIVE SYSTEM OPTIONS
        if (liveMode == 1) {
            System.out.println("SODEE is in live mode...");
            isLiveRunning = true;
            labelingOn = false;
            numberOfThreads = 10;
            newsDirectory = args[1]; // the news files would be found in that directory
            newsArchive = args[2];
            periode = new String[1];
            LocalDateTime dateToday = new LocalDateTime();
            String dayDate = Constants.DTF_YYYY_MM_DD.print(dateToday);
            periode[0] = dayDate;
        } else {
            System.out.println("SODEE is in normal mode...");
        }

        System.out.println("Read surface forms file...\t" + DateTime.now(DateTimeZone.UTC));
        ReadFileThread getSFFileContent = new ReadFileThread(SODEEConfig.allSurfaceForms);
        getSFFileContent.start();

        NPLabeler labeler = new NPLabeler();
        labeler.setLabelingOn(labelingOn);

        //START PROCESSING PERIODICAL DATA
        if (!isClassifyOnly) {
            for (int i = 0; i < periode.length; i++) {
                String dayDate = periode[i];
                DateTime dayDateDT = Constants.DTF_YYYY_MM_DD.parseDateTime(dayDate);
                long dayDateUNIX = dayDateDT.getMillis() / 1000;
                DateTime dayBeforeDT = dayDateDT.minusDays(1);
                long dayBeforeUNIX = dayBeforeDT.getMillis() / 1000;
                System.out.println("Recent day: " + dayDateDT.toString() + " unix: " + dayDateUNIX
                        + " Day before: " + dayBeforeDT.toString() + " unix: " + dayBeforeUNIX);

                String newsFilesDir;
                String annotFilesDir;
                if (isLiveRunning) {
                    newsFilesDir = "";
                    annotFilesDir = "";
                } else {
                    newsFilesDir = dayDate;
                    annotFilesDir = dayDate;
                }

                HashSet<EnhancedNP> nps;
                //HashMap<EnhancedNP, FeatureContainer> npInstances = new HashMap<>();
                HashMap<EnhancedNP, AnnotationFeatures> annotationFeaturesMap = new HashMap<>();
                HashMap<EnhancedNP, WikipediaFeatures> wikiFeaturesMap = new HashMap<>();
                HashMap<Integer, ArticleFeatures> articleFeaturesMap = new HashMap<>();
                HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeaturesMap = new HashMap<>();
                HashMap<EnhancedNP, NgramFeatures> ngramDataMap = new HashMap<>();

                DatasetProcessor dsProcessor = new DatasetProcessor();
                dsProcessor.setLabeler(labeler);

                ArticleNPDatabaseConnector artclNP_DBConn = new ArticleNPDatabaseConnector();
                AnnotationNPLinker annotPeriodHandler = new AnnotationNPLinker();
                annotPeriodHandler.setRlChecker(rlChecker);
                annotPeriodHandler.setPvAPIClient(pvAPIClient);
                annotPeriodHandler.setIsLiveRunning(isLiveRunning);
                //annotPeriodHandler.setSurfaceFormsSet(newSForms);

                AnnotationNPLinkHandler annotPeriodHandlerParallel = new AnnotationNPLinkHandler(userAgent,
                        pageViewAPI_BASE, isLiveRunning);
                annotPeriodHandlerParallel.setNumberOfThreads(numberOfThreads);

                ArticleNPPeriodHandler periodHandler
                        = new ArticleNPPeriodHandler(posTaggerModel,
                                rejectionRulesFile, positiveRulesFile, artclNP_DBConn, nerModel, minNPLength, maxNPLength, maxArticleLength, neFilterOn);

                System.out.println("Begin with processing day: " + dayDate
                        + "\t" + DateTime.now(DateTimeZone.UTC));

                // ARTICLE PARSING
                NewsArticleParser newsParser = new NewsArticleParser(newsDirectory, numberOfThreads);
                System.out.println("Start parsing articles..." + "\t" + DateTime.now(DateTimeZone.UTC));

                HashSet<NFArticle> articlesOfDay
                        = newsParser.getArticlesOfDay(newsFilesDir);
                infoSB.append("Article retrieved\t").append(articlesOfDay.size()).append("\n");
                System.out.println("Article retrieved (no filtering done so far) of Day " + dayDate + ": " + articlesOfDay.size());
                System.out.println("End parsing articles." + "\t" + DateTime.now(DateTimeZone.UTC));

                if (articlesOfDay.size() == 0) {
                    System.out.println("No Articles. SODEE shutdown.");
                    endMilliSec = DateTime.now().getMillis();
                    durationRun = endMilliSec - startMilliSec;
                    durationRunMinutes = ((double) durationRun / 1000.0) / (60.0);
                    System.out.println("Run took (in Minutes):"
                            + "\t" + durationRunMinutes);
                    System.exit(-2);
                }

                // MOVE THE PROCESSED NEWS ARTICLE FILES INTO A ARCHIVE
                boolean moved = false;

                moved = Helper.moveFilesToAnotherDir(newsDirectory, newsArchive);
                if (moved) {
                    System.out.println("The article files have been moved to the archive of processed files."
                            + "\t" + DateTime.now(DateTimeZone.UTC));
                } else {
                    System.out.println("The article files have not been moved to the archive of processed files."
                            + "\t" + DateTime.now(DateTimeZone.UTC));
                }

                //ANNOTATOR
                HashMap<Integer, ArrayList<Annotation>> annotationsOfDay = null;
                AnnotationWebServiceHandler annotWSHandler = null;
                Thread annotWSHandlerThread = null;
                Thread annotParserThread = null;
                if (isLiveRunning) {
                    System.out.println("Start getting annotations from webservice..." + "\t" + DateTime.now(DateTimeZone.UTC));
                    FullTextAnnotationClientXML annotClient = new FullTextAnnotationClientXML();
                    TextAnnotationWSClientv2 annotRESTClient = new TextAnnotationWSClientv2(annotationsWebServiceIP, false);
                    AnnotationWSClient annotWSClient = new AnnotationWSClient(annotationsWebServiceIP);
                    //annotWSHandler = new AnnotationWebServiceHandler(annotClient, annotRESTClient);
                    annotWSHandler = new AnnotationWebServiceHandler(annotClient, annotationsWebServiceIP);
                    annotWSHandler.setIsTopListFilterOn(isTopListFilterOn);
                    annotWSHandler.setNumberOfThreads(numberOfThreads);
                    annotWSHandler.setMaxArticleSize(maxArticleLength);
                    annotWSHandler.setTopList_feedTitle(topList_feedTitle);
                    annotWSHandler.setTopList_feedURI(topList_feedURI);
                    annotWSHandler.setTopList_HostName(topList_HostName);
                    annotWSHandler.setTopList_SourceName(topList_SourceName);
                    annotWSHandler.setArticlesOfDay(articlesOfDay);
                    annotWSHandlerThread = new Thread(annotWSHandler);
                    annotWSHandlerThread.start();
                } else {
                    //ANNOTATION PARSING
                    System.out.println("Start parsing annotations... "
                            + "\t" + DateTime.now(DateTimeZone.UTC));
                    annotParser.setAnnotFilesDir(annotFilesDir);
                    annotParserThread = new Thread(annotParser);
                    annotParserThread.start();
                }

                //NOUNPHRASE EXTRACTION + FEATURE CALCULATION OF ARTICLE, NP and Combined Features
                System.out.println("Start extracting nounphrases and calculating some features..."
                        + "\t" + DateTime.now(DateTimeZone.UTC));

                //SET FILTERS
                periodHandler.setMaxArticleSize(maxArticleLength);
                periodHandler.setTopList_feedTitle(topList_feedTitle);
                periodHandler.setTopList_feedURI(topList_feedURI);
                periodHandler.setTopList_HostName(topList_HostName);
                periodHandler.setTopList_SourceName(topList_SourceName);

                // WAIT FOR THE SURFACE FORMS
                try {
                    getSFFileContent.join(); // wait for loading the surface forms
                    surfaceFormsSet.addAll(getSFFileContent.getFileContent());
                    System.out.println("End parsing surface forms. Size: " + surfaceFormsSet.size() + "\t" + DateTime.now(DateTimeZone.UTC));
                } catch (InterruptedException ex) {
                    java.util.logging.Logger.getLogger(SODEEexec.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                annotPeriodHandlerParallel.setSurfaceFormsSet(surfaceFormsSet);

                //START PROCESSING
                periodHandler.setIsTopListFilterOn(isTopListFilterOn);
                periodHandler.processArticles(articlesOfDay, language);
                infoSB.append("Matched Articles\t").append(periodHandler.getMatchedArticleCount()).append("\n");
                nps = new HashSet<>(periodHandler.getNps());

                if (nps.size() == 0) {
                    System.out.println("No NPs. SODEE shutdown.");
                    if (isLiveRunning) {
                        moved = Helper.moveFilesToAnotherDir(newsDirectory, newsArchive);
                        if (moved) {
                            System.out.println("The article files has been moved to the archive of processed files."
                                    + "\t" + DateTime.now(DateTimeZone.UTC));
                        } else {
                            System.out.println("The article files has not been moved to the archive of processed files."
                                    + "\t" + DateTime.now(DateTimeZone.UTC));
                        }
                        Helper.writeTXTFile(infoSB.toString(), infoFile + "_"
                                + DateTime.now(DateTimeZone.UTC) + ".txt");
                    }
                    endMilliSec = DateTime.now().getMillis();
                    durationRun = endMilliSec - startMilliSec;
                    durationRunMinutes = ((double) durationRun / 1000.0) / (60.0);
                    System.out.println("Run took (in Minutes):"
                            + "\t" + durationRunMinutes);
                    System.exit(-2);
                }

                articleFeaturesMap.putAll(periodHandler.getArticleFeatures());
                combinedFeaturesMap.putAll(periodHandler.getCombinedFeatures());
                //System.out.println("Combined Features size: " + combinedFeaturesMap.size());
                infoSB.append("Nounphrases extracted\t").append(nps.size()).append("\n");
                System.out.println("End extracting nounphrases. Number of Nounphrases extracted: " + nps.size()
                        + "\t" + DateTime.now(DateTimeZone.UTC));

                System.out.println("Free memory " + Runtime.getRuntime().freeMemory() + " of total Heap: " + Runtime.getRuntime().totalMemory());
                newsParser = null;
                articlesOfDay = null; //release the unused article data

                //CALCULATE THE TIMESPAN FOR PERIODICAL FEATURES
                if (isLiveRunning) {
                    DateTime[] oldestNPTime = Helper.getFirstAndLastNPTimestamp(nps);
                    DateTime firstOccurredNPTimestamp = oldestNPTime[0];
                    DateTime lastOccurredNPTimestamp = oldestNPTime[1];
                    dayDate = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.print(firstOccurredNPTimestamp);
                    dayDateDT = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.parseDateTime(dayDate);
                    dayBeforeDT = dayDateDT.minusHours(24);
                    dayBeforeUNIX = dayBeforeDT.getMillis() / 1000;
                    dayDate = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.print(lastOccurredNPTimestamp);
                    dayDateDT = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.parseDateTime(dayDate);
                    dayDateUNIX = dayDateDT.getMillis() / 1000;
                }

                //STARTING THE NGRAM CALCULATION ON AN EXTERN SERVICE
                NGramNPProcessor ngramProc = new NGramNPProcessor(ngramServiceInput, ngramServiceOutput);
                ngramProc.setDayDate(dayDate);
                ngramProc.setNps(new HashSet<>(nps));
                Thread ngramProcThread = new Thread(ngramProc);
                ngramProcThread.start();

                //LINK ANNOTATION WITH NOUNPHRASE + FEATURE CALCULATION OF ANNOTATION FEATURES
                if (isLiveRunning) {
                    try {
                        annotWSHandlerThread.join();
                        annotationsOfDay = new HashMap<>(annotWSHandler.getAnnotationsOfArticles());
                    } catch (InterruptedException ex) {
                        System.err.println("The annotation webservice handler was interrupted. No annotations were parsed.");
                    }
                    System.out.println("End getting annotations from webservice... " + "\t" + DateTime.now(DateTimeZone.UTC));

                } else {
                    try {
                        annotParserThread.join();
                        annotationsOfDay = new HashMap<>(annotParser.getParsedAnnotationsMap());
                    } catch (InterruptedException ex) {
                        System.err.println("The annotation parser was interrupted. No annotations were parsed.");
                    }
                }
                System.out.println("Start linking annotations and calculate annotation features... "
                        + "\t" + DateTime.now(DateTimeZone.UTC));
                System.out.println("Number of nps before annotations processing: " + nps.size());

                //annotPeriodHandler.processAnnotations(annotationsOfDay, nps);
                //annotationFeaturesMap.putAll(new HashMap<>(annotPeriodHandler.getAnnotationFeats()));
                annotPeriodHandlerParallel.processParallel(annotationsOfDay, nps);
                annotationFeaturesMap.putAll(new HashMap<>(annotPeriodHandlerParallel.getAnnotationFeats()));
                wikiFeaturesMap.putAll(new HashMap<>(annotPeriodHandlerParallel.getWikiFeats()));

                System.out.println("Annotations Feature Size: " + annotationFeaturesMap.size());
                //wikiFeaturesMap.putAll(new HashMap<>(annotPeriodHandler.getWikiFeats()));
                System.out.println("Wikipedia Feature Size: " + wikiFeaturesMap.size());
                System.out.println("End linking annotations and calculating annotation features. "
                        + "\t" + DateTime.now(DateTimeZone.UTC));

                /*             
                 //GOOGLE NGRAM RETRIEVING (not used anymore)
                 System.out.println("Start retrieving google ngram features.");
                 ngramRetrvr.setDayDate(dayDate);
                 ngramRetrvr.setNps(nps);
                 ngramRetrvr.setCaseSensitive(Boolean.FALSE);
                 ngramRetrvr.setWithCache(Boolean.FALSE);
                 ngramRetrvr.setN(1);
                 ngramRetrvr.retrieveNGramFeatures(nps, true);
                 ngramDataMap.putAll(ngramRetrvr.getNgramData());
                 System.out.println("End retrieving google ngram features.");

                 dsProcessor.setNgramFeaturesMap(ngramRetrvr.getNgramData());
                 */
                //WAITING FOR FINISHING THE NGRAM-RETRIEVAL
                try {
                    ngramProcThread.join();
                } catch (InterruptedException ex) {
                    System.err.println("Something went wrong on waiting the ngram retrieval comes to an end.");
                }
                ngramDataMap = ngramProc.getNgramFeatures();

                //SET THE DATA FOR THE DB-OUTPUT
                AllNPFeaturesDBConnector allNPFeaturesDB = new AllNPFeaturesDBConnector(hasMySQL);
                allNPFeaturesDB.setLabeler(labeler);
                //allNPFeaturesDB.setAnnotationFeaturesMap(new HashMap<>(annotPeriodHandler.getAnnotationFeats()));
                //allNPFeaturesDB.setWikiFeaturesMap(new HashMap<>(annotPeriodHandler.getWikiFeats()));
                allNPFeaturesDB.setAnnotationFeaturesMap(new HashMap<>(annotPeriodHandlerParallel.getAnnotationFeats()));
                allNPFeaturesDB.setWikiFeaturesMap(new HashMap<>(annotPeriodHandlerParallel.getWikiFeats()));
                allNPFeaturesDB.setNps(new HashSet<>(periodHandler.getNps()));
                allNPFeaturesDB.setArticlesFeaturesMap(new HashMap<>(periodHandler.getArticleFeatures()));
                allNPFeaturesDB.setCombinedFeaturesMap(new HashMap<>(periodHandler.getCombinedFeatures()));
                allNPFeaturesDB.setNgramFeaturesMap(new HashMap<>(ngramDataMap));

                //START INSERTING THE DATA INTO DB
                Thread allNPFeaturesThread = new Thread(allNPFeaturesDB);
                allNPFeaturesThread.start();

                //WAITING FINISHING THE DB WRITE OUT
                try {
                    allNPFeaturesThread.join();
                } catch (InterruptedException ex) {
                    System.err.println("Database (allNPFeatures) insertion was interrupted.");
                }
                allNPFeaturesThread = null;
                nps = allNPFeaturesDB.getNpsNew();
                infoSB.append("Nounphrases (final)\t").append(nps.size()).append("\n");
                allNPFeaturesDB = new AllNPFeaturesDBConnector(hasMySQL);
                System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                        + " of total Heap: " + Runtime.getRuntime().totalMemory()
                        + "\t" + DateTime.now(DateTimeZone.UTC));

                //GET NPs OF THE DAY BEFORE TO CALCULATE THE PERIODICAL FEATURES
                List<ArchivedNP> npsOfDayBefore = new ArrayList<>();
                try {
                    npsOfDayBefore
                            = Helper.convertResultSetToNPList(allNPFeaturesDB.getNPsOfDay(dayDateUNIX, dayBeforeUNIX));
                } catch (SQLException ex) {
                    System.err.println("Something went wrong on retrieve the result set of archived nps into list.");
                }
                System.out.println("No. NPs at day before: " + npsOfDayBefore.size() + "\t"
                        + DateTime.now(DateTimeZone.UTC));

                System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                        + " of total Heap: " + Runtime.getRuntime().totalMemory()
                        + "\t" + DateTime.now(DateTimeZone.UTC));

                //CALCULATING THE PERIODICAL FEATURES
                System.out.println("Start processing periodical features. \t"
                        + DateTime.now(DateTimeZone.UTC));
                PeriodicalFeatureProcesser periodicalProcessor = new PeriodicalFeatureProcesser();
                periodicalProcessor.setNumberOfThreads(numberOfThreads);
                periodicalProcessor.setDayBeforeNPs(npsOfDayBefore);
                periodicalProcessor.setNps(nps);
                //System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                //        + " of total Heap: " + Runtime.getRuntime().totalMemory()
                //        + "\t" + DateTime.now(DateTimeZone.UTC));
                periodicalProcessor.process();
                System.out.println("End processing periodical features. \t"
                        + DateTime.now(DateTimeZone.UTC));
                System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                        + " of total Heap: " + Runtime.getRuntime().totalMemory()
                        + "\t" + DateTime.now(DateTimeZone.UTC));
                System.out.println("Start updating the database... \t"
                        + DateTime.now(DateTimeZone.UTC));
                allNPFeaturesDB.updateNPsPeriodicalFeatures(periodicalProcessor.getPeriodicalFeatMap());

                allNPFeaturesDB = null;

                /*
                 periodicalProcessor.setDbConn(allNPFeaturesDB);
                 periodicalProcessor.processNew(new HashSet<>(periodHandler.getNps()));
                
                 Map<EnhancedNP, List<String[]>> queryNPsWithTime
                 = allNPFeaturesDB.queryNPsWithTime(new HashSet<>(periodHandler.getNps()));
                 PeriodicalFeatureProcesser periodicalProcessor = new PeriodicalFeatureProcesser();
                 periodicalProcessor.setNpsFromDB(queryNPsWithTime);
                 boolean periodicalProcessed = periodicalProcessor.process();
                 HashMap<EnhancedNP, PeriodicalFeatures> periodicalFeatMap = new HashMap<>();
                 if (periodicalProcessed) {
                 periodicalFeatMap.putAll(periodicalProcessor.getPeriodicalFeatMap());
                 allNPFeaturesDB.updateNPsPeriodicalFeatures(periodicalFeatMap);                
                 } else {
                 System.out.println("Something went wrong with processing the periodical features. " + "\t"
                 + DateTime.now(DateTimeZone.UTC));
                 }
                 */
                System.out.println("End updating the database. \t"
                        + DateTime.now(DateTimeZone.UTC));
                System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                        + " of total Heap: " + Runtime.getRuntime().totalMemory()
                        + "\t" + DateTime.now(DateTimeZone.UTC));
                infoSB.append("End\t" + DateTime.now(DateTimeZone.UTC)).append("\n");
                System.out.println("\nEnd with processing day: " + dayDate + "\t"
                        + DateTime.now(DateTimeZone.UTC));

                System.out.println("\nFree memory " + Runtime.getRuntime().freeMemory() + " of total Heap: " + Runtime.getRuntime().totalMemory());
                //END RETRIEVAL FOR DAY, EITHER NEXT DAY OR IF ENABLED TRAINING/CLASSIFYING
            }
        }
        final DateTime endDT = DateTime.now(DateTimeZone.forID("Europe/Berlin"));

        //BEGIN CLASSIFIER SECTION
        if (isClassifyOnly) {
            System.out.println("SODEE runs with isClassifyOnly-Option: " + isClassifyOnly
                    + "\t" + DateTime.now(DateTimeZone.UTC));
            onlyRetrieving = false;
        }
        //IF A RUN IS ONLY FOR RETRIEVAL NO CLASSIFIER NEEDED
        if (isLiveRunning) {
            ModelImporteur modelImporteur = new ModelImporteur(classifierFile);
            DBDataImporteur dbImporteur = new DBDataImporteur(hasMySQL);
            ClassificationPipeline classifierPipe = new ClassificationPipeline(dbImporteur, modelImporteur);
            classifierPipe.setStartDT(beginDT);
            classifierPipe.setEndDT(endDT);
            System.out.println("Start classify the data ... \t"
                    + DateTime.now(DateTimeZone.UTC));
            classifierPipe.process();
            System.out.println("End classify the data ... \t"
                    + DateTime.now(DateTimeZone.UTC));

            Helper.writeTXTFile(infoSB.toString(), infoFile + "_"
                    + DateTime.now(DateTimeZone.UTC) + ".txt");
        }
        System.out.println("Ended: " + DateTime.now(DateTimeZone.UTC));
        endMilliSec = DateTime.now().getMillis();
        durationRun = endMilliSec - startMilliSec;
        durationRunMinutes = ((double) durationRun / 1000.0) / (60.0);
        System.out.println("Run took (in Minutes):"
                + "\t" + durationRunMinutes);
        System.exit(-1);
    }

}
