/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RBBNPE;

/**
 *
 * @author jore
 */
public class NoAppropriateOffsetException extends Exception {
    
    public NoAppropriateOffsetException(String message){
        super(message);
    }
    
}
