/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package RBBNPE;

/**
 *
 * @author johan
 */
public class ArchivedNP {
    
    private final String npString;
    private final long unixTime;
    private final int npOccurrenceInArticle;

    public ArchivedNP(String npString, long unixTime, int npOccurrenceInArticle) {
        this.npString = npString;
        this.unixTime = unixTime;
        this.npOccurrenceInArticle = npOccurrenceInArticle;
    }

    public String getNpString() {
        return npString;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public int getNpOccurrenceInArticle() {
        return npOccurrenceInArticle;
    }
    
}
