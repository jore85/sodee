/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RBBNPE;

import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jore
 */
public class NamedEntityHandler {

    private String modelPath;
    private CRFClassifier<CoreLabel> recogniser;

    public NamedEntityHandler(String modelPath) {
        this.modelPath = modelPath;
        this.recogniser = CRFClassifier.getClassifierNoExceptions(modelPath);
    }

    public HashMap<Integer, ArrayList<String[][]>> recogniseFromDocs(HashMap<Integer, ArrayList<BaseNounPhrase>> allNPs) {
        HashMap<Integer, ArrayList<String[][]>> m = new HashMap<>();
        Iterator<Map.Entry<Integer, ArrayList<BaseNounPhrase>>> iterator = allNPs.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, ArrayList<BaseNounPhrase>> next = iterator.next();
            m.put(next.getKey(), recogniseFromAllNP(next.getValue()));
        }
        return m;
    }

    public ArrayList<String[][]> recogniseFromAllNP(ArrayList<BaseNounPhrase> nps) {
        ArrayList<String[][]> recognisedNPs = new ArrayList<>();
        Iterator<BaseNounPhrase> iterator = nps.iterator();
        while (iterator.hasNext()) {
            BaseNounPhrase next = iterator.next();
            recognisedNPs.add(recognise(next.getPhraseString()));
        }
        return recognisedNPs;
    }

    public String[][] recognise(String phrase) {
        String[][] phraseNE = null;
        List<List<CoreLabel>> recognised = recogniser.classify(phrase);
        Iterator<List<CoreLabel>> iterator = recognised.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            if (i < 1) {
                List<CoreLabel> next = iterator.next();
                phraseNE = new String[next.size()][];
                Iterator<CoreLabel> iterator1 = next.iterator();
                while (iterator1.hasNext()) {
                    String[] w = new String[2];
                    CoreLabel word = iterator1.next();
                    w[0] = word.word();
                    w[1] = word.getString(CoreAnnotations.AnswerAnnotation.class);
                    System.out.println("Next size: " + next.size());
                    System.out.println("phraseNE size: " + phraseNE.length);
                    System.out.println("i: " + i);
                    phraseNE[i] = w;
                    System.out.println("w[0]: " + w[0] + " w[1]: " + w[1]);
                }
                i++;
            }
        }
        return phraseNE;
    }

    public static String toString(String[][] sentence) {
        String s = new String();
        for (int i = 0; i < sentence.length; i++) {
            String[] w = sentence[i];
            if (w != null) {
                if (!w[1].equals("O")) {
                    s = s.concat(w[0] + "||" + w[1] + " ");
                }
            } else {
                s = "Something went wrong on Named Entity Recognition";
            }
        }
        return s.trim();
    }

}
