/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RBBNPE;

import java.util.ArrayList;

/**
 *
 * @author jore
 */
public class RBBNPEExample {
    
    static String modelPath = "H:/StanfordPoS-Models/english-left3words-distsim.tagger";

    public static void main(String[] args) {
        String exampleText = "This text includes some noun phrases and some other phrases. I am only interested in the phrases, which are useful to my project.";
        POSBasedBaseNounPhraseExtractor extractor = new POSBasedBaseNounPhraseExtractor(modelPath, null, null);
        extractor.extractBaseNounPhrasesFromText(exampleText);
        ArrayList<BaseNounPhrase> baseNounPhrases = extractor.getBaseNounPhrases();
        System.out.println(baseNounPhrases);
    }

}
