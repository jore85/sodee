package RBBNPE;

import Config.Constants;
import FeatureExtraction.NPFeatureFunctions;
import java.util.Objects;
import org.joda.time.DateTime;

/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
/**
 *
 * @author johan
 */
public class EnhancedNP {

    private final int articleID;
    private final String dayDateTime;
    private final BaseNounPhrase np;
    private String neClass;

    private String posTag;

    private DateTime timestamp;

    boolean allCaps;
    boolean containsDigitAndAlpha;
    boolean containsNonAlpha;
    boolean endsWithPeriod;
    boolean firstCapital;
    boolean firstLetterCapitalized;
    boolean containsRealLetter;
    boolean hasInternalApostrophe;
    boolean hasInternalPeriod;
    boolean internalCaps;
    boolean isHyphenated;

    int occurrence;

    Boolean targetLabel;

    public EnhancedNP(int articleID, String dayDate, BaseNounPhrase np) {
        this.articleID = articleID;
        this.dayDateTime = dayDate; //yyyy-MM-dd'T'HH-mm-ss'Z'
        this.timestamp = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.parseDateTime(dayDate);
        this.np = np;
        this.posTag = np.getPosTag();
        this.neClass = "";
        calculateFeatures(np.getPhraseString());
    }

    public EnhancedNP(int articleID, String dayDateTime, BaseNounPhrase bnp,
            String neClass, String posTag, DateTime timestamp, boolean allCaps,
            boolean containsDigitAndAlpha, boolean containsNonAlpha, boolean endsWithPeriod,
            boolean firstCapital, boolean firstLetterCapitalized, boolean containsRealLetter,
            boolean hasInternalApostrophe, boolean hasInternalPeriod, boolean internalCaps,
            boolean hyphenated, int occurrence, int targetLabel) {
        this.articleID = articleID;
        this.dayDateTime = dayDateTime;
        this.np = bnp;
        this.neClass = neClass;

        this.posTag = posTag;

        this.timestamp = timestamp;

        this.allCaps = allCaps;
        this.containsDigitAndAlpha = containsDigitAndAlpha;
        this.containsNonAlpha = containsNonAlpha;
        this.endsWithPeriod = endsWithPeriod;
        this.firstCapital = firstCapital;
        this.firstLetterCapitalized = firstLetterCapitalized;
        this.containsRealLetter = containsRealLetter;
        this.hasInternalApostrophe = hasInternalApostrophe;
        this.hasInternalPeriod = hasInternalPeriod;
        this.internalCaps = internalCaps;
        this.isHyphenated = hyphenated;
        this.targetLabel = targetLabel == 1;
        this.occurrence = occurrence;

    }

    public int getArticleID() {
        return articleID;
    }

    public String getDayDateTime() {
        return dayDateTime;
    }

    public BaseNounPhrase getNp() {
        return np;
    }

    public String getNeClass() {
        return neClass;
    }

    public void setNeClass(String neClass) {
        this.neClass = neClass;
    }

    public String getPosTag() {
        return posTag;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isAllCaps() {
        return allCaps;
    }

    public boolean isContainsDigitAndAlpha() {
        return containsDigitAndAlpha;
    }

    public boolean isContainsNonAlpha() {
        return containsNonAlpha;
    }

    public boolean isEndsWithPeriod() {
        return endsWithPeriod;
    }

    public boolean isFirstCapital() {
        return firstCapital;
    }

    public boolean isFirstLetterCapitalized() {
        return firstLetterCapitalized;
    }

    public boolean isContainsRealLetter() {
        return containsRealLetter;
    }

    public boolean isHasInternalApostrophe() {
        return hasInternalApostrophe;
    }

    public boolean isHasInternalPeriod() {
        return hasInternalPeriod;
    }

    public boolean isInternalCaps() {
        return internalCaps;
    }

    public boolean isIsHyphenated() {
        return isHyphenated;
    }

    public void setOccurrence(int occurrence) {
        this.occurrence = occurrence;
    }

    public int getOccurrence() {
        return occurrence;
    }

    public Boolean getTargetLabel() {
        return targetLabel;
    }

    public void setTargetLabel(Integer targetLabel) {
        if (targetLabel == 1) {
            this.targetLabel = true;
        } else if (targetLabel == 0) {
            this.targetLabel = false;
        } else {
            this.targetLabel = null;
        }
    }

    @Override
    public String toString() {
        return "EnhancedNP{" + "articleID=" + articleID + ", dayDate=" + dayDateTime
                + ", np=" + np + ", NEClass=" + neClass + ", posTag=" + posTag
                + ", allCaps=" + allCaps + ", containsDigitAndAlpha=" + containsDigitAndAlpha
                + ", containsNonAlpha=" + containsNonAlpha + ", endsWithPeriod=" + endsWithPeriod
                + ", firstCapital=" + firstCapital + ", firstLetterCapitalized=" + firstLetterCapitalized
                + ", containsRealLetter=" + containsRealLetter + ", hasInternalApostrophe=" + hasInternalApostrophe
                + ", hasInternalPeriod=" + hasInternalPeriod + ", internalCaps=" + internalCaps
                + ", isHyphenated=" + isHyphenated + ", targetLabel=" + targetLabel + '}';
    }

    private void calculateFeatures(String phrase) {
        this.allCaps = NPFeatureFunctions.allCaps(phrase);
        this.containsDigitAndAlpha = NPFeatureFunctions.containsDigitAndAlpha(phrase);
        this.containsNonAlpha = NPFeatureFunctions.containsNonAlpha(phrase);
        this.containsRealLetter = NPFeatureFunctions.containsRealLetter(phrase);
        this.endsWithPeriod = NPFeatureFunctions.endsWithPeriod(phrase);
        this.firstCapital = NPFeatureFunctions.firstCapital(phrase);
        this.firstLetterCapitalized = NPFeatureFunctions.firstLetterCapital(phrase);
        this.hasInternalApostrophe = NPFeatureFunctions.hasInternalApostrophe(phrase);
        this.hasInternalPeriod = NPFeatureFunctions.hasInternalPeriod(phrase);
        this.internalCaps = NPFeatureFunctions.hasInternalCaps(phrase);
        this.isHyphenated = NPFeatureFunctions.isHyphenated(phrase);

    }

    public String[] toValueArray() {
        String[] values = {
            np.getPhraseString(),
            String.valueOf(articleID),
            String.valueOf(dayDateTime),
            neClass,
            posTag,
            String.valueOf(allCaps),
            String.valueOf(containsDigitAndAlpha),
            String.valueOf(containsNonAlpha),
            String.valueOf(endsWithPeriod),
            String.valueOf(firstCapital),
            String.valueOf(firstLetterCapitalized),
            String.valueOf(containsRealLetter),
            String.valueOf(hasInternalApostrophe),
            String.valueOf(hasInternalPeriod),
            String.valueOf(internalCaps),
            String.valueOf(isHyphenated)};
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        EnhancedNP thatNP = (EnhancedNP) o;
        return this.articleID == thatNP.articleID
                && this.np.equals(thatNP.np)
                && this.dayDateTime.equals(thatNP.dayDateTime);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.articleID;
        hash = 79 * hash + Objects.hashCode(this.dayDateTime);
        hash = 79 * hash + Objects.hashCode(this.np);
        return hash;
    }

}
