package NewsProcessing;

import DatabasesNetwork.ArticleNPDatabaseConnector;
import RBBNPE.EnhancedNP;
import FeatureExtraction.CombinedArticleNPFeatures;
import FeatureExtraction.ArticleFeatures;
import FeatureExtraction.FeatureContainer;
import core.NFArticle;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
/**
 *
 * @author johan
 */
public class ArticleNPPeriodHandler extends Thread {

    private final ArticleNPProcessor processor;
    private final ArticleNPDatabaseConnector artclNP_DBConn;
    private HashSet<EnhancedNP> nps;

    private HashMap<EnhancedNP, FeatureContainer> npInstances;

    private HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeatures;
    private HashMap<Integer, ArticleFeatures> articleFeatures;

    private int processedArticlesCount;
    private int processedNounPhrasesCount;
    private int maxArticleSize;
    private int matchedArticleCount;

    private HashSet<String> topList_feedTitle;
    private HashSet<String> topList_feedURI;
    private HashSet<String> topList_HostName;
    private HashSet<String> topList_SourceName;
    private boolean isTopListFilterOn;

    public ArticleNPPeriodHandler(String posTaggerModelPath,
            String rejectionRulesFile, String positiveRulesFile,
            ArticleNPDatabaseConnector artclNP_DBConn, String nerModelPath, int minNPLength, int maxNPLength, int maxArticleSize, boolean neFilterOn) {
        this.artclNP_DBConn = artclNP_DBConn;
        this.processor = new ArticleNPProcessor(posTaggerModelPath,
                positiveRulesFile, rejectionRulesFile, nerModelPath, minNPLength, maxNPLength, maxArticleSize, neFilterOn);
        this.processedArticlesCount = 0;
        this.processedNounPhrasesCount = 0;
    }

    public HashMap<EnhancedNP, FeatureContainer> getNpInstances() {
        return npInstances;
    }

    public void setNpInstances(HashMap<EnhancedNP, FeatureContainer> npInstances) {
        this.npInstances = npInstances;
    }

    public HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> getCombinedFeatures() {
        return combinedFeatures;
    }

    public HashMap<Integer, ArticleFeatures> getArticleFeatures() {
        return articleFeatures;
    }

    public HashSet<EnhancedNP> getNps() {
        return nps;
    }

    public int getProcessedArticlesCount() {
        return processedArticlesCount;
    }

    public int getProcessedNounPhrasesCount() {
        return processedNounPhrasesCount;
    }

    public void setMaxArticleSize(int maxArticleSize) {
        this.maxArticleSize = maxArticleSize;
    }

    public void setTopList_feedTitle(HashSet<String> topList_feedTitle) {
        this.topList_feedTitle = topList_feedTitle;
    }

    public void setTopList_feedURI(HashSet<String> topList_feedURI) {
        this.topList_feedURI = topList_feedURI;
    }

    public void setTopList_HostName(HashSet<String> topList_HostName) {
        this.topList_HostName = topList_HostName;
    }

    public void setTopList_SourceName(HashSet<String> topList_SourceName) {
        this.topList_SourceName = topList_SourceName;
    }

    public void setIsTopListFilterOn(boolean isTopListFilterOn) {
        this.isTopListFilterOn = isTopListFilterOn;
    }

    public int getMatchedArticleCount() {
        return matchedArticleCount;
    }

    /**
     * Extract the noun phrases, named entities, the noun phrase features and the
     * combined article + np features from the given set of articles.
     * 
     * @param articles A set of parsed news articles.
     * @param language The language, which should considered (e.g. eng for English)
     */
    public void processArticles(HashSet<NFArticle> articles, String language) {
        this.nps = new HashSet<>();
        HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeatures
                = new HashMap<>();
        HashMap<Integer, ArticleFeatures> artclFeatures = new HashMap<>();
        Iterator<NFArticle> iterator = articles.iterator();
        int i = 0;
        int j = 0;
        System.out.println("Number of articles to process: " + articles.size());
        while (iterator.hasNext()) {
            NFArticle article = iterator.next();            
            if (article.get_bodyClearText().length() <= this.maxArticleSize) {
                boolean topListPositive = false;
                if(isTopListFilterOn){                    
                    topListPositive = topList_feedTitle.contains(article.get_feedTitle())
                        || topList_feedURI.contains(article.get_feedUri())
                        || topList_HostName.contains(article.get_Hostname())
                        || topList_SourceName.contains(article.get_srcName());
                } else {
                    topListPositive = true;
                }
                if (topListPositive) {
                    if (article.get_lang().equalsIgnoreCase(language)) {
                        j++;
                        
                        try {
                            //System.out.println("Start processing article (" + i + "|" + articles.size() + "): " + article.get_articleID());
                            boolean articleProcessedSuccess = processor.process(article);
                            if (articleProcessedSuccess) {
                                artclFeatures.put(article.get_articleID(), processor.getNewsArtFeatures());
                                combinedFeatures.put(article.get_articleID(), processor.getCombinedFeatures());
                                nps.addAll(processor.getCombinedFeatures().keySet());
                            }

                            //System.out.println("End processing article: " + article.get_articleID());
                            //System.out.println("Processed articles: " + processedArticlesCount);
                            //System.out.println("Processed np: " + nps.size());
                            //articles.remove(article); //remove processed articles to release memory
                        } catch (ParseException ex) {
                            System.err.println("Something went wrong parsing the article: " + article.get_articleID());
                        }
                    }
                }
            }
            i++;
            if (i % 10000 == 0 || i == articles.size()) {
                System.out.println(i + " articles already processed. "
                        + nps.size() + " noun phrases already extracted." + "\t" + DateTime.now(DateTimeZone.UTC));
            }
        }
        this.matchedArticleCount = j;
        System.out.println("Articles matched filter lists: " + j);
        this.articleFeatures = artclFeatures;
        this.combinedFeatures = combinedFeatures;

        this.artclNP_DBConn.setArticleFeatures(articleFeatures);
        this.artclNP_DBConn.setCombinedFeatures(combinedFeatures);
        this.artclNP_DBConn.setNps(nps);

        Thread dbThread = new Thread(this.artclNP_DBConn);
        if (!dbThread.isAlive()) {
            dbThread.start();
            System.out.println("Article+NP Database Connection "
                    + "is started and now alive (" + dbThread.getName() + ").");
        }
        /*
         Thread dbThread = new Thread() {
         @Override
         public void run() {
         artclNP_DBConn.insertIntoDB(combinedFeatures, articleFeatures, nps);
         }
         };
         */
    }

}
