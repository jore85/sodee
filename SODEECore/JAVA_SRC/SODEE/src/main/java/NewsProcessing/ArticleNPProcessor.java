package NewsProcessing;

import Config.Constants;
import RBBNPE.EnhancedNP;
import FeatureExtraction.CombinedArticleNPFeatures;
import FeatureExtraction.ArticleFeatures;
import AnnotationProcessing.NPNELinker;
import RBBNPE.BaseNounPhrase;
import RBBNPE.POSBasedBaseNounPhraseExtractor;
import core.NFArticle;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
/**
 *
 * @author johan
 */
public class ArticleNPProcessor extends Thread {

    int minNPLength;
    int maxNPLength;
    int maxArticleSize;

    ArticleFeatures newsArtFeatures;
    HashMap<EnhancedNP, CombinedArticleNPFeatures> combinedFeatures;

    POSBasedBaseNounPhraseExtractor npExtractor;
    NamedEntityRecognizer ner;
    private final boolean neFilterOn;

    public ArticleNPProcessor(String posTaggerModelPath,
            String rejectionsRulesFile, String positiveRulesFile, String nerModelPath,
            int minNPLength, int maxNPLength, int maxArticleSize, boolean neFilterOn) {
        this.npExtractor = new POSBasedBaseNounPhraseExtractor(posTaggerModelPath,
                rejectionsRulesFile, positiveRulesFile);
        //this.npExtractor = new POSBasedBaseNounPhraseExtractor(posTaggerModelPath);
        this.ner = new NamedEntityRecognizer(nerModelPath);
        this.minNPLength = minNPLength;
        this.maxNPLength = maxNPLength;
        this.maxArticleSize = maxArticleSize;
        this.neFilterOn = neFilterOn;
    }

    public ArticleFeatures getNewsArtFeatures() {
        return newsArtFeatures;
    }

    public HashMap<EnhancedNP, CombinedArticleNPFeatures> getCombinedFeatures() {
        return combinedFeatures;
    }

    public POSBasedBaseNounPhraseExtractor getNpExtractor() {
        return npExtractor;
    }

    /**
     * Process an article: Extract nps, extract named entities, and calculate
     * the combined article + np features.
     *
     * @param article An parsed article.
     * @return "True" if the process was sucessful.
     * @throws ParseException Throws an exception if something goes wrong with
     * the article processing.
     */
    public boolean process(NFArticle article) throws ParseException {
        boolean success = false;
        if (article != null) {
            if (article.get_bodyClearText() != null) {
                if (article.get_bodyClearText().length() <= maxArticleSize) {
                    this.ner.setClearBodyText(article.get_bodyClearText());
                    Thread nerThread = new Thread(this.ner);
                    nerThread.start(); // START NER
                    this.newsArtFeatures = new ArticleFeatures(article);
                    this.newsArtFeatures = newsArtFeatures.processFurtherArticleFeatures(article);

                    //System.out.println(article.get_bodyClearText());
                    //NOUN-PHRASE-EXTRACTION
                    //System.out.println("Article Text Length: " + article.get_bodyClearText().length());
                    //System.out.println("Start extracting noun phrases...");
                    npExtractor.extractBaseNounPhrasesFromText(article.get_bodyClearText());
                    ArrayList<BaseNounPhrase> baseNounPhrases = npExtractor.getBaseNounPhrases();
                    //System.out.println("Noun phrases extracted. "
                    //        + "Number of noun phrases found: " + baseNounPhrases.size());
                    ArrayList<String[]> namedEntitiesInArticle = new ArrayList<>();
                    try {
                        nerThread.join();
                        namedEntitiesInArticle.addAll(this.ner.getNamedEntities());
                    } catch (InterruptedException ex) {
                        System.err.println("The Named Entity Recognizer was interrupted. Task " + nerThread.getName());
                    }

                    //CALCULATE COMBINED FEATURES OF NPs and ARTICLE
                    //System.out.println("Start combined feature calculation + linking NE to NP..."
                    //        + "\t" + DateTime.now(DateTimeZone.UTC));
                    this.combinedFeatures = new HashMap<>(baseNounPhrases.size());
                    Iterator<BaseNounPhrase> iterator = baseNounPhrases.iterator();
                    int i = 0;
                    //boolean hasNE = false;
                    //Convert a basis np into a enhanced np & calculate combined features out of the np and the article
                    while (iterator.hasNext()) {
                        boolean hasNE = false;
                        BaseNounPhrase np = iterator.next();
                        if (np != null && np.getPhraseString().length() > minNPLength) { // process only nps > minNPLength
                            if (np.getPhraseString().length() < maxNPLength) {
                                EnhancedNP enhancedNP = new EnhancedNP(article.get_articleID(),
                                        article.get_retrievedDate().toString(Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z), np);//Constants.DTF_YYYY_MM_DD
                                //String extractNEString = NPNELinker.extractNEString(namedEntitiesInArticle, enhancedNP);
                                // NAMED ENTITY and NP LINKING
                                String extractNEString = NPNELinker.getNEString(namedEntitiesInArticle, np);
                                if (neFilterOn) {
                                    // FILTER NE + NP
                                    if (extractNEString.contentEquals("PERSON")) {
                                        hasNE = true;
                                    } else if (extractNEString.contentEquals("LOCATION")) {
                                        hasNE = true;
                                    } else if (extractNEString.contentEquals("ORGANIZATION")) {
                                        hasNE = true;
                                    } else if (extractNEString.contentEquals("MISC")) {
                                        hasNE = true;
                                    }
                                    if (hasNE) {
                                        CombinedArticleNPFeatures combined
                                                = CombinedArticleNPFeatures.calculateArticleNPFeatures(np, article);
                                        enhancedNP.setNeClass(extractNEString);
                                        enhancedNP.setOccurrence(combined.getNpOccurenceInArticle());
                                        this.combinedFeatures.put(enhancedNP, combined);
                                        i++;
                                        success = true;
                                        hasNE = false;
                                    }
                                } else {
                                    CombinedArticleNPFeatures combined
                                            = CombinedArticleNPFeatures.calculateArticleNPFeatures(np, article);
                                    enhancedNP.setNeClass(extractNEString);
                                    enhancedNP.setOccurrence(combined.getNpOccurenceInArticle());
                                    this.combinedFeatures.put(enhancedNP, combined);
                                    success = true;
                                }
                            }

                        }
                    }
                    //System.out.println("There were " + i + " NPs with a NE-Tag");

                    //System.out.println("End combined feature calculation + linking NE to NP... "
                    //        + "\t" + DateTime.now(DateTimeZone.UTC));
                }
            }
        }
        return success;
    }

}
