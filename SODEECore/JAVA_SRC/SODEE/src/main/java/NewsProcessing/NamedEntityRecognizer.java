/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package NewsProcessing;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author johan
 */
public class NamedEntityRecognizer implements Runnable {

    private final String NERModelPath;
    private AbstractSequenceClassifier<CoreLabel> recognizer;
    private ArrayList<String[]> namedEntities;
    private String clearBodyText;

    public NamedEntityRecognizer(String NERModelPath) {
        this.NERModelPath = NERModelPath;
        try {
            this.recognizer = CRFClassifier.getClassifier(this.NERModelPath);
        } catch (IOException ex) {
            Logger.getLogger(NamedEntityRecognizer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassCastException ex) {
            Logger.getLogger(NamedEntityRecognizer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(NamedEntityRecognizer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String[]> getNamedEntities() {
        return namedEntities;
    }

    public void setClearBodyText(String clearBodyText) {
        this.clearBodyText = clearBodyText;
    }    

    private String recognizeNE(String text) {
        return recognizer.classifyToString(text, "inlineXML", true);
        //Output is inline xml as descriped as follows: ... <CLASS>Entity</CLASS> ...
    }   
    
    private ArrayList<String[]> parseInlineXMLText(String taggedText) {
        ArrayList<String[]> parsed = new ArrayList<>();
        Pattern inlineXMLTagsPattern = Pattern.compile("(<[\\/]?[PERSON|ORGANIZATION|LOCATION|MISC]*>)"); // 4 class NER
        Matcher matchTags = inlineXMLTagsPattern.matcher(taggedText);
        int startOpeningTagOffset = 0;
        int endOpeningTagOffset = 0;
        int startCloseTagOffset = 0;
        int endCloseTagOffset = 0;
        int entityRegion = 0;
        int tagRegion = 0;
        int originalStartOffsetOfEntity = 0;
        int originalEndOffsetOfEntity = 0;
        String tag;
        String entity;
        String[] ne = new String[4];// [0]: entity string; [1] NE-Class; [2] originalStartOffset; [3] originalEndOffset
        while (matchTags.find()) {            
            String group = matchTags.group();
            
            //first get data of the opening tag and calculate the Startoffset
            if (!group.contains("</")) {                
                startOpeningTagOffset = matchTags.start();
                endOpeningTagOffset = matchTags.end();
                originalStartOffsetOfEntity = startOpeningTagOffset - tagRegion; //substract all tag characters so far
                ne[2] = Integer.toString(originalStartOffsetOfEntity);
                tagRegion = tagRegion + (endOpeningTagOffset - startOpeningTagOffset);
                //System.out.println(tagRegion);
                tag = taggedText.substring(startOpeningTagOffset + 1, endOpeningTagOffset - 1);
                ne[1] = tag;
                //System.out.println(tag);
                endCloseTagOffset = 0;
                
            //second get the data of the closing tag
            } else {                
                startCloseTagOffset = matchTags.start();
                endCloseTagOffset = matchTags.end(); 
                tagRegion = tagRegion + (endCloseTagOffset - startCloseTagOffset);
                startOpeningTagOffset = 0;
            }
            
            //final get the entity string and add to the result list and calculate Endoffset
            if (startCloseTagOffset > 0 && endOpeningTagOffset > 0) {
                entityRegion = startCloseTagOffset - endOpeningTagOffset;
                //System.out.println(entityRegion);
                entity = taggedText.substring(endOpeningTagOffset, startCloseTagOffset);
                //System.out.println(entity);                
                originalEndOffsetOfEntity = originalStartOffsetOfEntity + entity.length();
                ne[0] = entity;
                ne[3] = Integer.toString(originalEndOffsetOfEntity);
                parsed.add(ne);
                ne = new String[4];
                endOpeningTagOffset = 0;
                startCloseTagOffset = 0;                
            }
        }
        return parsed;
    }
    
    public ArrayList<String[]> getAllNamedEntities(String clearBodyText){
        return parseInlineXMLText(recognizeNE(clearBodyText));
    }
    
    @Override
    public void run() {
        this.namedEntities = getAllNamedEntities(this.clearBodyText);        
    }
    
    private void testArray(String clearText, ArrayList<String[]> namedEntities){
        Iterator<String[]> iterator = namedEntities.iterator();
        while(iterator.hasNext()){
            String[] next = iterator.next();
            String ne = next[0];
            String neClass = next[1];
            //System.out.println(next[2]);
            //System.out.println(next[3]);
            int startOffset = Integer.parseInt(next[2]);
            int endOffset = Integer.parseInt(next[3]);
            String originalNEString = clearText.substring(startOffset, endOffset);
            System.out.println(originalNEString);
        }
    }

    public static void main(String[] args) {
        NamedEntityRecognizer namedEntityRecognizer
                = new NamedEntityRecognizer("H:\\Libs\\stanford-ner-2014-06-16\\classifiers\\english.conll.4class.distsim.crf.ser.gz");
        String example = "Good afternoon Rajat Raina, how are you today? "
                + "I go to school at Stanford University, which is located in California.";
        String xmlText = namedEntityRecognizer.recognizeNE(example);
        System.out.println(xmlText);
        ArrayList<String[]> namedEntities = namedEntityRecognizer.parseInlineXMLText(xmlText);
        namedEntityRecognizer.testArray(example, namedEntities);
    }    
}

