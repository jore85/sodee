package NewsProcessing;


import core.ArticleCollectionRetrievalParallel;
import core.NFArticle;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
/**
 *
 * @author johan
 */
public class NewsArticleParser extends Thread {

    String newsDirectory;
    int numberOfThreads;

    long parsedArticlesCount;

    public NewsArticleParser(String newsDirectory, int numberOfThreads) {
        this.newsDirectory = newsDirectory;
        this.numberOfThreads = numberOfThreads;
        this.parsedArticlesCount = 0;
    }

    public void setNewsDirectory(String newsDirectory) {
        this.newsDirectory = newsDirectory;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public long getParsedArticlesCount() {
        return parsedArticlesCount;
    }

    public long addParsedArticlesCount(long parsedArticlesCount) {
        return this.parsedArticlesCount = parsedArticlesCount + parsedArticlesCount;
    }

    public HashSet<NFArticle> getArticlesOfDay(String newsFilesDir) {
        String path = newsDirectory + newsFilesDir;
        System.out.println("Start reading articles from directory: " + path 
                + "\t" + DateTime.now(DateTimeZone.UTC));
        HashSet<NFArticle> articles = new HashSet<>();
        
        try {
            articles = new HashSet(ArticleCollectionRetrievalParallel.
                    getAllNFArticlesFromDayInParallel(path, numberOfThreads));
            parsedArticlesCount = parsedArticlesCount + articles.size();
        } catch (InterruptedException ex) {
            System.err.println("Interrupted Exception at parsing articles...");
            ex.printStackTrace();
        }
        System.out.println("End reading articles from directory: " + path 
                + "\t" + DateTime.now(DateTimeZone.UTC));    
        
        return articles;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
