package Classifier;

import Config.Credentials;
import Config.SODEEConfig;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import weka.core.Instances;
import weka.experiment.InstanceQuery;

/**
 * This class provides functionality to import and export the processed data
 * from and into a database.
 *
 * @author Johannes Reiss
 */
public class DBDataImporteur {

    private String DB_NAME;
    private String DB_SERVER;
    private String DB_PORT;
    private String DB_URL;
    private String DB_USER;
    private String DB_PWD;
    private String SQLITE_PATH;
    private String SQLITE_DB_URL;
    private String SQLITE_DB_FILENAME;

    private Connection conn;

    /**
     * Creates a new data importeur with an db connection.
     *
     * @param hasMYSQL Indicates if an MySQL database exists.
     */
    public DBDataImporteur(boolean hasMYSQL) {
        this.DB_NAME = SODEEConfig.DB_NAME;
        this.DB_SERVER = SODEEConfig.DB_SERVER;
        this.DB_PORT = SODEEConfig.DB_PORT;
        this.DB_URL = "jdbc:mysql://" + DB_SERVER + ":" + DB_PORT + "/" + DB_NAME + "?rewriteBatchedStatements=true";
        this.DB_USER = Credentials.ARTICEL_NP_DB_USER;
        this.DB_PWD = Credentials.ARTICEL_NP_DB_PWD;
        this.SQLITE_PATH = SODEEConfig.sqlite_DB;
        this.SQLITE_DB_FILENAME = SODEEConfig.SQLITE_DB_FILENAME;
        if (hasMYSQL) {
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                try {
                    this.conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PWD);
                    if (this.conn.isValid(0)) {
                        System.out.println("The Database Connection for all NPs Features is valid"
                                + "\t" + DateTime.now(DateTimeZone.UTC));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DBDataImporteur.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(DBDataImporteur.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                this.SQLITE_DB_URL = "jdbc:sqlite:" + this.SQLITE_PATH + this.SQLITE_DB_FILENAME;
                System.out.println("SQLite Path: " + this.SQLITE_DB_URL);
                Class.forName("org.sqlite.JDBC").newInstance();
                try {
                    this.conn = DriverManager.getConnection(SQLITE_DB_URL);
                    if (this.conn.isValid(0)) {
                        System.out.println("The SQLITE Database Connection for all NPs Features is valid"
                                + "\t" + DateTime.now(DateTimeZone.UTC));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DBDataImporteur.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(DBDataImporteur.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Method to obtain the noun phrase and their features from the database.
     * The result are converted into an set of features vectors.
     *
     * @param sqlQuery The database query.
     * @param name The name for the dataset.
     * @param weightForInstances The weight for the instances.
     * @return The obtained and converted instances of the noun phrase data.
     */
    public Instances getInstancesFromDB(String sqlQuery, String name, int weightForInstances) {
        Instances retrieved = SODEEDataInstanceNew.getEmptySODEEInstances(name);
        try {
            Statement createStatement = this.conn.createStatement();
            ResultSet executed = createStatement.executeQuery(sqlQuery);
            // Conversion is done here
            retrieved = SODEEDataInstanceNew.fillInstanceContainer(executed, retrieved, weightForInstances);
        } catch (SQLException ex) {
            Logger.getLogger(DBDataImporteur.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retrieved;
    }

    /**
     * This method updates the data in the database after the classification
     * took place.
     *
     * @param sqlQuery_UpdateData The update query for the database.
     * @param instancesToUpdate The classified data.
     */
    public void updateInstancesInDB(String sqlQuery_UpdateData, List<ClassifiedInstance> instancesToUpdate) {
        Iterator<ClassifiedInstance> iterator = instancesToUpdate.iterator();
        try {
            //"UPDATE allNPFeatures SET class = ?, probabilityTRUE = ?, probabilityFALSE = ? WHERE rowID = ?"
            PreparedStatement updateDataStmnt = this.conn.prepareStatement(sqlQuery_UpdateData);
            int i = 0;
            while (iterator.hasNext()) {
                ClassifiedInstance classifdInstance = iterator.next();
                double[] probability = classifdInstance.getProbability();
                double probabilityTRUE = 0;
                double probabilityFALSE = 0;
                if (probability.length == 2) {
                    probabilityTRUE = probability[1];
                    probabilityFALSE = probability[0];
                }
                updateDataStmnt.setBoolean(1, classifdInstance.getLabelAsBoolean());
                updateDataStmnt.setDouble(2, probabilityTRUE);
                updateDataStmnt.setDouble(3, probabilityFALSE);
                updateDataStmnt.setInt(4, classifdInstance.getRowID());
                updateDataStmnt.addBatch();
                i++;
                if ((i % 1000) == 0 || i == instancesToUpdate.size()) {
                    updateDataStmnt.execute();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBDataImporteur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
