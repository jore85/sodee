/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package Classifier;

// JAVA DOC: http://weka.sourceforge.net/doc.stable-3-8/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.*;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.Puk;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.WeightedInstancesHandlerWrapper;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;

/**
 *
 * @author johan
 */
public class WekaTest {

    public static void main(String[] args) {

        /*
         FastVector neClassNominalValues = new FastVector(4);
         neClassNominalValues.addElement("PERSON");
         neClassNominalValues.addElement("ORGANISATION");
         neClassNominalValues.addElement("LOCATION");
         neClassNominalValues.addElement("MISC");
         */
        ArrayList<String> neClassNominalValues = new ArrayList<>(4);
        neClassNominalValues.add("PERSON");
        neClassNominalValues.add("ORGANIZATION");
        neClassNominalValues.add("LOCATION");
        neClassNominalValues.add("MISC");

        /*
         FastVector posTags = new FastVector(4);
         posTags.addElement("NN");
         posTags.addElement("NNP");
         posTags.addElement("NNPS");
         posTags.addElement("NNS");
         */
        ArrayList<String> posTags = new ArrayList<>(7);
        posTags.add("NN");
        posTags.add("NNP");
        posTags.add("NNPS");
        posTags.add("NNS");
        posTags.add("JJS");
        posTags.add("JDI");
        posTags.add("JJ");
        posTags.add("");
        posTags.add("CD");
        posTags.add("$");
        posTags.add("WDT");

        /*
         FastVector classValues = new FastVector(2);
         classValues.addElement("true");
         classValues.addElement("false");
         */
        ArrayList<String> classValues = new ArrayList<>(2);
        classValues.add("positive");
        classValues.add("negative");

        /*
         FastVector booleanNominal = new FastVector(2);
         booleanNominal.addElement("TRUE");
         booleanNominal.addElement("FALSE");
         */
        ArrayList<String> booleanNominal = new ArrayList<>(2);
        booleanNominal.add("true");
        booleanNominal.add("false");

        /*
         FastVector dateCountsNominal = new FastVector(24);
         dateCountsNominal.addElement("0");
         dateCountsNominal.addElement("1");
         dateCountsNominal.addElement("2");
         dateCountsNominal.addElement("3");
         dateCountsNominal.addElement("4");
         dateCountsNominal.addElement("5");
         dateCountsNominal.addElement("6");
         dateCountsNominal.addElement("7");
         dateCountsNominal.addElement("8");
         dateCountsNominal.addElement("9");
         dateCountsNominal.addElement("10");
         dateCountsNominal.addElement("11");
         dateCountsNominal.addElement("12");
         dateCountsNominal.addElement("13");
         dateCountsNominal.addElement("14");
         dateCountsNominal.addElement("15");
         dateCountsNominal.addElement("16");
         dateCountsNominal.addElement("17");
         dateCountsNominal.addElement("18");
         dateCountsNominal.addElement("19");
         dateCountsNominal.addElement("20");
         dateCountsNominal.addElement("21");
         dateCountsNominal.addElement("22");
         dateCountsNominal.addElement("23");
         */
        ArrayList<String> dateCountsNominal = new ArrayList<>(24);
        dateCountsNominal.add("0");
        dateCountsNominal.add("1");
        dateCountsNominal.add("2");
        dateCountsNominal.add("3");
        dateCountsNominal.add("4");
        dateCountsNominal.add("5");
        dateCountsNominal.add("6");
        dateCountsNominal.add("7");
        dateCountsNominal.add("8");
        dateCountsNominal.add("9");
        dateCountsNominal.add("10");
        dateCountsNominal.add("11");
        dateCountsNominal.add("12");
        dateCountsNominal.add("13");
        dateCountsNominal.add("14");
        dateCountsNominal.add("15");
        dateCountsNominal.add("16");
        dateCountsNominal.add("17");
        dateCountsNominal.add("18");
        dateCountsNominal.add("19");
        dateCountsNominal.add("20");
        dateCountsNominal.add("21");
        dateCountsNominal.add("22");
        dateCountsNominal.add("23");

        //NP, Named_Entity_Class, Pos, hasRedLink, titleContainsNP, firstCapital, isAllCaps, dayOfWeek, lastFullHour, npOccurrenceNo1h, googleNgramFrequency, npPositionInArticle, articleCoverage, class
        //FastVector sodeeFeaturesVec = new FastVector(14);
        ArrayList<Attribute> sodeeFeatures = new ArrayList<>(21);
        sodeeFeatures.add(new Attribute("rowID"));
        sodeeFeatures.add(new Attribute("NP", (List<String>) null));
        sodeeFeatures.add(new Attribute("Named_Entity_Class", neClassNominalValues));
        sodeeFeatures.add(new Attribute("Pos", posTags));
        sodeeFeatures.add(new Attribute("hasRedLink", booleanNominal));
        sodeeFeatures.add(new Attribute("titleContainsNP", booleanNominal));
        sodeeFeatures.add(new Attribute("firstCapital", booleanNominal));
        sodeeFeatures.add(new Attribute("isAllCaps", booleanNominal));
        sodeeFeatures.add(new Attribute("dayOfWeek", dateCountsNominal));
        sodeeFeatures.add(new Attribute("lastFullHour", dateCountsNominal));
        sodeeFeatures.add(new Attribute("pageView24hSum"));
        sodeeFeatures.add(new Attribute("pageViewSlope24hSlope"));
        sodeeFeatures.add(new Attribute("pageViewSlope24hIntercept"));
        sodeeFeatures.add(new Attribute("npOccurrenceNo1h"));
        sodeeFeatures.add(new Attribute("npOccurrenceNo24h"));
        sodeeFeatures.add(new Attribute("npOccurrenceSlope24hSlope"));
        sodeeFeatures.add(new Attribute("npOccurrenceSlope24hIntercept"));
        sodeeFeatures.add(new Attribute("googleNgramFrequency"));
        sodeeFeatures.add(new Attribute("npPositionInArticle"));
        sodeeFeatures.add(new Attribute("articleCoverage"));
        sodeeFeatures.add(new Attribute("class", classValues));

        Instances fullDataSet = new Instances("SODEE01", sodeeFeatures, 0);
        fullDataSet.setClassIndex(fullDataSet.numAttributes() - 1);

        String SQLITE_DB_URL = "jdbc:sqlite:" + "/media/DataNTFS/sodee.db";
        //String SQLITE_DB_URL = "jdbc:sqlite:" + "F:\\MasterData\\sodee_2015-04-06_2015-04-17.db";

        double trues = 0;
        double falses = 0;
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC").newInstance();
            conn = DriverManager.getConnection(SQLITE_DB_URL);
            String stmntString = "SELECT rowID, NP, Named_Entity_Class, Pos, hasRedLink, "
                    + "titleContainsNP, firstCapital, isAllCaps, dayOfWeek, lastFullHour, pageView24hSum , pageViewSlope24hSlope, pageViewSlope24hIntercept,"
                    + "npOccurrenceNo1h, npOccurrenceNo24h, npOccurrenceSlope24hSlope, npOccurrenceSlope24hIntercept, googleNgramFrequency, npPositionInArticle, "
                    + "articleCoverage, class FROM allNPFeatures WHERE class = 1 ORDER BY RANDOM() LIMIT 200";
            Statement createStatement = conn.createStatement();
            ResultSet executeQuery = createStatement.executeQuery(stmntString);
            while (executeQuery.next()) {
                Instance row = new DenseInstance(fullDataSet.numAttributes());
                row.setValue((Attribute) fullDataSet.attribute(0), executeQuery.getInt("rowID"));
                row.setValue((Attribute) fullDataSet.attribute(1), executeQuery.getString("NP"));
                row.setValue((Attribute) fullDataSet.attribute(2), executeQuery.getString("Named_Entity_Class"));
                //System.out.println(executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(3), executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(4), executeQuery.getBoolean("hasRedLink") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(5), executeQuery.getBoolean("titleContainsNP") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(6), executeQuery.getBoolean("firstCapital") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(7), executeQuery.getBoolean("isAllCaps") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(8), executeQuery.getInt("dayOfWeek"));
                row.setValue((Attribute) fullDataSet.attribute(9), executeQuery.getInt("lastFullHour"));
                row.setValue((Attribute) fullDataSet.attribute(10), executeQuery.getInt("pageView24hSum"));
                row.setValue((Attribute) fullDataSet.attribute(11), executeQuery.getDouble("pageViewSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(12), executeQuery.getDouble("pageViewSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(13), executeQuery.getInt("npOccurrenceNo1h"));
                row.setValue((Attribute) fullDataSet.attribute(14), executeQuery.getInt("npOccurrenceNo24h"));
                row.setValue((Attribute) fullDataSet.attribute(15), executeQuery.getDouble("npOccurrenceSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(16), executeQuery.getDouble("npOccurrenceSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(17), executeQuery.getLong("googleNgramFrequency"));
                row.setValue((Attribute) fullDataSet.attribute(18), executeQuery.getDouble("npPositionInArticle"));
                row.setValue((Attribute) fullDataSet.attribute(19), executeQuery.getDouble("articleCoverage"));
                row.setValue((Attribute) fullDataSet.attribute(20), executeQuery.getInt("class"));
                row.setWeight(1);
                fullDataSet.add(row);
                trues++;
            }
            System.out.println("No. Instances in Dataset: " + fullDataSet.numInstances());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Class.forName("org.sqlite.JDBC").newInstance();
            conn = DriverManager.getConnection(SQLITE_DB_URL);
            String stmntString = "SELECT rowID, NP, Named_Entity_Class, Pos, hasRedLink, "
                    + "titleContainsNP, firstCapital, isAllCaps, dayOfWeek, lastFullHour, pageView24hSum, pageViewSlope24hSlope, pageViewSlope24hIntercept, "
                    + "npOccurrenceNo1h, npOccurrenceNo24h, npOccurrenceSlope24hSlope, npOccurrenceSlope24hIntercept, googleNgramFrequency, npPositionInArticle, "
                    + "articleCoverage, class FROM allNPFeatures WHERE class = 0 ORDER BY RANDOM() LIMIT 100000";
            Statement createStatement = conn.createStatement();
            ResultSet executeQuery = createStatement.executeQuery(stmntString);
            while (executeQuery.next()) {
                Instance row = new DenseInstance(fullDataSet.numAttributes());
                row.setValue((Attribute) fullDataSet.attribute(0), executeQuery.getInt("rowID"));
                row.setValue((Attribute) fullDataSet.attribute(1), executeQuery.getString("NP"));
                row.setValue((Attribute) fullDataSet.attribute(2), executeQuery.getString("Named_Entity_Class"));
                //System.out.println(executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(3), executeQuery.getString("Pos"));
                row.setValue((Attribute) fullDataSet.attribute(4), executeQuery.getBoolean("hasRedLink") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(5), executeQuery.getBoolean("titleContainsNP") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(6), executeQuery.getBoolean("firstCapital") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(7), executeQuery.getBoolean("isAllCaps") ? (1) : (0));
                row.setValue((Attribute) fullDataSet.attribute(8), executeQuery.getInt("dayOfWeek"));
                row.setValue((Attribute) fullDataSet.attribute(9), executeQuery.getInt("lastFullHour"));
                row.setValue((Attribute) fullDataSet.attribute(10), executeQuery.getInt("pageView24hSum"));
                row.setValue((Attribute) fullDataSet.attribute(11), executeQuery.getDouble("pageViewSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(12), executeQuery.getDouble("pageViewSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(13), executeQuery.getInt("npOccurrenceNo1h"));
                row.setValue((Attribute) fullDataSet.attribute(14), executeQuery.getInt("npOccurrenceNo24h"));
                row.setValue((Attribute) fullDataSet.attribute(15), executeQuery.getDouble("npOccurrenceSlope24hSlope"));
                row.setValue((Attribute) fullDataSet.attribute(16), executeQuery.getDouble("npOccurrenceSlope24hIntercept"));
                row.setValue((Attribute) fullDataSet.attribute(17), executeQuery.getLong("googleNgramFrequency"));
                row.setValue((Attribute) fullDataSet.attribute(18), executeQuery.getDouble("npPositionInArticle"));
                row.setValue((Attribute) fullDataSet.attribute(19), executeQuery.getDouble("articleCoverage"));
                row.setValue((Attribute) fullDataSet.attribute(20), executeQuery.getInt("class"));
                int classInt = executeQuery.getInt("class");
                boolean boolClass = classInt == 1;
                row.setValue((Attribute) fullDataSet.attribute(14), boolClass ? (1) : (0));
                row.setWeight(0.4);
                fullDataSet.add(row);
                falses++;
            }
            System.out.println("No. Instances in Dataset: " + fullDataSet.numInstances());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Distribution true: " + (trues / (trues + falses)) * 100);
        System.out.println("Distribution false: " + (falses / (trues + falses)) * 100);

        //fullDataSet = instanceQuery.retrieveInstances(queryString);
        int datasetSize = fullDataSet.numInstances();
        int trainingSetCardinality = Math.round(datasetSize * 0.65F);
        int testSetCardinality = datasetSize - trainingSetCardinality;
        int validationSetCardinality = Math.round(trainingSetCardinality * 0.80F);
        fullDataSet.randomize(new Random(1));
        Instances trainingData;
        Instances testData;
        Instances validateData;
        trainingData = new Instances(fullDataSet, 0, trainingSetCardinality);
        trainingData.setClassIndex(fullDataSet.numAttributes() - 1);
        testData = new Instances(fullDataSet, trainingSetCardinality, testSetCardinality);
        testData.setClassIndex(fullDataSet.numAttributes() - 1);

        //System.out.println("Training: \n" + trainingData.toSummaryString());
        //System.out.println("Training: \n" + testData.toSummaryString());
        System.out.println("Train: " + trainingData.numInstances());
        System.out.println("Test: " + testData.numInstances());
        //System.exit(-1);
        /*
         Remove rm = new Remove();
         String[] removeFilterOptions = {"-R", "1"};
         rm.setOptions(removeFilterOptions);

         StringToWordVector stringToWords = new StringToWordVector();
         String[] stringToWordFilterOptions = {"-R", "2"};
         stringToWords.setOptions(stringToWordFilterOptions);

         StringToNominal stringToNominalFilter = new StringToNominal();
         String[] stringToNomFilterOptions = {"-R", "3-4"};
         stringToNominalFilter.setOptions(stringToNomFilterOptions);

         NumericToNominal numToNominalFilter = new NumericToNominal();
         String[] numToNominalFilterOptions = {"-R", "9-10"};
         numToNominalFilter.setOptions(numToNominalFilterOptions);

         Filter[] allFilters = new Filter[4];
         allFilters[0] = rm;
         allFilters[1] = stringToWords;
         allFilters[2] = stringToNominalFilter;
         allFilters[3] = numToNominalFilter;
         */

        Remove rm = new Remove();
        String[] removeFilterOptions = {"-R", "1-2"};
        try {
            rm.setOptions(removeFilterOptions);
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        StringToWordVector stringToWords = new StringToWordVector();
        String[] stringToWordFilterOptions = {"-R", "2"};
        try {
            stringToWords.setOptions(stringToWordFilterOptions);
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Filter[] allFilters = new Filter[1];
        allFilters[0] = rm;

        MultiFilter filters = new MultiFilter();
        filters.setFilters(allFilters);

        // SVM CLASSIFIER
        SMO svmClassifier = new SMO();
        Puk pukKernel = new Puk();
        pukKernel.setOmega(4);
        pukKernel.setSigma(1);
        try {
            svmClassifier.setKernel(pukKernel);
            svmClassifier.setBuildCalibrationModels(true); // fitting => for a better generating of ROC-Curve
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        // ADABOOST
        // http://weka.sourceforge.net/doc.dev/weka/classifiers/meta/AdaBoostM1.html
        AdaBoostM1 adaBoM1 = new AdaBoostM1();
        String[] adaboOptions = new String[]{"-P", "90", "-I", "15", "-W", "weka.classifiers.functions.SMO"};
        try {
            adaBoM1.setOptions(adaboOptions);
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                /*
         MultilayerPerceptron mlpc = new MultilayerPerceptron();
         String[] mlpcOptions = new String[]{"-H", "i", "-M", "0.5"};
         try {
         mlpc.setOptions(mlpcOptions);
         } catch (Exception ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         }
         */
        
        //Naive Bayes
        NaiveBayes nb = new NaiveBayes();
        nb.setUseKernelEstimator(true);
        nb.setUseSupervisedDiscretization(true);
        
        //MultiBoost
        /*
        MultiBoostAB mboost = new MultiBoostAB();
        mboost.setUseResampling(false);
        mboost.setClassifier(svmClassifier);
        mboost.setWeightThreshold(90);
        mboost.setNumIterations(5);
        */
        
        /*
        
        AttributeSelection attributSelect = new AttributeSelection();
        InfoGainAttributeEval attributEval = new InfoGainAttributeEval();
        CfsSubsetEval correlationFeatureSubSel = new CfsSubsetEval();
        Ranker ranking = new Ranker();
        GreedyStepwise greedy = new GreedyStepwise();
        greedy.setGenerateRanking(true);
        greedy.setConservativeForwardSelection(true);
        //greedy.setSearchBackwards(true);
        try {
        rm.setInputFormat(trainingData);
        
        Instances filteredData = Filter.useFilter(trainingData, rm);
        
        correlationFeatureSubSel.buildEvaluator(filteredData);
        attributEval.buildEvaluator(filteredData);
        attributSelect.setEvaluator(correlationFeatureSubSel);
        //attributSelect.setSearch(greedy);
        //attributSelect.setEvaluator(correlationFeatureSubSel);
        attributSelect.SelectAttributes(filteredData);
        //double[][] rankedAttributes = attributSelect.rankedAttributes();
        System.out.println(attributSelect.toResultsString());
        int[] indices = attributSelect.selectedAttributes();
        System.out.println(Utils.arrayToString(indices));
        } catch (Exception ex) {
        Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        
        // http://weka.sourceforge.net/doc.dev/weka/classifiers/meta/WeightedInstancesHandlerWrapper.html
        WeightedInstancesHandlerWrapper weightHandler = new WeightedInstancesHandlerWrapper();
        String[] weightHandlerOptions = new String[]{"-force-resample-with-weights"};
        try {
            weightHandler.setOptions(weightHandlerOptions);
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        weightHandler.setClassifier(svmClassifier);

        FilteredClassifier fc = new FilteredClassifier();
        fc.setFilter(filters);
        fc.setClassifier(svmClassifier);

        //trainingData.setClassIndex(fullDataSet.numAttributes() - 1);
        //testData.setClassIndex(fullDataSet.numAttributes() - 1);
        Instances unlabeledData = new Instances(testData);

        boolean evaluation = true;
        Attribute rowIDAttr = fullDataSet.attribute(0);
        try {
            System.out.println("Model train...");
            fc.buildClassifier(trainingData);
            System.out.println("Model trained...");
            int rowID;
            if (evaluation == false) {

                String sqlUpdateNPWithPredictedClass = "UPDATE allNPFeatures SET predicted = ? WHERE rowID = ?";
                PreparedStatement updateClass_SQL = null;

                try {
                    conn = DriverManager.getConnection(SQLITE_DB_URL);
                    updateClass_SQL = conn.prepareStatement(sqlUpdateNPWithPredictedClass);
                } catch (SQLException ex) {
                    Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (int i = 0; i < unlabeledData.numInstances(); i++) {
                    rowID = (int) unlabeledData.instance(i).value(rowIDAttr); //get rowID
                    System.out.println("rowID: " + rowID);
                    double predictedLabel = fc.classifyInstance(unlabeledData.instance(i));
                    double[] distributionForInstance = fc.distributionForInstance(unlabeledData.instance(i));
                    unlabeledData.instance(i).setClassValue(predictedLabel);

                    updateClass_SQL.setBoolean(1, predictedLabel == 1.0);
                    updateClass_SQL.setInt(2, rowID);
                    updateClass_SQL.addBatch();
                    if ((i % 1000) == 0 || i == unlabeledData.numInstances()) {
                        updateClass_SQL.execute();
                    }
                }
            } else {
                Evaluation eval = new Evaluation(trainingData);
                eval.evaluateModel(fc, testData);
                int[] classDistribution = calcNumberInstancesOfClass(testData);
                System.out.println("No. \"positive\": " + classDistribution[0]);
                System.out.println("No. \"negative\": " + classDistribution[1]);
                //System.out.println(eval.toCumulativeMarginDistributionString());
                System.out.println(eval.toSummaryString());
                System.out.println(eval.toClassDetailsString());
                System.out.println(eval.toMatrixString());
                ThresholdCurve tc = new ThresholdCurve();
                Instances curve = tc.getCurve(eval.predictions());               
                generateROCCurve(curve);
                System.out.println("\n LIFT \n");
                generateLIFTCurve(curve, classDistribution[0], classDistribution[1]);
                //eval = new Evaluation(fullDataSet);
                //eval.crossValidateModel(fc, fullDataSet, 10, new Random(5));
                //System.out.println(eval.toSummaryString());
                //System.out.println(eval.toClassDetailsString());
                //System.out.println(eval.toMatrixString());
                /*
                ArrayList<Prediction> predictions = eval.predictions();
                System.out.println("LIFT 5%: " + calculateLIFTMetricThreshold(predictions, 0.05F));
                System.out.println("LIFT 10%: " + calculateLIFTMetricThreshold(predictions, 0.1F));
                System.out.println("LIFT 15%: " + calculateLIFTMetricThreshold(predictions, 0.15F));
                System.out.println("LIFT 30%: " + calculateLIFTMetricThreshold(predictions, 0.3F));
                System.out.println("LIFT 50%: " + calculateLIFTMetricThreshold(predictions, 0.5F));
                System.out.println("LIFT 75%: " + calculateLIFTMetricThreshold(predictions, 0.75F));
                System.out.println("LIFT 90%: " + calculateLIFTMetricThreshold(predictions, 0.9F));
                System.out.println("LIFT 95%: " + calculateLIFTMetricThreshold(predictions, 0.95F));
                System.out.println("LIFT 100%: " + calculateLIFTMetricThreshold(predictions, 1F));
                 */
            }

        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        /*
         //String modelOutputPath = SODEEConfig.savedWekaModels + "smo.model";
         String modelOutputPath = "H://smo.model";
         try {
         ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelOutputPath));
         oos.writeObject(fc);
         oos.flush();
         oos.close();
         } catch (FileNotFoundException ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         }
         fc = null;
         try { 
         ObjectInputStream instr = new ObjectInputStream(new FileInputStream(modelOutputPath));
         fc = (FilteredClassifier) instr.readObject();
         } catch (IOException ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         }
        
         try {            
         fc.buildClassifier(trainingData);
         Evaluation eval = new Evaluation(trainingData);
         eval.evaluateModel(fc, testData);
         System.out.println(eval.toSummaryString());
         } catch (Exception ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         }
         */
        unlabeledData.setClassIndex(unlabeledData.numAttributes() - 1);

        /*
         DatabaseSaver save;
         try {
         save = new DatabaseSaver();
         save.setUrl("jdbc:sqlite:" + "/media/DataNTFS/sodee_classified.db");
         //save.setUser("fracpete");
         //save.setPassword("fracpete");
         save.setInstances(unlabeledData);
         save.setRelationForTableName(false);
         save.setTableName("classifiedNPs");
         save.connectToDatabase();
         save.writeBatch();
         } catch (Exception ex) {
         Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
         }
         */
    }

    public static double calculateLIFTMetric(double tpr, double fpr, int trues, int negatives) {
        double liftMetric = 0;
        double x = tpr + fpr;
        double y = trues + negatives;
        liftMetric = x / y;
        return liftMetric;
    }
    
    public static void generateROCCurve(Instances dataPoints){        
        for(int i = 0; i < dataPoints.numInstances(); i++){
           double fpr = dataPoints.instance(i).value(4);
           double tpr = dataPoints.instance(i).value(5);
           System.out.println(fpr + "," + tpr);
        }
    }
    
    public static void generateLIFTCurve(Instances dataPoints, int trues, int negatives){
        for(int i = 0; i < dataPoints.numInstances(); i++){
            double tpr = dataPoints.instance(i).value(4);            
            double fpr = dataPoints.instance(i).value(5);
            System.out.println(calculateLIFTMetric(tpr, fpr, trues, negatives) + "," + tpr);
        }
    }
    
    public static int[] calcNumberInstancesOfClass(Instances data){
        int numInstances = data.numInstances();
        int[] result = new int[2]; // class "positive": [0] ; class "negative": [1]
        result[0] = 0;
        result[1] = 0;
        for(int i = 0; i < numInstances; i++){
            double classValue = data.get(i).classValue();
            if(classValue == 1){
                result[0]++;
            } else {
                result[1]++;
            }
        }
        return result;
    }

}
