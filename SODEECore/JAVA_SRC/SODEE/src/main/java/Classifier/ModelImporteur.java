package Classifier;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;

/**
 * This class provides functionality to import a trained model into the
 * classification pipeline.
 *
 * @author Johannes Reiss
 */
public class ModelImporteur implements Runnable {

    private final String pathToModel;
    private Classifier importedModel;
    private boolean isImported;

    public ModelImporteur(String pathToModel) {
        this.pathToModel = pathToModel;
        this.isImported = false;
    }

    public Classifier getImportedModel() {
        return importedModel;
    }

    public boolean isIsImported() {
        return isImported;
    }

    /**
     * This method imports the trained model from a given file.
     */
    @Override
    public void run() {
        try {
            ObjectInputStream instr = new ObjectInputStream(new FileInputStream(this.pathToModel));
            this.importedModel = (Classifier) instr.readObject();
            this.isImported = true;
        } catch (IOException ex) {
            Logger.getLogger(ModelImporteur.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ModelImporteur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
