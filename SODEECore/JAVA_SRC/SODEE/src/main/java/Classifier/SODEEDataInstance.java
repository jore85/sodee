package Classifier;

import FeatureExtraction.ArticleFeatures;
import FeatureExtraction.CombinedArticleNPFeatures;
import FeatureExtraction.NgramFeatures;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.EnhancedNP;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.SparseInstance;

/**
 *
 * @author jore
 */
public class SODEEDataInstance {

    private static FastVector features;
    
    private SparseInstance dataInstance;
    private double[] data;

    private int id;
    private String phrase;
    private String retrievedDate; //simpleDateFormat yyyy-MM-dd'T'HH-mm-ss'Z

    private static final Attribute posTag = new Attribute("posTag", (FastVector) null); // String    
    private static final Attribute allCaps = new Attribute("allCaps"); // boolean, true if term consists only of capitalized chars.
    private static final Attribute containsDigitAndAlpha = new Attribute("containsDigitAndAlpha"); // boolean, true if an item contains at least one digit as well as at least one alpha character
    private static final Attribute containsNonAlpha = new Attribute("containsNonAlpha"); // boolean, true if an item contains at least one non alpha character;
    private static final Attribute endsWithPeriod = new Attribute("endsWithPeriod"); // boolean, true if an item contains a period "." at the end
    private static final Attribute firstCapital = new Attribute("firstCapital"); // boolean, true if the first character of an item is a capital letter;
    private static final Attribute firstLetterCapitalized = new Attribute("firstLetterCapitalized"); // boolean, true if the first letter is a capital letter
    private static final Attribute containsRealLetter = new Attribute("containsRealLetter"); // boolean, true if the NP contains at least one "real letter"
    private static final Attribute hasInternalApostrophe = new Attribute("hasInternalApostrophe"); //boolean, true if an item contains an apostrophe
    private static final Attribute hasInternalPeriod = new Attribute("hasInternalPeriod"); // boolean, true if an item contains a period "." internally;
    private static final Attribute internalCaps = new Attribute("internalCaps"); // boolean, true if an item contains at least one capitalized character at any position except the first character;
    private static final Attribute isHyphenated = new Attribute("isHyphenated"); // boolean, true if an item contains a hyphen "-"
    private static final Attribute npLength = new Attribute("npLength"); // int, number of ALL characters in the NP
    private static final Attribute neClass = new Attribute("neClass"); //string, Named Entity Class of a NER
    
    private static final Attribute pageView24hSum = new Attribute("pageView24hSum"); // int, sum of pageview values within the last 24h
    private static final Attribute pageViewDays7d = new Attribute("pageViewDays7d"); // int, given the values of the last 7 days, return number of days where pageview value is > 0 (i.e., a value is existing in the db)
    private static final Attribute pageViewSum7d = new Attribute("pageViewSum7d"); // int, the sum of all pageview values of the last 7 days
    private static final Attribute pageViewDays14d = new Attribute("pageViewDays14d"); // int, given the values of the last 14 days, return number of days where pageview value is > 0 (i.e., a value is existing in the db).
    private static final Attribute pageViewDays7dMin100 = new Attribute("pageViewDays7dMin100"); // int, given the values of the last 7 days, return number of days where pageview value is >= 100.
    private static final Attribute pageViewDays14dMin100 = new Attribute("pageViewDays14dMin100"); // int, given the values of the last 14 days, return number of days where pageview value is >= 100.
    private static final Attribute pageViewSlope24hSlope = new Attribute("pageViewSlope24hSlope"); // double, slope after applying simple linear regression on the pageview values of the last 24 h
    private static final Attribute pageViewSlope24hIntercept = new Attribute("pageViewSlope24hIntercept"); // double, intercept after applying simple linear regression on pageview values of the last 24 h
    private static final Attribute pageViewSlope14dSlope = new Attribute("pageViewSlope14dSlope"); // double, slope after applying simple linear regression on pageview values of the last 14d
    private static final Attribute pageViewSlope14dIntercept = new Attribute("pageViewSlope14dIntercept"); // double, intercept after applying simple linear regression on pageview values of the last 14d

    //TODO: Implement the Wikipedia Categories Features
    private static final Attribute title = new Attribute("title", (FastVector) null); // String, title of the news article
    private static final Attribute country = new Attribute("country", (FastVector) null); // String, country where published
    private static final Attribute feedTitle = new Attribute("feedTitle", (FastVector) null); // String, title of the RSS feed (correlated with feedURI probably)
    private static final Attribute dayOfWeek = new Attribute("dayOfWeek"); // int, weakday, encoded in int
    private static final Attribute lastFullHour = new Attribute("lastFullHour"); // int, hour of the retrieved date of the article

    private static final Attribute npPositionInArticle = new Attribute("npPositionInArticle"); // double, position (beginning of startOffSet) of NP in current article; normalized, in [0,1]. Normalization: startOffset/ArticleLength
    private static final Attribute titleContainsNP = new Attribute("titleContainsNP"); // boolean, true if the title contains the NP
    private static final Attribute articleCoverage = new Attribute("articleCoverage"); // double, the relative (0,1] coverage of the article's bodytext by the NP, values might be very small, alternative: use counted words instead
    private static final Attribute npOccurenceInArticle = new Attribute("npOccurenceInArticle"); // int, the count of how often occure the np in the article's bodytext    

    private static final Attribute googleNgramFrequency = new Attribute("googleNgramFrequency"); //occurrence number of current NP in the Google ngram index
    private static final Attribute googleNgramSlope = new Attribute("googleNgramSlope"); //getSlope() of slope of ngram frequency of current NP in the Google ngram index
    private static final Attribute googleNgramR2 = new Attribute("googleNgramR2"); //R^2 value reg. ngram of current NP in the Google ngram index
    private static final Attribute usageSinceYear = new Attribute("usageSinceYear"); //year since the NP is used according to Google ngram index
    private static final Attribute matchCount1899 = new Attribute("matchCount1899"); //occurrence number of current NP in the Google ngram index in books from 1 (actually 1500) until 1899.
    private static final Attribute percentProperCaps = new Attribute("percentProperCaps"); //the percentage of case-insensitive matches for a NP where all words began with a capital letter

    private static final Attribute targetLabel = new Attribute("targetLabel");

    /*
     public SODEEDataInstance(WikipediaFeatures wikiFeat, NounPhraseFeatures npFeat,
     ArticleFeatures articleFeat, CombinedArticleNPFeatures combiFeat, int label) {
     this.features = new FastVector(getClass().getDeclaredFields().length - 6);
     addFeaturesToVector();
     this.data = new double[features.size()];
     addNounPhraseValues(npFeat);
     addWikiFeatureValues(wikiFeat);
     addArticleFeatureValues(articleFeat);
     addCombinedArticleNPFeatureValues(combiFeat);
     this.data[data.length - 1] = label;
     this.dataInstance = new SparseInstance(1.0, data);
     }
     */
    //with label
    public SODEEDataInstance(ArticleFeatures articleFeat, CombinedArticleNPFeatures combiFeat,
            WikipediaFeatures wikiFeat, NgramFeatures ngramFeat, EnhancedNP npFeat, boolean label) {
        features = new FastVector(getClass().getDeclaredFields().length - 6);
        addFeaturesToVector();
        this.data = new double[features.size()];
        addNounPhraseValues(npFeat);
        //addWikiFeatureValues(wikiFeat);
        addArticleFeatureValues(articleFeat);
        addCombinedArticleNPFeatureValues(combiFeat);
        addWikipediaFeatureValues(wikiFeat);
        addGoogleNGramFeatureValues(ngramFeat);
        this.data[data.length - 1] = label ? 1 : 0;
        this.dataInstance = new SparseInstance(1.0, data);
    }

    //without label
    public SODEEDataInstance(ArticleFeatures articleFeat, CombinedArticleNPFeatures combiFeat, WikipediaFeatures wikiFeat, NgramFeatures ngramFeat, EnhancedNP npFeat) {
        features = new FastVector(getClass().getDeclaredFields().length - 6);
        addFeaturesToVector();
        this.data = new double[features.size()];
        addNounPhraseValues(npFeat);
        //addWikiFeatureValues(wikiFeat);
        addArticleFeatureValues(articleFeat);
        addCombinedArticleNPFeatureValues(combiFeat);
        addWikipediaFeatureValues(wikiFeat);
        addGoogleNGramFeatureValues(ngramFeat);
        this.dataInstance = new SparseInstance(1.0, data);
    }

    public SODEEDataInstance() {
        features = new FastVector(getClass().getDeclaredFields().length - 6);
        addFeaturesToVector();
    }

    public void addFeatureValues(ArticleFeatures articleFeat, CombinedArticleNPFeatures combiFeat, WikipediaFeatures wikiFeat, NgramFeatures ngramFeat, EnhancedNP npFeat) {
        this.data = new double[features.size()];
        addNounPhraseValues(npFeat);
        //addWikiFeatureValues(wikiFeat);
        addArticleFeatureValues(articleFeat);
        addCombinedArticleNPFeatureValues(combiFeat);
        addWikipediaFeatureValues(wikiFeat);
        addGoogleNGramFeatureValues(ngramFeat);
        this.dataInstance = new SparseInstance(1.0, data);
    }

    public FastVector getAttributeVector() {
        return features;
    }

    public Instance getInstance() {
        return this.dataInstance;
    }

    private static void addFeaturesToVector() {
        features.addElement(posTag);
        features.addElement(allCaps);
        features.addElement(containsDigitAndAlpha);
        features.addElement(containsNonAlpha);
        features.addElement(endsWithPeriod);
        features.addElement(firstCapital);
        features.addElement(firstLetterCapitalized);
        features.addElement(containsRealLetter);
        features.addElement(hasInternalApostrophe);
        features.addElement(hasInternalPeriod);
        features.addElement(internalCaps);
        features.addElement(isHyphenated);
        features.addElement(npLength);
        features.addElement(neClass);

        features.addElement(pageView24hSum);
        features.addElement(pageViewDays7d);
        features.addElement(pageViewSum7d);
        features.addElement(pageViewDays14d);
        features.addElement(pageViewDays7dMin100);
        features.addElement(pageViewDays14dMin100);
        features.addElement(pageViewSlope24hSlope);
        features.addElement(pageViewSlope24hIntercept);
        features.addElement(pageViewSlope14dSlope);
        features.addElement(pageViewSlope14dIntercept);

        features.addElement(title);
        features.addElement(country);
        features.addElement(feedTitle);
        features.addElement(dayOfWeek);
        features.addElement(lastFullHour);

        features.addElement(npPositionInArticle);
        features.addElement(titleContainsNP);
        features.addElement(articleCoverage);
        features.addElement(npOccurenceInArticle);

        features.addElement(googleNgramFrequency);
        features.addElement(googleNgramSlope);
        features.addElement(googleNgramR2);
        features.addElement(usageSinceYear);
        features.addElement(matchCount1899);
        features.addElement(percentProperCaps);

        features.addElement(targetLabel);

    }

    private void addNounPhraseValues(EnhancedNP npFeat) {
        this.data[0] = posTag.addStringValue(npFeat.getPosTag());
        this.data[1] = npFeat.isAllCaps() ? 1 : 0;
        this.data[2] = npFeat.isContainsNonAlpha() ? 1 : 0;
        this.data[3] = npFeat.isContainsNonAlpha() ? 1 : 0;
        this.data[4] = npFeat.isEndsWithPeriod() ? 1 : 0;
        this.data[5] = npFeat.isFirstCapital() ? 1 : 0;
        this.data[6] = npFeat.isFirstLetterCapitalized() ? 1 : 0;
        this.data[7] = npFeat.isContainsRealLetter() ? 1 : 0;
        this.data[8] = npFeat.isHasInternalApostrophe() ? 1 : 0;
        this.data[9] = npFeat.isHasInternalPeriod() ? 1 : 0;
        this.data[10] = npFeat.isInternalCaps() ? 1 : 0;
        this.data[11] = npFeat.isIsHyphenated() ? 1 : 0;
        this.data[12] = npFeat.getNp().getPhraseString().length();
        this.data[13] = neClass.addStringValue(npFeat.getNeClass());
    }

    private void addWikipediaFeatureValues(WikipediaFeatures wikiFeat) {
        this.data[14] = wikiFeat.getPageView24hSum();
        this.data[15] = wikiFeat.getPageViewDays7d();
        this.data[16] = wikiFeat.getPageViewSum7d();
        this.data[17] = wikiFeat.getPageViewDays14d();
        this.data[18] = wikiFeat.getPageViewDays7dMin100();
        this.data[19] = wikiFeat.getPageViewDays14dMin100();
        this.data[20] = wikiFeat.getPageViewSlope24hSlope();
        this.data[21] = wikiFeat.getPageViewSlope24hIntercept();
        this.data[22] = wikiFeat.getPageViewSlope14dSlope();
        this.data[23] = wikiFeat.getPageViewSlope14dIntercept();
    }

    private void addArticleFeatureValues(ArticleFeatures articleFeat) {
        this.data[24] = title.addStringValue(articleFeat.getTitle());
        this.data[25] = country.addStringValue(articleFeat.getCountry());
        this.data[26] = feedTitle.addStringValue(articleFeat.getFeedTitle());
        this.data[27] = articleFeat.getDayOfWeek();
        this.data[28] = articleFeat.getLastFullHour();
    }

    private void addCombinedArticleNPFeatureValues(CombinedArticleNPFeatures combiFeat) {
        this.data[29] = combiFeat.getNpPositionInArticle();
        this.data[30] = combiFeat.isTitleContainsNP() ? 1 : 0;
        this.data[31] = combiFeat.getArticleCoverage();
        this.data[32] = combiFeat.getNpOccurenceInArticle();
    }

    private void addGoogleNGramFeatureValues(NgramFeatures ngramFeat) {
        this.data[33] = ngramFeat.getGoogleNgramFrequency();
        this.data[34] = ngramFeat.getGoogleNgramSlope();
        this.data[35] = ngramFeat.getGoogleNgramR2();
        this.data[36] = ngramFeat.getUsageSinceYear();
        this.data[37] = ngramFeat.getMatchCount1899();
        this.data[38] = ngramFeat.getPercentProperCaps();
    }

}
