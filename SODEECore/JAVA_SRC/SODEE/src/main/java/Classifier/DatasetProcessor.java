package Classifier;

import FeatureExtraction.AnnotationFeatures;
import FeatureExtraction.ArticleFeatures;
import FeatureExtraction.CombinedArticleNPFeatures;
import FeatureExtraction.FeatureContainer;
import FeatureExtraction.NgramFeatures;
import FeatureExtraction.PeriodicalFeatures.PeriodicalFeatures;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.EnhancedNP;
import Utility.Helper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

/**
 *
 * @author jore
 */
public class DatasetProcessor {

    private Instances datasetLabeled;
    private Instances datasetUnlabeled;
    //private Instances trainingDataset;
    //private Instances testDataset;

    private HashSet<EnhancedNP> nps;
    private HashMap<EnhancedNP, AnnotationFeatures> annotationFeaturesMap;
    private HashMap<EnhancedNP, WikipediaFeatures> wikiFeaturesMap;
    private HashMap<Integer, ArticleFeatures> articlesFeaturesMap;
    private HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeaturesMap;
    private HashMap<EnhancedNP, NgramFeatures> ngramFeaturesMap;

    private HashMap<EnhancedNP, PeriodicalFeatures> periodicFeaturesMap;

    private Map<String, Map<EnhancedNP, FeatureContainer>> allNPs;

    private NPLabeler labeler;

    /*
     public void setTrainingDataset(Instances trainingDataset) {
     this.trainingDataset = trainingDataset;
     }

     public void setTestDataset(Instances testDataset) {
     this.testDataset = testDataset;
     }
     */
    public void setLabeler(NPLabeler labeler) {
        this.labeler = labeler;
    }

    public void setNps(HashSet<EnhancedNP> nps) {
        this.nps = nps;
    }

    public void setAnnotationFeaturesMap(HashMap<EnhancedNP, AnnotationFeatures> annotationFeaturesMap) {
        this.annotationFeaturesMap = annotationFeaturesMap;
    }

    public void setWikiFeaturesMap(HashMap<EnhancedNP, WikipediaFeatures> wikiFeaturesMap) {
        this.wikiFeaturesMap = wikiFeaturesMap;
    }

    public void setArticlesFeaturesMap(HashMap<Integer, ArticleFeatures> articlesFeaturesMap) {
        this.articlesFeaturesMap = articlesFeaturesMap;
    }

    public void setCombinedFeaturesMap(HashMap<Integer, HashMap<EnhancedNP, CombinedArticleNPFeatures>> combinedFeaturesMap) {
        this.combinedFeaturesMap = combinedFeaturesMap;
    }

    public void setNgramFeaturesMap(HashMap<EnhancedNP, NgramFeatures> ngramFeaturesMap) {
        this.ngramFeaturesMap = ngramFeaturesMap;
    }

    public void setPeriodicFeaturesMap(HashMap<EnhancedNP, PeriodicalFeatures> periodicFeaturesMap) {
        this.periodicFeaturesMap = periodicFeaturesMap;
    }

    public Instances getDatasetLabeled() {
        return datasetLabeled;
    }

    public Instances getDatasetUnlabeled() {
        return datasetUnlabeled;
    }

    public Map<String, Map<EnhancedNP, FeatureContainer>> getAllNPs() {
        return allNPs;
    }

    private Instances addInstance(Instances inst, SODEEDataInstance dataInstance) {
        inst.add(dataInstance.getInstance());
        return inst;
    }

    public void process(String dataSetID, boolean withLabelling, boolean train, boolean test) {
        /*
         if (train && test) {
         this.evalHandler = new EvaluationHandler();
         }
         */
        this.allNPs = new HashMap<>();
        Iterator<EnhancedNP> iterator = nps.iterator();
        SODEEDataInstance sodeeInst = new SODEEDataInstance();
        this.datasetLabeled = new Instances(dataSetID, sodeeInst.getAttributeVector(), nps.size());
        this.datasetUnlabeled = new Instances(dataSetID, sodeeInst.getAttributeVector(), nps.size());
        //this.datasetLabeled = new Instances();
        //this.datasetUnlabeled = new Instances(dataSetID, SODEEDataInstance.getAttributeVector(), nps.size());
        while (iterator.hasNext()) {
            EnhancedNP np = iterator.next();
            int articleID = np.getArticleID();
            HashMap<EnhancedNP, CombinedArticleNPFeatures> combinedFeatures = this.combinedFeaturesMap.get(articleID);
            SODEEDataInstance sodeeDataInstance;
            if (this.articlesFeaturesMap.containsKey(articleID) && combinedFeatures.containsKey(np)
                    && this.wikiFeaturesMap.containsKey(np) && this.ngramFeaturesMap.containsKey(np)
                    && this.annotationFeaturesMap.containsKey(np)) {
                if (withLabelling) {
                    np = labeler.addLabel(np, this.annotationFeaturesMap.get(np));
                    this.datasetLabeled.setClassIndex(sodeeInst.getAttributeVector().size() - 1);
                    sodeeDataInstance = new SODEEDataInstance(this.articlesFeaturesMap.get(articleID),
                            combinedFeatures.get(np), this.wikiFeaturesMap.get(np),
                            this.ngramFeaturesMap.get(np), np, np.getTargetLabel());
                    this.datasetLabeled = addInstance(this.datasetLabeled, sodeeDataInstance);
                } else {
                    sodeeDataInstance = new SODEEDataInstance(this.articlesFeaturesMap.get(articleID),
                            combinedFeatures.get(np), this.wikiFeaturesMap.get(np),
                            this.ngramFeaturesMap.get(np), np);
                    this.datasetUnlabeled = addInstance(this.datasetUnlabeled, sodeeDataInstance);
                    //Instance classifiedInstance = runClassifierSingleInstance("SVM", sodeeDataInstance.getInstance());
                    //this.predictedDataset.add(classifiedInstance);
                }
                //put all nps into a map that represents all retrieved data, that is basis for a further calculating of periodical features 
                String npString = np.getNp().getPhraseString();
                FeatureContainer fc = new FeatureContainer(np, this.articlesFeaturesMap.get(articleID),
                        combinedFeatures.get(np), this.annotationFeaturesMap.get(np),
                        this.wikiFeaturesMap.get(np), this.ngramFeaturesMap.get(np));
                if (!allNPs.containsKey(npString)) {
                    Map<EnhancedNP, FeatureContainer> npsOfString = new HashMap<>();
                    npsOfString.put(np, fc);
                    allNPs.put(npString, npsOfString);
                } else {
                    allNPs.get(npString).put(np, fc);
                }
            }
        }
        System.out.println("The labeled dataset holds " + this.datasetLabeled.numInstances() + " instances");
        System.out.println("The unlabeled dataset holds " + this.datasetUnlabeled.numInstances() + " instances");
        /*
         if (train) {
         Instances[] splitTrainingTestData = splitTrainingTestData(this.datasetLabeled, 0.8f, 1);
         this.trainingDataset = splitTrainingTestData[0];
         this.testDataset = splitTrainingTestData[1];
         if (this.trainingDataset != null) {
         runClassifierBatch("SVM", true);
         }
         }
        
         if (test) {
         if (supportVecMachine.isTrained() && this.testDataset != null) {
         evalHandler.processEvaluation(trainingDataset, testDataset,
         supportVecMachine.getSvm());
         evalHandler.writeEvaluationSummary(dataSetID, evalHandler.getEva(), dataSetID);
         }
         }
         */
    }

    /*
     //modelName: "SVM"; 
     public void runClassifierBatch(String modelName, boolean train) {
     boolean classified = false;
     switch (modelName) {
     case "SVM":
     if (train) {
     supportVecMachine.trainSVM(trainingDataset);
     if (supportVecMachine.isTrained()) {
     System.out.println("SVM is trained.");
     }
     } else if (supportVecMachine.isTrained()) {
     classified = supportVecMachine.classify(this.datasetUnlabeled);
     }
     }
     if (classified) {
     System.out.println("Dataset was classified.");
     }                
     }
     */

    /*
     public Instance runClassifierSingleInstance(String modelName, Instance inst) {
     boolean classified = false;
     switch (modelName) {
     case "SVM":
     if (this.supportVecMachine.isTrained()) {
     inst = this.supportVecMachine.classify(inst);
     } else {
     System.out.println("SVM could not classify due to no training.");
     }
     }
     if (classified) {
     System.out.println("Instance was classified.");
     }
     return inst;
     }
     */
    public static Instances[] splitTrainingTestData(Instances dataset,
            float proportionTrainingData, int seed) {
        Instances[] trainingTestData = new Instances[2]; //[0]: Training Set, [1]: Test Set
        int datasetSize = dataset.numInstances();
        int trainingSetCardinality = Math.round(datasetSize * proportionTrainingData);
        int testSetCardinality = datasetSize - trainingSetCardinality;
        dataset.randomize(new Random(seed));
        trainingTestData[0] = new Instances(dataset, 0, trainingSetCardinality);
        trainingTestData[1] = new Instances(dataset, trainingSetCardinality, testSetCardinality);
        return trainingTestData;
    }

    public static boolean writeWekaARFFToFile(Instances dataset, String filePath) {
        boolean success = false;
        try {
            ArffSaver saver = new ArffSaver();
            saver.setFile(new File(filePath));
            saver.setInstances(dataset);
            saver.writeBatch();
            success = true;
        } catch (IOException ex) {
            Logger.getLogger(DatasetProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return success;
    }

    public List<String[]> toValueArray() {
        int size = this.nps.size();
        System.out.println("NPs size: " + size);
        System.out.println("Article Features Size: " + this.articlesFeaturesMap.size());
        System.out.println("Wikipedia Features Size: " + this.wikiFeaturesMap.size());
        System.out.println("ngram Features Size: " + this.ngramFeaturesMap.size());
        System.out.println("Annotation Features Size: " + this.annotationFeaturesMap.size());

        List<String[]> results = new ArrayList<>(size);
        Iterator<EnhancedNP> npIterator = this.nps.iterator();
        int i = 0;
        while (npIterator.hasNext()) {
            List<String[]> values = new ArrayList<>();
            EnhancedNP np = npIterator.next();
            int id = np.getArticleID();
            //String[] npValues = np.toValueArray();
            //values.add(npValues);

            int completeChecker = 0;

            //System.out.println("NP: " + np.getNp());
            HashMap<EnhancedNP, CombinedArticleNPFeatures> combined = this.combinedFeaturesMap.get(id);
            //System.out.println("Combined Features Size: " + combined.size());
            if (combined.containsKey(np)) {
                /*//System.out.println("is in Combined Features Map");
                 CombinedArticleNPFeatures combinedFeature = combined.get(np);
                 //System.out.println(combinedFeature.toString());
                 String[] combinedValues = combinedFeature.toValueArray();
                 values.add(combinedValues);*/
                completeChecker++;
            }

            if (this.articlesFeaturesMap.containsKey(id)) {
                /*//System.out.println("is in Article Features Map");
                 ArticleFeatures articleFeat = this.articlesFeaturesMap.get(id);
                 //System.out.println(articleFeat.toString());
                 String[] articleValues = articleFeat.toValueArray();
                 values.add(articleValues);*/
                completeChecker++;
            }

            if (this.wikiFeaturesMap.containsKey(np)) {
                /*//System.out.println("is in Wiki Features Map");
                 WikipediaFeatures wikiFeat = this.wikiFeaturesMap.get(np);
                 //System.out.println(wikiFeat.toString());
                 String[] wikiValues = wikiFeat.toValueArray();
                 values.add(wikiValues);*/
                completeChecker++;
            }

            if (this.ngramFeaturesMap.containsKey(np)) {
                /*//System.out.println("is in Ngram Features Map");
                 NgramFeatures ngramFeat = this.ngramFeaturesMap.get(np);
                 //System.out.println(ngramFeat.toString());
                 String[] ngramValues = ngramFeat.toValueArray();
                 values.add(ngramValues);*/
                completeChecker++;
            }

            if (this.annotationFeaturesMap.containsKey(np)) {
                /*//System.out.println("is in Annotation Features Map");
                 AnnotationFeatures annotFeat = this.annotationFeaturesMap.get(np);
                 //System.out.println(annotFeat.toString());
                 String[] annotationValues = annotFeat.toValueArray();
                 values.add(annotationValues);
                 np = labeler.addLabel(np, annotFeat);*/
                completeChecker++;
            }
            
            if(this.periodicFeaturesMap.containsKey(np)) {
                completeChecker++;
            }
            

            //System.out.println("Complete Checker: " + completeChecker);
            if (completeChecker == 6) { // == 5 for a complete dataset, else the csv is not correct formatted
                String[] npValues = np.toValueArray();
                values.add(npValues);
                //System.out.println("is in Combined Features Map");
                CombinedArticleNPFeatures combinedFeature = combined.get(np);
                //System.out.println(combinedFeature.toString());
                String[] combinedValues = combinedFeature.toValueArray();
                values.add(combinedValues);

                //System.out.println("is in Article Features Map");
                ArticleFeatures articleFeat = this.articlesFeaturesMap.get(id);
                //System.out.println(articleFeat.toString());
                String[] articleValues = articleFeat.toValueArray();
                values.add(articleValues);

                //System.out.println("is in Wiki Features Map");
                WikipediaFeatures wikiFeat = this.wikiFeaturesMap.get(np);
                //System.out.println(wikiFeat.toString());
                String[] wikiValues = wikiFeat.toValueArray();
                values.add(wikiValues);

                //System.out.println("is in Ngram Features Map");
                NgramFeatures ngramFeat = this.ngramFeaturesMap.get(np);
                //System.out.println(ngramFeat.toString());
                String[] ngramValues = ngramFeat.toValueArray();
                values.add(ngramValues);
                
                PeriodicalFeatures periodFeat = this.periodicFeaturesMap.get(np);
                String[] periodicValues = periodFeat.toValueArray();
                values.add(periodicValues);

                //System.out.println("is in Annotation Features Map");
                AnnotationFeatures annotFeat = this.annotationFeaturesMap.get(np);
                //System.out.println(annotFeat.toString());
                String[] annotationValues = annotFeat.toValueArray();
                values.add(annotationValues);
                np = labeler.addLabel(np, annotFeat);

                String[] label = {np.getTargetLabel().toString()};
                values.add(label);

                String[] allValues = Helper.combineValueArrays(values);
                results.add(allValues);
                i++;
            }
        }
        double ratio = (((double) i / (double) size) * (double) 100);
        System.out.println(i + " of " + size + " where completly processed. This equates to " + ratio + " Percent");
        return results;
    }

}
