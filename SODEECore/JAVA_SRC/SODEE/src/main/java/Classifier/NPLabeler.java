package Classifier;

import FeatureExtraction.AnnotationFeatures;
import RBBNPE.EnhancedNP;
import Utility.Helper;
import java.util.ArrayList;

/**
 * This class provides a function to label data.
 *
 * @author Johannes Reiss
 */
public class NPLabeler {

    private boolean labelingOn;

    public void setLabelingOn(boolean labelingOn) {
        this.labelingOn = labelingOn;
    }

    /**
     * This method labels the given noun phrase with the target label. The logic
     * behind the labelling is sourced from Markus Faerber.
     *
     * @param np The noun phrase to label.
     * @param annotFeat The corresponding annotation.
     * @return The labelled noun phrase.
     */
    public EnhancedNP addLabel(EnhancedNP np, AnnotationFeatures annotFeat) {
        Integer targetLabel = 0; //0 false, 1 true
        if (this.labelingOn) {
            /*String phraseString = np.getNp().getPhraseString();
             Iterator<String> realNovelEntIterator = this.realNovelEntities.iterator();
             while (realNovelEntIterator.hasNext()) {
             String realNovelEntity = realNovelEntIterator.next();
             if (phraseString.contentEquals(realNovelEntity)) {
             targetLabel = 1;
             np.setTargetLabel(targetLabel);
             break;
             }
             }*/
            //From Markus Faerber, see class NPProcessSlace.java
            int noveltyClass = annotFeat.getNoveltyClass();
            String wikiTitle = annotFeat.getWikiTitle();
            if (!wikiTitle.equals("NIL") && noveltyClass == 4) {
                targetLabel = 1;
            } else {
                targetLabel = 0;
            }
        }
        np.setTargetLabel(targetLabel);

        return np;
    }

}
