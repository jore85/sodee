/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classifier;

import Config.Constants;
import Config.SODEEConfig;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import weka.classifiers.Classifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.unsupervised.attribute.Remove;

/**
 * The classification pipeline of SODEE.
 *
 * @author Johannes Reiss
 */
public class ClassificationPipeline {

    private DBDataImporteur dbImport;
    private ModelImporteur modelImport;
    private FilteredClassifier cls;
    private DateTime startDT;
    private DateTime endDT;

    public ClassificationPipeline(DBDataImporteur dbImport,
            ModelImporteur modelImport) {
        this.dbImport = dbImport;
        this.modelImport = modelImport;
    }

    public void setStartDT(DateTime startDT) {
        this.startDT = startDT;
    }

    public void setEndDT(DateTime endDT) {
        this.endDT = endDT;
    }

    /**
     * This methods holds the complete classification pipeline. It contains the
     * model import, the data import from a database, the final preprocessing of
     * the data and predicts the target label of the imported noun phrases.
     */
    public void process() {
        String beginDate = String.valueOf(startDT.toString());
        //System.out.println(beginDate);
        String endDate = String.valueOf(endDT.toString());
        //System.out.println(endDate);
        Thread modelImportThread = new Thread(this.modelImport);
        modelImportThread.setName("ModelImport");
        modelImportThread.start();
        String sqlQuery_GetData = "SELECT rowID, NP, Named_Entity_Class, Pos, hasRedLink, "
                + "titleContainsNP, firstCapital, isAllCaps, dayOfWeek, lastFullHour, pageView24hSum,"
                + " pageViewSlope24hSlope, pageViewSlope24hIntercept, "
                + "npOccurrenceNo1h, npOccurrenceNo24h, npOccurrenceSlope24hSlope, "
                + "npOccurrenceSlope24hIntercept, googleNgramFrequency, npPositionInArticle, "
                + "articleCoverage, class FROM allNPFeatures WHERE processedTime >= '"
                + beginDate + "' AND processedTime <= '" + endDate + "'";
        //System.out.println(sqlQuery_GetData);
        //the data for the classifier (not filtered, because filtering is done in the classifier)
        Instances instancesToClassify = this.dbImport.getInstancesFromDB(sqlQuery_GetData, "SODEELive", 1);
        instancesToClassify.setClassIndex(instancesToClassify.numAttributes() - 1);

        try {
            modelImportThread.join();
        } catch (InterruptedException ex) {
            System.err.println("The model import was interrupted. No model imported.");
        }

        //Set filter
        Remove rm = new Remove();
        String[] removeFilterOptions = {"-R", "1,2,3,8,9,10"};
        try {
            rm.setOptions(removeFilterOptions);
        } catch (Exception ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Filter[] allFilters = new Filter[1];
        allFilters[0] = rm;

        MultiFilter filters = new MultiFilter();
        filters.setFilters(allFilters);

        //Save Dataset before filtering 
        ArffSaver arffWriter = new ArffSaver();
        arffWriter.setInstances(instancesToClassify);
        String fullDataSetFile = SODEEConfig.arffOutput + "full.arff";
        try {
            arffWriter.setFile(new File(fullDataSetFile));
            arffWriter.writeBatch();
        } catch (IOException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            filters.setInputFormat(instancesToClassify);
            //instancesToClassify = Filter.useFilter(instancesToClassify, filters);
        } catch (Exception ex) {
            Logger.getLogger(ClassificationPipeline.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Save Dataset after filtering
        arffWriter = new ArffSaver();
        arffWriter.setInstances(instancesToClassify);
        String filteredDataSetFile = SODEEConfig.arffOutput + "filtered.arff";
        try {
            arffWriter.setFile(new File(filteredDataSetFile));
            arffWriter.writeBatch();
        } catch (IOException ex) {
            Logger.getLogger(WekaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        Classifier importedModel = null;
        if (this.modelImport.isIsImported()) {
            importedModel = this.modelImport.getImportedModel();
            this.cls = new FilteredClassifier();
            this.cls.setFilter(filters);
            this.cls.setClassifier(importedModel);
        }

        //Classify
        // get id attribute, later it is used to get the ID of the instance 
        // (for import in the database after classification)
        Attribute rowIDAttr = instancesToClassify.attribute(0);

        List<ClassifiedInstance> classifiedInstances = new ArrayList<>(instancesToClassify.numInstances());
        System.out.println("No. NPs to classify: " + instancesToClassify.numInstances()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        for (int i = 0; i < instancesToClassify.numInstances(); i++) {
            int rowID = (int) instancesToClassify.instance(i).value(rowIDAttr); //get rowID            
            //System.out.println("rowID: " + rowID);
            int predictedLabel;
            try {
                predictedLabel = (int) this.cls.classifyInstance(instancesToClassify.instance(i));
                double[] distributionForInstance = this.cls.distributionForInstance(instancesToClassify.instance(i));
                classifiedInstances.add(i, new ClassifiedInstance(rowID, predictedLabel, distributionForInstance));
                //System.out.println(rowID + "\t" + predictedLabel
                //        + "\t" + distributionForInstance[0] + "\t" + distributionForInstance[1]);
            } catch (Exception ex) {
                Logger.getLogger(ClassificationPipeline.class.getName()).log(Level.SEVERE, null, ex);
            }
            String sqlQuery_UpdateData = "UPDATE allNPFeatures SET class = ?, probabilityTRUE = ?, probabilityFALSE = ? WHERE rowID = ?";
            this.dbImport.updateInstancesInDB(sqlQuery_UpdateData, classifiedInstances);
        }
        System.out.println("No. NPs classified: " + classifiedInstances.size()
                + "\t" + DateTime.now(DateTimeZone.UTC));
    }

}
