/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package Classifier;

/**
 * This class is a container for the classifier result. It holds the rowID, the
 * predicted label and the probabilities of the classes.
 * 
 * @author Johannes Reiss
 */
public class ClassifiedInstance {

    int rowID;
    int label;
    double[] probability;

    public ClassifiedInstance(int rowID, int label, double[] probability) {
        this.rowID = rowID;
        this.label = label;
        this.probability = probability;
    }

    public int getRowID() {
        return rowID;
    }

    public int getLabel() {
        return label;
    }
    
    public boolean getLabelAsBoolean(){
        return label == 1;
    }

    public double[] getProbability() {
        return probability;
    }

}
