package Classifier;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * A model of a feature vector for the SODEE Classifier.
 *
 * @author jore
 */
public class SODEEDataInstanceNew {

    private ArrayList<String> neClassNominalValues = new ArrayList<>(4);
    private ArrayList<String> posTags = new ArrayList<>(7);
    private ArrayList<String> classValues = new ArrayList<>(2);
    private ArrayList<String> booleanNominal = new ArrayList<>(2);
    private ArrayList<String> dateCountsNominal = new ArrayList<>(24);
    private ArrayList<Attribute> sodeeFeatures = new ArrayList<>(21);

    /**
     * Constructs a model of a feature vector for the SODEE Classifier. The
     * features/attributes are final and so no others features could be used
     * with this version of the code.
     */
    public SODEEDataInstanceNew() {
        setNamedEntityClasses();
        setPosTags();
        setClassValues();
        setBooleans();
        setDayHoursNominal();
        setSODEEFeatures();
    }

    private void setNamedEntityClasses() {
        neClassNominalValues.add("PERSON");
        neClassNominalValues.add("ORGANIZATION");
        neClassNominalValues.add("LOCATION");
        neClassNominalValues.add("MISC");
    }

    private void setPosTags() {
        posTags.add("NN");
        posTags.add("NNP");
        posTags.add("NNPS");
        posTags.add("NNS");
        posTags.add("JJS");
        posTags.add("JDI");
        posTags.add("JJ");
        posTags.add("");
        posTags.add("CD");
        posTags.add("$");
        posTags.add("WDT");
    }

    private void setClassValues() {
        classValues.add("positive");
        classValues.add("negative");
    }

    private void setBooleans() {
        booleanNominal.add("true");
        booleanNominal.add("false");
    }

    private void setDayHoursNominal() {
        dateCountsNominal.add("0");
        dateCountsNominal.add("1");
        dateCountsNominal.add("2");
        dateCountsNominal.add("3");
        dateCountsNominal.add("4");
        dateCountsNominal.add("5");
        dateCountsNominal.add("6");
        dateCountsNominal.add("7");
        dateCountsNominal.add("8");
        dateCountsNominal.add("9");
        dateCountsNominal.add("10");
        dateCountsNominal.add("11");
        dateCountsNominal.add("12");
        dateCountsNominal.add("13");
        dateCountsNominal.add("14");
        dateCountsNominal.add("15");
        dateCountsNominal.add("16");
        dateCountsNominal.add("17");
        dateCountsNominal.add("18");
        dateCountsNominal.add("19");
        dateCountsNominal.add("20");
        dateCountsNominal.add("21");
        dateCountsNominal.add("22");
        dateCountsNominal.add("23");
    }

    private void setSODEEFeatures() {
        sodeeFeatures.add(new Attribute("rowID"));
        sodeeFeatures.add(new Attribute("NP", (List<String>) null));
        sodeeFeatures.add(new Attribute("Named_Entity_Class", neClassNominalValues));
        sodeeFeatures.add(new Attribute("Pos", posTags));
        sodeeFeatures.add(new Attribute("hasRedLink", booleanNominal));
        sodeeFeatures.add(new Attribute("titleContainsNP", booleanNominal));
        sodeeFeatures.add(new Attribute("firstCapital", booleanNominal));
        sodeeFeatures.add(new Attribute("isAllCaps", booleanNominal));
        sodeeFeatures.add(new Attribute("dayOfWeek", dateCountsNominal));
        sodeeFeatures.add(new Attribute("lastFullHour", dateCountsNominal));
        sodeeFeatures.add(new Attribute("pageView24hSum"));
        sodeeFeatures.add(new Attribute("pageViewSlope24hSlope"));
        sodeeFeatures.add(new Attribute("pageViewSlope24hIntercept"));
        sodeeFeatures.add(new Attribute("npOccurrenceNo1h"));
        sodeeFeatures.add(new Attribute("npOccurrenceNo24h"));
        sodeeFeatures.add(new Attribute("npOccurrenceSlope24hSlope"));
        sodeeFeatures.add(new Attribute("npOccurrenceSlope24hIntercept"));
        sodeeFeatures.add(new Attribute("googleNgramFrequency"));
        sodeeFeatures.add(new Attribute("npPositionInArticle"));
        sodeeFeatures.add(new Attribute("articleCoverage"));
        sodeeFeatures.add(new Attribute("class", classValues));
    }

    /**
     * Returns an empty container for weka instances with the set attributes in
     * SODEE. One must set the class attribute first.
     *
     * @param instancesName the name of the instance container
     * @return a container for weka instances
     */
    public static Instances getEmptySODEEInstances(String instancesName) {
        SODEEDataInstanceNew sodeeInstance = new SODEEDataInstanceNew();
        return new Instances(instancesName, sodeeInstance.sodeeFeatures, 0);
    }

    /**
     * Method to fill an empty instances container with instances retrieved from
     * a database query and return the filled instances container, e.g. for
     * using it in a classifier.
     *
     * @param queryResults The result set of a database query.
     * @param instancesContainer The container which should be filled in this
     * method.
     * @param instanceWeight A weight for the instances of this set. For Default
     * set it 1
     * @return The filled container with the retrieved instances
     */
    public static Instances fillInstanceContainer(ResultSet queryResults, Instances instancesContainer, int instanceWeight) {
        try {
            while (queryResults.next()) {
                Instance row = new DenseInstance(instancesContainer.numAttributes());
                row.setValue((Attribute) instancesContainer.attribute(0), queryResults.getInt("rowID"));
                row.setValue((Attribute) instancesContainer.attribute(1), queryResults.getString("NP"));
                row.setValue((Attribute) instancesContainer.attribute(2), queryResults.getString("Named_Entity_Class"));
                //System.out.println(executeQuery.getString("Pos"));
                row.setValue((Attribute) instancesContainer.attribute(3), queryResults.getString("Pos"));
                row.setValue((Attribute) instancesContainer.attribute(4), queryResults.getBoolean("hasRedLink") ? (1) : (0));
                row.setValue((Attribute) instancesContainer.attribute(5), queryResults.getBoolean("titleContainsNP") ? (1) : (0));
                row.setValue((Attribute) instancesContainer.attribute(6), queryResults.getBoolean("firstCapital") ? (1) : (0));
                row.setValue((Attribute) instancesContainer.attribute(7), queryResults.getBoolean("isAllCaps") ? (1) : (0));
                row.setValue((Attribute) instancesContainer.attribute(8), queryResults.getInt("dayOfWeek"));
                row.setValue((Attribute) instancesContainer.attribute(9), queryResults.getInt("lastFullHour"));
                row.setValue((Attribute) instancesContainer.attribute(10), queryResults.getInt("pageView24hSum"));
                row.setValue((Attribute) instancesContainer.attribute(11), queryResults.getDouble("pageViewSlope24hSlope"));
                row.setValue((Attribute) instancesContainer.attribute(12), queryResults.getDouble("pageViewSlope24hIntercept"));
                row.setValue((Attribute) instancesContainer.attribute(13), queryResults.getInt("npOccurrenceNo1h"));
                row.setValue((Attribute) instancesContainer.attribute(14), queryResults.getInt("npOccurrenceNo24h"));
                row.setValue((Attribute) instancesContainer.attribute(15), queryResults.getDouble("npOccurrenceSlope24hSlope"));
                row.setValue((Attribute) instancesContainer.attribute(16), queryResults.getDouble("npOccurrenceSlope24hIntercept"));
                row.setValue((Attribute) instancesContainer.attribute(17), queryResults.getLong("googleNgramFrequency"));
                row.setValue((Attribute) instancesContainer.attribute(18), queryResults.getDouble("npPositionInArticle"));
                row.setValue((Attribute) instancesContainer.attribute(19), queryResults.getDouble("articleCoverage"));
                row.setValue((Attribute) instancesContainer.attribute(20), queryResults.getInt("class"));
                row.setWeight(instanceWeight);
                instancesContainer.add(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SODEEDataInstanceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instancesContainer;
    }
}
