/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FeatureExtraction.PeriodicalFeatures;

/**
 *
 * @author jore
 */
public class PeriodicalFeatures {
    
    private final int npOccurrenceNo1h;
    private final int npOccurrenceNo24h;
    
    private final double npOccurrenceSlope24hSlope;
    private final double npOccurrenceSlope24hIntercept;
    private final double npOccurrenceSlope24hRSquare;
    private final double npOccurrenceSlope24hSlopeStdErr;
    private final double npOccurrenceSlope24hInterceptStdErr;
    
    public PeriodicalFeatures(int npOccurrenceNo1h, int npOccurrenceNo24h,
            double npOccurrenceSlope24hSlope, double npOccurrenceSlope24hIntercept,
            double npOccurrenceSlope24hRSquare, double npOccurrenceSlope24hSlopeStdErr,
            double npOccurrenceSlope24hInterceptStdErr) {
        this.npOccurrenceNo1h = npOccurrenceNo1h;
        this.npOccurrenceNo24h = npOccurrenceNo24h;
        this.npOccurrenceSlope24hSlope = npOccurrenceSlope24hSlope;
        this.npOccurrenceSlope24hIntercept = npOccurrenceSlope24hIntercept;
        this.npOccurrenceSlope24hRSquare = npOccurrenceSlope24hRSquare;
        this.npOccurrenceSlope24hSlopeStdErr = npOccurrenceSlope24hSlopeStdErr;
        this.npOccurrenceSlope24hInterceptStdErr = npOccurrenceSlope24hInterceptStdErr;
    }
    
    public int getNpOccurrenceNo1h() {
        return npOccurrenceNo1h;
    }
    
    public int getNpOccurrenceNo24h() {
        return npOccurrenceNo24h;
    }
    
    public double getNpOccurrenceSlope24hSlope() {
        return npOccurrenceSlope24hSlope;
    }
    
    public double getNpOccurrenceSlope24hIntercept() {
        return npOccurrenceSlope24hIntercept;
    }
    
    public double getNpOccurrenceSlope24hRSquare() {
        return npOccurrenceSlope24hRSquare;
    }
    
    public double getNpOccurrenceSlope24hSlopeStdErr() {
        return npOccurrenceSlope24hSlopeStdErr;
    }
    
    public double getNpOccurrenceSlope24hInterceptStdErr() {
        return npOccurrenceSlope24hInterceptStdErr;
    }
    
    @Override
    public String toString() {
        return "PeriodicalFeatures{" + "npOccurrenceNo1h=" + npOccurrenceNo1h
                + ", npOccurrenceNo24h=" + npOccurrenceNo24h + ", npOccurrenceSlope24hSlope=" + npOccurrenceSlope24hSlope
                + ", npOccurrenceSlope24hIntercept=" + npOccurrenceSlope24hIntercept
                + ", npOccurrenceSlope24hRSquare=" + npOccurrenceSlope24hRSquare
                + ", npOccurrenceSlope24hSlopeStdErr=" + npOccurrenceSlope24hSlopeStdErr
                + ", npOccurrenceSlope24hInterceptStdErr=" + npOccurrenceSlope24hInterceptStdErr + '}';
    }
    
    public String[] toValueArray() {
        String[] values = {String.valueOf(npOccurrenceNo1h),
            String.valueOf(npOccurrenceNo24h), String.valueOf(npOccurrenceSlope24hSlope),
            String.valueOf(npOccurrenceSlope24hIntercept), String.valueOf(npOccurrenceSlope24hRSquare),
            String.valueOf(npOccurrenceSlope24hSlopeStdErr), String.valueOf(npOccurrenceSlope24hInterceptStdErr)};
        return values;
    }
    
}
