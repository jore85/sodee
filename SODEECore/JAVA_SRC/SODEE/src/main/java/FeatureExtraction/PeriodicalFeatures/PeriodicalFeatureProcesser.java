/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package FeatureExtraction.PeriodicalFeatures;

import RBBNPE.ArchivedNP;
import RBBNPE.EnhancedNP;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * This class handles the periodical feature retrieval.
 * 
 * @author Johannes Reiss
 */
public class PeriodicalFeatureProcesser {

    private Map<EnhancedNP, PeriodicalFeatures> periodicalFeatMap;

    private List<ArchivedNP> dayBeforeNPs;
    private Set<EnhancedNP> nps;

    private int numberOfThreads;

    public Map<EnhancedNP, PeriodicalFeatures> getPeriodicalFeatMap() {
        return periodicalFeatMap;
    }

    public void setPeriodicalFeatMap(Map<EnhancedNP, PeriodicalFeatures> periodicalFeatMap) {
        this.periodicalFeatMap = periodicalFeatMap;
    }

    public void setDayBeforeNPs(List<ArchivedNP> dayBeforeNPs) {
        this.dayBeforeNPs = dayBeforeNPs;
    }

    public void setNps(Set<EnhancedNP> nps) {
        this.nps = nps;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }
    
    /**
     * This method processes the given noun phrases (also the current nps and 
     * the nps of the day before) and initalise the periodical features retrieval
     * with an executor service. 
     * 
     * The retrieval logic is hold by the PeriodicalFeatureProcessorCallable.
     */
    public void process() {
        this.periodicalFeatMap = new HashMap<>(nps.size());
        Iterator<EnhancedNP> iterator = this.nps.iterator();
        ExecutorService es = Executors.newFixedThreadPool(this.numberOfThreads);
        List<Future<PeriodicalFeatCalcResult>> processResults = new ArrayList<>(nps.size());
        List<PeriodicalFeatureProcessorCallable> npsToProcess = new ArrayList<>(nps.size());
        int i = 0;
        while (iterator.hasNext()) {
            EnhancedNP np = iterator.next();
            npsToProcess.add(new PeriodicalFeatureProcessorCallable(np, nps, dayBeforeNPs));
            /*processResults.add(es.submit(periodicFeatCalculator));
             i++;
             if (((i % 10000) == 0) || i == nps.size()) {
             System.out.println(i + " nps were processed out of " + nps.size()
             + " existing nps." + "\t" + DateTime.now(DateTimeZone.UTC)); // \nReturns: " + executeBatch.length);
             System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
             + " of total Heap: " + Runtime.getRuntime().totalMemory()
             + "\t" + DateTime.now(DateTimeZone.UTC));
             }*/
        }
        try {
            processResults = es.invokeAll(npsToProcess);
        } catch (InterruptedException ex) {
            Logger.getLogger(PeriodicalFeatureProcesser.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Periodical Features were calculated now catch the results... No. of processedResults: " + processResults.size()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                + " of total Heap: " + Runtime.getRuntime().totalMemory()
                + "\t" + DateTime.now(DateTimeZone.UTC));
        Iterator<Future<PeriodicalFeatCalcResult>> iterator1 = processResults.iterator();
        i = 0;
        while (iterator1.hasNext()) {
            Future<PeriodicalFeatCalcResult> processedResult = iterator1.next();
            try {
                PeriodicalFeatCalcResult result = processedResult.get();
                this.periodicalFeatMap.put(result.getNp(), result.getPeriodicFeat());
            } catch (InterruptedException ex) {
                Logger.getLogger(PeriodicalFeatureProcesser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(PeriodicalFeatureProcesser.class.getName()).log(Level.SEVERE, null, ex);
            }
            i++;
            if (((i % 10000) == 0) || i == processResults.size()) {
                System.out.println(i + " results were processed out of " + nps.size()
                        + " existing results." + "\t" + DateTime.now(DateTimeZone.UTC)); // \nReturns: " + executeBatch.length);
                //System.out.println("Free memory " + Runtime.getRuntime().freeMemory()
                //        + " of total Heap: " + Runtime.getRuntime().totalMemory()
                //        + "\t" + DateTime.now(DateTimeZone.UTC));
            }
        }
        System.out.println("Periodical Feature Results were catched. Shutdown the Threadpool." + "\t" + DateTime.now(DateTimeZone.UTC));
        es.shutdown();
    }
}
