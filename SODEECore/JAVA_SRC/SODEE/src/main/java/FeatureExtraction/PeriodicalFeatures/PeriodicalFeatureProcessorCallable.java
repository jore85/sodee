/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package FeatureExtraction.PeriodicalFeatures;

import RBBNPE.ArchivedNP;
import RBBNPE.EnhancedNP;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.joda.time.DateTime;

/**
 * This class hold the logic of the periodical features retrieval.
 *
 * @author Johannes Reiss
 */
public class PeriodicalFeatureProcessorCallable implements Callable<PeriodicalFeatCalcResult> {

    private List<ArchivedNP> dayBeforeNPs;
    private Set<EnhancedNP> nps;

    private final EnhancedNP np;

    public PeriodicalFeatureProcessorCallable(EnhancedNP np) {
        this.np = np;
    }

    PeriodicalFeatureProcessorCallable(EnhancedNP np, Set<EnhancedNP> nps,
            List<ArchivedNP> dayBeforeNPs) {
        this.dayBeforeNPs = dayBeforeNPs;
        this.nps = nps;
        this.np = np;
    }

    public void setDayBeforeNPs(List<ArchivedNP> dayBeforeNPs) {
        this.dayBeforeNPs = dayBeforeNPs;
    }

    public void setNps(Set<EnhancedNP> nps) {
        this.nps = nps;
    }

    /**
     * This method retrieves the periodical features of the given noun phrases.
     *
     * @return The result of the periodical features retrieval.
     * @throws Exception
     */
    @Override
    public PeriodicalFeatCalcResult call() throws Exception {
        PeriodicalFeatCalcResult result = null;
        EnhancedNP npT1 = this.np;
        String npStringT1 = npT1.getNp().getPhraseString();
        DateTime t1 = npT1.getTimestamp();
        int hourT1 = t1.getHourOfDay();
        DateTime minus24h = t1.minusHours(24); //use that value as limit for second loop
        DateTime minus1h = t1.minusHours(1);
        int occurrenceSum = 0;
        int occurrence1h = 0;
        int[] occurrence24h = new int[24]; //Calc index for 24h array: hourT1 mod hourT2 = y -> [23-y]
        Iterator<EnhancedNP> sameDayNPsIterator = this.nps.iterator();
        //GO THROUGH THE NPS OF THE SAME DAY
        //System.out.println("Go through " + this.nps.size() + " NPs current day... "
        //       + "\t" + DateTime.now(DateTimeZone.UTC));
        while (sameDayNPsIterator.hasNext()) {
            EnhancedNP npT2 = sameDayNPsIterator.next();
            DateTime t2 = npT2.getTimestamp();
            String npStringT2 = npT2.getNp().getPhraseString();
            int hourT2 = t2.getHourOfDay();
            if (npStringT1.equalsIgnoreCase(npStringT2)) {
                //System.out.println("npT1: " + npStringT1 + " - " + t1.toString() + " hour: " + hourT1
                //    + " npT2: " + npStringT2 + " - " + t2.toString() + " hour: " + hourT2);
                occurrenceSum++;
                if (t2.isAfter(minus1h)) {
                    occurrence1h++;
                }
                int y = 24 - (hourT2 - hourT1);
                occurrence24h[y % 24]++;
            }
        }
        //GO THROUGH THE NPS OF THE DAY BEFORE
        //System.out.println("Go through " + this.dayBeforeNPs.size() + " NPs of day before... "
        //        + "\t" + DateTime.now(DateTimeZone.UTC));
        Iterator<ArchivedNP> dayBeforeNPsIterator = this.dayBeforeNPs.iterator();
        while (dayBeforeNPsIterator.hasNext()) {
            ArchivedNP npT2 = dayBeforeNPsIterator.next();
            DateTime t2 = new DateTime(npT2.getUnixTime());
            if (t2.isAfter(minus24h)) {
                String npStringT2 = npT2.getNpString();
                int hourT2 = t2.getHourOfDay();
                if (npStringT1.equalsIgnoreCase(npStringT2)) {
                    //System.out.println("npT1: " + npStringT1 + " - " + t1.toString() + " hour: " + hourT1
                    //   + " npT2: " + npStringT2 + " - " + t2.toString() + " hour: " + hourT2);
                    occurrenceSum++;
                    int y = 24 - (hourT2 - hourT1);
                    occurrence24h[y % 24]++;
                }
            }
        }
        SimpleRegression regressionOver24h = regressionOver24h(occurrence24h);
        double intercept = regressionOver24h.getIntercept();
        double slope = regressionOver24h.getSlope();
        double slopeStdErr = regressionOver24h.getSlopeStdErr();
        double interceptStdErr = regressionOver24h.getInterceptStdErr();
        double rSquare = regressionOver24h.getRSquare();
        PeriodicalFeatures periodical = new PeriodicalFeatures(occurrence1h, occurrenceSum,
                slope, intercept, rSquare, slopeStdErr, interceptStdErr);
        result = new PeriodicalFeatCalcResult(npT1, periodical);
        return result;
    }

    private SimpleRegression regressionOver24h(int[] hourlyCounts) {
        SimpleRegression reg = new SimpleRegression();
        for (int i = 0; i < hourlyCounts.length; i++) {
            reg.addData(i, hourlyCounts[i]);
            //System.out.println("Hour (first occur = 1): " + (i + 1) + " value: " + hourlyCounts[i]);
        }
        return reg;
    }

}
