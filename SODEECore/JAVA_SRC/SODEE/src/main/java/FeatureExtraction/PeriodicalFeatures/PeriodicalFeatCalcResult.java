/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package FeatureExtraction.PeriodicalFeatures;

import RBBNPE.EnhancedNP;

/**
 *
 * @author johan
 */
public class PeriodicalFeatCalcResult {
    
    private final EnhancedNP np;
    private final PeriodicalFeatures periodicFeat;

    public PeriodicalFeatCalcResult(EnhancedNP np, PeriodicalFeatures periodicFeat) {
        this.np = np;
        this.periodicFeat = periodicFeat;
    }

    public EnhancedNP getNp() {
        return np;
    }

    public PeriodicalFeatures getPeriodicFeat() {
        return periodicFeat;
    }
    
    
    
}
