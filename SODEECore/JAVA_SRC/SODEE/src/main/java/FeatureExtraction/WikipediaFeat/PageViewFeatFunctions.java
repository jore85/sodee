package FeatureExtraction.WikipediaFeat;


/*
 * The functions in the present class are extracted from WPPVFeatureExtractor.java 
 * by LIN Zhan.
 */
import Utility.DateUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 *
 * @author jore
 */
public class PageViewFeatFunctions {

    private static int[] count25h;
    private static int[] count15d;
    private Connection conn;

    public int[] getCount25h() {
        return count25h;
    }

    public static int[] retrieveCount25h(Connection c, String title, Date date) {
        count25h = new int[25];
        Timestamp tstmp = new Timestamp(DateUtility.floorToHour(new Timestamp(date.getTime())).getTime());
        //LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
        //Timestamp tstmp = Timestamp.valueOf(ldt.truncatedTo(ChronoUnit.HOURS));
        try {
            PreparedStatement stmt = c.prepareStatement(
                    "SELECT date, count FROM all_pageviews WHERE (wikipedia_title=?) AND (date<? AND date>=DATE_SUB(?, INTERVAL 25 HOUR))");
            stmt.setString(1, title);
            stmt.setTimestamp(2, tstmp);
            stmt.setTimestamp(3, tstmp);
            ResultSet queryResult = stmt.executeQuery();
            count25h = convertQueryResult(queryResult, 25, tstmp, (3600 * 1000));

        } catch (SQLException ex) {
            Logger.getLogger(PageViewFeatFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count25h;
    }

    public int[] getCount15d() {
        return count15d;
    }

    public static int[] retrieveCount15d(Connection c, String title, Date date) {
        count15d = new int[15];
        Timestamp tstmp = new Timestamp(DateUtility.floorToHour(new Timestamp(date.getTime())).getTime());
        //LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
        //Timestamp tstmp = Timestamp.valueOf(ldt.truncatedTo(ChronoUnit.HOURS));
        try {
            PreparedStatement stmt = c.prepareStatement(
                    "SELECT date, count FROM all_pageviews_by_day WHERE (wikipedia_title=?) AND (date<? AND date>=DATE_SUB(?, INTERVAL 15 DAY))");
            stmt.setString(1, title);
            stmt.setTimestamp(2, tstmp);
            stmt.setTimestamp(3, tstmp);
            ResultSet queryResult = stmt.executeQuery();
            count25h = convertQueryResult(queryResult, 15, tstmp, (24 * 3600 * 1000));

        } catch (SQLException ex) {
            Logger.getLogger(PageViewFeatFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count15d;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    private static int[] convertQueryResult(ResultSet result, int length, Timestamp date, int offset) throws SQLException {
        int[] r = new int[length];
        for (int i = 0; i < r.length; i++) {
            r[i] = 0;
        }
        while (result.next()) {
            long diff = date.getTime() - result.getTimestamp("date").getTime() - 1;
            r[(int) (diff / offset)] = result.getInt("count");
        }
        result.close();
        return r;
    }

    public static boolean pageView24hExist(int[] count25Hours, String title, Date date) {
        for (int i = 0; i < 24; i++) {
            if (count25Hours[i] != 0) {
                return true;
            }
        }
        return false;
    }

    public static int pageView24hSum(int[] count25Hours, String title, Date date) {
        int sum = 0;
        for (int i = 0; i < 24; i++) {
            if (count25Hours[i] != 0) {
                sum += count25Hours[i];
            }
        }
        return sum;
    }

    public static int pageViewDays7d(int[] count15Days, String title, Date date) {
        int sum = 0;
        for (int i = 0; i < 7; i++) {
            sum += (count15Days[i] > 0) ? 1 : 0;
        }
        return sum;
    }

    public static int pageViewDays14d(int[] count15Days, String title, Date date) {
        int sum = 0;
        for (int i = 0; i < 14; i++) {
            sum += (count15Days[i] > 0) ? 1 : 0;
        }
        return sum;
    }

    public static int pageViewDays7dMin100(int[] count15Days, String title, Date date) {
        int sum = 0;
        for (int i = 0; i < 7; i++) {
            sum += (count15Days[i] >= 100) ? 1 : 0;
        }
        return sum;
    }

    public static int pageViewDays14dMin100(int[] count15Days, String title, Date date) {
        int sum = 0;
        for (int i = 0; i < 14; i++) {
            sum += (count15Days[i] >= 100) ? 1 : 0;
        }
        return sum;
    }

    public static long pageViewSum7d(int[] count15Days, String title, Date date) {
        int sum = 0;
        for (int i = 0; i < 7; i++) {
            sum += count15Days[i];
        }

        return sum;
    }

    public static PageViewRegressionResult pageViewRegression24h(int[] count25Hours) {
        SimpleRegression regression = new SimpleRegression();
        for (int i = 25; i > 1; i--) { // i here index in array count25h, do not consider current hour==index 0.
            try {
                regression.addData((double) (25 - i), count25Hours[i - 1]);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return new PageViewRegressionResult(regression);
    }

    public static PageViewRegressionResult pageViewRegression14d(int[] count15Days) {
        SimpleRegression regression = new SimpleRegression();
        for (int i = 15; i > 1; i--) { // i here index in array count25h, do not consider current hour==index 0.
            try {
                regression.addData((double) (25 - i), count15Days[i - 1]);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return new PageViewRegressionResult(regression);
    }

    public static int[] convertAPIResultIntoArray(List<String[]> apiResult) {
        //System.out.println(apiResult.size());
        int[] result = new int[15];
        for (int i = 0; i < result.length; i++) {
            if (i < apiResult.size()) {
                String[] get = apiResult.get(i);
                //System.out.println(get[0]);
                if (get[0].equals("404")) {
                    result = new int[1];
                    result[0] = -1;
                    return result;
                }
                result[i] = Integer.parseInt(get[1]);
            } else {
                result[i] = 0;
            }
        }
        return result;
    }

}
