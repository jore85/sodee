package FeatureExtraction.WikipediaFeat;

import Config.SODEEConfig;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.apache.http.util.EntityUtils;

/**
 * This class provides functionality to check if a noun phrase is a red link in
 * the Wikipedia. For this check the Wikipedia API is used. As fallback or in if
 * the access of the API is not possible, a generated list with red links is
 * used.
 *
 * @author Johannes Reiss
 */
public class RedLinkChecker {

    private final String userAgent;
    private CloseableHttpClient pvHttpClient;
    private HashSet<String> redLinkList_EN;

    private static String API_ENDPOINT;
    private static final String API_action = "action=";
    private static final String API_list = "list=";
    private static final String API_limit = "limit=";
    private static final String API_pageTitle = "titles=";
    private static final String API_format = "format=json&formatversion=2";

    /**
     * Inital a simple HTTP client is created.
     *
     * @param userAgent The user agent of that client. Is a requirement for the
     * fair use of the Wikipedia API.
     */
    public RedLinkChecker(String userAgent) {
        this.userAgent = userAgent;
        this.pvHttpClient = HttpClients.createDefault();
    }

    public void setRedLinkList_EN(HashSet<String> redLinkList_EN) {
        this.redLinkList_EN = redLinkList_EN;
    }

    /**
     * This method retrieves the info if the given page is a red linked page in
     * wikipedia.
     *
     * @param pageTitle The title of the page
     * @param lang The language of the Wikipedia, i.e. en for english
     * @param live If true then the method tries get the info via Wikipedia-API,
     * else the parsed list of red links will be checked
     * @return True if the given page is a red link in Wikipedia
     */
    public boolean hasRedLink(String pageTitle, String lang, boolean live) {
        pageTitle = convertNounPhraseToWikiTitle(pageTitle);
        boolean hasRedLink = false;
        //for live system, use Wikimedia API
        if (live) {
            try {
                pageTitle = URLEncoder.encode(pageTitle, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(RedLinkChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
            boolean pageIDExist = pageIDexist(pageTitle, lang);
            if (pageIDExist == false) {
                boolean hasBackLink = hasBackLink(pageTitle, lang);
                if (hasBackLink == true) {
                    hasRedLink = true;
                }
            }
            return hasRedLink;
        } else {
            switch (lang) {
                case "en":
                    return hasRedLink(redLinkList_EN, pageTitle);
                default:
                    return false;
            }

        }
    }

    /**
     * Checks if the given page title is contained in a list with red links.
     *
     * @param redLinks The list with red links
     * @param pageTitle The title of the page
     * @return True if the given page is a red link in Wikipedia
     */
    public static boolean hasRedLink(HashSet<String> redLinks, String pageTitle) {
        boolean hasRedLink = false;
        if (redLinks != null) {
            if (redLinks.contains(pageTitle)) {
                hasRedLink = true;
            }
        }
        return hasRedLink;
    }

    private boolean pageIDexist(String pageTitle, String lang) {
        boolean pageIDExist = true;
        String pageIDResponse = pageIDRequest("query&prop=pageprops", pageTitle, lang);
        if (pageIDResponse != null) {
            int pageID = getPageID(pageIDResponse);
            if (pageID == -1) {
                pageIDExist = false;
                System.err.println("Could not get the page id (for " + lang
                        + " Wikipedia) of the title: " + pageTitle);
            }
        }
        return pageIDExist;
    }

    // checks if there are back links for the given page title
    private boolean hasBackLink(String pageTitle, String lang) {
        boolean hasBackLink = false;
        String backLinkResponse = backLinkRequest("query", "backlinks", "5", pageTitle, lang);
        if (backLinkResponse != null) {
            hasBackLink = parseBackLinkResponse(backLinkResponse);
        }
        return hasBackLink;
    }

    // make a api request with the given URI string
    private String getResponse(String uri) {
        String result = "";
        HttpGet get = new HttpGet(uri);
        try {
            HttpResponse response = pvHttpClient.execute(get);
            int status = response.getStatusLine().getStatusCode();
            if (status == 404) {
                result = "404";
            } else {
                HttpEntity entity = response.getEntity();
                if (entity.getContentType().getValue().equalsIgnoreCase(
                        ContentType.APPLICATION_JSON.withCharset("utf-8").
                        toString())) {
                    result = EntityUtils.toString(entity);
                } else {
                    result = "404";
                }
            }
        } catch (IOException ex) {

        }
        return result;
    }

    private String backLinkRequest(String action, String list, String limit,
            String title, String lang) {
        API_ENDPOINT = "https://" + lang + ".wikipedia.org/w/api.php";
        String requestString = API_ENDPOINT + "?" + API_action + action + "&" + API_list + list + "&" + "bltitle=" + title + "&"
                + "bllimit=" + limit + "&" + "blfilterredir=all&redirects" + "&" + API_format;
        return getResponse(requestString);
    }

    private String pageIDRequest(String action, String title, String lang) {
        API_ENDPOINT = "https://" + lang + ".wikipedia.org/w/api.php";
        String requestString = API_ENDPOINT + "?" + API_action + action
                + "&" + API_pageTitle + title + "&" + API_format;
        return getResponse(requestString);
    }

    private boolean parseBackLinkResponse(String responseString) {
        if (responseString != null) {
            if (!responseString.contentEquals("404")) {
                Gson gson = new Gson();
                JsonObject jobj = gson.fromJson(responseString, JsonObject.class);
                if (jobj != null) {
                    JsonObject query = jobj.getAsJsonObject("query");
                    if (query != null) {
                        JsonArray pages = query.getAsJsonArray("backlinks");
                        if (pages != null) {
                            Iterator<JsonElement> iterator = pages.iterator();
                            while (iterator.hasNext()) {
                                JsonObject next = iterator.next().getAsJsonObject();
                                JsonPrimitive pageid = next.getAsJsonPrimitive("pageid");
                                if (pageid != null) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    // get page id from the response of an api request
    private int getPageID(String responseString) {
        int pageId = -1;
        if (responseString != null) {
            if (!responseString.contentEquals("404")) {
                Gson gson = new Gson();
                JsonObject jobj = gson.fromJson(responseString, JsonObject.class);
                if (jobj != null) {
                    JsonObject query = jobj.getAsJsonObject("query");
                    if (query != null) {
                        JsonArray pages = query.getAsJsonArray("pages");
                        if (pages != null) {
                            Iterator<JsonElement> iterator = pages.iterator();
                            while (iterator.hasNext()) {
                                JsonObject next = iterator.next().getAsJsonObject();
                                JsonPrimitive pageid = next.getAsJsonPrimitive("pageid");
                                // if the request respond with a page id, then this is returned
                                if (pageid != null) {
                                    pageId = pageid.getAsInt();
                                    return pageId;
                                }
                            }
                        }
                    }
                }
            }
        }
        // if no page id exist or in an error case, -1 as error state is returned
        return pageId;
    }

    private String convertNounPhraseToWikiTitle(String title) {
        return title.replace(' ', '_');
    }

    public static void main(String[] args) {
        RedLinkChecker redLinkCh = new RedLinkChecker(SODEEConfig.userAgent);
        boolean hasRedLink = redLinkCh.hasRedLink("Oliver_Barrett_IV", "en", true);
        System.out.println(hasRedLink);
    }

}
