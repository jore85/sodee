package FeatureExtraction.WikipediaFeat;

/**
 * This class represents the data, which is needed for a Wikimedia PageviewAPI-Request.
 * @author jore
 */
public class PageViewAPIRequest {
    
    private final String project;
    private final String access;
    private final String agent; 
    private final String article;
    private final String granularity; 
    private final String startDate;
    private final String endDate;
    
    /**
     * Constructor of a Wikimedia PageviewAPI-Request. 
     * 
     * @param project The project of wikimedia, e.g. en.wikipedia.org
     * @param access Filter for the access method, e.g. all-access
     * @param agent Filter for the agents, e.g. all-agent
     * @param article The title of the article, e.g. London or Harry_Potter
     * @param granularity The time unit for the response data. As of today: daily
     * @param startDate Start date in format: YYYYMMDD 
     * @param endDate End date in format: YYYYMMDD 
     */
    public PageViewAPIRequest(String project, String access, String agent, 
            String article, String granularity, String startDate, String endDate) {
        this.project = project;
        this.access = access;
        this.agent = agent;
        this.article = article;
        this.granularity = granularity;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getProject() {
        return project;
    }

    public String getAccess() {
        return access;
    }

    public String getAgent() {
        return agent;
    }

    public String getArticle() {
        return article;
    }

    public String getGranularity() {
        return granularity;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }
    
    
    
}
