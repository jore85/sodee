package FeatureExtraction.WikipediaFeat.PageViewAPIClientNew;
/*
 * This following source code is part of the master thesis of Johannes Reiss
 */

import FeatureExtraction.WikipediaFeat.PageViewFeatFunctions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author johan
 */
public class WMPageViewAPIClientNew {

    private final String api_base; //https://wikimedia.org/api/rest_v1/metrics/pageviews/
    private final String userAgent;

    private final DefaultHttpClient client;

    private final String project = "en.wikipedia.org";
    private final String access = "all-access";
    private final String agent = "all-agents";

    public WMPageViewAPIClientNew(String api_base, String userAgent) {
        this.api_base = api_base;
        this.userAgent = userAgent;
        this.client = new DefaultHttpClient();
        this.client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);
    }

    /**
     * Call that method to get the page views of a wikipedia page as a list with
     * the daily values within the retrieved timespan.
     *
     * @param wikiPage The wikipedia page, whitespaces are replaced with "_"
     * @param granularity set on "daily"
     * @param modus set on "per-article"
     * @param startDate the start date of the timespan
     * @param endDate the end date of the timespan
     * @return A list with the retrieved values as a array for each requested
     * day. The Array is formated: [0] article, [1] Pageviews, [2] Timestamp
     */
    public List<String[]> makePageViewRequestAndGetResult(String wikiPage, String granularity, String modus,
            String startDate, String endDate) {
        List<String[]> result = new ArrayList<>();
        HttpGet makePageViewRequest = makePageViewRequest(wikiPage, granularity, modus, startDate, endDate);
        try {
            HttpResponse makeGETRequestOnAPI = makeGETRequestOnAPI(makePageViewRequest);
            result = responseHandler(makeGETRequestOnAPI);
        } catch (IOException ex) {
            Logger.getLogger(WMPageViewAPIClientNew.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public void releaseConnection() {
        client.close();
    }

    public HttpResponse makeGETRequestOnAPI(HttpGet getRequest) throws IOException {
        getRequest.setHeader("User-Agent", this.userAgent);
        getRequest.setHeader("host", "wikimedia.org");
        CloseableHttpResponse execute = client.execute(getRequest);
        return execute;
    }

    public HttpGet makePageViewRequest(String wikiPage, String granularity, String modus,
            String startDate, String endDate) { //2015-09-01 = 2015090100, modus = per-article
        if (wikiPage.contains(" ")) {
            wikiPage = convertIntoWikiPageName(wikiPage);
        }
        try {
            wikiPage = URLEncoder.encode(wikiPage, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(WMPageViewAPIClientNew.class.getName()).log(Level.SEVERE, null, ex);
        }
        String requestURI = api_base + modus + "/" + this.project + "/" + this.access
                + "/" + this.agent + "/" + wikiPage + "/" + granularity + "/"
                + startDate + "/" + endDate;
        //System.err.println(requestURI);
        return new HttpGet(requestURI);
    }

    private String convertIntoWikiPageName(String wikiPage) {
        return wikiPage.replaceAll(" ", "_");
    }

    public List<String[]> responseHandler(HttpResponse response) {
        List<String[]> requestResult = null;
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode == 200) {
            InputStream jsonContent;
            try {
                jsonContent = response.getEntity().getContent();
                BufferedReader bf = new BufferedReader(new InputStreamReader(jsonContent));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = bf.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                bf.close();
                jsonContent.close();
                String jsonString = sb.toString();
                requestResult = parseJSONContent(jsonString);
            } catch (IOException ex) {
                Logger.getLogger(WMPageViewAPIClientNew.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedOperationException ex) {
                Logger.getLogger(WMPageViewAPIClientNew.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (requestResult == null) {
                requestResult = new ArrayList<>(1);
                requestResult.add(new String[]{"-1"});
            }
        } else {
            EntityUtils.consumeQuietly(response.getEntity()); // consume the response anyway
            requestResult = new ArrayList<>(1);
            requestResult.add(new String[]{"404"});
        }
        return requestResult;
    }

    private List<String[]> parseJSONContent(String json) {
        JsonObject jobj = new JsonParser().parse(json).getAsJsonObject();
        JsonArray itemArray = jobj.getAsJsonArray("items");
        List<String[]> result = new ArrayList<>(itemArray.size()); // 0: article, 1: Pageviews, 2: Timestamp
        Iterator<JsonElement> iterator = itemArray.iterator();
        while (iterator.hasNext()) {
            String[] row = new String[3];
            JsonElement next = iterator.next();
            JsonObject obj = next.getAsJsonObject();
            row[0] = obj.get("article").getAsString();
            row[1] = String.valueOf(obj.get("views").getAsLong());
            row[2] = obj.get("timestamp").getAsString();
            result.add(row);
        }
        return result;
    }

    public static void main(String[] args) {
        WMPageViewAPIClientNew client = new WMPageViewAPIClientNew("http://wikimedia.org/api/rest_v1/metrics/pageviews/", "SODEE");
        HttpGet makePageViewRequest = client.makePageViewRequest("Donald_Trump", "daily", "per-article", "2015081600", "2015090100");
        try {
            HttpResponse getResponse = client.makeGETRequestOnAPI(makePageViewRequest);
            List<String[]> responseHandler = client.responseHandler(getResponse);
            Iterator<String[]> iterator = responseHandler.iterator();
            int[] convertAPIResultIntoArray = PageViewFeatFunctions.convertAPIResultIntoArray(responseHandler);
            while (iterator.hasNext()) {
                String[] next = iterator.next();
                for (int i = 0; i < next.length; i++) {
                    System.out.println(next[i]);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(WMPageViewAPIClientNew.class.getName()).log(Level.OFF, null, ex);
        }

    }
}
