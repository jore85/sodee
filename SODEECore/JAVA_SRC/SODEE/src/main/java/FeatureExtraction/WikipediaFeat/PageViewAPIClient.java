package FeatureExtraction.WikipediaFeat;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author jore
 */
public class PageViewAPIClient {
    
    String userAgent;    
    CloseableHttpClient pvHttpClient;
    static final String API_BASEURI = "https://wikimedia.org/api/rest_v1";
    static final String PAGEVIEW_IDENT = "/metrics/pageviews/per-article";
    
    public PageViewAPIClient(String userAgent){
        this.userAgent = userAgent;
        this.pvHttpClient = HttpClients.createDefault();        
    }
    
    
    private String getResponse(String uri){
        String result = "";
        HttpGet get = new HttpGet(uri);
        try {
            HttpResponse response = pvHttpClient.execute(get);
            int status = response.getStatusLine().getStatusCode();
            if(status == 404){
                result = "404";
            } else {
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            }
        } catch (IOException ex) {
            Logger.getLogger(PageViewAPIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }     
    
    private static String uriBuilder(String project, String access, String agent, String article, String granularity, String startDate, String endDate){
        String result = "";
        
        result = result.concat(API_BASEURI + PAGEVIEW_IDENT);
        result = result.concat(result + "/" + project + "/" + access + "/" + agent 
                + "/" + article + "/" + granularity + "/" + startDate + "/" + endDate);
        return result;        
    }
    
    public PageViewAPIResult executeRequest(PageViewAPIRequest requestData){
        String requestURI = PageViewAPIClient.uriBuilder(requestData.getProject(), requestData.getAccess(),
                requestData.getAgent(), requestData.getArticle(), requestData.getGranularity(),
                requestData.getStartDate(), requestData.getEndDate());
        String response = getResponse(requestURI);
        return new PageViewAPIResult(response);
    }
           
    public static void main(String[] args){        
        PageViewAPIClient c = new PageViewAPIClient("ugcrf@student.kit.edu");
        PageViewAPIRequest request = new PageViewAPIRequest("en.wikipedia.org", 
                "all-access", "all-agent", "germany", "daily", "20150601", "20150601");        
        PageViewAPIResult r = c.executeRequest(request);
    }    
    
}
