package FeatureExtraction.WikipediaFeat;

import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 *
 * @author jore
 */
public class PageViewRegressionResult {
    
    double slope;
    double intercept;
    double rSquare;
    double slopeStdErr;
    double interceptStdErr;
    
    public PageViewRegressionResult(SimpleRegression reg){
        this.slope = reg.getSlope();
        this.intercept = reg.getIntercept();
        this.rSquare = reg.getRSquare();
        this.slopeStdErr = reg.getSlopeStdErr();
        this.interceptStdErr = reg.getInterceptStdErr();
    }

    public double getSlope() {
        return slope;
    }

    public double getIntercept() {
        return intercept;
    }

    public double getrSquare() {
        return rSquare;
    }

    public double getSlopeStdErr() {
        return slopeStdErr;
    }

    public double getInterceptStdErr() {
        return interceptStdErr;
    }
    
}
