/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FeatureExtraction.WikipediaFeat;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Iterator;

/**
 * This class represents data of a response from a Wikimedia PageviewAPI-Request.
 * @author jore
 */
public class PageViewAPIResult {

    private String resultString;
    private String project;
    private String article;
    private String granularity;
    private String timestamp;
    private String access;
    private String agent;
    private long views;

    /**
     * The constructor for a PageviewAPI-response. Per default it holds the raw response string.
     * @param resultString The raw result, which is responded by the API after a successful request.
     */
    public PageViewAPIResult(String resultString) {
        this.resultString = resultString;
        parseResultString();
    }

    /*
    The raw string is mostly formated in JSON-Style and so it is parsed in the creation of the object.
    */   
    private void parseResultString() {
        Gson gson = new Gson();
        JsonObject jobj = new JsonParser().parse(resultString).getAsJsonObject();
        JsonArray itemArray = jobj.getAsJsonArray("items");
        Iterator<JsonElement> iterator = itemArray.iterator();
        while (iterator.hasNext()) {
            JsonElement next = iterator.next();
            JsonObject obj = next.getAsJsonObject();
            this.project = obj.get("project").getAsString();
            this.article = obj.get("article").getAsString();
            this.granularity = obj.get("granularity").getAsString();
            this.timestamp = obj.get("timestamp").getAsString();
            this.access = obj.get("access").getAsString();
            this.agent = obj.get("agent").getAsString();
            this.views = obj.get("views").getAsLong();
        }

    }

    public String getResultString() {
        return resultString;
    }

    public String getProject() {
        return project;
    }

    public String getArticle() {
        return article;
    }

    public String getGranularity() {
        return granularity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getAccess() {
        return access;
    }

    public String getAgent() {
        return agent;
    }

    public long getViews() {
        return views;
    }
    
    @Override
    public String toString(){
        String s = "Call result: ";
        s = s.concat(s + "\n" 
                + project + ";" + article + ";" + timestamp + ";" + views);
        return s;
    }

}
