package FeatureExtraction.WikipediaFeat;

import AnnotationProcessing.Annotation;
import Config.Constants;
import Config.SODEEConfig;
import DatabasesNetwork.PageViewDatabaseConnector;
import FeatureExtraction.WikipediaFeat.PageViewAPIClientNew.WMPageViewAPIClientNew;
import FeatureExtraction.WikipediaFeatures;
import RBBNPE.BaseNounPhrase;
import RBBNPE.EnhancedNP;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;

/**
 *
 * @author jore
 */
public class WikipediaNPProcessor {

    private WikipediaFeatures wikiFeatures;

    private PageViewDatabaseConnector pvDBConnection;
    //private WikiCategoryRetriever wikiCatR;
    private RedLinkChecker rlChecker;

    private WMPageViewAPIClientNew pvAPIClient;

    public WikipediaFeatures getWikiFeatures() {
        return wikiFeatures;
    }

    public void setPvDBConnection(PageViewDatabaseConnector pvDBConnection) {
        this.pvDBConnection = pvDBConnection;
    }

    public void setRlChecker(RedLinkChecker rlChecker) {
        this.rlChecker = rlChecker;
    }

    public void setPvAPIClient(WMPageViewAPIClientNew pvAPIClient) {
        this.pvAPIClient = pvAPIClient;
    }

    /*
     public void setWikiCatR(WikiCategoryRetriever wikiCatR) {
     this.wikiCatR = wikiCatR;
     }
     */
    public WikipediaFeatures process(Annotation ann, EnhancedNP np, boolean live) {
        WMPageViewAPIClientNew wmAPIClientForPV = this.pvAPIClient;
        Date dt;
        int[] count15d;
        int[] count25h;
        //System.out.println(np.getDayDateTime() + "\t" + np.getTimestamp().toString() + "\t" + np.getTimestamp().getMillis());
        //System.out.println(np.getTimestamp().toString());
        //try {
        //dt = Constants.SDF.parse(np.getTimestamp().toString());
        dt = new Date(np.getTimestamp().getMillis());

        if (ann.getMention().equals("NIL")) { // if a NIL -> the np was not linked 
            this.wikiFeatures = new WikipediaFeatures(np.getNp().getPhraseString(), dt, ann.getKb_entity());
            // get red link status
            boolean hasRedLink = rlChecker.hasRedLink(np.getNp().getPhraseString(), "en", live);
            this.wikiFeatures.setHasRedLink(hasRedLink);
            // get page views from the not linked np
            DateTime dayDate = np.getTimestamp();
            DateTime dayDateBefore14d = dayDate.minusDays(15);
            List<String[]> pvResult14d = wmAPIClientForPV.makePageViewRequestAndGetResult(np.getNp().getPhraseString(),
                    "daily", "per-article", Constants.DTF_YYYYMMDD00.print(dayDateBefore14d),
                    Constants.DTF_YYYYMMDD00.print(dayDate));
            this.wikiFeatures = setValuesFromAPI(this.wikiFeatures, pvResult14d, ann, dt);
            return this.wikiFeatures;
        } else {

            this.wikiFeatures = new WikipediaFeatures(np.getNp().getPhraseString(), dt, ann.getKb_entity());
            if (!live) {
                count15d = PageViewFeatFunctions.retrieveCount15d(pvDBConnection.getConn(), ann.getKb_entity(), dt);
                if (count15d != null) {
                    this.wikiFeatures = setDayValues(this.wikiFeatures, count15d, ann, dt);
                }
                count25h = PageViewFeatFunctions.retrieveCount25h(pvDBConnection.getConn(), ann.getKb_entity(), dt);
                if (count25h != null) {
                    this.wikiFeatures = setHourValues(this.wikiFeatures, count25h, ann, dt);
                }
                //HashSet<String> wikiPageCategories = WikipediaFeatures.getWikiPageCategories(wikiCatR, ann.getKb_entity());
                //this.wikiFeatures.setPageCategories(wikiPageCategories);                    
            } else {              //if live == true, switch to Wikimedia API
                DateTime dayDate = np.getTimestamp();
                DateTime dayDateBefore14d = dayDate.minusDays(15);
                List<String[]> pvResult14d = wmAPIClientForPV.makePageViewRequestAndGetResult(np.getNp().getPhraseString(),
                        "daily", "per-article", Constants.DTF_YYYYMMDD00.print(dayDateBefore14d),
                        Constants.DTF_YYYYMMDD00.print(dayDate));
                this.wikiFeatures = setValuesFromAPI(this.wikiFeatures, pvResult14d, ann, dt);
            }
            boolean hasRedLink = rlChecker.hasRedLink(ann.getKb_entity(), "en", live);
            this.wikiFeatures.setHasRedLink(hasRedLink);
            return this.wikiFeatures;
        }
        //} catch (ParseException ex) {
        //    System.err.println("Could not parse date of np (in wikipediaNPProcessor.process)");
        //    System.err.println(np.getDayDateTime());
        //    System.err.println(np.getTimestamp().toString());
        //}
        //wmAPIClientForPV.releaseConnection();

    }

    private WikipediaFeatures setDayValues(WikipediaFeatures wikiFeature,
            int[] count15d, Annotation ann, Date dt) {
        wikiFeature.setPageViewDays7d(PageViewFeatFunctions.pageViewDays7d(count15d, ann.getKb_entity(), dt));
        wikiFeature.setPageViewDays7dMin100(PageViewFeatFunctions.pageViewDays7dMin100(count15d, ann.getKb_entity(), dt));
        wikiFeature.setPageViewSum7d(PageViewFeatFunctions.pageViewSum7d(count15d, ann.getKb_entity(), dt));
        wikiFeature.setPageViewDays14d(PageViewFeatFunctions.pageViewDays14d(count15d, ann.getKb_entity(), dt));
        wikiFeature.setPageViewDays14dMin100(PageViewFeatFunctions.pageViewDays14dMin100(count15d, ann.getKb_entity(), dt));
        PageViewRegressionResult regression14d = PageViewFeatFunctions.pageViewRegression14d(count15d);
        wikiFeature.setPageViewSlope14dIntercept(regression14d.intercept);
        wikiFeature.setPageViewSlope14dInterceptStdErr(regression14d.interceptStdErr);
        wikiFeature.setPageViewSlope14dRSquare(regression14d.rSquare);
        wikiFeature.setPageViewSlope14dSlope(regression14d.slope);
        wikiFeature.setPageViewSlope14dSlopeStdErr(regression14d.slopeStdErr);
        return wikiFeature;
    }

    private WikipediaFeatures setValuesFromAPI(WikipediaFeatures wikiFeature, List<String[]> result, Annotation ann, Date dt) {
        int[] count14d = PageViewFeatFunctions.convertAPIResultIntoArray(result);
        if (count14d.length > 0) {
            if (count14d[0] == -1) {
                return wikiFeature;
            } else {
                if (count14d.length == 15) {
                    wikiFeature = setDayValues(wikiFeature, count14d, ann, dt);
                }
                int pv24hSum = count14d[count14d.length - 1]; //the last element is today
                if (pv24hSum > 0) {
                    wikiFeature.setPageView24hExist(true);
                }
                wikiFeature.setPageView24hSum(pv24hSum);
            }
        }
        return wikiFeature;
    }

    private WikipediaFeatures setHourValues(WikipediaFeatures wikiFeature,
            int[] count25h, Annotation ann, Date dt) {
        wikiFeature.setPageView24hExist(PageViewFeatFunctions.pageView24hExist(count25h, ann.getKb_entity(), dt));
        wikiFeature.setPageView24hSum(PageViewFeatFunctions.pageView24hSum(count25h, ann.getKb_entity(), dt));
        PageViewRegressionResult regression24h = PageViewFeatFunctions.pageViewRegression24h(count25h);
        wikiFeature.setPageViewSlope24hIntercept(regression24h.intercept);
        wikiFeature.setPageViewSlope24hInterceptStdErr(regression24h.interceptStdErr);
        wikiFeature.setPageViewSlope24hRSquare(regression24h.rSquare);
        wikiFeature.setPageViewSlope24hSlope(regression24h.slope);
        wikiFeature.setPageViewSlope24hSlopeStdErr(regression24h.slopeStdErr);
        return wikiFeature;
    }

    public static void main(String[] args) {
        WikipediaNPProcessor processor = new WikipediaNPProcessor();
        RedLinkChecker rlCheck = new RedLinkChecker(SODEEConfig.userAgent);
        WMPageViewAPIClientNew pvAPI = new WMPageViewAPIClientNew("http://wikimedia.org/api/rest_v1/metrics/pageviews/", SODEEConfig.userAgent);
        processor.setRlChecker(rlCheck);
        processor.setPvAPIClient(pvAPI);
        boolean isLive = true;
        BaseNounPhrase bnp = new BaseNounPhrase("Donald Trump", "Donald Trump", 0, 14, "NNP"); //String phraseString, String phraseStringWithPOSTags, int startOffset, int endOffset, String posTag
        EnhancedNP np = new EnhancedNP(0, "2016-01-10T03-30-00", bnp);
        Annotation ann = new Annotation("Donald Trump", "Donald Trump", 0, 14, "https://en.wikipedia.org/wiki/Donald_Trump", 1.0F, 123);// String mention, String kb_entity, int startOffset, int endOffset, String kb_url, float weight, int docID
        processor.process(ann, np, isLive);
        WikipediaFeatures wikiFeatures1 = processor.getWikiFeatures();
        System.out.println(wikiFeatures1.toString());
        bnp = new BaseNounPhrase("abcs", "abcsNNP", 0, 14, "NNP"); //String phraseString, String phraseStringWithPOSTags, int startOffset, int endOffset, String posTag
        np = new EnhancedNP(0, "2016-01-10T03-30-00", bnp);
        ann = new Annotation("abcs", "abcs", 0, 14, "https://en.wikipedia.org/wiki/abcs", 1.0F, 123);
        processor.process(ann, np, isLive);
        wikiFeatures1 = processor.getWikiFeatures();
        System.out.println(wikiFeatures1.toString());
    }

}
