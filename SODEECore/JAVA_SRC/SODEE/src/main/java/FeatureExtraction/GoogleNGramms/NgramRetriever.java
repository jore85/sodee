/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package FeatureExtraction.GoogleNGramms;

import FeatureExtraction.NgramFeatures;
import Config.SODEEConfig;
import GoogleNGramMFA.application.Batch;
import RBBNPE.EnhancedNP;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author johan
 */
public class NgramRetriever extends Thread {

    private Batch googleNGramBatchProcesser;
    private Boolean caseSensitive;
    private Boolean withCache;
    private int n;
    private long processedNPs;
    private String dayDate;
    private FileWriter fw;

    private HashSet<EnhancedNP> nps;
    private HashMap<EnhancedNP, NgramFeatures> ngramData;

    public HashMap<EnhancedNP, NgramFeatures> getNgramData() {
        return ngramData;
    }

    public void setWithCache(Boolean withCache) {
        this.withCache = withCache;
    }

    public void setNps(HashSet<EnhancedNP> nps) {
        this.nps = nps;
    }

    public void setGoogleNGramBatchProcesser(Batch googleNGramBatchProcesser) {
        this.googleNGramBatchProcesser = googleNGramBatchProcesser;
    }

    public void setCaseSensitive(Boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void setDayDate(String dayDate) {
        this.dayDate = dayDate;
    }

    public void retrieveNGramFeatures(HashSet<EnhancedNP> nps, Boolean withCache) {
        if (withCache) {
            try {
                this.fw = new FileWriter(SODEEConfig.outputPath + "/NgramData/" + this.dayDate + ".txt");
            } catch (IOException ex) {
                Logger.getLogger(NgramRetriever.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.ngramData = new HashMap<>();
        Iterator<EnhancedNP> iterator = nps.iterator();
        while (iterator.hasNext()) {
            EnhancedNP np = iterator.next();
            String npPhrase = np.getNp().getPhraseString();
            int length = npPhrase.split(" ").length;
            this.n = length;

            long googleNgramFrequency = 0;
            float googleNgramSlope = 0;
            float googleNgramR2 = 0;
            int usageSinceYear = 0;
            long matchCount1899 = 0;
            float percentProperCaps = 0;
            NgramFeatures ngramFeat;

            if (googleNGramBatchProcesser != null) {
                googleNgramFrequency = googleNGramBatchProcesser.getFrequency(npPhrase, n);

                googleNgramSlope = googleNGramBatchProcesser.getSlope(npPhrase, n, caseSensitive);
                googleNgramR2 = googleNGramBatchProcesser.getR2(npPhrase, n, caseSensitive);

                percentProperCaps = googleNGramBatchProcesser.getPercentProperCaps(npPhrase, n);
                usageSinceYear = googleNGramBatchProcesser.getUsageSinceYear(npPhrase, n, caseSensitive);

                List<Integer> matchCountWithinYearRange
                        = googleNGramBatchProcesser.getMatchCountWithinYearRange(npPhrase, n, 1, 1899, 1, caseSensitive);
                matchCount1899 = getSumOfList(matchCountWithinYearRange);
            }
            if (withCache) {
                try {
                    writeNgramFeatureFile(this.fw, npPhrase, googleNgramFrequency,
                            googleNgramSlope, googleNgramR2, percentProperCaps,
                            usageSinceYear, matchCount1899);
                } catch (IOException ex) {
                    Logger.getLogger(NgramRetriever.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ngramFeat = new NgramFeatures(googleNgramFrequency, googleNgramSlope,
                    googleNgramR2, usageSinceYear, matchCount1899, percentProperCaps);
            ngramData.put(np, ngramFeat);
            this.processedNPs = processedNPs++;
        }
        if (withCache) {
            try {
                this.fw.close();
            } catch (IOException ex) {
                Logger.getLogger(NgramRetriever.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private long getSumOfList(List<Integer> valueList) {
        long sum = 0;
        Iterator<Integer> iterator = valueList.iterator();
        while (iterator.hasNext()) {
            sum = sum + iterator.next();
        }
        return sum;
    }

    private void writeNgramFeatureFile(FileWriter fw, String npPhrase, long ngramFrequency,
            float ngramSlope, float ngramR2, float percentProperCaps,
            int usageSinceYear, long matchCount1899) throws IOException {
        String objectString
                = npPhrase + ";" + ngramFrequency + ";" + ngramSlope + ";" + ngramR2
                + ";" + percentProperCaps + ";" + usageSinceYear + ";" + matchCount1899;
        fw.write(objectString);
    }

    @Override
    public void run() {
        retrieveNGramFeatures(this.nps, this.withCache);
    }

}
