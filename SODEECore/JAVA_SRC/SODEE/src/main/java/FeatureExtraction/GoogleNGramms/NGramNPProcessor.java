/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package FeatureExtraction.GoogleNGramms;

import FeatureExtraction.NgramFeatures;
import RBBNPE.EnhancedNP;
import Utility.Helper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 *
 * @author johan
 */
public class NGramNPProcessor extends Thread {

    private HashMap<EnhancedNP, NgramFeatures> ngramFeatures;
    private HashSet<EnhancedNP> nps;
    private HashSet<EnhancedNP> npsCopy;

    private String dayDate;
    private final String ngramServiceInputFolder; //save the file here
    private final String ngramServiceOutputFolder; //watch that folder for the ngram results
    private final Path watchedFolderPath;
    private final FileSystem fs;
    private WatchService service;

    public NGramNPProcessor(String ngramServiceInputFolder, String ngramServiceOutputFolder) {
        this.ngramServiceInputFolder = ngramServiceInputFolder;
        this.ngramServiceOutputFolder = ngramServiceOutputFolder;
        this.watchedFolderPath = Paths.get(this.ngramServiceOutputFolder);
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(this.watchedFolderPath,
                    "basic:isDirectory", NOFOLLOW_LINKS);

            if (!isFolder) {
                throw new IllegalArgumentException("Path: "
                        + this.watchedFolderPath + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }
        this.fs = this.watchedFolderPath.getFileSystem();
        try {
            this.service = fs.newWatchService();
            watchedFolderPath.register(service, ENTRY_CREATE);
        } catch (IOException ex) {
            System.err.println("Watch Service for the ngram result handling fails.");
        }
    }

    public void setNps(HashSet<EnhancedNP> nps) {
        this.nps = nps;
        //this.npsCopy.addAll(nps);
    }

    public void setDayDate(String dayDate) {
        this.dayDate = dayDate;
    }

    public HashMap<EnhancedNP, NgramFeatures> getNgramFeatures() {
        return ngramFeatures;
    }

    @Override
    public void run() {
        System.out.println("Start ngram retrieval with an extern service..."
                + "\t" + DateTime.now(DateTimeZone.UTC));
        String fileName = this.dayDate + ".txt";
        String filePathInput = this.ngramServiceInputFolder + fileName;
        String filePathOutput = this.ngramServiceOutputFolder;
        Helper.writeNPFile(filePathInput, this.nps);
        watchForResults(fileName, filePathOutput);
        System.out.println("End ngram retrieval with an extern service..."
                + "\t" + DateTime.now(DateTimeZone.UTC));
    }

    private boolean watchForResults(String fileName, String filePathResults) {
        boolean result = false;
        //first check if the result file is already finished
        boolean alreadyFinished = alreadyFinished(fileName, filePathResults);
        if (alreadyFinished != true) {
            WatchKey key = null;
            boolean serviceInProgress = true;
            try {
                while (serviceInProgress) {
                    key = service.take();

                    // Dequeueing events
                    WatchEvent.Kind<?> kind = null;
                    boolean finished = false;
                    for (WatchEvent<?> watchEvent : key.pollEvents()) {
                        // Get the type of the event
                        kind = watchEvent.kind();
                        if (OVERFLOW == kind) {
                            continue; //loop
                        } else if (ENTRY_CREATE == kind) {
                            // A new Path was created
                            Path newPath = ((WatchEvent<Path>) watchEvent).context();
                        // Output

                            //System.out.println("New path created: " + newPath);
                            if (newPath.toString().endsWith(".finished")) {
                                finished = true;
                                //System.out.println("File is complete.");

                            }
                            if (finished) {
                                String pathString = newPath.toString();
                                String newResultFilePath = pathString.substring(0, pathString.length() - ".finished".length());
                                //File npFile = new File(inputFolder + newNPFilePath);
                                //System.out.println("File exist: " + npFile.exists());
                                //npFile.setReadable(true);
                                //System.out.println("File path: " + npFile.canRead());
                                //System.out.println("File path: " + npFile.getPath());
                                //System.out.println("File path: " + npFile.getAbsolutePath());
                                //StringBuffer retrieveNgrams = retrieveNgramsFromFile(newNPFilePath, googleNgramBatchProcess);
                                //StringBuffer retrieveNgrams = retrieveNgramsFromFile(inputFolder + newNPFilePath, googleNgramBatchProcess);
                                //String newNPFileName = getLastElementOfURI(newNPFilePath);
                                //System.out.println(outputFolder + newPath);
                                readNPNgramsFromFile(filePathResults + newResultFilePath);
                                result = true;
                                finished = false;
                                serviceInProgress = false;
                            }
                        }
                    }

                    if (!key.reset()) {
                        break; //loop
                    }
                }
            } catch (InterruptedException ex) {
                System.err.println("Something went wrong on watching the ngram result directory.");
            }
        } else {
            readNPNgramsFromFile(filePathResults + fileName);
            result = true;
        }
        return result;
    }

    private boolean alreadyFinished(String fileName, String filePathResults) {
        boolean isAlreadyFinished = false;
        File output = new File(filePathResults);
        FileFilter ff = new WildcardFileFilter(fileName + ".finished");
        File[] files = output.listFiles(ff);
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].getName().equals(fileName)) {
                    isAlreadyFinished = true;
                    return isAlreadyFinished;
                }
            }
        }
        return isAlreadyFinished;
    }

    private void readNPNgramsFromFile(String pathToResultFile) {
        this.ngramFeatures = new HashMap<>();
        BufferedReader br = null;
        String sCurrentLine = "";
        long googleNgramFrequency = 0;
        float googleNgramSlope = 0;
        float googleNgramR2 = 0;
        int usageSinceYear = 0;
        long matchCount1899 = 0;
        float percentProperCaps = 0;
        int articleID = 0;
        String dayDateNP = "";
        String np = "";
        try {
            //System.out.println("filePath: " + f.getAbsolutePath());
            br = new BufferedReader(new FileReader(pathToResultFile));
            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println("Current line: " + sCurrentLine);
                String[] cols = sCurrentLine.split("\t");
                //System.out.println("Ngram Results: " + cols.length);
                if (cols.length == 9) {
                    articleID = Integer.parseInt(cols[0]);
                    dayDateNP = cols[1];
                    np = cols[2];
                    googleNgramFrequency = Long.parseLong(cols[3]);
                    googleNgramSlope = Float.parseFloat(cols[4]);
                    googleNgramR2 = Float.parseFloat(cols[5]);
                    usageSinceYear = Integer.parseInt(cols[6]);
                    matchCount1899 = Long.parseLong(cols[7]);
                    percentProperCaps = Float.parseFloat(cols[8]);

                    //Search the matching enhancedNP of the recent NP
                    Iterator<EnhancedNP> iterator = this.nps.iterator();
                    while (iterator.hasNext()) {
                        EnhancedNP next = iterator.next();
                        //System.out.println("NGram for " + next.getNp().getPhraseString() + " ");
                        if (next.getNp().getPhraseString().equals(np)
                                && next.getDayDateTime().equals(dayDateNP)) {
                            NgramFeatures ngramFeature = new NgramFeatures(googleNgramFrequency,
                                    googleNgramSlope, googleNgramR2,
                                    usageSinceYear, matchCount1899, percentProperCaps);
                            //System.out.print(ngramFeature.toString() + "\n");
                            this.ngramFeatures.put(next, ngramFeature);
                            this.nps.remove(next);
                            break;
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println("Reading a line of the ngram results file fails.");
        }
    }
}
