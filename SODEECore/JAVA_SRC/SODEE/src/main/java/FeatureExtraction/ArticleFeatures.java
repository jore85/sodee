package FeatureExtraction;

import static Config.Constants.SDF;
import core.NFArticle;
import java.text.ParseException;
import java.util.HashSet;
import org.joda.time.DateTime;

/**
 * Representation of the selected article features or the article meta
 * information.
 *
 * @author Johannes Reiss
 */
public class ArticleFeatures {

    private final DateTime retrievedDate;
    private final DateTime publishedDate;
    private final int articleID;
    private final String title;
    private final String articleURI;
    private final String country;
    private final String feedTitle;
    private String feedURI;
    private String hostName;
    private String sourceName;

    private HashSet<String> sourceTags;
    private HashSet<String> tags;

    private int dayOfWeek; // weakday, encoded in int
    private int lastFullHour; // hour of the retrieved date of the article

    /**
     * A representation of the considered article meta information
     *
     * @param article An already parsed article
     * @throws ParseException Throws exception if something goes wrong with the
     * date parsing.
     */
    public ArticleFeatures(NFArticle article) throws ParseException {
        if (article.get_retrievedDate() == null) {
            this.retrievedDate = new DateTime(SDF.parse("1970-01-01T00-00-01Z").getTime());
        } else {
            this.retrievedDate = article.get_retrievedDate();
        }
        if (article.get_publishDate() == null) {
            this.publishedDate = new DateTime(SDF.parse("1970-01-01T00-00-01Z").getTime());
        } else {
            this.publishedDate = article.get_publishDate();
        }
        this.articleID = article.get_articleID();
        this.title = article.get_title();
        this.articleURI = article.get_uri();
        if (article.get_country() == null) {
            this.country = "NOLAND";
        } else {
            this.country = article.get_country();
        }
        if (article.get_feedTitle() == null) {
            this.feedTitle = "NOFEEDTITLE";
        } else {
            this.feedTitle = article.get_feedTitle();
        }
        this.feedURI = article.get_feedUri();
        this.hostName = article.get_Hostname();
        this.sourceName = article.get_srcName();
    }

    /**
     * Constructs the representation from given values.
     */
    ArticleFeatures(int articleID, DateTime retrievedDate, DateTime publishedDate,
            String title, String articleURI, String country, String feedTitle,
            int dayOfWeek, int lastFullHour) {
        this.articleID = articleID;
        this.retrievedDate = retrievedDate;
        this.publishedDate = publishedDate;
        this.title = title;
        this.articleURI = articleURI;
        this.country = country;
        this.feedTitle = feedTitle;
        this.dayOfWeek = dayOfWeek;
        this.lastFullHour = lastFullHour;
    }

    public DateTime getRetrievedDate() {
        return retrievedDate;
    }

    public DateTime getPublishedDate() {
        return publishedDate;
    }

    public int getArticleID() {
        return articleID;
    }

    public String getTitle() {
        return title;
    }

    public String getArticleURI() {
        return articleURI;
    }

    public String getCountry() {
        return country;
    }

    public String getFeedTitle() {
        return feedTitle;
    }

    public String getFeedURI() {
        return feedURI;
    }

    public String getHostName() {
        return hostName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public HashSet<String> getSourceTags() {
        return sourceTags;
    }

    public HashSet<String> getTags() {
        return tags;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public int getLastFullHour() {
        return lastFullHour;
    }

    /**
     * Calculate extended features, such as the "tag set", the "day of week" and
     * the "last full hour" of a given article.
     *
     * @param article The original article object.
     * @return An article feature object with all features of the given article.
     * @throws ParseException Throws exception if something goes wrong in the
     * feature calculation.
     */
    public ArticleFeatures processFurtherArticleFeatures(NFArticle article) throws ParseException {
        ArticleFeatures features = new ArticleFeatures(article);
        if (article.get_tags() != null) {
            features.tags = getTagSet(article.get_tags());
        }
        if (article.get_srcTags() != null) {
            features.sourceTags = getTagSet(article.get_srcTags());
        }
        if (article.get_retrievedDate() != null) {
            features.dayOfWeek = article.get_retrievedDate().getDayOfWeek();
            features.lastFullHour = article.get_retrievedDate().getHourOfDay();
        }
        /*else if (article.get_publishDate() != null) {
         features.dayOfWeek = article.get_publishDate().getDayOfWeek();
         features.lastFullHour = article.get_publishDate().getHourOfDay();
         }*/
        return features;
    }

    private static HashSet<String> getTagSet(String tagVector) {
        HashSet<String> tagSet = new HashSet<>();
        String[] splitted = new String[0];
        if (tagVector.contains(",")) {
            splitted = tagVector.split(",");
        } else if (tagVector.contains(";")) {
            splitted = tagVector.split(";");
        }
        for (int i = 0; i < splitted.length; i++) {
            tagSet.add(splitted[i]);
        }
        return tagSet;
    }

    @Override
    public String toString() {
        return "ArticleFeatures{" + "retrievedDate=" + retrievedDate
                + ", publishedDate=" + publishedDate + ", articleID=" + articleID
                + ", title=" + title + ", articleURI=" + articleURI
                + ", country=" + country + ", feedTitle=" + feedTitle
                + ", feedURI=" + feedURI + ", hostName=" + hostName
                + ", sourceName=" + sourceName + ", dayOfWeek=" + dayOfWeek
                + ", lastFullHour=" + lastFullHour + '}';
    }

    public String[] toValueArray() {
        String[] values = {retrievedDate.toString(Config.Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z),
            publishedDate.toString(Config.Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z),
            title, articleURI, country, feedTitle, String.valueOf(dayOfWeek), String.valueOf(lastFullHour)};
        return values;
    }

}
