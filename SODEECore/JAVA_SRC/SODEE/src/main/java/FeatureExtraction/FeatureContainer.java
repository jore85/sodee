package FeatureExtraction;

import Config.Constants;
import FeatureExtraction.PeriodicalFeatures.PeriodicalFeatures;
import RBBNPE.BaseNounPhrase;
import RBBNPE.EnhancedNP;
import java.util.Date;
import org.joda.time.DateTime;

/**
 *
 * @author jore
 */
public class FeatureContainer {

    private EnhancedNP np;

    private ArticleFeatures articleFeats;
    private CombinedArticleNPFeatures combinedFeats;
    private AnnotationFeatures annotFeats;
    private WikipediaFeatures wikiFeats;
    private NgramFeatures ngramFeats;
    private PeriodicalFeatures periodFeats;

    public FeatureContainer(EnhancedNP np) {
        this.np = np;
    }

    public FeatureContainer(EnhancedNP np, ArticleFeatures articleFeats,
            CombinedArticleNPFeatures combinedFeats, AnnotationFeatures annotFeats,
            WikipediaFeatures wikiFeats, NgramFeatures ngramFeats) {
        this.np = np;
        this.articleFeats = articleFeats;
        this.combinedFeats = combinedFeats;
        this.annotFeats = annotFeats;
        this.wikiFeats = wikiFeats;
        this.ngramFeats = ngramFeats;
    }

    public FeatureContainer(String[] redValues) {
        this.np = FeatureContainer.parseNPFromArray(redValues);
        this.articleFeats = FeatureContainer.parseArticleFeaturesFromArray(redValues);
        this.combinedFeats = FeatureContainer.parseCombinedFeaturesFromArray(redValues);
        this.annotFeats = FeatureContainer.parseAnnotationFeaturesFromArray(redValues);
        this.ngramFeats = FeatureContainer.parseNGramFeaturesFromArray(redValues);
    }

    public ArticleFeatures getArticleFeats() {
        return articleFeats;
    }

    public void setArticleFeats(ArticleFeatures articleFeats) {
        this.articleFeats = articleFeats;
    }

    public CombinedArticleNPFeatures getCombinedFeats() {
        return combinedFeats;
    }

    public void setCombinedFeats(CombinedArticleNPFeatures combinedFeats) {
        this.combinedFeats = combinedFeats;
    }

    public AnnotationFeatures getAnnotFeats() {
        return annotFeats;
    }

    public void setAnnotFeats(AnnotationFeatures annotFeats) {
        this.annotFeats = annotFeats;
    }

    public WikipediaFeatures getWikiFeats() {
        return wikiFeats;
    }

    public void setWikiFeats(WikipediaFeatures wikiFeats) {
        this.wikiFeats = wikiFeats;
    }

    public NgramFeatures getNgramFeats() {
        return ngramFeats;
    }

    public void setNgramFeats(NgramFeatures ngramFeats) {
        this.ngramFeats = ngramFeats;
    }

    public EnhancedNP getNp() {
        return np;
    }

    public PeriodicalFeatures getPeriodFeats() {
        return periodFeats;
    }

    public void setPeriodFeats(PeriodicalFeatures periodFeats) {
        this.periodFeats = periodFeats;
    }
    

    public static ArticleFeatures parseArticleFeaturesFromArray(String[] artclFeat) {
        DateTime retrievedDate = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.parseDateTime(artclFeat[20]);
        DateTime publishedDate = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.parseDateTime(artclFeat[21]);
        int articleID = Integer.parseInt(artclFeat[1]);
        String title = artclFeat[22];
        String articleURI = artclFeat[23];
        String country = artclFeat[24];
        String feedTitle = artclFeat[25];
        //String feedURI;
        //String hostName;
        //String sourceName;
        int dayOfWeek = Integer.parseInt(artclFeat[26]); // weakday, encoded in int
        int lastFullHour = Integer.parseInt(artclFeat[27]); // hour of the retrieved date of the article
        return new ArticleFeatures(articleID, retrievedDate, publishedDate, title, articleURI, country, feedTitle, dayOfWeek, lastFullHour);
    }

    public static CombinedArticleNPFeatures parseCombinedFeaturesFromArray(String[] combinedFeat) {
        double npPositionInArticle = Double.parseDouble(combinedFeat[16]);
        double articleCoverage = Double.parseDouble(combinedFeat[17]);
        boolean titleContainsNP = Boolean.parseBoolean(combinedFeat[18]);
        int npOccurrenceInArticle = Integer.parseInt(combinedFeat[19]);
        return new CombinedArticleNPFeatures(npPositionInArticle, articleCoverage, titleContainsNP, npOccurrenceInArticle);
    }

    public static AnnotationFeatures parseAnnotationFeaturesFromArray(String[] annotFeat) {
        int articleID = Integer.parseInt(annotFeat[1]);
        String np = annotFeat[0];
        String mention = annotFeat[53];
        String wikiTitle = annotFeat[54];
        String wikiURI = annotFeat[55];
        float annotationWeight = Float.parseFloat(annotFeat[56]);
        int noveltyClass = Integer.parseInt(annotFeat[57]);
        long timeToInsertion = Long.parseLong(annotFeat[58]);
        return new AnnotationFeatures(articleID, np, mention, wikiTitle, wikiURI, annotationWeight, noveltyClass, timeToInsertion);
    }

    public static WikipediaFeatures parseWikipediaFeaturesFromArray(String[] wikiFeat) {
        String nounPhrase = wikiFeat[0];
        String wikiEntity = wikiFeat[28];
        //Date npDate;
        boolean hasRedLink = Boolean.parseBoolean(wikiFeat[29]);

        //boolean pageView24hExist;
        int pageView24hSum = Integer.parseInt(wikiFeat[31]);
        int pageViewDays7d = Integer.parseInt(wikiFeat[32]);
        long pageViewSum7d = Long.parseLong(wikiFeat[33]);
        int pageViewDays14d = Integer.parseInt(wikiFeat[34]);
        int pageViewDays7dMin100 = Integer.parseInt(wikiFeat[35]);
        int pageViewDays14dMin100 = Integer.parseInt(wikiFeat[36]);

        double pageViewSlope24hSlope = Double.parseDouble(wikiFeat[37]);
        double pageViewSlope24hIntercept = Double.parseDouble(wikiFeat[38]);
        //double pageViewSlope24hRSquare;
        //double pageViewSlope24hSlopeStdErr;
        //double pageViewSlope24hInterceptStdErr;

        double pageViewSlope14dSlope = Double.parseDouble(wikiFeat[42]);
        double pageViewSlope14dIntercept = Double.parseDouble(wikiFeat[43]);
        //double pageViewSlope14dRSquare;
        //double pageViewSlope14dSlopeStdErr;
        //double pageViewSlope14dInterceptStdErr;

        return new WikipediaFeatures(nounPhrase, wikiEntity, hasRedLink, pageView24hSum, pageViewDays7d,
                pageViewSum7d, pageViewDays14d, pageViewDays7dMin100, pageViewDays14dMin100,
                pageViewSlope24hSlope, pageViewSlope24hIntercept, pageViewSlope14dSlope, pageViewSlope14dIntercept);
    }

    public static NgramFeatures parseNGramFeaturesFromArray(String[] ngramFeat) {
        long googleNgramFrequency = Long.parseLong(ngramFeat[47]); //occurrence number of current NP in the Google ngram index
        float googleNgramSlope = Float.parseFloat(ngramFeat[48]); //getSlope() of slope of ngram frequency of current NP in the Google ngram index
        float googleNgramR2 = Float.parseFloat(ngramFeat[49]); //R^2 value reg. ngram of current NP in the Google ngram index
        int usageSinceYear = Integer.parseInt(ngramFeat[50]); //year since the NP is used according to Google ngram index
        long matchCount1899 = Long.parseLong(ngramFeat[51]); //occurrence number of current NP in the Google ngram index in books from 1 (actually 1500) until 1899.
        float percentProperCaps = Float.parseFloat(ngramFeat[52]); //the percentage of case-insensitive matches for a NP where all words began with a capital letter
        return new NgramFeatures(googleNgramFrequency, googleNgramSlope, googleNgramR2,
                usageSinceYear, matchCount1899, percentProperCaps);
    }

    public static EnhancedNP parseNPFromArray(String[] np) {
        int articleID = Integer.parseInt(np[1]);
        String dayDateTime = np[2];
        BaseNounPhrase bnp = new BaseNounPhrase(np[0], "", 0, 0, np[4]);
        String neClass = np[3];
        String posTag = np[4];
        DateTime timestamp = Constants.DTF_yyyy_MM_dd_T_HH_mm_ss_Z.parseDateTime(np[2]);
        boolean allCaps = Boolean.parseBoolean(np[5]);
        boolean containsDigitAndAlpha = Boolean.parseBoolean(np[6]);
        boolean containsNonAlpha = Boolean.parseBoolean(np[7]);
        boolean endsWithPeriod = Boolean.parseBoolean(np[8]);
        boolean firstCapital = Boolean.parseBoolean(np[9]);
        boolean firstLetterCapitalized = Boolean.parseBoolean(np[10]);
        boolean containsRealLetter = Boolean.parseBoolean(np[11]);
        boolean hasInternalApostrophe = Boolean.parseBoolean(np[12]);
        boolean hasInternalPeriod = Boolean.parseBoolean(np[13]);
        boolean internalCaps = Boolean.parseBoolean(np[14]);
        boolean isHyphenated = Boolean.parseBoolean(np[15]);
        int occurrences = Integer.parseInt(np[19]);
        int targetLabel = Integer.parseInt(np[59]);

        return new EnhancedNP(articleID, dayDateTime, bnp, neClass, posTag, timestamp,
                allCaps, containsDigitAndAlpha, containsNonAlpha, endsWithPeriod, firstCapital,
                firstLetterCapitalized, containsRealLetter, hasInternalApostrophe,
                hasInternalPeriod, internalCaps, isHyphenated, occurrences, targetLabel);
    }

}
