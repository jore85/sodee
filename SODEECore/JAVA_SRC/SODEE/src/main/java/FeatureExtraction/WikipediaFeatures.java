package FeatureExtraction;

import FeatureExtraction.WikipediaFeat.RedLinkChecker;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.DateTime;

public class WikipediaFeatures {

    private final String nounPhrase;
    private String wikiEntity;
    private Date npDate;

    private boolean hasRedLink;

    private boolean pageView24hExist;
    private int pageView24hSum;
    private int pageViewDays7d;
    private long pageViewSum7d;
    private int pageViewDays14d;
    private int pageViewDays7dMin100;
    private int pageViewDays14dMin100;

    private double pageViewSlope24hSlope;
    private double pageViewSlope24hIntercept;
    private double pageViewSlope24hRSquare;
    private double pageViewSlope24hSlopeStdErr;
    private double pageViewSlope24hInterceptStdErr;

    private double pageViewSlope14dSlope;
    private double pageViewSlope14dIntercept;
    private double pageViewSlope14dRSquare;
    private double pageViewSlope14dSlopeStdErr;
    private double pageViewSlope14dInterceptStdErr;

    private HashSet<String> pageCategories;

    public WikipediaFeatures(String nounPhrase, Date npDate, String wikiArticleTitle) {
        this.nounPhrase = nounPhrase;
        this.npDate = npDate;
        this.wikiEntity = wikiArticleTitle;
    }

    public WikipediaFeatures(String nounPhrase, Date npDate) {
        this.nounPhrase = nounPhrase;
        this.npDate = npDate;
        this.pageView24hExist = false;
        this.pageView24hSum = 0;
        this.pageViewDays7d = 0;
        this.pageViewSum7d = 0;
        this.pageViewDays14d = 0;
        this.pageViewDays7dMin100 = 0;
        this.pageViewDays14dMin100 = 0;
        this.pageViewSlope24hSlope = 0;
        this.pageViewSlope24hIntercept = 0;
        this.pageViewSlope24hRSquare = 0;
        this.pageViewSlope24hSlopeStdErr = 0;
        this.pageViewSlope24hInterceptStdErr = 0;
        this.pageViewSlope14dSlope = 0;
        this.pageViewSlope14dIntercept = 0;
        this.pageViewSlope14dRSquare = 0;
        this.pageViewSlope14dSlopeStdErr = 0;
        this.pageViewSlope14dInterceptStdErr = 0;
    }

    public WikipediaFeatures(String nounPhrase, String wikiEntity, boolean hasRedLink, int pageView24hSum, int pageViewDays7d,
            long pageViewSum7d, long pageViewDays14d, int pageViewDays7dMin100, int pageViewDays14dMin100, double pageViewSlope24hSlope,
            double pageViewSlope24hIntercept, double pageViewSlope14dSlope, double pageViewSlope14dIntercept) {
        this.nounPhrase = nounPhrase;
        this.wikiEntity = wikiEntity;
        this.hasRedLink = hasRedLink;
        this.pageView24hSum = pageView24hSum;
        this.pageViewDays7d = pageViewDays7d;
        this.pageViewSum7d = pageViewSum7d;
        this.pageViewDays7dMin100 = pageViewDays7dMin100;
        this.pageViewDays14dMin100 = pageViewDays14dMin100;
        this.pageViewSlope24hSlope = pageViewSlope24hSlope;
        this.pageViewSlope24hIntercept = pageViewSlope24hIntercept;
        this.pageViewSlope14dSlope = pageViewSlope14dSlope;
        this.pageViewSlope14dIntercept = pageViewSlope14dIntercept;
    }

    public String getNounPhrase() {
        return nounPhrase;
    }

    public String getWikiEntity() {
        return wikiEntity;
    }

    public Date getNpDate() {
        return npDate;
    }

    public boolean isHasRedLink() {
        return hasRedLink;
    }

    public boolean isPageView24hExist() {
        return pageView24hExist;
    }

    public int getPageView24hSum() {
        return pageView24hSum;
    }

    public int getPageViewDays7d() {
        return pageViewDays7d;
    }

    public long getPageViewSum7d() {
        return pageViewSum7d;
    }

    public int getPageViewDays14d() {
        return pageViewDays14d;
    }

    public int getPageViewDays7dMin100() {
        return pageViewDays7dMin100;
    }

    public int getPageViewDays14dMin100() {
        return pageViewDays14dMin100;
    }

    public double getPageViewSlope24hSlope() {
        return pageViewSlope24hSlope;
    }

    public double getPageViewSlope24hIntercept() {
        return pageViewSlope24hIntercept;
    }

    public double getPageViewSlope24hRSquare() {
        return pageViewSlope24hRSquare;
    }

    public double getPageViewSlope24hSlopeStdErr() {
        return pageViewSlope24hSlopeStdErr;
    }

    public double getPageViewSlope24hInterceptStdErr() {
        return pageViewSlope24hInterceptStdErr;
    }

    public double getPageViewSlope14dSlope() {
        return pageViewSlope14dSlope;
    }

    public double getPageViewSlope14dIntercept() {
        return pageViewSlope14dIntercept;
    }

    public double getPageViewSlope14dRSquare() {
        return pageViewSlope14dRSquare;
    }

    public double getPageViewSlope14dSlopeStdErr() {
        return pageViewSlope14dSlopeStdErr;
    }

    public double getPageViewSlope14dInterceptStdErr() {
        return pageViewSlope14dInterceptStdErr;
    }

    public Set<String> getPageCategories() {
        return pageCategories;
    }

    public void setHasRedLink(boolean hasRedLink) {
        this.hasRedLink = hasRedLink;
    }

    public void setPageView24hExist(boolean pageView24hExist) {
        this.pageView24hExist = pageView24hExist;
    }

    public void setPageView24hSum(int pageView24hSum) {
        this.pageView24hSum = pageView24hSum;
    }

    public void setPageViewDays7d(int pageViewDays7d) {
        this.pageViewDays7d = pageViewDays7d;
    }

    public void setPageViewSum7d(long pageViewSum7d) {
        this.pageViewSum7d = pageViewSum7d;
    }

    public void setPageViewDays14d(int pageViewDays14d) {
        this.pageViewDays14d = pageViewDays14d;
    }

    public void setPageViewDays7dMin100(int pageViewDays7dMin100) {
        this.pageViewDays7dMin100 = pageViewDays7dMin100;
    }

    public void setPageViewDays14dMin100(int pageViewDays14dMin100) {
        this.pageViewDays14dMin100 = pageViewDays14dMin100;
    }

    public void setPageViewSlope24hSlope(double pageViewSlope24hSlope) {
        this.pageViewSlope24hSlope = pageViewSlope24hSlope;
    }

    public void setPageViewSlope24hIntercept(double pageViewSlope24hIntercept) {
        this.pageViewSlope24hIntercept = pageViewSlope24hIntercept;
    }

    public void setPageViewSlope24hRSquare(double pageViewSlope24hRSquare) {
        this.pageViewSlope24hRSquare = pageViewSlope24hRSquare;
    }

    public void setPageViewSlope24hSlopeStdErr(double pageViewSlope24hSlopeStdErr) {
        this.pageViewSlope24hSlopeStdErr = pageViewSlope24hSlopeStdErr;
    }

    public void setPageViewSlope24hInterceptStdErr(double pageViewSlope24hInterceptStdErr) {
        this.pageViewSlope24hInterceptStdErr = pageViewSlope24hInterceptStdErr;
    }

    public void setPageViewSlope14dSlope(double pageViewSlope14dSlope) {
        this.pageViewSlope14dSlope = pageViewSlope14dSlope;
    }

    public void setPageViewSlope14dIntercept(double pageViewSlope14dIntercept) {
        this.pageViewSlope14dIntercept = pageViewSlope14dIntercept;
    }

    public void setPageViewSlope14dRSquare(double pageViewSlope14dRSquare) {
        this.pageViewSlope14dRSquare = pageViewSlope14dRSquare;
    }

    public void setPageViewSlope14dSlopeStdErr(double pageViewSlope14dSlopeStdErr) {
        this.pageViewSlope14dSlopeStdErr = pageViewSlope14dSlopeStdErr;
    }

    public void setPageViewSlope14dInterceptStdErr(double pageViewSlope14dInterceptStdErr) {
        this.pageViewSlope14dInterceptStdErr = pageViewSlope14dInterceptStdErr;
    }

    public void setPageCategories(HashSet<String> pageCategories) {
        this.pageCategories = pageCategories;
    }

    private String convertNounPhraseToWikiTitle(String title) {
        return title.replace(' ', '_');
    }

    /*
     private void calculatePVFeaturesHours(Connection c) {
     int[] count25h = PageViewFeatFunctions.retrieveCount25h(c, wikiEntity, npDate);
     this.pageView24hExist = PageViewFeatFunctions.pageView24hExist(count25h, wikiEntity, npDate);
     this.pageView24hSum = PageViewFeatFunctions.pageView24hSum(count25h, wikiEntity, npDate);
     PageViewRegressionResult regHours = PageViewFeatFunctions.pageViewRegression24h(count25h);
     this.pageViewSlope24hSlope = regHours.getSlope();
     this.pageViewSlope24hIntercept = regHours.getIntercept();
     this.pageViewSlope24hRSquare = regHours.getrSquare();
     this.pageViewSlope24hSlopeStdErr = regHours.getSlopeStdErr();
     this.pageViewSlope24hInterceptStdErr = regHours.getInterceptStdErr();
     }

     private void calculatePVFeaturesDays(Connection c) {
     int[] count15d = PageViewFeatFunctions.retrieveCount15d(c, wikiEntity, npDate);
     this.pageViewDays7d = PageViewFeatFunctions.pageViewDays7d(count15d, wikiEntity, npDate);
     this.pageViewDays14d = PageViewFeatFunctions.pageViewDays14d(count15d, wikiEntity, npDate);
     this.pageViewDays14dMin100 = PageViewFeatFunctions.pageViewDays14dMin100(count15d, wikiEntity, npDate);
     this.pageViewDays7dMin100 = PageViewFeatFunctions.pageViewDays7dMin100(count15d, wikiEntity, npDate);
     PageViewRegressionResult regDays = PageViewFeatFunctions.pageViewRegression14d(count15d);
     this.pageViewSlope14dSlope = regDays.getSlope();
     this.pageViewSlope14dIntercept = regDays.getIntercept();
     this.pageViewSlope14dRSquare = regDays.getrSquare();
     this.pageViewSlope14dSlopeStdErr = regDays.getSlopeStdErr();
     this.pageViewSlope14dInterceptStdErr = regDays.getInterceptStdErr();
     }

     public static void featureCalculator(Connection c,
     HashMap<Integer, ArrayList<LinkedPhrase>> linkedPhrases,
     HashMap<Integer, Date> allDates, HashSet<String> redLinks, 
     RedLinkChecker rlChecker, WikiCategoryRetriever wikiCatR) {
     String head = "phraseID;nounPhrase;wikiArticleTitle;npDate;hasRedLink;pageView24hExist;"
     + "pageView24hSum;pageViewDays7d;pageViewSum7d;pageViewDays14d;"
     + "pageViewDays7dMin100;pageViewDays14dMin100;pageViewDays14dMin100;"
     + "pageViewSlope24hSlope;pageViewSlope24hIntercept;pageViewSlope24hRSquare;"
     + "pageViewSlope24hSlopeStdErr;pageViewSlope24hInterceptStdErr;"
     + "pageViewSlope14dSlope;pageViewSlope14dIntercept;pageViewSlope14dRSquare;"
     + "pageViewSlope14dSlopeStdErr;pageViewSlope14dInterceptStdErr";
     //System.out.println(head);
     Iterator<Map.Entry<Integer, ArrayList<LinkedPhrase>>> iterator
     = linkedPhrases.entrySet().iterator();
     while (iterator.hasNext()) {
     Map.Entry<Integer, ArrayList<LinkedPhrase>> next = iterator.next();
     Integer id = next.getKey();
     ArrayList<LinkedPhrase> phraseAnnotations = next.getValue();
     //if (phraseAnnotations.size() == 1) {
     Date articleDate = allDates.get(id);
     Iterator<LinkedPhrase> iterator1 = phraseAnnotations.iterator();
     while (iterator1.hasNext()) {
     LinkedPhrase next1 = iterator1.next();
     String phrase = next1.getPhrase();
     next1.getAnnotation().getKb_entity();
     WikipediaFeatures wikiFeatures = new WikipediaFeatures(id.toString(), phrase, articleDate);
     wikiFeatures.calculatePVFeaturesHours(c);
     wikiFeatures.calculatePVFeaturesDays(c);
     wikiFeatures.hasRedLink = checkRedLink(rlChecker, phrase, redLinks);
     wikiFeatures.pageCategories = getWikiPageCategories(wikiCatR, phrase);
     //System.out.println(wikiFeatures);
     }
     //}
     }
     }

     /*public static boolean checkRedLink(Set<String> redLinks, String linkedPhrase){
     return redLinks.contains(linkedPhrase);
     }*/
    public static boolean checkRedLink(RedLinkChecker rlChecker, String phrase, HashSet<String> redLinks) {
        return rlChecker.hasRedLink(redLinks, phrase);
    }

    /*
     public static HashSet<String> getWikiPageCategories(WikiCategoryRetriever wikiCatR, String phrase) {
     return wikiCatR.getSetOfCategoriesAPI(phrase);
     }*/

    /*
     @Override
     public String toString() {
     return nounPhrase + ";" + wikiEntity
     + ";" + npDate + ";" + hasRedLink
     + ";" + pageView24hExist + ";"
     + pageView24hSum + ";" + pageViewDays7d
     + ";" + pageViewSum7d + ";"
     + pageViewDays14d + ";" + pageViewDays7dMin100
     + ";" + pageViewDays14dMin100
     + ";" + pageViewSlope24hSlope
     + ";" + pageViewSlope24hIntercept
     + ";" + pageViewSlope24hRSquare
     + ";" + pageViewSlope24hSlopeStdErr
     + ";" + pageViewSlope24hInterceptStdErr
     + ";" + pageViewSlope14dSlope
     + ";" + pageViewSlope14dIntercept
     + ";" + pageViewSlope14dRSquare
     + ";" + pageViewSlope14dSlopeStdErr
     + ";" + pageViewSlope14dInterceptStdErr;
     }
     */
    @Override
    public String toString() {
        return "WikipediaFeatures{" + "nounPhrase=" + nounPhrase
                + ", wikiEntity=" + wikiEntity + ", npDate=" + npDate
                + ", hasRedLink=" + hasRedLink + ", pageView24hExist="
                + pageView24hExist + ", pageView24hSum=" + pageView24hSum
                + ", pageViewDays7d=" + pageViewDays7d + ", pageViewSum7d=" + pageViewSum7d
                + ", pageViewDays14d=" + pageViewDays14d
                + ", pageViewDays7dMin100=" + pageViewDays7dMin100
                + ", pageViewDays14dMin100=" + pageViewDays14dMin100
                + ", pageViewSlope24hSlope=" + pageViewSlope24hSlope
                + ", pageViewSlope24hIntercept=" + pageViewSlope24hIntercept
                + ", pageViewSlope24hRSquare=" + pageViewSlope24hRSquare
                + ", pageViewSlope24hSlopeStdErr=" + pageViewSlope24hSlopeStdErr
                + ", pageViewSlope24hInterceptStdErr=" + pageViewSlope24hInterceptStdErr
                + ", pageViewSlope14dSlope=" + pageViewSlope14dSlope
                + ", pageViewSlope14dIntercept=" + pageViewSlope14dIntercept
                + ", pageViewSlope14dRSquare=" + pageViewSlope14dRSquare
                + ", pageViewSlope14dSlopeStdErr=" + pageViewSlope14dSlopeStdErr
                + ", pageViewSlope14dInterceptStdErr=" + pageViewSlope14dInterceptStdErr
                + ", pageCategories=" + pageCategories + '}';
    }

    public String[] toValueArray() {
        String[] values = {wikiEntity, String.valueOf(hasRedLink),
            String.valueOf(pageView24hExist),
            String.valueOf(pageView24hSum),
            String.valueOf(pageViewDays7d),
            String.valueOf(pageViewSum7d),
            String.valueOf(pageViewDays14d),
            String.valueOf(pageViewDays7dMin100),
            String.valueOf(pageViewDays14dMin100),
            String.valueOf(pageViewSlope24hSlope),
            String.valueOf(pageViewSlope24hIntercept),
            String.valueOf(pageViewSlope24hRSquare),
            String.valueOf(pageViewSlope24hSlopeStdErr),
            String.valueOf(pageViewSlope24hInterceptStdErr),
            String.valueOf(pageViewSlope14dSlope),
            String.valueOf(pageViewSlope14dIntercept),
            String.valueOf(pageViewSlope14dRSquare),
            String.valueOf(pageViewSlope14dSlopeStdErr),
            String.valueOf(pageViewSlope14dInterceptStdErr)};
        return values;
    }

}
