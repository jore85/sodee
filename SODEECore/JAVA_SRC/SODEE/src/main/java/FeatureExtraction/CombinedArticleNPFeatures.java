package FeatureExtraction;

import RBBNPE.BaseNounPhrase;
import core.NFArticle;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jore
 */
public class CombinedArticleNPFeatures {

    private double npPositionInArticle;
    private double articleCoverage;
    private boolean titleContainsNP;
    private int npOccurenceInArticle;

    public CombinedArticleNPFeatures() {

    }

    public CombinedArticleNPFeatures(double npPositionInArticle, double articleCoverage, boolean titleContainsNP, int npOccurrenceInArticle) {
        this.npPositionInArticle = npPositionInArticle;
        this.articleCoverage = articleCoverage;
        this.titleContainsNP = titleContainsNP;
        this.npOccurenceInArticle = npOccurrenceInArticle;
    }

    public double getNpPositionInArticle() {
        return npPositionInArticle;
    }

    public double getArticleCoverage() {
        return articleCoverage;
    }

    public boolean isTitleContainsNP() {
        return titleContainsNP;
    }

    public int getNpOccurenceInArticle() {
        return npOccurenceInArticle;
    }

    private static boolean titleContainsNP(String phrase, String articleTitle) {
        return articleTitle.contains(phrase);
    }

    private static double npPositionInArticle(double startOffset, double articleLength) {
        double position = 0;
        position = (startOffset / articleLength);
        //System.out.println("start offset: " + startOffset + " article length: " + articleLength + "position in article: " + position);
        return position;
    }

    private static double articleCoverage(double npLength, double articleLength) {
        double articleCoverage = 0;
        articleCoverage = (npLength / articleLength);
        //System.out.println("npLength: " + npLength + " articleLength: " + articleLength + " article coverage: " + articleCoverage);
        return articleCoverage;
    }

    private static int npOccurenceInArticle(String phrase, String bodyText) {
        int occurrence = 0;
        occurrence = StringUtils.countMatches(bodyText, phrase);
        //System.out.println("occurrences in body text: "  + occurrence);
        return occurrence;
    }

    public static CombinedArticleNPFeatures calculateArticleNPFeatures(BaseNounPhrase np, NFArticle article) {
        int npLength = np.getPhraseString().length();
        int npStartOffset = np.getStartOffset();
        int bodyTextLength = article.get_bodyClearText().length();
        double npPositionInArticle1 = npPositionInArticle(npStartOffset, bodyTextLength);
        boolean titleContainsNP1 = titleContainsNP(np.getPhraseString(), article.get_title());
        double articleCoverage1 = articleCoverage(npLength, bodyTextLength);
        int npOccurenceInArticle1 = npOccurenceInArticle(np.getPhraseString(), article.get_bodyClearText());
        return new CombinedArticleNPFeatures(npPositionInArticle1, articleCoverage1,
                titleContainsNP1, npOccurenceInArticle1);
    }

    @Override
    public String toString() {
        return "CombinedArticleNPFeatures{" + "npPositionInArticle="
                + npPositionInArticle + ", articleCoverage=" + articleCoverage
                + ", titleContainsNP=" + titleContainsNP
                + ", npOccurenceInArticle=" + npOccurenceInArticle
                + '}';
    }

    public String[] toValueArray() {
        String[] values = {String.valueOf(npPositionInArticle),
            String.valueOf(articleCoverage), String.valueOf(titleContainsNP),
            String.valueOf(npOccurenceInArticle)};
        return values;
    }

}
