/*
 * This following source code is part of the master thesis of Johannes Reiss
 */
package FeatureExtraction;

/**
 *
 * @author johan
 */
public class NgramFeatures {

    private final long googleNgramFrequency; //occurrence number of current NP in the Google ngram index
    private final float googleNgramSlope; //getSlope() of slope of ngram frequency of current NP in the Google ngram index
    private final float googleNgramR2; //R^2 value reg. ngram of current NP in the Google ngram index
    private final int usageSinceYear; //year since the NP is used according to Google ngram index
    private final long matchCount1899; //occurrence number of current NP in the Google ngram index in books from 1 (actually 1500) until 1899.
    private final float percentProperCaps; //the percentage of case-insensitive matches for a NP where all words began with a capital letter

    public NgramFeatures(long googleNgramFrequency, float googleNgramSlope,
            float googleNgramR2, int usageSinceYear,
            long matchCount1899, float percentProperCaps) {
        this.googleNgramFrequency = googleNgramFrequency;
        this.googleNgramSlope = googleNgramSlope;
        this.googleNgramR2 = googleNgramR2;
        this.usageSinceYear = usageSinceYear;
        this.matchCount1899 = matchCount1899;
        this.percentProperCaps = percentProperCaps;
    }

    public long getGoogleNgramFrequency() {
        return googleNgramFrequency;
    }

    public float getGoogleNgramSlope() {
        return googleNgramSlope;
    }

    public float getGoogleNgramR2() {
        return googleNgramR2;
    }

    public int getUsageSinceYear() {
        return usageSinceYear;
    }

    public long getMatchCount1899() {
        return matchCount1899;
    }

    public float getPercentProperCaps() {
        return percentProperCaps;
    }

    @Override
    public String toString() {
        return "NgramFeatures{" + "googleNgramFrequency=" + googleNgramFrequency
                + ", googleNgramSlope=" + googleNgramSlope + ", googleNgramR2=" + googleNgramR2
                + ", usageSinceYear=" + usageSinceYear + ", matchCount1899=" + matchCount1899
                + ", percentProperCaps=" + percentProperCaps + '}';
    }

    public String[] toValueArray() {
        String[] values = {
            String.valueOf(googleNgramFrequency),
            String.valueOf(googleNgramSlope),
            String.valueOf(googleNgramR2),
            String.valueOf(usageSinceYear),
            String.valueOf(matchCount1899),
            String.valueOf(percentProperCaps)
        };
        return values;
    }

}
