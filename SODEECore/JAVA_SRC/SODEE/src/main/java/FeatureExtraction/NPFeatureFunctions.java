package FeatureExtraction;

import RBBNPE.BaseNounPhrase;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jore
 */
public class NPFeatureFunctions {

  
    
    public static boolean allCaps(String phrase) {
        return StringUtils.isAllUpperCase(phrase);
    }

    public static boolean containsDigitAndAlpha(String prhase) {
        return StringUtils.isAlphanumeric(prhase);
    }

    public static boolean containsNonAlpha(String phrase) {
        return phrase.matches("[^a-zA-Z\\s:]");
    }

    public static boolean endsWithPeriod(String phrase) {
        return StringUtils.endsWith(phrase, ".");
    }

    public static boolean firstCapital(String phrase) {
        char first = phrase.charAt(0);
        return StringUtils.isAllUpperCase(String.valueOf(first));
    }

    public static boolean firstLetterCapital(String phrase) {
        return phrase.matches("[A-Z]?");
    }

    public static boolean containsRealLetter(String phrase) {
        return StringUtils.isAlpha(phrase);
    }

    public static boolean hasInternalApostrophe(String phrase) {
        return phrase.matches("[^\\w]*'\\b");
    }

    public static boolean hasInternalPeriod(String phrase) {
        return phrase.matches("[^\\w]*\\.\\b");
    }

    public static boolean hasInternalCaps(String phrase) {
        if (phrase.length() >= 2) {
            for (int i = 1; i < phrase.length(); i++) {
                return Character.isUpperCase(phrase.charAt(i));
            }
        }
        return false;
    }

    public static boolean isHyphenated(String phrase) {
        return phrase.matches("\\p{L}+(?>-\\p{L}+)+");
    }
    
    public static int calculateNPLength(String phrase) {
        return phrase.length();
    }


}
