package FeatureExtraction;

/**
 *
 * @author jore
 */
public class AnnotationFeatures {

    private int articleID;
    private String np;
    private String mention;
    private String wikiTitle;
    private String wikiURI;
    private float annotationWeight;

    private int noveltyClass;
    private long timeToInsertion; // insertionDate - retrievalDate

    public AnnotationFeatures(int articleID, String np, String mention,
            String wikiTitle, String wikiURI, float annotationWeight, int noveltyClass) {
        this.articleID = articleID;
        this.np = np;
        this.mention = mention;
        this.wikiTitle = wikiTitle;
        this.wikiURI = wikiURI;
        this.annotationWeight = annotationWeight;
        this.noveltyClass = noveltyClass;
    }

    AnnotationFeatures(int articleID, String np, String mention, String wikiTitle, 
            String wikiURI, float annotationWeight, int noveltyClass, long timeToInsertion) {
        this.articleID = articleID;
        this.np = np;
        this.mention = mention;
        this.wikiTitle = wikiTitle;
        this.wikiURI = wikiURI;
        this.annotationWeight = annotationWeight;
        this.noveltyClass = noveltyClass;
        this.timeToInsertion = timeToInsertion;
    }

    public int getArticleID() {
        return articleID;
    }

    public String getNp() {
        return np;
    }

    public String getMention() {
        return mention;
    }

    public String getWikiTitle() {
        return wikiTitle;
    }

    public String getWikiURI() {
        return wikiURI;
    }

    public float getAnnotationWeight() {
        return annotationWeight;
    }

    public int getNoveltyClass() {
        return noveltyClass;
    }

    public long getTimeToInsertion() {
        return timeToInsertion;
    }

    public void setTimeToInsertion(long timeToInsertion) {
        this.timeToInsertion = timeToInsertion;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }

    public void setNp(String np) {
        this.np = np;
    }

    @Override
    public String toString() {
        return "AnnotationFeatures{" + "articleID=" + articleID
                + ", np=" + np + ", mention=" + mention + ", wikiTitle=" + wikiTitle
                + ", wikiURI=" + wikiURI + ", annotationWeight=" + annotationWeight
                + ", noveltyClass=" + noveltyClass + ", timeToInsertion=" + timeToInsertion + '}';
    }

    public String[] toValueArray() {
        String[] results = {mention, wikiTitle, wikiURI, String.valueOf(annotationWeight),
            String.valueOf(noveltyClass), String.valueOf(timeToInsertion)};
        return results;
    }

}
